clc
clear all
close all

%%

% choose the output you wish to view (allways choose last output)
output = 100; % can make this into a for-loop; e.g., for output=1:N .... so on
    % Corrects the file name.
    if (length(num2str(output))==1)     outputNum=['000',num2str(output)];
    elseif (length(num2str(output))==2)     outputNum=['00',num2str(output)];
    elseif (length(num2str(output))==3)     outputNum=['0',num2str(output)];
    elseif (length(num2str(output)) >3)     outputNum=num2str(output);
    end
%%
% filename = ['tflbm01_',num2str(outputNum),'.h5'];
filename ='Re_15e5_ZH11_reTest_0195.h5';
Nx = h5read(filename,'/Nx');
Ny = h5read(filename,'/Ny');
Nz = h5read(filename,'/Nz');

rho0 = h5read(filename,'/Density_0');
rho0 = reshape(rho0,Nx,Ny);

% rho1 = h5read(filename,'/Density_1');
% rho1 = reshape(rho1,Nx,Ny);

V  = h5read(filename,'/Velocity_0');
Vx = V(1:3:end);
Vy = V(2:3:end);
Vz = V(3:3:end);

Vx = reshape(Vx,Nx,Ny);
Vy = reshape(Vy,Nx,Ny);
Vz = reshape(Vz,Nx,Ny);

V  = sqrt(Vx.^2 + Vy.^2 + Vz.^2);

figure;imagesc(V');
colormap('jet');colorbar;
hold on
hstream=streamslice(Vx',Vy');
set(hstream,'Color','r');
title('Velocity Contour and Stream')
xlabel('Nx')
ylabel('Ny')
