To run mechsys on any external folder 

1) Copy all files in this folder [excluding 'Load_Plot_Example.m'] into target folder. 

2) In (copied) CMakeList.txt replace <# script1> with the filename (do not include .cpp) of script(s) you wish to compile.

3) Compile by entering the following commands:
	
	ccmake .

	[press c, wait, and press c again, then g]

	cmake .
	
	make
	
..Done..

For the convience of users an example MATLAB script named: 'Load_Plot_Example.m' has been provided, which is a simple example that demonstrates how to load .h5 data files, extract relavent variables (i.e., rho and velocity) and plotting these variables.

Let me know if you run into any issues:
chris.from@qut.edu.au

