
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    double          * rho0tmp;
    double Nlayers ;
    // double *         rhoL;
    // double *         rhoR;
    double      UoL;
    double      UoR;
    size_t CBCnodes;
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Setup(FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    
    // // double Vel = dat.Uo;
    // double VelL = dat.UoL;
    // double VelR = dat.UoR;
    // Vec3_t VL(0.0,VelL, 0.0);
    // Vec3_t VR(0.0,-VelR, 0.0);
    // Vec3_t V0(0.0,0.0, 0.0);


    // #ifdef USE_OMP
    // #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    // #endif
    // for (size_t il=0;il<dom.Nl;il++)
    // for (size_t i=0;i<dom.Ndim(0);i++)
    // for (size_t j=0;j<dom.Ndim(1);j++)
    // {
    //     if (!dom.IsSolid[il][i][j][0]) continue;
    //     if ((int)i<(int)dom.Ndim(0)/2) dom.Vel[il][i][j][0] = VL;
    //     if ((int)i>(int)dom.Ndim(0)/2) dom.Vel[il][i][j][0] = VR;
    // }

}
/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */


int main(int argc, char **argv) try
{
    size_t Nproc = 1;
    if (argc>=2) Nproc=atoi(argv[1]);
    
    // ! Modelling parameters
        size_t nx = 200;//512;
        size_t ny = 10;
        double tau01 = 1.2;//
        double tau02 = 0.9;//
        double dt    = 1.0;
        double dx    = 1.0;
        double xmin0 = 50;//nx/4.0;
        double xmax0 = 150;//nx/2.0;
        double R0 = ny/3.0; //radius
        double C = 1.0;// 0.15; // constant parameter for changing surface tension
            if (argc>=3) C=atof(argv[2]);
        double Amplify = 70.0; // 275.0;//
            if (argc>=4) Amplify=atof(argv[3]);
        char const * FileKey = "FIT01";
            if (argc>=5) FileKey=argv[4];
    /* -------------------------------------------- */

    // ! Lattice Structures
        // * D2Q9
            // double cs    = 1.0/sqrt(3.0);
        // * D2Q17
            // double cs    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));//*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
            /*ZOT*/
            // double cs       = sqrt(1.0/((5.0 + sqrt(10.0))/3.0));
        // * D2Q21
            // double cs      = (1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)))
        // * D2Q25
            // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));//*(1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
            // * ZOT
            // double cs =sqrt((1.0-sqrt(2.0/5.0)));
        // * D2Q37
            double cs      = (1.0/1.19697977039307435897239); 
    /* -------------------------------------------- */

    /* Dimensionless Numbers */
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        double dxx = ((int)nx/(int)ny)*1.0/nx;
        double intDp = 10.0; // 5*dxx thickness rule for the interface between the twofluids. Ref. Hessling 2017 JChemPhys.146 054111
    /* -------------------------------------------- */
    
    /* Dimensionless Variables */
        // double Umax = Ma*cs;
        double ULmax = 0.0, URmax = 0.0; 
        double waveL = ((ny-0.0)/1.0);
        double massRatio = 1.0;//5.0/2.0;//
        double muRatio = 1.0;
    /* -------------------------------------------- */
    
    /* Kinematic Viscosity */
        Array<double> nu(2);
        nu[0] = cs*cs*(tau01-0.5)*dx*dx/dt; 
        nu[1] = cs*cs*(tau02-0.5)*dx*dx/dt; 
        

    /* Molecular Mass   */
        Array<double> molMass(2);
        molMass[0] = 1.0;
        molMass[1] = massRatio*molMass[0];// molecular mass of B [dimensionless ratio]
        
    /* Number Density   */
        Array<double> nmol(2);
        nmol[0] = 1.00;//
        nmol[1] = 1.00;//
        double rhotmp0 = nmol[0]*molMass[0];
        double rhotmp1 = nmol[1]*molMass[1];
    /* Atwood Number */
        double At = (nmol[1]*molMass[1] - nmol[0]*molMass[0])/(nmol[1]*molMass[1] + nmol[0]*molMass[0]);

    /* Do not touch */
        FLBM::Domain Dom(D2Q37, ELS, MRT,nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    UserData dat;
    Dom.UserData = &dat;
    dat.UoL = ULmax;// /2.0;
    dat.UoR = URmax;// /2.0;
    // dat.rhoL = new double [Dom.Nl];
    // dat.rhoR = new double [Dom.Nl];
    // dat.rhoL[0] = rhotmp0*0.01;
    // dat.rhoL[1] = rhotmp1;
    // dat.rhoR[0] = rhotmp0;
    // dat.rhoR[1] = rhotmp1*0.01;
    /* -------------------------------------------- */

    /* Print out stuff */
        printf("\n%s--- Atwood Number   = %g%s\n",TERM_CLR1,At,TERM_RST);
        std::cout << " MolMass = " << molMass[0] << std::endl;
        std::cout << " nu = " << nu[0] << std::endl;
        

        // double TotalTime = 4000;
        // double TimeResOut = TotalTime/30;
        // double TimeResOut = floor((waveL/((ULmax+URmax)/2.0))/5.0); // characteristic timescale* /10
        double TotalTime = 40000.0;//TimeResOut*40.0;
        double TimeResOut = TotalTime/5;
        // std::cout << "\n characteristic timescale* (=1.0 sec) = " << floor(waveL/((ULmax+URmax)/2.0)) << std::endl;
        std::cout << " Total Simulation time        = " << TotalTime << "\n"<< std::endl;

    Vec3_t V0 = OrthoSys::O;
    Vec3_t V1 = OrthoSys::O;
    
    double pL = rhotmp1;
    double pR = rhotmp0;
    double cHigh = 1.0; // High concentration
    double cLow = 0.0;//0.001; // Low Concentration 
    double rhotmpL=0.0;
    double rhotmpR=0.0;

    
    // ! Setting up the different fluids */
        for (size_t i=0; i<nx; ++i)
        {
            for (size_t j=0; j<ny; ++j)
            {
                // * Ref: PHYSICAL REVIEW E 84, 046710 (2011)
                // Inner Droplet 
                rhotmpL = (cLow  + 0.5*(cHigh-cLow)*( tanh((2.0*((int)i-xmin0))/(intDp)) - tanh((2.0*((int)i-xmax0))/(intDp))));

                // Outer surrounding fluid
                rhotmpR = (cHigh + 0.5*(cLow-cHigh)*( tanh((2.0*((int)i-xmin0))/(intDp)) - tanh((2.0*((int)i-xmax0))/(intDp))));

                if (rhotmpL <= cLow) rhotmpL=cLow;
                if (rhotmpR <= cLow) rhotmpR=cLow;
                rhotmpL *=pL;
                rhotmpR *=pR;

                Dom.AssignFluidParameters(0,iVec3_t(i,j,0),rhotmpR,V0);
                Dom.AssignFluidParameters(1,iVec3_t(i,j,0),rhotmpL,V1);
            }
        }
        
        
    /* Mixing interaction */ 
        double sumRho = 0.0;
        size_t k = 0, nxy=0;
        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j){
                nxy++;
                for (size_t k=0; k<nu.size(); ++k){
                    sumRho += Dom.Rho[k][i][j][0];
                }
        }
        sumRho      /= (nxy);
    
    /* Interaction Potential constant variables*/ 
        Dom.Psi[0] = 1.0;
        Dom.Psi[1] = 1.0;
        Dom.Rhoref[0] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        Dom.Rhoref[1] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        
        double rhoInt = rhotmp0*cHigh*0.5 + rhotmp1*cHigh*0.5; /*
            * under the assumpt that max interaction occurs at the interface */ 
        double TauEf = (Dom.Tau[0]*rhotmp0 + Dom.Tau[1]*rhotmp1)/(rhotmp0+rhotmp1);
        std::cout << " Rho(Int) = " << rhoInt << std::endl;
        std::cout << " spatial average  <Rho> = " << sumRho << ", nxy=" << nxy << std::endl;
    /* Mixing interaction */ 
        double Gmix = 2.0/(rhoInt);
        
        // Gmix = Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]);
        Gmix = Gmix*Amplify/100.0;
        // Gmix /= Dom.e4;
        // Gmix /= Dom.e2;
        // double EOSINT = -Dom.e2*0.5*Gmix*(rhotmp0*rhotmp1);//
        double EOSINT = 0.5*Gmix*((rhoInt*0.5)*(rhoInt*0.5));//
        EOSINT = 0.0;
        double EOSINTtmp = 0.0;
        for (size_t i=0; i<nx; ++i){
            size_t j    =   ny/2;
            EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*Dom.Rho[0][i][j][0]*Dom.Rho[1][i][j][0], EOSINTtmp);
            // EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*Dom.PsiIntEq(0,Dom.Rho[0][i][j][0])*Dom.PsiIntEq(1,Dom.Rho[1][i][j][0]), EOSINTtmp);
        }
        EOSINT = EOSINTtmp;
        // double EOSINT = -Dom.e2*0.5*(Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]))*(rhotmp0*rhotmp1);//
        
    /* Self Interaction */
        double Psi0 = rhotmp0;
        double Psi1 = rhotmp1;
        // double Gself0 = ((2.0)/(Dom.e2*pow(Dom.PsiIntEq(0,Psi0),2.0)))*(Dom.e2*rhotmp0  - Dom.e2*(rhotmp0+rhotmp1)*1.0 + EOSINT); //
        // Gself0 *= C;
        // double Gself1 = ((2.0)/(Dom.e2*pow(Dom.PsiIntEq(1,Psi1),2.0)))*(Dom.e2*rhotmp1  - 1.0*Dom.e2*(rhotmp0 - 0.5*Gself0*pow(Dom.PsiIntEq(0,Psi0),2.0))); // - EOSINT); //
        
        double Gself0 = ((2.0)/(Dom.e2*pow(Dom.PsiIntEq(0,Psi0),2.0)))*(-Dom.Cs2*Psi0  + Dom.Cs2*(Psi0) - EOSINT);
        double Gself1 = ((2.0)/(Dom.e2*pow(Dom.PsiIntEq(1,Psi1),2.0)))*(-Dom.Cs2*Psi1  + Dom.Cs2*(Psi1) - EOSINT);
        // Gself0 *= -1.0;//*0.99;
        // Gself1 *= -1.0;//*1.01;
        Gself0 *= C;//*0.99;
        Gself1 *= C;//*1.01;
        // Gself0 /= Dom.e2;
        // Gself1 /= Dom.e2;

        Dom.Gs[0]   = 0.0, Dom.Gs[1]    = 0.0;
        // Gself0   = 0.0, Gself1    = 0.0;

        if (C==0.0) Gself1 = 0.0;
        double EOS0 =  (Dom.Cs2*Psi0 + 0.5*Dom.e2*Gself0*pow(Dom.PsiIntEq(0,Psi0),2.0) + EOSINT);
        double EOS1 =  (Dom.Cs2*Psi1 + 0.5*Dom.e2*Gself1*pow(Dom.PsiIntEq(1,Psi1),2.0) + EOSINT);
    
        std::cout << " G-mixing crit = " << Gmix << "; at " << Amplify << "% (G/p^2 = "<< (Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0])) << ")" << std::endl;
        std::cout << " Gself 0          = " << Gself0 << std::endl;
        std::cout << " Gself 1          = " << Gself1 << std::endl;
        std::cout << " Equation of State " << std::endl;
        std::cout << "  -->  EoS Int          = " << EOSINT << std::endl;
        std::cout << "  -->  P0         = " << EOS0 << ",  (Po = " << Dom.Cs2*rhotmp0 << ")" << std::endl;
        std::cout << "  -->  P1         = " << EOS1 << ",  (Po = " << Dom.Cs2*rhotmp1 << ")" << std::endl;
    
    /* interaction strengths */
        Dom.Gmix     =  Gmix;
        Dom.G [0]    =  Gself0;
        Dom.G [1]    =  Gself1;
        
        Dom.Nproc = Nproc;
        Dom.FApplyForcesE();
        Dom.InitializeMixture();
        // std::cout << " !TEST " << std::endl;
        Dom.CalcPressureTensor = true;

    /*(Do not touch)*/
    /* Solve  Total time , Time step, Start, line: ,report| name| render| number of CPU */
    Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,floor(0.1*TimeResOut*Dom.dt),Setup,NULL,FileKey,false,Nproc);
    // Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,0*Dom.dt,NULL,NULL,FileKey,true,Nproc);
    
    return 0;
    
}
MECHSYS_CATCH
