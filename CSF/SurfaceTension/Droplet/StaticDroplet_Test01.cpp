
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    std::ofstream oss_st;       ///< file for surface tension calculation
    double          * rho0tmp;
    double Nlayers ;
    double          rhotmp0;
    double          rhotmp1;
    double      UoL;
    double      UoR;
    size_t CBCnodes;
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Report (FLBM::Domain & dom, void * UD)
{
    
    UserData & dat = (*static_cast<UserData *>(UD));
    double max1 = 0.0;
    double min1 = (int) dom.Ndim(0);
    size_t yloc = dom.Ndim(1)/2;
    for (size_t i=1;i<dom.Ndim(0);i++)
    {
        if (dom.Rho[1][dom.Ndim(0)/2 + i][yloc][0] <= 0.5*dat.rhotmp1) 
        {
            max1 = dom.Ndim(0)/2 + i - 1;
            break;
        }
    }
    for (size_t i=1;i<dom.Ndim(0);i++)
    {
        if (dom.Rho[0][i][yloc][0] <= 0.5*dat.rhotmp0) 
        {
            min1 = i + 1;
            break;
        }
    }
    double R   = 0.5*(max1 - min1);
    // Dom.CalcPressureTensor = true;
    
    
    dat.oss_st << dom.Time << Util::_8s << R << std::endl;

}
/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */


int main(int argc, char **argv) try
{
    size_t Nproc = 1;
    if (argc>=2) Nproc=atoi(argv[1]);
    
    // ! Modelling parameters
        size_t nx = 100;//512;
        size_t ny = nx;
        double tau01 = 1.6;//1.2;//
        double tau02 = tau01*(0.9/1.2);//
        double dt   = 1.0;
        double dx   = 1.0;
        double R0   = ny/4.0; //radius standard size 
                
        double C    = 1.0;// 0.15; // self-interaction constant parameter -- for changing surface tension
            if (argc>=3) C=atof(argv[2]);
        double Amplify = 70.0; // 275.0;//
            if (argc>=4) Amplify=atof(argv[3]);
        char const * FileKey = "SDT01";
            if (argc>=5) FileKey=argv[4];
            if (argc>=6) R0=atof(argv[5]);
        // nx = 4*(int)R0;
        // ny = nx;
        double x0 = nx/2.0 - 1.0;
        double y0 = ny/2.0 - 1.0;
    
    /* -------------------------------------------- */

    // ! Lattice Structures
        // * D2Q9
            // double cs    = 1.0/sqrt(3.0);
        // * D2Q17
            // double cs    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));//*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
            /*ZOT*/
            // double cs       = sqrt(1.0/((5.0 + sqrt(10.0))/3.0));
        // * D2Q21
            // double cs      = (1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)))
        // * D2Q25
            // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));//*(1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
            // * ZOT
            // double cs =sqrt((1.0-sqrt(2.0/5.0)));
        // * D2Q37
            // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0)));// Ref. Philippi
            double cs      = (1.0/1.19697977039307435897239);// Ref. Shan
        // * D2Q49 ZOTT (Zero-One-Two-Three)
            // double cs     = sqrt((2.0/3.0) + (1.0/3.0)*pow(7.0/(5.0*(3.0*sqrt(30.0) - 5.0)), (1.0/3.0)) - pow(((3.0*sqrt(30.0) - 5.0))/7.0, (1.0/3.0))/pow(35.0, (2.0/3.0)));
            // cs     = sqrt(cs); // Should equal  cs^2 == 0.697 953 322 019 683 1
    /* -------------------------------------------- */

    /* Dimensionless Numbers */
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        double dxx = ((int)nx/(int)ny)*1.0/nx;
        double intDp = 5.0*(nx/100.0); // 5*dxx thickness rule for the interface between the twofluids. Ref. Hessling 2017 JChemPhys.146 054111
        // intDp = nx*(4.0/10.0)-R0;
        // intDp = nx*(3.0/10.0)-R0;
    /* -------------------------------------------- */
    
    /* Dimensionless Variables */
        // double Umax = Ma*cs;
        double ULmax = 0.0, URmax = 0.0; 
        double waveL = ((ny-0.0)/1.0);
        double massRatio = 1.0;//5.0/2.0;//
        double muRatio = 1.0;
    /* -------------------------------------------- */
    
    /* Kinematic Viscosity */
        Array<double> nu(2);
        nu[0] = cs*cs*(tau01-0.5)*dx*dx/dt; 
        nu[1] = cs*cs*(tau02-0.5)*dx*dx/dt; 
        // nu[0] = 7.0/15.0; //* from Q21
        // nu[1] = 4.0/15.0;


    /* Molecular Mass   */
        Array<double> molMass(2);
        molMass[0] = 1.0;
        molMass[1] = massRatio*molMass[0];// molecular mass of B [dimensionless ratio]
        
    /* Number Density   */
        Array<double> nmol(2);
        nmol[0] = 1.00;//
        nmol[1] = 1.00;//
        double rhotmp0 = nmol[0]*molMass[0];
        double rhotmp1 = nmol[1]*molMass[1];
        
    /* Atwood Number */
        double At = (nmol[1]*molMass[1] - nmol[0]*molMass[0])/(nmol[1]*molMass[1] + nmol[0]*molMass[0]);

    /* Do not touch */
        FLBM::Domain Dom(D2Q37, ELS, MRT,nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    UserData dat;
    Dom.UserData = &dat;
    dat.UoL = ULmax;// /2.0;
    dat.UoR = URmax;// /2.0;
    dat.rhotmp0 = rhotmp0;
    dat.rhotmp1 = rhotmp1;
    /* -------------------------------------------- */

    /* Print out stuff */
        printf("\n%s--- Atwood Number   = %g%s\n",TERM_CLR1,At,TERM_RST);
        std::cout << " MolMass = " << molMass[0] << std::endl;
        std::cout << " nu = " << nu[0] << std::endl;
        

        // double TotalTime = 4000;
        // double TimeResOut = TotalTime/30;
        // double TimeResOut = floor((waveL/((ULmax+URmax)/2.0))/5.0); // characteristic timescale* /10
        double TotalTime = 40000.0;//TimeResOut*40.0;
        double TimeResOut = TotalTime/10;
        // std::cout << "\n characteristic timescale* (=1.0 sec) = " << floor(waveL/((ULmax+URmax)/2.0)) << std::endl;
        std::cout << " Total Simulation time        = " << TotalTime << "\n"<< std::endl;

    Vec3_t V0 = OrthoSys::O;
    Vec3_t V1 = OrthoSys::O;
    
    double pL = rhotmp1;
    double pR = rhotmp0;
    double cHigh    =   1.0;    // High concentration
    double cLow     =   0.0;//1.0e-4;  // Low Concentration 
    double rhotmpL  =   0.0;
    double rhotmpR  =   0.0;

    
    // ! Setting up the different fluids */
        for (size_t i=0; i<nx; ++i)
        {
            for (size_t j=0; j<ny; ++j)
            {
                // * Ref: PHYSICAL REVIEW E 84, 046710 (2011)
                // Inner Droplet 
                rhotmpL = 0.5*((cHigh+cLow)-(cHigh-cLow)*tanh( (2.0*(sqrt(pow((int)i - x0, 2.0) + pow((int)j - y0, 2.0)) - R0))/(intDp) ));

                // Outer surrounding fluid
                rhotmpR = 0.5*((cLow+cHigh)-(cLow-cHigh)*tanh( (2.0*(sqrt(pow((int)i - x0, 2.0) + pow((int)j - y0, 2.0)) - R0))/(intDp) ));

                if (rhotmpL <= cLow) rhotmpL=cLow;
                if (rhotmpR <= cLow) rhotmpR=cLow;
                rhotmpL *=pL;
                rhotmpR *=pR;

                Dom.AssignFluidParameters(0,iVec3_t(i,j,0),rhotmpR,V0);
                Dom.AssignFluidParameters(1,iVec3_t(i,j,0),rhotmpL,V1);
            }
        }
        
        
    /* Mixing interaction */ 
        double sumRho = 0.0;
        size_t k = 0, nxy=0;
        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j){
                nxy++;
                for (size_t k=0; k<nu.size(); ++k){
                    sumRho += Dom.Rho[k][i][j][0];
                }
        }
        sumRho      /= (nxy);
    
    /* Interaction Potential constant variables*/ 
        Dom.Psi[0] = 1.0;
        Dom.Psi[1] = 1.0;
        Dom.Rhoref[0] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        Dom.Rhoref[1] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        
        double rhoInt = rhotmp0*cHigh*0.5 + rhotmp1*cHigh*0.5; /*
            * under the assumpt that max interaction occurs at the interface */ 
        double TauEf = (Dom.Tau[0]*rhotmp0 + Dom.Tau[1]*rhotmp1)/(rhotmp0+rhotmp1);
        
        std::cout << " spatial average  <Rho> = " << sumRho << ", nxy=" << nxy << std::endl;
    /* Mixing interaction */ 
        double Gmix = 2.0/(rhoInt);
        
        // Gmix = Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]);
        Gmix = Gmix*Amplify/100.0;
        // Gmix /= Dom.e4;
        // Gmix /= Dom.e2;
        // double EOSINT = -Dom.e2*0.5*Gmix*(rhotmp0*rhotmp1);//
        // double EOSINT = 0.5*Gmix*((rhoInt*0.5)*(rhoInt*0.5));//
        double EOSINT = 0.0;
        double EOSINTtmp = 0.0;
        rhoInt = 0.0;
        for (size_t i=0; i<nx; ++i){
            size_t j    =   ny/2 - 1;
            double rhoA = Dom.Rho[0][i][j][0];//PsiInterInt(
            double rhoB = Dom.Rho[1][i][j][0];
            if (rhoA >= 0.2*rhotmp0 && rhoB >= 0.2*rhotmp1){
                double psiA = Dom.PsiInterInt(0,rhoA);
                double psiB = Dom.PsiInterInt(1,rhoB);
                EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*psiA*psiB, EOSINTtmp);
                // EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*Dom.PsiIntraInt(0,Dom.Rho[0][i][j][0])*Dom.PsiIntraInt(1,Dom.Rho[1][i][j][0]), EOSINTtmp);
                // rhoInt = std::max(Dom.Rho[0][i][j][0] + Dom.Rho[1][i][j][0], rhoInt);
                
                //* Note Should this be defined before and used to define EOSINTtmp??
                if (rhoA == rhoB) rhoInt = rhoA + rhoB;
            }
        }
        EOSINT = EOSINTtmp;
        // double EOSINT = -Dom.e2*0.5*(Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]))*(rhotmp0*rhotmp1);//
        std::cout << " Rho(Int) = " << rhoInt << std::endl;
        
    /* Self Interaction */
        double Psi0 = rhotmp0;//*0.5;
        double Psi1 = rhotmp1;//*0.5;
        // double Gself0 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(0,Psi0),2.0)))*(Dom.e2*rhotmp0  - Dom.e2*(rhotmp0+rhotmp1)*1.0 + EOSINT); //
        // Gself0 *= C;
        // double Gself1 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(1,Psi1),2.0)))*(Dom.e2*rhotmp1  - 1.0*Dom.e2*(rhotmp0 - 0.5*Gself0*pow(Dom.PsiIntraInt(0,Psi0),2.0))); // - EOSINT); //
        
        double Gself0 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(0,Psi0),2.0)))*(-Dom.Cs2*Psi0  + Dom.Cs2*(Psi0) - EOSINT);
        double Gself1 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(1,Psi1),2.0)))*(-Dom.Cs2*Psi1  + Dom.Cs2*(Psi1) - EOSINT);
        Gself0 *= C;//*0.99;
        Gself1 *= C;//*1.01;
        // Gself0 /= Dom.e2;
        // Gself1 /= Dom.e2;

        // Gself0   = 0.0, Gself1    = 0.0;
        // Gself0   = -0.150459, Gself1    = -0.150459; //* for Q17 and ZOT lattices: Q17ZOT and Q25ZOT
        Gself0   = -1.3792119, Gself1    = -1.3792119; //* for Q25 only (and Q37)
        // Gself0   = -1.0156015, Gself1    = -1.0156015; //* for Q21 only (and Q49)
        
        // Gmix /= (1.0-Dom.epsilon);
        // Gself0 /= (1.0-Dom.epsilon), Gself1 /= (1.0-Dom.epsilon);
        
        if (C==0.0) Gself1 = 0.0;
        double EOS0 =  (Dom.Cs2*Psi0 + 0.5*Dom.e2*Gself0*pow(Dom.PsiIntraInt(0,Psi0),2.0) + EOSINT);
        double EOS1 =  (Dom.Cs2*Psi1 + 0.5*Dom.e2*Gself1*pow(Dom.PsiIntraInt(1,Psi1),2.0) + EOSINT);
    
        std::cout << " G-mixing crit = " << Gmix << "; at " << Amplify << "% (G/p^2 = "<< (Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0])) << ")" << std::endl;
        std::cout << " Gself 0          = " << Gself0 << std::endl;
        std::cout << " Gself 1          = " << Gself1 << std::endl;
        std::cout << " Equation of State " << std::endl;
        std::cout << "  -->  EoS Int          = " << EOSINT << std::endl;
        std::cout << "  -->  P0         = " << EOS0 << ",  (Po = " << Dom.Cs2*rhotmp0 << ")" << std::endl;
        std::cout << "  -->  P1         = " << EOS1 << ",  (Po = " << Dom.Cs2*rhotmp1 << ")" << std::endl;
    
    /* interaction strengths */
        Dom.Gs[0]   = 0.0, Dom.Gs[1]    = 0.0;
        Dom.Gmix     =  Gmix;
        Dom.G [0]    =  Gself0;
        Dom.G [1]    =  Gself1;
        
        Dom.Nproc = Nproc;
        Dom.FApplyForcesE();
        Dom.InitializeMixture();
        // std::cout << " !TEST " << std::endl;
        Dom.CalcPressureTensor = true;
        
        String runflags;
    runflags.Printf("%s_RunFlags.txt",FileKey);
    std::ofstream out;
    out.open(runflags.CStr(),std::ios::out);
    out << Util::_10_6  <<  "StaticDroplet_Test01: " << Util::_8s << FileKey << std::endl;
    out << Util::_8s << " cpus" << Util::_8s << "const" << Util::_8s << "Amp"   << Util::_8s << "Ro" << std::endl;
    out << Util::_8s << std::setprecision(1) << Nproc  << Util::_8s << std::setprecision(1) <<  C      << Util::_8s << std::setprecision(1) << Amplify << Util::_8s << std::setprecision(1) << R0  << std::endl;
    out.close();

        String fs;
    fs.Printf("%s.res",FileKey);
    dat.oss_st.open(fs.CStr(),std::ios::out);
    dat.oss_st << Util::_10_6  <<  "Time" << Util::_8s << "R" << std::endl;
    /*(Do not touch)*/
    /* Solve  Total time , Time step, Start, line: ,report| name| render| number of CPU */
    Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,floor(0.1*TimeResOut*Dom.dt),NULL,Report,FileKey,false,Nproc);
    // Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,0*Dom.dt,NULL,NULL,FileKey,true,Nproc);
    
    dat.oss_st.close();

    return 0;
    
}
MECHSYS_CATCH
