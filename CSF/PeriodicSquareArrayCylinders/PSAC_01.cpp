/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// FLow in a pipe with obstacle

#include<iostream>
// MechSys
#include <mechsys/flbm/Domain.h>



struct UserData
{
    std::ofstream   oss_st;       ///< file for drag coefficient calculation
    Vec3_t          g;
    double      * Vel;
    double        vmax;
    double          Tf;
    double        rho;
    double      R;
    double      phi;
    size_t      count;
    double      kprev;
    double      CDsum;
    double      nu;
    bool ****   IsFB;
    size_t  Nlayers;
    char const * TheFileName;
    #ifdef USE_OCL
    cl::Buffer        bBCVel;
    cl::Program       UserProgram;
    #endif
};

void Report (FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    
    if (dom.Time<=dat.Tf-1) 
    {
        size_t nx = dom.Ndim(0);
        size_t ny = dom.Ndim(1);
        size_t nz = dom.Ndim(2);
        double M    = 0.0;
        double Vx   = 0.0;
        double Vy   = 0.0;
        // double Fx    = 0.0;//OrthoSys::O;
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        Vec3_t F = OrthoSys::O;
        double nc   = 0;
        double num  = 0;
        double Ad = nx*ny;
        Vec3_t Flux = OrthoSys::O;
        // Vec3_t Fluxx = OrthoSys::O;
        // size_t obsX   = ny/2;   // x position
	    // size_t obsY   = ny/2;//+3; // y position
        // dom.FluidSolidInteraction();
        // #ifdef USE_OMP
        // #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
        // #endif
        for (size_t ix=0;ix<nx;ix++)
        {
            for (size_t iy=0;iy<ny;iy++)
            {
                if (dom.IsSolid[0][ix][iy][0]) // continue; 
                {
                //     // Vx   += dom.Vel[0][ix][iy][0][0];
                //     // Vy   += dom.Vel[0][ix][iy][0][1];
                //     // F(0)   += dom.BForce[0][ix][iy][0][0];//dom.Rho[0][ix][iy][0]*(dom.Vel[0][ix][iy][0]-dom.Velprev[0][ix][iy][0]);

                    // if (dot(dom.nvec[0][ix][iy][0],dom.nvec[0][ix][iy][0])!=0.0)
                    // {
                    //     Vec3_t Vsum = 0.0;
                    //     double rhoSum = 0.0;
                    //     for (size_t k=0;k<dom.Nneigh;k++)
                    //     {
                    //         // Fluxx += dom.F[0][ix][iy][0][k]*dom.C[k]*dom.C[k];
                    //         Vsum += dom.F[0][ix][iy][0][k]*dom.C[k];
                    //         rhoSum += dom.F[0][ix][iy][0][k];
                    //     }
                    //     Flux += Vsum;// + 0.5*dom.dt*dat.g;//*dom.Rho[0][ix][iy][0];// - dom.dt*dom.Rho[0][ix][iy][0]*dat.g;
                    //     M    += rhoSum;
                    //     nc++;
                    // }
                    num++;
                    // F += dom.Rho[0][ix][iy][0]*dat.g;//*dom.Cs2;
                }
                else
                {
                    Flux += dom.Vel[0][ix][iy][0];//*dom.Rho[0][ix][iy][0];// + 0.5*dom.dt*dat.g*dom.Rho[0][ix][iy][0];// - dom.dt*dom.Rho[0][ix][iy][0]*dat.g;
                    M    += dom.Rho[0][ix][iy][0];
                    nc++;
                }
                // if (ix < dom.latticeLength)
                // {
                    Vx   += dom.Vel[0][ix][iy][0][0];
                    Vy   += dom.Vel[0][ix][iy][0][1];
                    // M    += dom.Rho[0][ix][iy][0];
                    // Flux += dom.Vel[0][ix][iy][0]+ 0.5*dom.dt*dat.g;//*dom.Rho[0][ix][iy][0];
                    // Flux += 0.5*dat.g*dom.Rho[0][ix][iy][0];
                    // F += dom.BForce[0][ix][iy][0];// - dat.g*dom.Rho[0][ix][iy][0];
                    // for (size_t k=0;k<dom.Nneigh;k++)
                    // {
                    //     Fluxx += dom.F[0][ix][iy][0][k]*dom.C[k]*dom.C[k];
                    //     // Flux += dom.F[0][ix][iy][0][k]*dom.C[k];
                    //     // M += dom.F[0][ix][iy][0][k];
                    // }
                //     num++;
                // }
            }
        }
        
        // nc = Ad - (PI*(pow(2.0*dat.R,2.0))/4.0);
        // nc = Ad*(1.0 - dat.phi);
        // F /= nc;//
        F += dom.tmpSum;//*2.0;
        // Vx = dom.tmpSum(1);
        // num = dom.tmpSum(2);
        
        // Flux/=M;
        M   /=Ad;
        Flux/=Ad;
        // Fluxx/=nc;
        Vx  /=Ad;
        Vy  /=Ad;
        Vec3_t Uact(Vx, Vy, 0.0);// (Flux/M);
        Uact *=(nc/Ad); // seepage velocity (actual velocity) = u*Ad/Af
        // Uact *=0.5;//
        // Vx  = Uact(0);
        // Vy  = Uact(1);
        // Flux /=1.0;
        // M=(1.0-dat.phi);
        // Flux/=M;
        // double phi_latt=(1.0-(Ad-nc)/Ad);
        double phi_latt=(1.0-(Ad-num)/Ad);
        std::cout << "Ave Mass Denisty  = " << M << "\n";
        std::cout << "phi(lat)  = " <<  phi_latt << ", phi_actual = " << dat.phi << std::endl;
        // if (phi_latt<dat.phi || phi_latt>dat.phi*1.025) dat.phi = phi_latt;
        double Fmag = sqrt(F(0)*F(0) +F(1)*F(1));
        // double Fluxmag = sqrt(Flux(0)*Flux(0) + Flux(1)*Flux(1));
        double Umag = sqrt(Uact(0)*Uact(0) +Uact(1)*Uact(1));
        // Umag *=(Ad/nc);//(nc/Ad); // seepage velocity (actual velocity) = u*Ad/Af
        // Uact(0) *= (nc/Ad);
        // std::cout << "sum Force(x)  = " << F(0) << std::endl;
        // std::cout << "number of nodes  = " << ((Ad)/nc) << ", "<< nc << ", " << Ad << std::endl;
        
        // Flux*=(Ad/nc);//*=(nc/Ad);
        // Flux/=(1.0-(dat.phi/(PI/4.0)));
        // Flux/=(1.0-dat.phi);
        // Fluxx*=(Ad/nc);
        // Flux*=(1.0-dat.phi);

        double CD  = F(0)/(0.5*((Flux(0)*Flux(0)))*M*2.0*dat.R); // Definition of Drag force over a cylinder (2D sphere)
        double Re  = (Flux(0)*(2.0*dat.R))/(dat.nu);
        double k_c = (4.0*PI*dat.nu*Flux(0)*M)/F(0); //sqrt(dot(F,F))
        // double k_c = sqrt(dot(F,F))/(M*dat.nu*sqrt(dot(Uact,Uact)));
        // double ndF = 1.0/k_c;//
        double ndF = F(0)/(M*dat.nu*Flux(0));
        // double ndF = (F(0))/(M*dat.nu*Fluxx(0));
        double k_ctA = (log(sqrt(1.0/dat.phi)) -0.738 + dat.phi - 0.887*pow(dat.phi,2.0) + 2.039*pow(dat.phi,3.0) - 2.421*pow(dat.phi,4.0));
        double k_ctB = ((9.0*PI)/(2.0*sqrt(2.0)))*pow((1.0-sqrt(dat.phi/(PI/4.0))),(-5.0/2.0));
        double k = k_c*(Ad)/(4.0*PI);
        if (Flux(0)<1.0e-12) 
        {
        // if (Vx<1.0e-12) 
        // {
            Flux = OrthoSys::O;
            CD = 0.0;
            Re = 0.0;
        }
        
        // std::cout << ">> CD  = " << CD << ",  @ Re  = " << Re << std::endl;
        
        if (dom.Time>(dat.Tf*0.25)) 
        {
            dat.CDsum +=CD;
            dat.count++;
            // std::cout << "-> CD(ave)  = " << dat.CDsum/dat.count << std::endl;
        }
        std::cout << ">> k_c*  = " << k_c << ",  @ Re  = " << Re << ", @ dk  = " << (k-dat.kprev) << "\n" ;
        std::cout << "-> F/mu*U = " << ndF;
        std::cout << "; && 4*PI/k_t:  (A) = " << (4.0*PI)/k_ctA << ", (B) = " << k_ctB << "\n";
        std::cout << "                          Error%        (A%)= " << ((k_ctA-k_c)/k_ctA)*100.0 << ",     (B%)= " << ((((4.0*PI)/k_ctB)-k_c)/((4.0*PI)/k_ctB))*100.0 << "\n" << std::endl;
        dat.oss_st << Util::_10_6 << dom.Time   << Util::_8s << M       << Util::_8s << Re     << Util::_8s << dat.phi     << Util::_8s << dat.CDsum/dat.count << Util::_8s << (4.0*PI)/k_ctA      << Util::_8s << k_ctB        << Util::_8s << k_c    << Util::_8s << ndF             << Util::_8s << k   << Util::_8s << (k-dat.kprev) << std::endl;
        
        if (fabs(k-dat.kprev)<1e-5)
        {
            String fileOutput;
            fileOutput.Printf    ("%s_Output",dat.TheFileName);
            dom.WriteXDMF(fileOutput.CStr());
            // return 0;
            dom.Time = dat.Tf;
        }
        dat.kprev = k;
    }
    else
    {
        dat.oss_st.close();
    }
}

void Setup (FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t i=0;i<dom.Ndim(0);i++)
    {
        for (size_t j=0;j<dom.Ndim(1);j++) // for (size_t j=dom.latticeLength-1;j<dom.Ndim(1)-dom.latticeLength;j++)
        {
            if (dom.IsSolid[0][i][j][0]) continue;
                dom.BForce[0][i][j][0] += dom.Rho[0][i][j][0]*dat.g;//*dom.Cs2;
            // else
            // {
            //     dom.BForce[0][i][j][0] += dom.Rho[0][i][j][0]*dat.g;
            // }

        }
    }
}


int main(int argc, char **argv) try
{
    size_t Nproc = 1; 
    if (argc>=2) Nproc=atoi(argv[1]);
    // double tau = 0.60; // double Re     = 10.0;                  // Reynold's number
    // if (argc>=3) tau=atof(argv[2]);
    // double nu = 1.0/6.0;
    // if (argc>=3) nu=atof(argv[2]);
    size_t nx = 100;//ny*2.0; // 2400
    size_t ny = nx;//12.5;//210; // 300
    // double Vs = nx*ny; // two-dimensional volume
    // double radius = 42.0;// 42;//ny/10 + 1;           // radius of inner circle (obstacle)
    const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
    double phi = 0.20;//Ar/Vs;
    if (argc>=3) phi=atof(argv[2]);
    char const * FileKey = "PSAC01";
    if (argc>=4) FileKey=argv[3];
    
    double radius = (((int)nx)/2.0)*sqrt((phi*4.0)/PI);
    // size_t nx = ceil(((int)radius*2.0)/sqrt((phi*4.0)/PI));
    // size_t ny = nx;//12.5;//210; // 300
    double Xi = phi/(PI/4.0);
    double phiAct = (pow((2.0*(radius)/nx),2.0)*PI)/4.0;//Ar/Vs;
    if (phi>0.25) phiAct = (pow((2.0*(radius-1.0)/nx),2.0)*PI)/4.0;
    /* D2Q9 */ 
        // double cs    = 1.0/sqrt(3.0);
    /* D2Q17 */ 
        // double cs    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));//*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
        /*ZOT*/
        // double cs       = sqrt(1.0/((5.0 + sqrt(10.0))/3.0));
    /* D2Q21 */ 
        // double cs      = (1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)))
    /* D2Q25 */ 
        // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));//*(1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
        /* ZOT */
        double cs =sqrt((1.0-sqrt(2.0/5.0)));
    
    // double nu = (cs*cs)*(tau-0.5);
    // double tau = (nu/(cs*cs)) + 0.5;
    double Re     = 20.0;//*((2.0*radius)/50.4627);//
    
    double Ma = 0.005;//42325;//0.10;//0.0547723;//0.0778172;//0.30;
    double u_max = Ma*cs;
    /* Dimensionless Gravity */
    double gtmp = pow((u_max),2.0)/(radius);//((nx*0.50 -radius));//((nx - 2.0*radius));
    // double gtmp = 1.0e-6;// pow((u_max),2)/(2.0*radius);//(2.0*radius); // gravity [m/s^2] = u^2/l, l=diameter.
    // double u_max = (Re*nu/(2.0*radius));
    // double gtmp = pow((Re*nu/(radius*2.0)),2.0)/(radius*2.0); // viscocity
    // double u_max = sqrt(gtmp*(radius));//(radius*2.0));
    // double Ma = u_max/cs;
    
    double nu     = u_max*(2.0*radius)/Re; // viscocity
    double tau = (nu/(cs*cs)) + 0.5;
    // double Re     = u_max*(2.0*radius)/nu; // viscocity
    
    FLBM::Domain Dom(D2Q25ZOT, E8, nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    
    UserData dat;
    Dom.UserData = &dat;
    dat.TheFileName = FileKey;
    dat.Tf = 400000.0;
    dat.vmax = u_max;
    dat.R = radius;
    dat.phi = phiAct;
    dat.g           =   gtmp,    0.0,  0.0; 
    dat.nu = nu;
    dat.count = 0;
    dat.CDsum = 0.0;
    dat.Vel = new double[ny];
    /*Print stuff 
    */
        printf("%s   # CPU's                         = %zd%s\n ",TERM_CLR2,Nproc,TERM_RST);
        printf("\n%s--- gravity*        =  %g%s\n",TERM_CLR1,dat.g[0],TERM_RST);
        printf("\n%s--- U*(max)         = %g%s\n",TERM_CLR1,u_max,TERM_RST);
        printf("\n%s--- Mach Number     = %g%s\n",TERM_CLR1,Ma,TERM_RST);
        printf("\n%s--- Sphere Diameter = %g%s\n",TERM_CLR1,radius*2.0,TERM_RST);
        std::cout << "---- Domain(nx,ny) = (" << nx << ", "<< ny << ") \n" << std::endl;
        printf("\n%s--- time scale t* (D/Uo) = %g%s\n",TERM_CLR1,(radius*2.0)/u_max,TERM_RST);
        printf("\n%s--- dt*             = 1.0/  %g%s\n",TERM_CLR1,sqrt(nx/(dat.g[0])),TERM_RST);
        printf("\n%s--- Re Number       = %g%s\n",TERM_CLR1,Re,TERM_RST);
        printf("\n%s--- Solid Concentration       = %g%s\n",TERM_CLR1,phiAct,TERM_RST);
        // printf("\n%s--- Solid Concentration       = %g%s\n",TERM_CLR1,Xi,TERM_RST);

        // for (size_t i=0;i<ny;i++)
        // {
        //     // set parabolic profile
        //     double L  = ny - 2.0;                       // channel width in cell units
        //     double yp = i - 1.5;                      // ordinate of cell
        //     double vx = dat.vmax*4/(L*L)*(L*yp - yp*yp); // horizontal velocity
        //     dat.Vel[i] = vx;
        // }
    //
    dat.rho  = 1.0;
    dat.Nlayers = Dom.latticeLength; 
    dat.kprev = 0.0;
	// set inner obstacle
	size_t obsX   = (size_t)(((int)nx-1)/2);   // x position
	size_t obsY   = (size_t)(((int)ny-1)/2);//+3; // y position
    // size_t radiusHB = radius + 0.0;//dat.Nlayers;// dat.Nlayers;
    
    double ns = 0.0; // count number of solids nodes
    double Ad = nx*ny;
    for (size_t i=0;i<nx;i++)
    for (size_t j=0;j<ny;j++)
    {
        if ((i-obsX)*(i-obsX)+(j-obsY)*(j-obsY)<=(int)radius*(int)radius) // if (pow((int)(i)-(int)obsX,2.0) + pow((int)(j)-(int)obsY,2.0) <= pow(radius,2.0))
        {
            Dom.IsSolid[0][i][j][0] = true; 
    
        }
        if ((i-obsX)*(i-obsX)+(j-obsY)*(j-obsY)<=(int)(radius+1)*(int)(radius+1)) // if (pow((int)(i)-(int)obsX,2.0) + pow((int)(j)-(int)obsY,2.0) <= pow(radius,2.0))
        {
            Dom.IsHBSolid[0][i][j][0] = true; 
    
        }
        ns++;
    }

    // dat.phi = (1.0 - (Ad-ns)/Ad);
    // Assigning solid boundaries at top and bottom
        // for (size_t i=0;i<nx;i++)
        // {
        //     for (size_t j=0;j< dat.Nlayers;j++)
        //     {
        //         Dom.IsSolid[0][i][j       ][0]     = true;
        //         Dom.IsSolid[0][i][ny-(1+j)][0]     = true;
        //     }
        // }
    // --------------------------------------------

    // double dat.rho = dat.rho;
    // // Vec3_t v0(dat.vmax*0.80,0.0,0.0);
    // Vec3_t v0(10.0*dat.vmax*1.0/dat.Tf,0.0,0.0);
    // // Vec3_t v0(0.0,0.0,0.0);

    // for (size_t ix=0;ix<nx;ix++)
    // for (size_t iy=0;iy<ny;iy++)
    // {
    //     iVec3_t idx(ix,iy,0);
    //     Dom.Initialize(0,idx,dat.rho,v0);
    // }

    Vec3_t v0(0.0,0.0,0.0);
    // double dt = 1.0;
    // Vec3_t v0 = (0.5*dt*dat.rho*dat.g)/dat.rho;
    v0 = (0.5*dat.rho*dat.g)/dat.rho;
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    {
        iVec3_t idx(ix,iy,0);
        Dom.Initialize(0,idx,dat.rho,v0);
        // Dom.InitializeEF(0,idx,dat.rho,v0);
        // if (!Dom.IsSolid[0][ix][iy][0]) //continue;
        // {
        //     Dom.BForce[0][ix][iy][0] += Dom.Rho[0][ix][iy][0]*dat.g;///dom.Cs2;
        // }
    }
    
    Dom.DefNormVec();

    // String fileParameters;
    // fileParameters.Printf    ("%s_Parameters",FileKey);
    // Dom.WritePARAMETERS(fileParameters.CStr());

    String fs;
    String TheFileKey;
    TheFileKey.Printf("%s",FileKey);
    fs.Printf("%s_force.res",TheFileKey.CStr());
    dat.oss_st.open(fs.CStr());
    dat.oss_st << Util::_10_6 << "Time"     << Util::_8s << "Rho"   << Util::_8s << "Re"   << Util::_8s << "Phi"   << Util::_8s << "CD(ave)" << Util::_8s << "k_c*(A)"           << Util::_8s << "k_c*(B)"   << Util::_8s << "k_c"   << Util::_8s << "-> F/mu*U = "  << Util::_8s << "k" << Util::_8s << "dk \n";
    Dom.Solve(dat.Tf, dat.Tf/40.0, 1.0, Setup, Report, FileKey, false, Nproc);

    return 0;
    dat.oss_st.close();


}
MECHSYS_CATCH
