
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    Vec3_t          g;
    double          * rho0tmp;
    double Nlayers ;
    double *         rho;
    
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Setup(FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));

    // iVec3_t idx(0,0,0);
    // iVec3_t idl(0,0,0);
    // Vec3_t V(0.0,0.0, 0.0);
    // Array<double> rho(2);
    //     rho[0] = 1.00;//
    //     rho[1] = 1.00;//
    // idx=0.0         , dom.Ndim(1)-dom.latticeLength , 0;
    // idl=dom.Ndim(0) , dom.Ndim(1)                   , 1;

    // dom.VelBB(0,Top, idx, idl, V);
    // dom.VelBB(1,Top, idx, idl, V);
    // dom.PressBB(0,Top, idx, idl, rho);
    // dom.PressBB(1,Top, idx, idl, rho);

    // idx=0           , 0                 , 0;
    // idl=dom.Ndim(0) , dom.latticeLength , 1;

    // dom.VelBB(0,Bottom, idx, idl, V);
    // dom.VelBB(1,Bottom, idx, idl, V);

    // dom.PressBB(0,Bottom, idx, idl, rho);
    // dom.PressBB(1,Bottom, idx, idl, rho);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t il=0;il<dom.Nl;il++)
    for (size_t i=0;i<dom.Ndim(0);i++)
    for (size_t j=0;j<dom.Ndim(1);j++)//for (size_t j=dom.latticeLength;j<dom.Ndim(1)-dom.latticeLength;j++)
    {
        if (!dom.IsSolid[il][i][j][0])
        {
            dom.BForce[il][i][j][0][1] += (dom.Rho[il][i][j][0]*dat.g[1]);
        }
        else
        {
            dom.Vel[il][i][j][0] = 0.0,0.0,0.0;
        }
    }
}
/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */


int main(int argc, char **argv) try
{
    size_t Nproc = 1; 
    if (argc>=2) Nproc=atoi(argv[1]);
    /* Modelling parameters */
    size_t nx = 200;
    size_t ny = nx*4.0;
    double realLength = 0.1; // in meters
    double dx = realLength/nx;
    double TotalTime = 20000;
    double TimeResOut = TotalTime/30;
    /* Reynolds Number */
    double Re = 1.0*256.0; 
    if (argc>=3) Re=atof(argv[2]);
    char const * FileKey = "RT_LBu_T3";
    if (argc>=4) FileKey=argv[3];
    
        int interfaceLength = 4.0;
        int obsY = ny - ny/2;// - ny/4;
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        double Amplitude = ((int)nx*0.1)*dx;// e.g. 0.1 == 10% perturbation interface
    
    /* D2Q9 */ 
        // double cs    = 1.0/sqrt(3.0);
    /* D2Q17 */ 
        // double cs    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));//*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
        /*ZOT*/
        // double cs       = sqrt(1.0/((5.0 + sqrt(10.0))/3.0));
    /* D2Q21 */ 
        // double cs      = (1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)))
    /* D2Q25 */ 
        // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));//*(1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
        /* ZOT */
        double cs =sqrt((1.0-sqrt(2.0/5.0)));
    
    /* Dimensionless Gravity */
    double Ma = 0.100;
    double vel = Ma*cs;// 
    // double vel = 0.04;// 
    // double Ma = vel/cs;
    double gtmp = pow((vel),2)/(nx*1.0);
    double C = 0.225; // constant parameter for changing surface tension
    double Amplify = 110.0;//
    
    double massRatio = 3.0;//
    double muRatio  = 3.0;//
    double muRef    =0.0831911;

    /* Kinematic Viscosity   */
    Array<double> nu(2);
    nu[0]  = (vel*nx)/Re;
    nu[1] = (3.0/3.0)*(vel*nx)/Re; // 

    /* Molecular Mass   */
    Array<double> molMass(2);
        molMass[0] = 1.0;//0.5;//muRef/nu[0];//molMass[1]/3.0;// molecular mass of A [dimensionless ratio]
        molMass[1] = massRatio*molMass[0];// molecular mass of B [dimensionless ratio]
        
    /* Number Density   */
    Array<double> nmol(2);
        nmol[0] = 1.00;//
        nmol[1] = 1.00;//
    double rhotmp0 = nmol[0]*molMass[0];
    double rhotmp1 = nmol[1]*molMass[1];
    /* Atwood Number */
    double At = (nmol[1]*molMass[1] - nmol[0]*molMass[0])/(nmol[1]*molMass[1] + nmol[0]*molMass[0]);

    FLBM::Domain Dom(D2Q25ZOT, ELS, MRT, nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    
    UserData dat;
    Dom.UserData = &dat;
    // double gtmp = pow(Re*nu[1]/(sqrt(nx)*nx),2.0);
    dat.g           =   0.0,    -gtmp,  0.0; 

    printf("\n%s--- Running at Re   = %g%s\n",TERM_CLR1,Re,TERM_RST);
    printf("\n%s--- Atwood Number   = %g%s\n",TERM_CLR1,At,TERM_RST);
    std::cout << " MolMass = " << molMass[0] << std::endl;
    std::cout << " nu = " << nu[0] << std::endl;
    std::cout << " Mach Number      = " << Ma << std::endl; 
    std::cout << " Dimensionless Gravity = " << dat.g[1] << std::endl;
    std::cout << " dt*              = 1.0/" << sqrt(nx/(-dat.g[1])) << std::endl;
    std::cout << " Velocity*        = " << dat.g[1]*sqrt(nx/(-dat.g[1])) << std::endl; 
    
        
        for (size_t i=0; i<nx; ++i)
        {
            for (size_t j=0; j<Dom.latticeLength+1; ++j)
            {
                Dom.IsSolid[0][i][j][0] = true; 
                Dom.IsSolid[1][i][j][0] = true; 
                Dom.IsSolid[0][i][Dom.Ndim(1)-1-j][0] = true; 
                Dom.IsSolid[1][i][Dom.Ndim(1)-1-j][0] = true; 
            }
        }
        Dom.DefNormVec();
        
    // Vec3_t Vo = (0.0, -dat.g[1]*sqrt(nx/(-dat.g[1])), 0.0);
    double Dp = 5.0;
    double Dv = 5.0;
    Vec3_t V0 = OrthoSys::O;
    Vec3_t V1 = OrthoSys::O;
    double uy = 0.0;
    
    double pL =  rhotmp1;
    double pR =  rhotmp0;
    // double vL =  dat.Uo;
    // double vR = -dat.Uo;
    double cHigh = 1.0; // High concentration
    double cLow = 0.01; // Low Concentration 
    double rhotmpL=0.0;
    double rhotmpR=0.0;
    double wavek = 20.0*PI;//((int)nx/(int)ny)*(1.0/3.0)*10.0*PI;
    // double Amplitude = ((int)nx*0.02)*dx;// 0.1 == 10% perturbation interface
    // dat.CBCnodes = 3 + Dom.latticeLength;

    /* Setting up the different fluids */
        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j)
        {
            // Side-Bottom Concentration 
            rhotmpL = 0.5*((cLow+cHigh)-(cLow-cHigh)*tanh(( dx*(((int)j+1.0)-ceil((int)ny/2.0)) - Amplitude*cos(wavek*((int)i+1.0)*dx) )/(Dp*dx)));
             // Side-Top Concentration 
            rhotmpR = 0.5*((cHigh+cLow)-(cHigh-cLow)*tanh(( dx*(((int)j+1.0)-ceil((int)ny/2.0)) - Amplitude*cos(wavek*((int)i+1.0)*dx) )/(Dp*dx)));
                // V1(1)   = 0.5*((vL+vR)-(vL-vR)*tanh(( dx*(((int)j+1.0)-ceil((int)ny/2.0)) + Amplitude*cos(wavek*((int)i+1.0)*dx) )/(Dv*dx)));
                // V0(1)   = V1(1); // Works since V_Right = - V_Left 
            rhotmpL *=pL;
            rhotmpR *=pR;
            if (rhotmpL <= (pL*cLow)*1.01) rhotmpL=cLow;
            if (rhotmpR <= (pL*cLow)*1.01) rhotmpR=cLow;

            Dom.AssignFluidParameters(0,iVec3_t(i,j,0),rhotmpR,V0);
            Dom.AssignFluidParameters(1,iVec3_t(i,j,0),rhotmpL,V1);
        }
        
    /* Interaction Potential constant variables*/ 
        Dom.Psi[0] = 1.0;
        Dom.Psi[1] = 1.0;
        Dom.Rhoref[0] = rhotmp1*0.85;//1.0;
        Dom.Rhoref[1] = rhotmp1*0.85;//1.0;

        double summ = 0.0;
        double sumRho = 0.0;
        double sumRho0 = 0.0;
        double sumRho1 = 0.0;
        size_t k = 0,nxy=0;
        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j)
        {
            if (Dom.IsSolid[0][i][j][0]) continue;
                nxy++;
                sumRho0 += Dom.Rho[0][i][j][0];
                sumRho1 += Dom.Rho[1][i][j][0];
                for (size_t k=0; k<nu.size(); ++k)
                {
                    summ += Dom.Nmol[k][i][j][0];
                    sumRho += Dom.Rho[k][i][j][0];
                }
        }
        summ        /= (nxy);
        sumRho0     /= (nxy);
        sumRho1     /= (nxy);
        sumRho      /= (nxy);
        double rhoInt = (rhotmp0*(cHigh-cLow)*0.5+rhotmp1*(cHigh-cLow)*0.5)/2.0;
        std::cout << " Rho(Int) = " << rhoInt << std::endl;
        std::cout << " spatial average  <Rho> = " << sumRho << ", nxy=" << nxy << std::endl;
    /* Mixing interaction */ 
        // double Gcrit01 = (Dom.Tau[0]-0.5)*(nu.size()*(molMass[0]))/(Dom.Tau[0]*summ);
        // double Gcrit02 = (Dom.Tau[1]-0.5)*(nu.size()*(molMass[1]))/(Dom.Tau[1]*summ);
        // double Gmix = std::max(Gcrit01,Gcrit02);
        double Gcrit01 = (Dom.Tau[0]-0.5)*((1.0)/((Dom.Tau[0]-0.5)*rhoInt));//*Dom.Rhoref[0]*Dom.Rhoref[0];
        double Gcrit02 = (Dom.Tau[1]-0.5)*((1.0)/((Dom.Tau[1]-0.5)*rhoInt));//*Dom.Rhoref[0]*Dom.Rhoref[0];
        double Gmix = std::max(Gcrit01,Gcrit02);
        
        // Gmix = Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]);
        Gmix = Gmix*Amplify/100.0;
        // Gmix *= Dom.e4;
        double EOSINT = -Dom.Cs2*0.5*Gmix*(rhotmp0*rhotmp1);//(sumRho0*sumRho1);//(rhotmp0*rhotmp1);
        // double EOSINT = -0.50*Gmix*Dom.Cs2*(Dom.PsiIntEq(0,rhotmp0)*Dom.PsiIntEq(0,rhotmp1));
        
        double Gself0 = ((2.0)/(Dom.Cs2*pow(Dom.PsiIntEq(0,rhotmp0),2.0)))*(Dom.Cs2*rhotmp0  - Dom.Cs2*(rhotmp0+rhotmp1)*1.0 + EOSINT); //
        // Gself0 *= Dom.e4;
        Gself0 *= C;//Dom.e4;
        double Gself1 = ((2.0)/(Dom.Cs2*pow(Dom.PsiIntEq(1,rhotmp1),2.0)))*(Dom.Cs2*rhotmp1  - 1.0*Dom.Cs2*(rhotmp0 - 0.5*Gself0*pow(Dom.PsiIntEq(0,rhotmp0),2.0))); // - EOSINT); //
        // Gself1 *= Dom.e4;
        // double Gself1 = ((2.0*Dom.e4)/(Dom.Cs2*pow(Dom.PsiIntEq(0,rhotmp1),2.0)))*(Dom.Cs2*rhotmp1  - Dom.Cs2*(rhotmp0+rhotmp1)*1.0 - EOSINT ); //
        // double Gself0 = ((2.0*Dom.e4)/(Dom.Cs2*pow(Dom.PsiIntEq(0,rhotmp0),2.0)))*(Dom.Cs2*rhotmp0  - 1.0*Dom.Cs2*(rhotmp1 - 0.5*Gself1*pow(Dom.PsiIntEq(0,rhotmp1),2.0)) ); // - EOSINT); //
        double EOS0 =  (Dom.Cs2*rhotmp0 - 0.5*Gself0*Dom.Cs2*pow(Dom.PsiIntEq(0,rhotmp0),2.0)+ EOSINT);
        double EOS1 =  (Dom.Cs2*rhotmp1 - 0.5*Gself1*Dom.Cs2*pow(Dom.PsiIntEq(1,rhotmp1),2.0)+ EOSINT);

        std::cout << " G-mixing crit = " << Gmix << "; at " << Amplify << "%" << std::endl;
        std::cout << " Gself 0          = " << -Gself0 << std::endl;
        std::cout << " Gself 1          = " << -Gself1 << std::endl;
        std::cout << " Equation of State " << std::endl;
        std::cout << "  -->  EoS Int          = " << EOSINT << std::endl;
        std::cout << "  -->  P0         = " << EOS0 << ",  (Po = " << Dom.Cs2*rhotmp0 << ")" << std::endl;
        std::cout << "  -->  P1         = " << EOS1 << ",  (Po = " << Dom.Cs2*rhotmp1 << ")" << std::endl;
        
        
    /* interaction strengths */
        Dom.Gmix     =  Gmix;
        Dom.G [0]    =  -Gself0;
        Dom.G [1]    =  -Gself1;
        // Dom.Gs[0]    =  1.0;
        // Dom.Gs[1]    =  1.0;

        Dom.FApplyForcesE();
        Dom.InitializeMixture();

    /* Solve  Total time , Time step, Start, line: ,report| name| render| number of CPU */
    Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,0*Dom.dt,Setup,NULL,FileKey,true,Nproc);
    return 0;
    
}
MECHSYS_CATCH
