
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    double          * rho0tmp;
    double Nlayers ;
    // double *         rhoL;
    // double *         rhoR;
    double      UoL;
    double      UoR;
    size_t CBCnodes;
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Setup(FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    
    // double Vel = dat.Uo;
    double VelL = dat.UoL;
    double VelR = dat.UoR;
    Vec3_t VL(0.0,VelL, 0.0);
    Vec3_t VR(0.0,-VelR, 0.0);
    Vec3_t V0(0.0,0.0, 0.0);
    // iVec3_t idx(0,0,0);
    // iVec3_t idl(0,0,0);
    
    // idx=0           ,            0, 0;
    // idl=dom.latticeLength ,  dom.Ndim(1), 1;

    // dom.VelBB(0,Left, idx, idl, V0);
    // dom.VelBB(1,Left, idx, idl, VL);
    // // dom.PressBB(0,Left, idx, idl, dat.rhoL);
    // // dom.PressBB(1,Left, idx, idl, dat.rhoL);
    
    // idx=dom.Ndim(0)-dom.latticeLength ,           0,  0;
    // idl=dom.Ndim(0)             , dom.Ndim(1),  1;

    // dom.VelBB(0,Right, idx, idl, VR);
    // dom.VelBB(1,Right, idx, idl, V0);

    // // dom.PressBB(0,Right, idx, idl, dat.rhoR);
    // // dom.PressBB(1,Right, idx, idl, dat.rhoR);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t il=0;il<dom.Nl;il++)
    for (size_t i=0;i<dom.Ndim(0);i++)
    for (size_t j=0;j<dom.Ndim(1);j++)
    {
        if (!dom.IsSolid[il][i][j][0]) continue;
        if ((int)i<(int)dom.Ndim(0)/2) dom.Vel[il][i][j][0] = VL;
        if ((int)i>(int)dom.Ndim(0)/2) dom.Vel[il][i][j][0] = VR;
    }

}
/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */


int main(int argc, char **argv) try
{
    size_t Nproc = 1;
    if (argc>=2) Nproc=atoi(argv[1]);
    
    // ! Modelling parameters
        size_t nx = 256;//512;
        size_t ny = nx/1.0;
        double x0 = nx/2.0 - 1.0;
        double C = 1.0;// 0.15; // constant parameter for changing surface tension
            if (argc>=3) C=atof(argv[2]);
        double Amplify = 70.0; // 275.0;//
            if (argc>=4) Amplify=atof(argv[3]);
        char const * FileKey = "KH_T3-2";
            if (argc>=5) FileKey=argv[4];
    /* -------------------------------------------- */

    // ! Lattice Structures
        // * D2Q9
            // double cs    = 1.0/sqrt(3.0);
        // * D2Q17
            // double cs    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));//*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
            /*ZOT*/
            // double cs       = sqrt(1.0/((5.0 + sqrt(10.0))/3.0));
        // * D2Q21
            // double cs      = (1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)));//*(1.0/(sqrt(3.0/2.0)))
        // * D2Q25
            // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));//*(1.0/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
            // * ZOT
            // double cs =sqrt((1.0-sqrt(2.0/5.0)));
        // * D2Q37
            // double cs      = (1.0/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0)));// Ref. Philippi
            double cs      = (1.0/1.19697977039307435897239);// Ref. Shan
        // * D2Q49 ZOTT (Zero-One-Two-Three)
            // double cs     = sqrt((2.0/3.0) + (1.0/3.0)*pow(7.0/(5.0*(3.0*sqrt(30.0) - 5.0)), (1.0/3.0)) - pow(((3.0*sqrt(30.0) - 5.0))/7.0, (1.0/3.0))/pow(35.0, (2.0/3.0)));
            // cs     = sqrt(cs); // Should equal  cs^2 == 0.697 953 322 019 683 1
    /* -------------------------------------------- */

    /* Dimensionless Numbers */
        double Re = 4.0*256.0; //* Reynolds Number.
        double Ma = 0.20;//*0.8782;//*1.13865461750; //* Mach Number if Ma << 0.3 flow will be incompressible.

        int interfacewidth = 5.0;
        int obsY = ny - ny/2;// Interface location
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        double dx = ((int)nx/(int)ny)*0.1/nx;
        double Dp = 5.0; // 5*dx thickness rule for the interface between the twofluids. Ref. Hessling 2017 JChemPhys.146 054111
        double Dv = 5.0;
        double Amplitude = ((int)ny*0.01)*dx;// // 0.1 == 10% perturbation interface
        double wavek = 20.0*PI;//((int)nx/(int)ny)*(1.0/3.0)*10.0*PI;
    /* -------------------------------------------- */
    
    /* Dimensionless Variables */
        // double Umax = Ma*cs;
        double ULmax = 0.0, URmax = 0.0; 
        ULmax = Ma*cs, URmax = Ma*cs;
        double waveL = ((ny-0.0)/1.0);
        double massRatio = 1.0;//5.0/2.0;//
        double muRatio = 1.0;
    /* -------------------------------------------- */
    
    /* Kinematic Viscosity */
        Array<double> nu(2);
        nu[0] = (URmax*waveL)/(Re);
        nu[1] = (massRatio/muRatio)*(ULmax*waveL)/(Re); //(muRatio/massRatio)*(Umax*waveL)/(Re); 
        // ULmax = (muRatio/(waveL*massRatio))*((URmax*waveL)/(nu[0]));
        // nu[1] = (ULmax*waveL)/(Re);

    /* Molecular Mass   */
        Array<double> molMass(2);
        molMass[0] = 1.0;
        molMass[1] = massRatio*molMass[0];// molecular mass of B [dimensionless ratio]
        
    /* Number Density   */
        Array<double> nmol(2);
        nmol[0] = 1.00;//
        nmol[1] = 1.00;//
        double rhotmp0 = nmol[0]*molMass[0];
        double rhotmp1 = nmol[1]*molMass[1];
    /* Atwood Number */
        double At = (nmol[1]*molMass[1] - nmol[0]*molMass[0])/(nmol[1]*molMass[1] + nmol[0]*molMass[0]);

    /* Do not touch */
        FLBM::Domain Dom(D2Q37, ELS, MRT,nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    UserData dat;
    Dom.UserData = &dat;
    dat.UoL = ULmax;// /2.0;
    dat.UoR = URmax;// /2.0;
    // dat.rhoL = new double [Dom.Nl];
    // dat.rhoR = new double [Dom.Nl];
    // dat.rhoL[0] = rhotmp0*0.01;
    // dat.rhoL[1] = rhotmp1;
    // dat.rhoR[0] = rhotmp0;
    // dat.rhoR[1] = rhotmp1*0.01;
    /* -------------------------------------------- */

    /* Print out stuff */
        printf("\n%s--- Running at Re   = %g%s\n",TERM_CLR1,Re,TERM_RST);
        printf("\n%s--- Atwood Number   = %g%s\n",TERM_CLR1,At,TERM_RST);
        std::cout << " MolMass = " << molMass[0] << std::endl;
        std::cout << " nu = " << nu[0] << std::endl;
        std::cout << " Mach Number      = " << Ma << std::endl; 
        std::cout << "--- Velocity* " << (dat.UoL+dat.UoR)/2.0 << ", V(max) = " << ((ULmax+URmax)/2.0) << std::endl; 
        

        // double TotalTime = 4000;
        // double TimeResOut = TotalTime/30;
        double TimeResOut = floor((waveL/((ULmax+URmax)/2.0))/5.0); // characteristic timescale* /10
        double TotalTime = TimeResOut*40.0;

        std::cout << "\n characteristic timescale* (=1.0 sec) = " << floor(waveL/((ULmax+URmax)/2.0)) << std::endl;
        std::cout << " Total Simulation time        = " << TotalTime << "\n"<< std::endl;
    
    // ! Solid boundaries
        for (size_t i=0; i<Dom.latticeLength+1; ++i)
        for (size_t j=0; j<Dom.Ndim(1); ++j){
            Dom.IsSolid[0][i][j][0] = true; 
            Dom.IsSolid[1][i][j][0] = true; 
            Dom.IsSolid[0][Dom.Ndim(0)-1-i][j][0] = true; 
            Dom.IsSolid[1][Dom.Ndim(0)-1-i][j][0] = true; 
        }
    
    Dom.DefNormVec();

    Vec3_t V0 = OrthoSys::O;
    Vec3_t V1 = OrthoSys::O;
    double uy = 0.0;
    
    double pL = rhotmp1;
    double pR = rhotmp0;
    double vL = dat.UoL;
    double vR = -dat.UoR;
    double cHigh = 1.0; // High concentration
    double cLow = 0.0001;//0.01; // Low Concentration 
    double rhotmpL=0.0;
    double rhotmpR=0.0;
    dat.CBCnodes = 3 + Dom.latticeLength;

    // ! Setting up the different fluids */
        for (size_t i=0; i<nx; ++i)
        {
            for (size_t j=0; j<ny; ++j)
            {
                // Side-L Concentration 
                rhotmpL = 0.5*((cHigh+cLow)-(cHigh-cLow)*tanh(( dx*((int)i - x0) + Amplitude*cos(wavek*( (int)j )*dx) )/(Dp*dx)));
                // rhotmpL *=pL;
                V1(1)   = 0.5*((vL + vR) - (vL - vR)*tanh((     dx*((int)i - x0) + Amplitude*cos(wavek*( (int)j )*dx) )/(Dv*dx)));
                
                // Side-R Concentration 
                rhotmpR = 0.5*((cLow+cHigh)-(cLow-cHigh)*tanh(( dx*((int)i - x0) + Amplitude*cos(wavek*( (int)j )*dx) )/(Dp*dx)));
                // rhotmpR *=pR;
                V0(1)   = 0.5*((vL + vR) - (vL - vR)*tanh((     dx*((int)i - x0) + Amplitude*cos(wavek*( (int)j )*dx) )/(Dv*dx)));

                if (rhotmpL <= cLow) rhotmpL=cLow;
                if (rhotmpR <= cLow) rhotmpR=cLow;
                rhotmpL *=pL;
                rhotmpR *=pR;

                if (Dom.IsSolid[0][i][j][0]){
                    V1(1) = 0.0, V0(1) = 0.0;
                    if ((int)i<(int)Dom.Ndim(0)/2) V1(1) = vL,V0(1) = vL;
                    if ((int)i>(int)Dom.Ndim(0)/2) V0(1) = vR,V1(1) = vR;
                }

                // if (rhotmpL <= (pL*cLow)*1.01) rhotmpL=cLow;
                // if (rhotmpR <= (pL*cLow)*1.01) rhotmpR=cLow;

                // if (rhotmpL==0) rhotmpL=pL*cLow;//cLow;//
                // if (rhotmpR==0) rhotmpR=pR*cLow;//cLow;//

                // Dom.Initialize (0,iVec3_t(i,j,0),rhotmpR,V0);
                // Dom.Initialize (1,iVec3_t(i,j,0),rhotmpL,V1);

                Dom.AssignFluidParameters(0,iVec3_t(i,j,0),rhotmpR,V0);
                Dom.AssignFluidParameters(1,iVec3_t(i,j,0),rhotmpL,V1);
            }
        }

        // std::cout << " Dimensionless number density (Fluid A) n = " << nmol[0] << " | & rho  " << rhotmp0*Dom.molMass[0] << std::endl;
        // std::cout << " Dimensionless number density (Fluid B) n = " << nmol[1] << " | & rho  " << rhotmp1*Dom.molMass[1] << std::endl;
        
        
    /* Mixing interaction */ 
        double summ = 0.0, sumRho = 0.0, sumRho0 = 0.0, sumRho1 = 0.0;
        size_t k = 0, nxy=0;
        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j){
            // if (j >= ceil(obsY-(interfacewidth/2.0)+Amplitude*(nx*1.0)*cos(((2.0*PI)*(1.0/(nx*1.0))*(int)(i)))) && j <= ceil(obsY+(interfacewidth/2.0)+Amplitude*(nx*1.0)*cos(((2.0*PI)*(1.0/(nx*1.0))*(int)(i))))){
                nxy++;
                sumRho0 += Dom.Rho[0][i][j][0];
                sumRho1 += Dom.Rho[1][i][j][0];
                for (size_t k=0; k<nu.size(); ++k){
                    summ += Dom.Nmol[k][i][j][0];
                    sumRho += Dom.Rho[k][i][j][0];
                }
            // }
        }
        summ        /= (nxy), sumRho0     /= (nxy), sumRho1     /= (nxy), sumRho      /= (nxy);
    
    /* Interaction Potential constant variables*/ 
        Dom.Psi[0] = 1.0;
        Dom.Psi[1] = 1.0;
        Dom.Rhoref[0] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        Dom.Rhoref[1] = rhotmp0*1.0;//*(2.0/PI);//1.0;
        double Psi0 = rhotmp0;
        double Psi1 = rhotmp1;
        // if (rhoInt>0) sumRho=rhoInt, std::cout << " Rho(Int) = " << rhoInt << std::endl;
        // double rhoInt = (Psi0*(cHigh-cLow)*0.5+Psi1*(cHigh-cLow/massRatio)*0.5)/2.0;
        double rhoInt = rhotmp0*cHigh*0.5 + rhotmp1*cHigh*0.5; /*
            * under the assumpt that max interaction occurs at the interface */ 
        double TauEf = (Dom.Tau[0]*rhotmp0 + Dom.Tau[1]*rhotmp1)/(rhotmp0+rhotmp1);
        std::cout << " Rho(Int) = " << rhoInt << std::endl;
        std::cout << " spatial average  <Rho> = " << sumRho << ", nxy=" << nxy << std::endl;
    /* Mixing interaction */ 
        // double Gcrit01 = (Dom.Tau[0]-0.5)*(nu.size()*(molMass[0]))/(Dom.Tau[0]*summ);
        // double Gcrit02 = (Dom.Tau[1]-0.5)*(nu.size()*(molMass[1]))/(Dom.Tau[1]*summ);
        // double Gmix = std::max(Gcrit01,Gcrit02);
        // double Gcrit01 = (Dom.Tau[0]-0.5)*((1.0)/((Dom.Tau[0]-0.5)*rhoInt));//*Dom.Rhoref[0]*Dom.Rhoref[0];
        // double Gcrit02 = (Dom.Tau[1]-0.5)*((1.0)/((Dom.Tau[1]-0.5)*rhoInt));//*Dom.Rhoref[0]*Dom.Rhoref[0];
        // double Gmix = std::max(Gcrit01,Gcrit02);
        // double Gmix = (TauEf-0.5)*((1.0)/((TauEf-0.5)*rhoInt));//*Dom.Rhoref[0]*Dom.Rhoref[0];
        double Gmix = 2.0/(rhoInt);
        
        // Gmix = Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]);
        Gmix = Gmix*Amplify/100.0;
        // Gmix /= Dom.e4;
        // Gmix /= Dom.e2;
        // double EOSINT = -Dom.e2*0.5*Gmix*(rhotmp0*rhotmp1);//
        double EOSINT = 0.5*Gmix*((rhoInt*0.5)*(rhoInt*0.5));//
        EOSINT = 0.0;
        double EOSINTtmp = 0.0;
        for (size_t i=0; i<nx; ++i){
            size_t j    =   ny/2;
            EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*Dom.Rho[0][i][j][0]*Dom.Rho[1][i][j][0], EOSINTtmp);
            // EOSINTtmp     =   std::max(0.5*Dom.e2*Gmix*Dom.PsiIntraInt(0,Dom.Rho[0][i][j][0])*Dom.PsiIntraInt(1,Dom.Rho[1][i][j][0]), EOSINTtmp);
        }
        EOSINT = EOSINTtmp;
        // double EOSINT = -Dom.e2*0.5*(Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0]))*(rhotmp0*rhotmp1);//
        
    /* Self Interaction */
        // double Gself0 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(0,Psi0),2.0)))*(Dom.e2*rhotmp0  - Dom.e2*(rhotmp0+rhotmp1)*1.0 + EOSINT); //
        // Gself0 *= C;
        // double Gself1 = ((2.0)/(Dom.e2*pow(Dom.PsiIntraInt(1,Psi1),2.0)))*(Dom.e2*rhotmp1  - 1.0*Dom.e2*(rhotmp0 - 0.5*Gself0*pow(Dom.PsiIntraInt(0,Psi0),2.0))); // - EOSINT); //
        
        double Gself0 = ((-2.0)/(Dom.e2*pow(Dom.PsiIntraInt(0,Psi0),2.0)))*(-Dom.Cs2*Psi0  + Dom.Cs2*(Psi0) + EOSINT);
        double Gself1 = ((-2.0)/(Dom.e2*pow(Dom.PsiIntraInt(1,Psi1),2.0)))*(-Dom.Cs2*Psi1  + Dom.Cs2*(Psi1) + EOSINT);
        Gself0 *= C;//*0.99;
        Gself1 *= C;//*1.01;
        // Gself0 /= Dom.e2;
        // Gself1 /= Dom.e2;

        Dom.Gs[0]   = 0.0, Dom.Gs[1]    = 0.0;
        // Gself0   = 0.0, Gself1    = 0.0;

        if (C==0.0) Gself1 = 0.0;
        double EOS0 =  (Dom.Cs2*Psi0 - 0.5*Dom.e2*Gself0*pow(Dom.PsiIntraInt(0,Psi0),2.0) - EOSINT);
        double EOS1 =  (Dom.Cs2*Psi1 - 0.5*Dom.e2*Gself1*pow(Dom.PsiIntraInt(1,Psi1),2.0) - EOSINT);
    
        std::cout << " G-mixing crit = " << Gmix << "; at " << Amplify << "% (G/p^2 = "<< (Gmix/(Dom.Rhoref[0]*Dom.Rhoref[0])) << ")" << std::endl;
        std::cout << " Gself 0          = " << Gself0 << std::endl;
        std::cout << " Gself 1          = " << Gself1 << std::endl;
        std::cout << " Equation of State " << std::endl;
        std::cout << "  -->  EoS Int          = " << EOSINT << std::endl;
        std::cout << "  -->  P0         = " << EOS0 << ",  (Po = " << Dom.Cs2*rhotmp0 << ")" << std::endl;
        std::cout << "  -->  P1         = " << EOS1 << ",  (Po = " << Dom.Cs2*rhotmp1 << ")" << std::endl;
    
    /* interaction strengths */
        Dom.Gmix     =  Gmix;
        Dom.G [0]    =  Gself0;
        Dom.G [1]    =  Gself1;
        
        Dom.Nproc = Nproc;
        Dom.FApplyForcesE();
        Dom.InitializeMixture();
        // std::cout << " !TEST " << std::endl;
        Dom.CalcPressureTensor = true;
        
    /*(Do not touch)*/
    /* Solve  Total time , Time step, Start, line: ,report| name| render| number of CPU */
    Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,floor(0.1*TimeResOut*Dom.dt),Setup,NULL,FileKey,true,Nproc);
    // Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,0*Dom.dt,NULL,NULL,FileKey,true,Nproc);
    
    return 0;
    
}
MECHSYS_CATCH
