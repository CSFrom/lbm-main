/* /////////////////////////////////////////////////////////////////////////////////
||================================================================================||
||                                                                                ||
|| Copyright (C) 2019 Chistropher Soriano From                                    ||
|| *Modified Version of:                                                          ||
||     * ---------------------------------------------------------------------- * ||
||     *  MechSys - Open Library for Mechanical Systems                         * ||
||     *  Copyright (C) 2016 Sergio Galindo                                     * ||
||     *                                                                        * ||
||     *  This program is free software: you can redistribute it and/or modify  * ||
||     *  it under the terms of the GNU General Public License as published by  * ||
||     *  the Free Software Foundation, either version 3 of the License, or     * ||
||     *  any later version.                                                    * ||
||     *                                                                        * ||
||     *  This program is distributed in the hope that it will be useful,       * ||
||     *  but WITHOUT ANY WARRANTY; without even the implied warranty of        * ||
||     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          * ||
||     *  GNU General Public License for more details.                          * ||
||     *                                                                        * ||
||     *  You should have received a copy of the GNU General Public License     * ||
||     *  along with this program. If not, see <http://www.gnu.org/licenses/>   * ||
||     * ---------------------------------------------------------------------- * ||
||       (see: http://mechsys.nongnu.org)                                         ||
||                                                                                ||
||================================================================================||
*/ /////////////////////////////////////////////////////////////////////////////////

#ifndef MECHSYS_FLBM_DOMAIN_H
#define MECHSYS_FLBM_DOMAIN_H

// Hdf5
#ifdef USE_HDF5
#include <hdf5.h>
#include <hdf5_hl.h>
#endif

#ifdef USE_OCL
#include <mechsys/oclaux/cl.hpp>
#endif

// Std lib
#ifdef USE_OMP
#include <omp.h>
#endif

//STD
#include<iostream>

// Mechsys
#include <mechsys/linalg/matvec.h>
#include <mechsys/util/util.h>
#include <mechsys/util/numstreams.h>
#include <mechsys/util/stopwatch.h>
#include <mechsys/util/numstreams.h>

enum LBMethod
{
    D2Q5,       ///< 2D 5 velocities
    D2Q9,       ///< 2D 9 velocities
    D2Q17,      ///< 2D 17 velocities -- based on set of abscissae on a cartesian grid.
    D2Q17ZOT,      ///< 2D 17 velocities -- based on set of abscissae on a cartesian grid. PhysRevE.81.036702 (2010)
    D2Q21,      ///< 2D 21 velocities -- based on set of abscissae on a cartesian grid.
    D2Q25,      ///< 2D 25 velocities -- based on set of abscissae on a cartesian grid. PhysRevE.73.056702 (2006)
    D2Q25ZOT,      ///< 2D 25 velocities -- based on set of abscissae on a cartesian grid. PhysRevE.79.046701 (2009)
    // D2Q25ZOTM,      ///< 2D 25 velocities -- "as above" but allow for cs2 to be modified.
    D2Q37,      ///< 2D 37 velocities -- based on set of abscissae on a cartesian grid. X. Shan and H. Chen, Int. J. Mod. Phys. C 18, 635 (2007) (Alt. PhysRevE.73.056702 (2006))
    D2Q49ZOTT,      ///< 2D 49 velocities -- based on set of abscissae on a cartesian grid. PhysRevE.79.046701 (2009)
    // D2Q16,      ///< 2D 16 velocities -- based on quadrature order
    D3Q15,      ///< 3D 15 velocities
    D3Q19,      ///< 3D 19 velocities
    //D3Q27     ///< 3D 27 velocities
};

enum FOrder
{
    E4,         ///< Fourth-Order Forcing, 1st band range interaction (similar to D2Q9 lattice structure)
    E8,         ///< Eighth-Order Forcing, 2-band long-range interaction
    E10,         ///< Eighth-Order Forcing, 2-band long-range interaction
    E12,         ///< Eighth-Order Forcing, 2-band long-range interaction
    ELS,        ///< Interaction Forcing, SAME range of interaction as Lattice Stucture.
    ELSISO,     ///< Enforce on lattice interaction range to a certain isotropy order.
};

enum RelaxType
{
    SRT, // Single Relaxation   
    MRT, // Multiple Relaxation
};

enum BoundaryLocation
{
    Right,  //  <--
    Top,    //  V
    Left,   //  -->
    Bottom, //  ^
};

namespace FLBM
{

#ifdef USE_OCL
typedef struct lbm_aux
{
    size_t     Nl;          ///< Number of Lattices
    size_t     Nneigh;      ///< Number of Neighbors
    size_t     FNneigh;     ///< Number of Neighbors in Forcing Term (E th order)
    size_t     NCPairs;     ///< Number of cell pairs
    size_t     Nx;          ///< Integer vector with the dimensions of the LBM domain
    size_t     Ny;          ///< Integer vector with the dimensions of the LBM domain
    size_t     Nz;          ///< Integer vector with the dimensions of the LBM domain
    size_t     Op[27];      ///< Array with the opposite directions for bounce back calculation
    cl_double3    C[27];       ///< Collection of discrete velocity vectors
    cl_double3    Cf[27];       ///< Collection of discrete forcing vectors
    double     EEk[27];     ///< Dyadic product of discrete velocities for LES calculation
    double     W[27];       ///< Collection of discrete weights
    double     Wf[27];       ///< Collection of discrete forcing weights
    double     Tau[2];      ///< Collection of characteristic collision times
    double     G[2];        ///< Collection of cohesive constants for multiphase simulation
    double     Gs[2];       ///< Collection of cohesive constants for multiphase simulation
    double     Rhoref[2];   ///< Collection of cohesive constants for multiphase simulation
    double     Psi[2];      ///< Collection of cohesive constants for multiphase simulation
    double     Gmix;        ///< Repulsion constant for multicomponent simulation
    double     Cs;          ///< Lattice speed
    double     Cs2;          ///< Lattice speed
    double     Sc;          ///< Smagorinsky constant
    
} d_lbm_aux;
#endif

inline size_t Pt2idx(iVec3_t iv, iVec3_t & Dim) // Calculates the index of the cell at coordinates iv for a cubic lattice of dimensions Dim
{
    return iv(0) + iv(1)*Dim(0) + iv(2)*Dim(0)*Dim(1);
}

inline void   idx2Pt(size_t n, iVec3_t & iv, iVec3_t & Dim) // Calculates the coordinates from the index
{
    iv(0) = n%Dim(0);
    iv(1) = (n/Dim(0))%(Dim(1));
    iv(2) = n/(Dim(0)*Dim(1));
}

class Domain
{
public:
	static const double   WEIGHTSD2Q5   [ 5]; ///< Weights for the equilibrium distribution functions (D2Q5)
    static const double   WEIGHTSD2Q9   [ 9]; ///< Weights for the equilibrium distribution functions (D2Q9)
    static const double   WEIGHTSD2Q17  [17]; ///< Weights for the equilibrium distribution functions (D2Q17)
    static const double   WEIGHTSD2Q17ZOT  [17]; ///< Weights for the equilibrium distribution functions (D2Q17 ZOT)
    static const double   WEIGHTSD2Q21  [21]; ///< Weights for the equilibrium distribution functions (D2Q21)
    static const double   WEIGHTSD2Q25  [25]; ///< Weights for the equilibrium distribution functions (D2Q25)
    static const double   WEIGHTSD2Q25ZOT  [25]; ///< Weights for the equilibrium distribution functions (D2Q25 ZOT)
    static const double   WEIGHTSD2Q37  [37]; ///< Weights for the equilibrium distribution functions (D2Q37)
    // static const double   WEIGHTSD2Q16  [16]; ///< Weights for the equilibrium distribution functions (D2Q16)
	static const double   WEIGHTSD3Q15  [15]; ///< Weights for the equilibrium distribution functions (D3Q15)
	static const double   WEIGHTSD3Q19  [19]; ///< Weights for the equilibrium distribution functions (D3Q19)
	//static const double   WEIGHTSD3Q27  [27]; ///< Weights for the equilibrium distribution functions (D3Q27)
	static const Vec3_t   LVELOCD2Q5    [ 5]; ///< Local velocities (D2Q5) 
    static const Vec3_t   LVELOCD2Q9    [ 9]; ///< Local velocities (D2Q9) 
    static const Vec3_t   LVELOCD2Q17   [17]; ///< Local velocities (D2Q17)
    static const Vec3_t   LVELOCD2Q17ZOT   [17]; ///< Local velocities (D2Q17 ZOT)
    static const Vec3_t   LVELOCD2Q21   [21]; ///< Local velocities (D2Q21)
    static const Vec3_t   LVELOCD2Q25   [25]; ///< Local velocities (D2Q25)
    static const Vec3_t   LVELOCD2Q25ZOT   [25]; ///< Local velocities (D2Q25 ZOT)
    static const Vec3_t   LVELOCD2Q37   [37]; ///< Local velocities (D2Q37)
    static const Vec3_t   LVELOCD2Q49ZOTT   [49]; ///< Local velocities (D2Q49 ZOTT)
    // static const Vec3_t   LVELOCD2Q16   [16]; ///< Local velocities (D2Q16)
	static const Vec3_t   LVELOCD3Q15   [15]; ///< Local velocities (D3Q15)
	static const Vec3_t   LVELOCD3Q19   [19]; ///< Local velocities (D3Q19)
	//static const Vec3_t   LVELOCD3Q27   [27]; ///< Local velocities (D3Q27)
	static const size_t   OPPOSITED2Q5  [ 5]; ///< Opposite directions (D2Q5) 
    static const size_t   OPPOSITED2Q9  [ 9]; ///< Opposite directions (D2Q9) 
    static const size_t   OPPOSITED2Q17 [17]; ///< Opposite directions (D2Q17)
    static const size_t   OPPOSITED2Q17ZOT [17]; ///< Opposite directions (D2Q17 ZOT)
    static const size_t   OPPOSITED2Q21 [21]; ///< Opposite directions (D2Q21)
    static const size_t   OPPOSITED2Q25 [25]; ///< Opposite directions (D2Q25)
    static const size_t   OPPOSITED2Q25ZOT [25]; ///< Opposite directions (D2Q25 ZOT)
    static const size_t   OPPOSITED2Q37 [37]; ///< Opposite directions (D2Q37)
    static const size_t   OPPOSITED2Q49ZOTT [49]; ///< Opposite directions (D2Q49 ZOTT)
    // static const size_t   OPPOSITED2Q16 [16]; ///< Opposite directions (D2Q16)
	static const size_t   OPPOSITED3Q15 [15]; ///< Opposite directions (D3Q15)
	static const size_t   OPPOSITED3Q19 [19]; ///< Opposite directions (D3Q19)
	//static const size_t   OPPOSITED3Q27 [27]; ///< Opposite directions (D3Q27)
    static const double   MD2Q5       [5][5]; ///< MRT transformation matrix (D2Q5)
    static const double   MD2Q9       [9][9]; ///< MRT transformation matrix (D2Q9)
    static const double   MD2Q17    [17][17]; ///< MRT transformation matrix (D2Q17)
    static const double   MD3Q15    [15][15]; ///< MRT transformation matrix (D3Q15)
    static const double   MD3Q19    [19][19]; ///< MRT transformation matrix (D3Q19)
    //static const size_t   MD3Q27    [27][27]; ///< MRT transformation matrix (D3Q27)
    //static const double   SD2Q5          [5]; ///< MRT relaxation time vector (D2Q5)
    //static const double   SD2Q9          [9]; ///< MRT relaxation time vector (D2Q9)
    //static const double   SD3Q15        [15]; ///< MRT relaxation time vector (D3Q15)
    //static const double   SD3Q19        [19]; ///< MRT relaxation time vector (D3Q19)
    //static       double   SD3Q19        [27]; ///< MRT relaxation time vector (D3Q27)
    static const double   FWEIGHTSE4   [ 8]; ///< Weights for the Interaction Force isotropy Order E4.
    static const double   FWEIGHTSE8   [24]; ///< Weights for the Interaction Force isotropy Order E8.
    static const double   FWEIGHTSE10  [36]; ///< Weights for the Interaction Force isotropy Order E10.
    static const double   FWEIGHTSE12  [48]; ///< Weights for the Interaction Force isotropy Order E12.
    static const Vec3_t   LVELOCDFE4   [ 8]; ///< The isotropy direction for the 4th-isotropy Order (E4) Interaction forcing Term.
    static const Vec3_t   LVELOCDFE8   [24]; ///< The isotropy direction for the 8th-isotropy Order (E8) Interaction forcing Term.
    static const Vec3_t   LVELOCDFE10  [36]; ///< The isotropy direction for the 10th-isotropy Order (E10) Interaction forcing Term.
    static const Vec3_t   LVELOCDFE12  [48]; ///< The isotropy direction for the 12th-isotropy Order (E12) Interaction forcing Term.
    // static const size_t   OPPOSITEFE8  [24]; ///< The opposite directions Force (E8).
    //typedefs
    typedef void (*ptDFun_t) (Domain & Dom, void * UserData);

    //Constructors
    Domain (LBMethod      Method, ///< Type of array, for example D2Q9
    FOrder                TheOrder, ///< Type of array, for example D2Q9
    RelaxType             RelaxationType,         ///< The type of Relaxation, i.e., SRT, or MRT.
    Array<double>         nu,     ///< Viscosity for each fluid
    iVec3_t               Ndim,   ///< Cell divisions per side
    double                dx,     ///< Space spacing
    double                dt);    ///< Time step

    //Special constructor with only one component, the parameters are the same as above
    Domain (LBMethod      Method, ///< Type of array, for example D2Q9
    FOrder                TheOrder, ///< Type of array, for example D2Q9        
    double                nu,     ///< Viscosity for each fluid
    iVec3_t               Ndim,   ///< Cell divisions per side
    double                dx,     ///< Space spacing
    double                dt);    ///< Time step

    //If using specific Nmol and molMass values
    Domain (LBMethod      Method,               ///< Type of array, for example D2Q9
    FOrder                TheOrder,             ///< Type of array, for example D2Q9
    RelaxType             RelaxationType,         ///< The type of Relaxation, i.e., SRT, or MRT.
    Array<double>         MolarMass,            ///< Viscosity for each fluid
    Array<double>         nu,                   ///< Viscosity for each fluid
    iVec3_t               Ndim,                 ///< Cell divisions per side
    double                dx,                   ///< Space spacing
    double                dt);                  ///< Time step

    //Methods
    void   SolidPhase(Vec3_t const & XC, double RC);                                ///< Same function as SolidDisk producing a solid sphere
    void   ApplyForcesSC();                                                         ///< Apply the molecular forces for the single component case
    void   ApplyForcesSCSS();                                                       ///< Apply the molecular forces for the single component case with solid surfaces
    void   ApplyForcesMP();                                                         ///< Apply the molecular forces for the multiphase case
    void   ApplyForcesSCMP();                                                       ///< Apply the molecular forces for the both previous cases
    void   FApplyForcesSCMP();                                                      ///< Quick version of ApplyforceSCMP (does not consider boundaries)
    void   ApplyForcesTEST();                                                       ///< TEST forcing function
    void   ApplyForces3MP();                                                        ///< Apply forces between three different components
    void   ApplyForcesE();                                                          ///< Apply the molecular forces using Higher-order interaction forcing model.
    void   FApplyForcesE() ;                                                        ///< Faster version of the above as it neglect IsSolids.
    void   FFApplyForcesE_SC() ;                                                        ///< Faster version of the above as it neglect IsSolids - Single Component.
    void   FFApplyForcesE() ;                                                        ///< Faster version of the above as it neglect IsSolids.
    void   FApplyForcesMPI();                                                       ///< Multi-pseudoPotential-Interaction (MPI) Higher-order forcing model.
    void   ApplyForces3E();                                                         ///< Apply the molecular forces using Higher-order interaction forcing model.
    void   FApplyForces3E() ;                                                        ///< Faster version of the above as it neglect IsSolids.
    void   FluidSolidInteraction();
    void   CollideMRT();                                                            ///< The collide step of LBM with MRT
    void   CollideSC();                                                             ///< The collide step of LBM for single component simulations
    void   CollideDBC();                                                            ///< Collide function with Diffuse Scattering without Force contribution.
    void   CollideSC_DBC();                                                         ///< The collide step of LBM for single component simulations with Diffused Boundary Condition.
    void   CollideSC_EFDBC();
    void   CollideEF_MP_DBC();                                                      ///< Diffused Boundary condition for Multicomponent, Explicit forcing.
    void   CollideEDM();
    void   CollideEF_MP_Test();
    void   CollideEF_MP_Test_B();
    void   CollideEF_SC();                                                          ///< The collide step of LBM for enhanced forcing scheme
    void   CollideMP();                                                             ///< BACKUP for: The collide step of LBM for multi phase simulations
    void   CollideEF();                                                             ///< Chris EDIT  for the Collide step for LBM Multi-component.
    void   CollideEF_MRT();                                                         ///< Chris EDIT  for the Collide step for LBM Multi-component.
    void   CollideEF_MRT_Test();
    void   StreamSC();                                                              ///< The stream step of LBM SC
    void   StreamEF_SC();                                                           ///< The strean step of LBM EFS
    void   StreamMP();                                                              ///< The stream step of LBM MP
    void   StreamMP_EF();                                                           ///< The stream step of LBM MP EF  
    void   Initialize(size_t k, iVec3_t idx, double Rho, Vec3_t & Vel);             ///< Initialize each cell with a given density and velocity
    void   InitializeEF(size_t k, iVec3_t idx, double Rho, Vec3_t & Vel);           ///< Initialize each cell with a given density and MIXture velocity
    void   AssignFluidParameters(size_t k, iVec3_t idx, double Rho, Vec3_t & Vel);  ///< Assign values to each cell - does not initialize distribution ! *Usage: Multi-Component systems
    void   InitializeMixture();                                                     ///< InitializeEF --> initialize mixture equilibrium distribution
    void   StorePrevious();                                                         ///< Store previous data (Usage: CBC boundary condition)
    void   DefBoundaryDist();                                                       ///< Define boundary distribution based on Lattice Structure
    void   DefNormVec();                                                            ///< Define unit vector normal to solid-boundary based on Lattice Structure
    void   ConstructInteractionArray();                                             ///< Interaction Array Contructor.
    void   VelBB(size_t k, BoundaryLocation Location, iVec3_t idx, iVec3_t ldx, Vec3_t & Vel);           ///< BB at boundary
    void   PressBB(size_t k, BoundaryLocation Location, iVec3_t idx, iVec3_t ldx, Array<double> Rho);
    double PsiEq(size_t k, double Rho, double Rho_reference, double Psi_reference); ///< Calculate interaction Pseudo-Potential.
    double PsiIntraInt(size_t k, double Rho);                                       ///< Calculate (Self) Intra-interaction Pseudo-Potential.
    double PsiInterInt(size_t k, double Rho);                                       ///< Calculate (Cross) Intra-interaction Pseudo-Potential.
    double PsiType(size_t k, double Rho, int Type);
    double dPsiType(size_t k, double Rho, int Type);
    void   CalculatePotentials();                                                    ///< Calculate all pseudo-potentials and store in memory
    void   CalculatePotentials_SC();                                                    ///< Calculate all pseudo-potentials and store in memory
    void   PressureTensor();
    void   PressureTensor_SingleComp();
    void   PressureTensorCalc();
    void   MomentumFluxTensor();
    double Feq(size_t k, double Rho, Vec3_t & Vel);                               ///< The equilibrium function
    double Geq(size_t k, Vec3_t & Vel);                                   ///< geq = feq/rho,  Equilibrium Distribution Equation for computing unknowns at boundary.
    double ForceFeq(size_t k, double Rho, double * NonEq, Vec3_t & V, Vec3_t & A,Vec3_t * & Pij, size_t maxDim);//
    void Solve(double Tf, double dtOut, double whenOut, ptDFun_t ptSetup=NULL, ptDFun_t ptReport=NULL,
    char const * FileKey=NULL, bool RenderVideo=true, size_t Nproc=1);            ///< Solve the Domain dynamics
    
    //Writing Methods
    void WriteXDMF          (char const * FileKey);                                ///< Write the domain data in xdmf file
    void WritePARAMETERS    (char const * FileKey);                                ///< Write the domain Parameters
    //Methods for OpenCL
    #ifdef USE_OCL
    void UpLoadDevice ();                                                         ///< Upload the buffers into the coprocessor device
    void DnLoadDevice ();                                                         ///< Download the buffers into the coprocessor device
    void ApplyForceCL ();                                                         ///< Apply forces in the coprocessor
    void CollideCL    ();                                                         ///< Apply collision operator in the coprocessor
    void StreamCL     ();                                                         ///< Apply streaming operator in the coprocessor
    #endif
    
    double Exp10T(double X);
    // double PowF(double X, double Y); // May need to review in the future for faster implementation.
    
    #ifdef USE_OMP
    omp_lock_t      lck;                      ///< to protect variables in multithreading
    #endif

    //Data
    double ***** F;                           ///< The array containing the individual functions with the order of the lattice, the x,y,z coordinates and the order of the function.
    double ***** Ftemp;                       ///< A similar array to hold provitional data
    double ***** Fprev;                       ///< Distribution array to hold previous Distribution (Usage: CBC boundary condition)
    bool   ***** IsFluid;                       ///< Distribution k that will advect towards fluid nodes.
    bool   ****  IsSolid;                     ///< An array of bools with an identifier to see if the cell is a solid cell
    bool   ****  IsHBSolid;                     ///< An array of bools with an identifier to see if the cell is a solid cell
    Vec3_t ****  Vel;                         ///< The fluid velocities
    Vec3_t ****  nvec;                         ///< Unit vector normal to wall, in the directing towards the fluid nodes
    Vec3_t ****  Velprev;                         ///< The fluid velocities
    Vec3_t ****  BForce;                      ///< Body Force for each cell
    Vec3_t ***** Pij;                         ///< The Pressure Tensor (Rank 2, ij)
    Vec3_t ***** Pkin;
    double ****  Rho;                         ///< The fluid densities
    double ****  RhoW;                         ///< The fluid densities
    double ****  PsiS;                         ///< The fluid Self pseudo-potential
    double ****  PsiC;                         ///< The fluid Cross pseudo-potential
    double ****  Nmol;                         ///< The fluid number density
    double ****  Nmolprev;                     ///< The previous fluid number density (Usage: CBC boundary condition)
    double *     molMass;                      ///< The fluid and/or mixture molecular-mass
    double *     Tau;                         ///< The characteristic time of the lattice
    double *     G;                           ///< The attractive constant for multiphase simulations
    double *     Gs;                          ///< The attractive constant for solid phase
    double *     Psi;                         ///< Parameters for the Shan Chen pseudo potential //! <-- Maybe delete*
    double *     isoPsi;                       ///< Isotropy dependent Psi
    double      e2;                          ///< Second order Isotropy Tensor for psi
    double      e4;                          ///< Fourth order Isotropy Tensor for psi
    double      epsilon;                          ///< Fourth order Isotropy Tensor for psi
    double      eAlpha;                          ///< Fourth order Isotropy Tensor for psi
    double      eBeta;                          ///< Fourth order Isotropy Tensor for psi
    double *     Rhoref;                      ///< Parameters for the Shan Chen pseudo potential
    double       Gmix;                        ///< The mixing constant between two component
    double *     Gmmix;                        ///< The mixing constant between multiple components (>2)
    double       Gint;                        ///< The specific interaction strength between two component simulations, in a multicomponent (2>) system
    size_t const * Op;                        ///< An array containing the indexes of the opposite direction for bounce back conditions
    double const *  W;                        ///< An array with the direction weights
    double const *  Wf;                        ///< An array with the direction weights for force terms
    double *  Wff;                        ///< An array with the direction weights for force terms
    double *  Wtmp;                        ///< An array with the direction weights for force terms
    double *  Wzot;                        ///< An array with weights for zero-one-three lattice structures. 
    Vec3_t const *  Cf;                        ///< An array of the force terms interactions on the lattice.
    size_t const * Opf;                        ///< An array containing the indexes of the opposite direction for bounce back conditions
    double *     EEk;                         ///< Diadic product of the velocity vectors
    Vec3_t const *  C;                        ///< The array of lattice velocities
    Mat_t        M;                           ///< Transformation matrix to momentum space for MRT calculations
    Mat_t        Minv;                        ///< Inverse Transformation matrix to momentum space for MRT calculations
    // Mat_t        SMat;                           ///< Transformation matrix to momentum space for MRT calculations
    double ***   MAlpha;                           ///< Transformation matrix to momentum space for MRT calculations
    Vec_t        S;                           ///< Vector of relaxation times for MRT
    double **    SVec;                        ///< For each component Vector of relaxation times for MRT
    size_t       Nneigh;                      ///< Number of Neighbors, depends on the scheme
    size_t       FNneigh;                     ///< Number of Neighbors in Forcing Term (E th order)
    double       dt;                          ///< Time Step
    double       dx;                          ///< Grid size
    double       dtR;                          ///< Time Step [Real unit]
    double       dxR;                          ///< Grid size [Real unit]
    double       Cs;                          ///< Lattice Velocity
    double       Cs2 = 0.0;                          ///< Lattice Sound Speed, e.g., Cs= sqrt[(dx/dt)/sqrt(3)] for D2Q9
    double *      epsiC;                          ///< Isotropy potential const.
    bool         IsFirstTime;                 ///< Bool variable checking if it is the first time function Setup is called
    iVec3_t      Ndim;                        ///< Lattice Dimensions
    size_t       Ncells;                      ///< Number of cells
    size_t       Nproc;                       ///< Number of processors for openmp
    size_t       idx_out;                     ///< The discrete time step for output
    String       FileKey;                     ///< File Key for output files
    String       LBmodel;                     ///< Initialize identifier
    String       ForceModel;                     ///< Initialize identifier
    void *       UserData;                    ///< User Data
    size_t       Step;                        ///< Lenght of averaging cube to save data
    double       Time;                        ///< Simulation time variable
    size_t       Nl;                          ///< Number of lattices (fluids)
    double       Sc;                          ///< Smagorinsky constant
    Vec3_t       tmpSum;                          ///< Sum of variable (e.g., Forces or Pressure tensors)

    size_t Udist;
    size_t Udiagonal;
    size_t kAxial;
    size_t latticeLength;
    size_t ** CBB;
    size_t ** CBBd;
    size_t ** CKA;

    size_t * OppBB;

    int FeqOrder = 0;
    int PsiIntraType = 0; // * Self Interaction Pseudo-Potential
    int PsiInterType = 0; // * Cross Interaction Pseudo-Potential

    // * Pressure Tensor
    double * symm_groups;
    double * symm_emax;
    double ****  Pxx;
    double ****  Pyy;
    double ****  Pxy;                         ///< Various Components of the Pressure Tensor
    bool         CalcPressureTensor;                 ///< Bool variable for calculating Pressure Tensor in WritePARAMETERS
    Vec3_t * PTconsts; ///< for analytical Pressure tensor equation.

    #define hotcode __attribute__ ((hot))

    //Array for pair calculation
    size_t       NCellPairs;                  ///< Number of cell pairs
    iVec3_t   *  CellPairs;                   ///< Pairs of cells for molecular force calculation

    #ifdef USE_OCL
    cl::CommandQueue CL_Queue;                ///< CL Queue for coprocessor commands 
    cl::Context      CL_Context;              ///< CL Context for coprocessor commands
    cl::Program      CL_Program;              ///< CL program object containing the kernel routines
    cl::Device       CL_Device;               ///< Identity of the accelrating device
    cl::Buffer     * bF;                      ///< Buffer with the distribution functions
    cl::Buffer     * bFtemp;                  ///< Buffer with the distribution functions temporal
    cl::Buffer     * bIsSolid;                ///< Buffer with the solid bool information
    cl::Buffer     * bIsHBSolid;              ///< Buffer with the solid bool information
    cl::Buffer     * bBForce;                 ///< Buffer with the body forces
    cl::Buffer     * bVel;                    ///< Buffer with the cell velocities
    cl::Buffer     * bRho;                    ///< Buffer with the cell densities
    cl::Buffer     * bCellPair;               ///< Buffer with the pair cell information for force calculation
    cl::Buffer       blbmaux;                 ///< Buffer with the strcuture containing generic lbm information
    size_t           N_Groups;                ///< Number of work gropous that the GPU can allocate
    #endif
};

inline Domain::Domain(LBMethod TheMethod, FOrder ForceOrder,  RelaxType TheRelaxType, Array<double> nu, iVec3_t TheNdim, double Thedx, double Thedt)
{
    Util::Stopwatch stopwatch;
    printf("\n%s--- Initializing LBM Domain --------------------------------------------%s\n",TERM_CLR1,TERM_RST);
    if (nu.Size()==0) throw new Fatal("LBM::Domain: Declare at leat one fluid please");
    if (TheNdim(2) >1&&(TheMethod==D2Q17 || TheMethod==D2Q9 ||TheMethod==D2Q5 ))  throw new Fatal("LBM::Domain: D2Q9 scheme does not allow for a third dimension, please set Ndim(2)=1 or change to D3Q15");
    if (TheNdim(2)==1&&(TheMethod==D3Q15||TheMethod==D3Q19))  throw new Fatal("LBM::Domain: Ndim(2) is 1. Either change the method to D2Q9 or increase the z-dimension");

    Time        = 0.0;
    dt          = Thedt;
    dx          = Thedx;
    dtR          = Thedt; // Real [seconds]
    dxR          = Thedx; // Real [meters]
    // dt          = dtR;
    // dx          = dxR;
    dt          = 1.0;
    dx          = 1.0;
    Cs          = dx/dt;
    // Cs          = 1.0;
    Step        = 1;
    Sc          = 0.17;
    tmpSum      = OrthoSys::O;
    Nl          = nu.Size();
    Ndim        = TheNdim;
    Ncells      = Ndim(0)*Ndim(1)*Ndim(2);
    IsFirstTime = true;
    CalcPressureTensor = false;
    PTconsts    = new Vec3_t  [4]; // c2, A, and B.
    PTconsts[0] = OrthoSys::O;
    PTconsts[1] = OrthoSys::O;
    PTconsts[2] = OrthoSys::O;
    PTconsts[3] = OrthoSys::O;
    // Wzot = new double [3]; // zero-one-three weights


    if (TheMethod==D2Q5)
    {
        Nneigh = 5;
        W      = WEIGHTSD2Q5;
        C      = LVELOCD2Q5;
        Op     = OPPOSITED2Q5;
    }
    if (TheMethod==D2Q9)
    {
        Nneigh = 9;
        W      = WEIGHTSD2Q9;
        C      = LVELOCD2Q9;
        Op     = OPPOSITED2Q9;
        Cs2         = (Cs/(sqrt(3)))*(Cs/(sqrt(3)));
        LBmodel = "D2Q9";
        FeqOrder = 2;
    }
    if (TheMethod==D2Q17)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17;
        C      = LVELOCD2Q17;
        Op     = OPPOSITED2Q17;
        Cs2         = (Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
        LBmodel = "D2Q17";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q17ZOT)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17ZOT;
        C      = LVELOCD2Q17ZOT;
        Op     = OPPOSITED2Q17ZOT;
        // Cs2         = Cs*Cs*(1.0/((5.0+sqrt(10.0))/3.0)); // should be the same as below
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q17ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q21)
    {
        Nneigh = 21;
        W      = WEIGHTSD2Q21;
        C      = LVELOCD2Q21;
        Op     = OPPOSITED2Q21;
        Cs2         = (Cs/(sqrt(3.0/2.0)))*(Cs/(sqrt(3.0/2.0)));
        LBmodel = "D2Q21";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25)
    {
        Nneigh = 25;
        W      = WEIGHTSD2Q25;
        C      = LVELOCD2Q25;
        Op     = OPPOSITED2Q25;
        Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
        LBmodel = "D2Q25";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25ZOT)
    {
        Nneigh = 25;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q25ZOT;
        C      = LVELOCD2Q25ZOT;
        Op     = OPPOSITED2Q25ZOT;
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q25 ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q37)
    {
        Nneigh = 37;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q37;
        C      = LVELOCD2Q37;
        Op     = OPPOSITED2Q37;
        // Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0))); // Ref. Philippi, where a = 0.846393
        Cs2      = pow((1.0/1.19697977039307435897239),2.0); // Ref. Shan, r = that long number
        Cs2     *= Cs*Cs;
        LBmodel = "D2Q37";
        FeqOrder = 4;
    }
    if (TheMethod==D2Q49ZOTT) //* Zero - One - Two - Three (ZOTT) Ref: S. S. Chikatamarla Phys Rev E 79, 046701 (2009)
    {
        Nneigh = 49;
        double tmpcsA = 0, tmpcsB = 0;
        tmpcsA = (7.0/(5.0*(3.0*sqrt(30.0) - 5.0)));
        tmpcsB = 1.0/(7.0/((3.0*sqrt(30.0) - 5.0)));

        Cs2     = (2.0/3.0) + (1.0/3.0)*pow(tmpcsA,(1.0/3.0)) - ((pow(tmpcsB, (1.0/3.0))) / (pow(35.0, (2.0/3.0))));
        Cs2     *= Cs*Cs; // Should equal  == 0.697 953 322 019 683 1, According to PhysRevE.79.046701 (2009)
        // Cs2 = 0.6979533220196831;
        // Cs2 = Cs*Cs*(1.0-sqrt(2.0/5.0)); //* Change ref. temp to match ZOT lattices: 17ZOT and 25ZOT
        Wzot = new double [4];
        Wzot[0] = (1.0/36.0)*(Cs2*(3.0*(14.0 - 5.0*Cs2)*Cs2 - 49.0) + 36.0);
        Wzot[1] = (1.0/16.0)*(Cs2*(Cs2*(5.0*Cs2 - 13.0) + 12.0));
        Wzot[2] = (1.0/40.0)*(Cs2*(5.0*(2.0 - Cs2)*Cs2 - 3.0));
        Wzot[3] = (1.0/720.0)*(Cs2*(15.0*(Cs2 - 1.0)*Cs2 + 4.0));
        C      = LVELOCD2Q49ZOTT;
        Op     = OPPOSITED2Q49ZOTT;
        Wtmp = new double [Nneigh];
        for (size_t n=0;n<Nneigh;n++) {
            size_t xx = (size_t)( (int)fabs(C[n](0)) );
            size_t yy = (size_t)( (int)fabs(C[n](1)) );
            Wtmp[n] = Wzot[xx]*Wzot[yy];
            // std::cout << " W ["<< n <<"] = " << Wtmp[n] << std::endl;
            // std::cout << " x = "<< xx <<", y = " << yy << std::endl;
            // std::cout << " Wx = "<< Wzot[xx] <<", Wy = " << Wzot[yy] << "\n"<< std::endl;
        }
        W = (const double *) Wtmp;
        LBmodel = "D2Q49 ZOTT";
        FeqOrder = 4;
    }

    if (TheMethod==D3Q15)
    {
        Nneigh = 15;
        W      = WEIGHTSD3Q15;
        C      = LVELOCD3Q15;
        Op     = OPPOSITED3Q15;
        M.Resize(Nneigh,Nneigh);
        Minv.Resize(Nneigh,Nneigh);
        std::cout << "miu" << std::endl;
        for (size_t n=0;n<Nneigh;n++)
        for (size_t m=0;m<Nneigh;m++)
        {
            M(n,m) = MD3Q15[n][m];
        }
        Inv(M,Minv);
    }
    if (TheMethod==D3Q19)
    {
        Nneigh = 19;
        W      = WEIGHTSD3Q19;
        C      = LVELOCD3Q19;
        Op     = OPPOSITED3Q19;
    }


    if (ForceOrder==E4)
    {
        //E4
        FNneigh = 8;
        Cf     = LVELOCDFE4;
        Wf     = FWEIGHTSE4;
        ForceModel = "Isotropy Order = 4";
        epsilon = 0.0;
    }
    if (ForceOrder==E8)
    {
        //E8
        FNneigh = 24;
        Cf     = LVELOCDFE8;
        Wf     = FWEIGHTSE8;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] ; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] ; // fourth order Isotropy Tensor for psi
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 8";
        printf("%s  >>> Isotropy Order E8, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s      ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s      ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==E10)
    {
        //E10
        FNneigh = 36;
        Cf     = LVELOCDFE10;
        Wf     = FWEIGHTSE10;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] + 18.0*Wf[24] +  40.0*Wf[28]; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] + 40.5*Wf[24] + 100.0*Wf[28]; // fourth order Isotropy Tensor for psi
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 10";
        printf("%s  >>> Isotropy Order E10, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
        // /* Display Output in order to double-check */
        // for (size_t k=0;k<FNneigh;k++)
        // {
        //     std::cout << " Wf[" << k << "] = " << Wf[k] << ",  \n";
        // }
        // std::cout << std::endl;
    }

    if (ForceOrder==E12)
    {
        //E12
        FNneigh = 48;
        Cf     = LVELOCDFE12;
        Wf     = FWEIGHTSE12;
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 12";
        printf("%s  >>> Isotropy Order E12, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==ELS)
    {
        //Forcing interaction-range to equal the same as lattice structure.
        FNneigh = Nneigh;
        Wff = new double [FNneigh];  
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        // symm_groups[0] = 0.0, symm_emax[0] = 0.0, Wff[0] = 0.0;
        Cf     = C;
        // for (size_t n=1;n<FNneigh;n++){
        for (size_t n=0;n<FNneigh;n++){
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
            Wff[n]     = W[n]*(1.0/(Cs2));  // sum W*C[k]^2 = 1.0
            // Wff[n]     = W[n];              // sum W*C[k]^2 = cs2
            // std::cout << " W ("<< n <<") = " << W[n] << " || Wf("<< n << ") = " << Wff[n] << "\n" << std::endl;
        }
        Wf = (const double *) Wff; // std::cout << " Wf " << Wf[1] << std::endl;
        
        ForceModel = "equal to lattice structure";
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            eAlpha = (e2 + 6.0*e4)/12.0; // True for any lattice, and can also consider the case e2!=1
            eBeta  = (3.0*e4 - e2)/12.0;
            // double eEta = (1.0 + 3.0*e4)/20.0; // Only works for E8, it changes between lattices
            // double eGamma  = (-1.0 + 2.0*e4)/20.0;
            
            epsilon = 2.0*eBeta/eAlpha;
            // ForceModel = "Isotropy Order 4 enforce on Lattice Structure";
            printf("%s  >>>                    cs2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
            printf("%s  >>>                epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
            printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
            printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
            printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
            printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    

    Tau    = new double [Nl];
    epsiC  = new double [Nl];
    G   = new double [Nl];
    molMass   = new double [Nl];
    Gs  = new double [Nl];
    Rhoref = new double [Nl];
    Gmmix = new double [Nl];
    Psi    = new double [Nl];
    Gmix= 0.0;
    Gint= 0.0;

    F       = new double **** [Nl];
    Ftemp   = new double **** [Nl];
    IsFluid = new bool   **** [Nl];
    Vel     = new Vec3_t ***  [Nl];
    Velprev = new Vec3_t ***  [Nl];
    nvec    = new Vec3_t ***  [Nl];
    Fprev   = new double **** [Nl];
    BForce  = new Vec3_t ***  [Nl];
    Pij     = new Vec3_t **** [Nl];
    Pkin    = new Vec3_t **** [Nl];
    Pxx     = new double *** [Nl];
    Pyy     = new double *** [Nl];
    Pxy     = new double *** [Nl];
    Rho     = new double *** [Nl];
    RhoW     = new double *** [Nl];
    PsiS     = new double *** [Nl];
    PsiC     = new double *** [Nl];
    Nmol    = new double *** [Nl];
    Nmolprev = new double ***  [Nl];
    
    IsSolid = new bool   ***  [Nl];
    IsHBSolid = new bool   ***  [Nl];

    for (size_t i=0;i<Nl;i++)
    {
        molMass [i]    = 1.0;
        Tau     [i]    = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
        epsiC   [i]    = 1.0/epsilon; //! Maybe delete this parameter.
        G       [i]    = 0.0;
        Gs      [i]    = 0.0;
        Rhoref  [i]    = 200.0;
        Gmmix   [i]    = 0.0;
        // Rhoref  [i]    = 1.0;
        Psi     [i]    = 4.0;
        // Psi     [i]    = 1.0;
        F       [i]    = new double *** [Ndim(0)];
        Ftemp   [i]    = new double *** [Ndim(0)];
        Fprev   [i]    = new double *** [Ndim(0)];
        IsFluid [i]    = new bool   *** [Ndim(0)];
        Vel     [i]    = new Vec3_t **  [Ndim(0)];
        Velprev [i]    = new Vec3_t **  [Ndim(0)];
        nvec    [i]    = new Vec3_t **  [Ndim(0)];
        BForce  [i]    = new Vec3_t **  [Ndim(0)];
        Pij     [i]    = new Vec3_t *** [Ndim(0)];
        Pkin    [i]    = new Vec3_t *** [Ndim(0)];
        Pxx     [i]    = new double **  [Ndim(0)];
        Pyy     [i]    = new double **  [Ndim(0)];
        Pxy     [i]    = new double **  [Ndim(0)];
        Rho     [i]    = new double **  [Ndim(0)];
        RhoW    [i]    = new double **  [Ndim(0)];
        PsiS    [i]    = new double **  [Ndim(0)];
        PsiC    [i]    = new double **  [Ndim(0)];
        Nmol    [i]    = new double **  [Ndim(0)];
        Nmolprev [i]    = new double **  [Ndim(0)];
        IsSolid [i]    = new bool   **  [Ndim(0)];
        IsHBSolid [i]    = new bool   **  [Ndim(0)];
        for (size_t nx=0;nx<Ndim(0);nx++)
        {
            F       [i][nx]    = new double ** [Ndim(1)];
            Ftemp   [i][nx]    = new double ** [Ndim(1)];
            Fprev   [i][nx]    = new double ** [Ndim(1)];
            IsFluid [i][nx]    = new bool   ** [Ndim(1)];
            Vel     [i][nx]    = new Vec3_t *  [Ndim(1)];
            Velprev [i][nx]    = new Vec3_t *  [Ndim(1)];
            nvec    [i][nx]    = new Vec3_t *  [Ndim(1)];
            BForce  [i][nx]    = new Vec3_t *  [Ndim(1)];
            Pij     [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pkin    [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pxx     [i][nx]    = new double *  [Ndim(1)];
            Pyy     [i][nx]    = new double *  [Ndim(1)];
            Pxy     [i][nx]    = new double *  [Ndim(1)];
            Rho     [i][nx]    = new double *  [Ndim(1)];
            RhoW    [i][nx]    = new double *  [Ndim(1)];
            PsiS    [i][nx]    = new double *  [Ndim(1)];
            PsiC    [i][nx]    = new double *  [Ndim(1)];
            Nmol    [i][nx]    = new double *  [Ndim(1)];
            Nmolprev [i][nx]    = new double *  [Ndim(1)];
            IsSolid [i][nx]    = new bool   *  [Ndim(1)];
            IsHBSolid [i][nx]    = new bool   *  [Ndim(1)];
            for (size_t ny=0;ny<Ndim(1);ny++)
            {
                F       [i][nx][ny]    = new double * [Ndim(2)];
                Ftemp   [i][nx][ny]    = new double * [Ndim(2)];
                Fprev   [i][nx][ny]    = new double * [Ndim(2)];
                IsFluid [i][nx][ny]    = new bool   * [Ndim(2)];
                Vel     [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Velprev [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                nvec    [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                BForce  [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Pij     [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pkin    [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pxx     [i][nx][ny]    = new double   [Ndim(2)];
                Pyy     [i][nx][ny]    = new double   [Ndim(2)];
                Pxy     [i][nx][ny]    = new double   [Ndim(2)];
                Rho     [i][nx][ny]    = new double   [Ndim(2)];
                RhoW    [i][nx][ny]    = new double   [Ndim(2)];
                PsiS    [i][nx][ny]    = new double   [Ndim(2)];
                PsiC    [i][nx][ny]    = new double   [Ndim(2)];
                Nmol    [i][nx][ny]    = new double   [Ndim(2)];
                Nmolprev    [i][nx][ny]    = new double   [Ndim(2)];
                IsSolid [i][nx][ny]    = new bool     [Ndim(2)];
                IsHBSolid [i][nx][ny]    = new bool     [Ndim(2)];
                for (size_t nz=0;nz<Ndim(2);nz++)
                {
                    RhoW [i][nx][ny][nz]    = 0.0;
                    Pij  [i][nx][ny][nz]    = new Vec3_t   [3];
                    Pkin [i][nx][ny][nz]    = new Vec3_t   [3];
                    F    [i][nx][ny][nz]    = new double [Nneigh];
                    Ftemp[i][nx][ny][nz]    = new double [Nneigh];
                    Fprev[i][nx][ny][nz]    = new double [Nneigh];
                    IsFluid[i][nx][ny][nz]  = new bool   [Nneigh];
                    IsSolid[i][nx][ny][nz]  = false;
                    IsHBSolid[i][nx][ny][nz]  = false;
                    for (size_t nn=0;nn<Nneigh;nn++)
                    {
                        F    [i][nx][ny][nz][nn] = 0.0;
                        Ftemp[i][nx][ny][nz][nn] = 0.0;
                        Fprev[i][nx][ny][nz][nn] = 0.0;
                        IsFluid[i][nx][ny][nz][nn] = false;
                    }
                }
            }
        }
    }

    Udist = 0;
    Udiagonal = 0;
    kAxial = 0;
    for (size_t k=1;k<Nneigh;k++)
    {
        if (C[k][0]>0)
        {
            Udist++;
            if ((C[k][1]!=0)) // Diagonal components
            {
                Udiagonal++;
            }
        }
    }
    kAxial = 2*(Udist-Udiagonal)+1; // +1 to account for k=0 

    CBB = new size_t * [Udist]; 
    CBBd = new size_t * [Udiagonal];
    for (size_t k=0;k<Udist;k++)
    {
        CBB[k] = new size_t [4];
    }
    for (size_t k=0;k<Udiagonal;k++)
    {
        CBBd[k] = new size_t [4];
    }
    
    CKA = new size_t * [kAxial];    
    for (size_t k=0;k<kAxial;k++)
    {
        CKA[k] = new size_t [2];
    }
    
    CKA[0][0]=0;
    CKA[0][1]=0;
    size_t ix=1,iy=1;
    for (size_t k=1;k<Nneigh;k++) {
            if (C[k][0]==0) CKA[ix][0]=k, ix++;
            if (C[k][1]==0) CKA[iy][1]=k, iy++;
    }
    
    latticeLength = 0;
    if (Nneigh>=FNneigh) {
        for (size_t k=1;k<Nneigh;k++) {
            size_t clength = fabs(C[k][1]);
            latticeLength = std::max(clength,latticeLength);
            
        } }
    else {
        for (size_t k=1;k<FNneigh;k++) {
            size_t clength = fabs(Cf[k][1]);
            latticeLength = std::max(clength,latticeLength);
        } }
    // latticeLength += 1;
    // std::cout << "TEST " << latticeLength << std::endl;

    OppBB = new size_t [4];
    OppBB[0] = 2;
    OppBB[1] = 3;
    OppBB[2] = 0;
    OppBB[3] = 1;

    DefBoundaryDist();
    // DefNormVec();
    ConstructInteractionArray();

    if (TheRelaxType==MRT) // Only for D2Q17 currently
    {
        if (TheMethod==D2Q17)
        {
            Nneigh = 17;
            M.Resize(Nneigh,Nneigh);
            Minv.Resize(Nneigh,Nneigh);
            
            
            S.Resize(Nneigh);
            
            for (size_t n=0;n<Nneigh;n++)
            {
                for (size_t m=0;m<Nneigh;m++)
                {
                    M(n,m) = MD2Q17[n][m];
                }
            }
            Inv(M,Minv);
            
            MAlpha    = new double **  [Nl];
            SVec    = new double *  [Nl];
            // MAlphaTemp1    = new double *  [Nneigh];
            // MAlphaTemp2    = new double *  [Nneigh];
            for (size_t i=0;i<Nl;i++)
            {
                MAlpha[i]  = new double * [Nneigh];
                SVec[i]    = new double   [Nneigh];
                for (size_t n=0;n<Nneigh;n++)
                {
                    MAlpha[i][n]   = new double [Nneigh];
                    SVec[i][n]   = 0.0;
                    // MAlphaTemp1[n]   = new double [Nneigh];
                    // MAlphaTemp2[n]   = new double [Nneigh];
                    for (size_t m=0;m<Nneigh;m++)
                    {
                        MAlpha[i][n][m] = 0.0;
                        // MAlphaTemp1[n][m]   = 0.0;
                        // MAlphaTemp2[n][m]   = 0.0;
                    }
                }
            }

            
            
            for (size_t i=0;i<Nl;i++)
            {
                auto MAlphaTemp1 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto MAlphaTemp2 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto SMat = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                // S = new double [Nneigh];
                double tau = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
                // double BulkNu = (2.0/3.0)*(Cs2*(tau-0.5)); // (2.0/3.0)*nu[i];
                double tauBulk = (2.0/3.0)*nu[i]*dtR/(Cs2*dxR*dxR)+0.5;//(0.5 + ((2.0*BulkNu)/Cs2));
                double TRT = 1.0/6.0; // 1/12 Best for Advection, 1/6 Best for Diffusion.
                double omegaODD = 1.0/(0.5+(TRT/(tau - 0.5)));
                double omega_other = (8.0*((1.0/tau)  - 2.0))/((1.0/tau) - 8.0);
                S = 1.0/tau, 1.0/tauBulk, 1.0/tauBulk, omegaODD, omega_other, omegaODD, omega_other, 1.0/tau, 1.0/tau, omegaODD, omegaODD, omegaODD, omegaODD, 1.0/tauBulk, 1.0/tauBulk, 1.0/tau, 1.0/tau;
                //~   0th  ,       1    ,      2     ,   3     ,    4    ,   5     ,    6    ,    7   ,    8   ,    9    ,   10    ,   11    ,   12    ,     13      ,     14     ,   15   ,   16   ~ Moment Order.
                // S = 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau;
                for (size_t k=0;k<Nneigh;k++)
                {
                    (*SMat)(k,k) = S(k);
                    SVec[i][k] = S(k);
                    // std::cout << SVec[i][k] << " ||";
                }
                // std::cout << std::endl;

                double a = 1.0,b = 0.0;

                LinAlg::Gemm(a, *SMat, M, b, *MAlphaTemp1);
                LinAlg::Gemm(a, Minv, *MAlphaTemp1, b, *MAlphaTemp2);
                for (size_t j=0;j<Nneigh;j++)
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        MAlpha[i][j][k]=  (*MAlphaTemp2)(j,k);// fneq2(j,k);//MAlphaTemp2(j,k);
                        // std::cout << MAlpha[i][j][k] << " ||";
                    }
                    // std::cout << std::endl;
                }

                delete(MAlphaTemp1);
                delete(MAlphaTemp2);

            }
            // printf("%s  Still running                    %s\n ",TERM_CLR2,TERM_RST);
            // cout << Matrix3i(Vector3i(2,5,6).asDiagonal()) << endl;
        }
    }


    EEk = new double [Nneigh];
    for (size_t k=0;k<Nneigh;k++)
    {
        EEk[k]    = 0.0;
        for (size_t n=0;n<3;n++)
        for (size_t m=0;m<3;m++)
        {
            // EEk[k] += fabs(C[k][n]*C[k][m]);
            EEk[k] += C[k][n]*C[k][m];
        }
    }
    // Print out
        printf("%s  Lattice Model                    = %s\n ",TERM_CLR2, LBmodel.c_str());
        printf("%s  ==> lattice sound-speed:   Cs^2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
        printf("%s  ==> Force Model: %s\n",TERM_CLR2, ForceModel.c_str());
        printf("%s  Num of cells                     = %zd%s ",TERM_CLR2,Nl*Ncells,TERM_RST);
        printf("%s  , For N = %zd%s ",TERM_CLR2,Nl,TERM_RST); printf("%s Components %s\n",TERM_CLR2,TERM_RST);
#ifdef USE_OMP
    omp_init_lock(&lck);
#endif
}

inline Domain::Domain(LBMethod TheMethod, FOrder ForceOrder, double Thenu, iVec3_t TheNdim, double Thedx, double Thedt)
{
    Array<double> nu(1);
    nu[0] = Thenu;
    
    Util::Stopwatch stopwatch;
    printf("\n%s--- Initializing LBM Domain --------------------------------------------%s\n",TERM_CLR1,TERM_RST);
    if (nu.Size()==0) throw new Fatal("LBM::Domain: Declare at leat one fluid please");
    if (TheNdim(2) >1&&(TheMethod==D2Q17 || TheMethod==D2Q9 ||TheMethod==D2Q5 ))  throw new Fatal("LBM::Domain: D2Q9 scheme does not allow for a third dimension, please set Ndim(2)=1 or change to D3Q15");
    if (TheNdim(2)==1&&(TheMethod==D3Q15||TheMethod==D3Q19))  throw new Fatal("LBM::Domain: Ndim(2) is 1. Either change the method to D2Q9 or increase the z-dimension");

    Time        = 0.0;
    dtR          = Thedt; // Real [seconds]
    dxR          = Thedx; // Real [meters]
    // dt          = dtR;
    // dx          = dxR;
    dt          = 1.0;
    dx          = 1.0;
    Cs          = dx/dt;
    // Cs          = 1.0;
    Cs2         = (Cs/(sqrt(3)))*(Cs/(sqrt(3)));
    Step        = 1;
    Sc          = 0.17;
    tmpSum      = OrthoSys::O;
    Nl          = 1;
    Ndim        = TheNdim;
    Ncells      = Ndim(0)*Ndim(1)*Ndim(2);
    IsFirstTime = true;
    CalcPressureTensor = false;
    PTconsts    = new Vec3_t  [4]; // c2, A, and B.
    PTconsts[0] = OrthoSys::O;
    PTconsts[1] = OrthoSys::O;
    PTconsts[2] = OrthoSys::O;
    PTconsts[3] = OrthoSys::O;
    // Wzot = new double [3]; // zero-one-three weights

    if (TheMethod==D2Q5)
    {
        Nneigh = 5;
        W      = WEIGHTSD2Q5;
        C      = LVELOCD2Q5;
        Op     = OPPOSITED2Q5;
        LBmodel = "D2Q5";
    }
    if (TheMethod==D2Q9)
    {
        Nneigh = 9;
        W      = WEIGHTSD2Q9;
        C      = LVELOCD2Q9;
        Op     = OPPOSITED2Q9;
        Cs2         = (Cs/(sqrt(3)))*(Cs/(sqrt(3)));
        LBmodel = "D2Q9";
        FeqOrder = 2;
    }
    if (TheMethod==D2Q17)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17;
        C      = LVELOCD2Q17;
        Op     = OPPOSITED2Q17;
        Cs2         = (Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
        LBmodel = "D2Q17";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q17ZOT)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17ZOT;
        C      = LVELOCD2Q17ZOT;
        Op     = OPPOSITED2Q17ZOT;
        // Cs2         = Cs*Cs*(1.0/((5.0+sqrt(10.0))/3.0)); // should be the same as below
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q17ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q21)
    {
        Nneigh = 21;
        W      = WEIGHTSD2Q21;
        C      = LVELOCD2Q21;
        Op     = OPPOSITED2Q21;
        Cs2         = (Cs/(sqrt(3.0/2.0)))*(Cs/(sqrt(3.0/2.0)));
        LBmodel = "D2Q21";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25)
    {
        Nneigh = 25;
        W      = WEIGHTSD2Q25;
        C      = LVELOCD2Q25;
        Op     = OPPOSITED2Q25;
        Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
        LBmodel = "D2Q25";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25ZOT)
    {
        Nneigh = 25;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q25ZOT;
        C      = LVELOCD2Q25ZOT;
        Op     = OPPOSITED2Q25ZOT;
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q25 ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q37)
    {
        Nneigh = 37;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q37;
        C      = LVELOCD2Q37;
        Op     = OPPOSITED2Q37;
        // Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0))); // Ref. Philippi, where a = 0.846393
        Cs2      = pow((1.0/1.19697977039307435897239),2.0); // Ref. Shan, r = that long number
        Cs2     *= Cs*Cs;
        LBmodel = "D2Q37";
        FeqOrder = 4;
    }
    if (TheMethod==D2Q49ZOTT) //* Zero - One - Two - Three (ZOTT) Ref: S. S. Chikatamarla Phys Rev E 79, 046701 (2009)
    {
        Nneigh = 49;
        double tmpcsA = 0, tmpcsB = 0;
        tmpcsA = (7.0/(5.0*(3.0*sqrt(30.0) - 5.0)));
        tmpcsB = 1.0/(7.0/((3.0*sqrt(30.0) - 5.0)));

        Cs2     = (2.0/3.0) + (1.0/3.0)*pow(tmpcsA,(1.0/3.0)) - ((pow(tmpcsB, (1.0/3.0))) / (pow(35.0, (2.0/3.0))));
        Cs2     *= Cs*Cs; // Should equal  == 0.697 953 322 019 683 1, According to PhysRevE.79.046701 (2009)
        // Cs2 = 0.6979533220196831;
        // Cs2 = Cs*Cs*(1.0-sqrt(2.0/5.0)); //* Change ref. temp to match ZOT lattices: 17ZOT and 25ZOT
        Wzot = new double [4];
        Wzot[0] = (1.0/36.0)*(Cs2*(3.0*(14.0 - 5.0*Cs2)*Cs2 - 49.0) + 36.0);
        Wzot[1] = (1.0/16.0)*(Cs2*(Cs2*(5.0*Cs2 - 13.0) + 12.0));
        Wzot[2] = (1.0/40.0)*(Cs2*(5.0*(2.0 - Cs2)*Cs2 - 3.0));
        Wzot[3] = (1.0/720.0)*(Cs2*(15.0*(Cs2 - 1.0)*Cs2 + 4.0));
        C      = LVELOCD2Q49ZOTT;
        Op     = OPPOSITED2Q49ZOTT;
        Wtmp = new double [Nneigh];
        for (size_t n=0;n<Nneigh;n++) {
            size_t xx = (size_t)( (int)fabs(C[n](0)) );
            size_t yy = (size_t)( (int)fabs(C[n](1)) );
            Wtmp[n] = Wzot[xx]*Wzot[yy];
            // std::cout << " W ["<< n <<"] = " << Wtmp[n] << std::endl;
            // std::cout << " x = "<< xx <<", y = " << yy << std::endl;
            // std::cout << " Wx = "<< Wzot[xx] <<", Wy = " << Wzot[yy] << "\n"<< std::endl;
        }
        W = (const double *) Wtmp;
        LBmodel = "D2Q49 ZOTT";
        FeqOrder = 4;
    }
    if (TheMethod==D3Q15)
    {
        Nneigh = 15;
        W      = WEIGHTSD3Q15;
        C      = LVELOCD3Q15;
        Op     = OPPOSITED3Q15;
        M.Resize(Nneigh,Nneigh);
        Minv.Resize(Nneigh,Nneigh);
        for (size_t n=0;n<Nneigh;n++)
        for (size_t m=0;m<Nneigh;m++)
        {
            M(n,m) = MD3Q15[n][m];
        }
        Inv(M,Minv);
        double tau = 3.0*Thenu*dt/(dx*dx)+0.5;
        double s   = 8.0*(2.0-1.0/tau)/(8.0-1.0/tau);
        S.Resize(Nneigh);
        S = 0.0,1.0/tau,1.0/tau,0.0,s,0.0,s,0.0,s,1.0/tau,1.0/tau,1.0/tau,1.0/tau,1.0/tau,s;
        LBmodel = "D3Q15";
    }
    if (TheMethod==D3Q19)
    {
        Nneigh = 19;
        W      = WEIGHTSD3Q19;
        C      = LVELOCD3Q19;
        Op     = OPPOSITED3Q19;
        LBmodel = "D3Q19";
    }
    
    if (ForceOrder==E4)
    {
        //E4
        FNneigh = 8;
        Cf     = LVELOCDFE4;
        Wf     = FWEIGHTSE4;
        ForceModel = "Isotropy Order = 4";
        epsilon = 0.0;
    }
    if (ForceOrder==E8)
    {
        //E8
        FNneigh = 24;
        Cf     = LVELOCDFE8;
        Wf     = FWEIGHTSE8;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] ; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] ; // fourth order Isotropy Tensor for psi
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 8";
        printf("%s  >>> Isotropy Order E8, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s      ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s      ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==E10)
    {
        //E10
        FNneigh = 36;
        Cf     = LVELOCDFE10;
        Wf     = FWEIGHTSE10;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] + 18.0*Wf[24] +  40.0*Wf[28]; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] + 40.5*Wf[24] + 100.0*Wf[28]; // fourth order Isotropy Tensor for psi
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 10";
        printf("%s  >>> Isotropy Order E10, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
        // /* Display Output in order to double-check */
        // for (size_t k=0;k<FNneigh;k++)
        // {
        //     std::cout << " Wf[" << k << "] = " << Wf[k] << ",  \n";
        // }
        // std::cout << std::endl;
    }

    if (ForceOrder==E12)
    {
        //E12
        FNneigh = 48;
        Cf     = LVELOCDFE12;
        Wf     = FWEIGHTSE12;
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 12";
        printf("%s  >>> Isotropy Order E12, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==ELS)
    {
        //Forcing interaction-range to equal the same as lattice structure.
        FNneigh = Nneigh;
        Wff = new double [FNneigh];  
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        // symm_groups[0] = 0.0, symm_emax[0] = 0.0, Wff[0] = 0.0;
        Cf     = C;
        // for (size_t n=1;n<FNneigh;n++){
        for (size_t n=0;n<FNneigh;n++){
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
            Wff[n]     = W[n]*(1.0/(Cs2));  // sum W*C[k]^2 = 1.0
            // Wff[n]     = W[n];              // sum W*C[k]^2 = cs2
            // std::cout << " W ("<< n <<") = " << W[n] << " || Wf("<< n << ") = " << Wff[n] << "\n" << std::endl;
        }
        Wf = (const double *) Wff; // std::cout << " Wf " << Wf[1] << std::endl;
        
        ForceModel = "equal to lattice structure";
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            eAlpha = (e2 + 6.0*e4)/12.0; // True for any lattice, and can also consider the case e2!=1
            eBeta  = (3.0*e4 - e2)/12.0;
            // double eEta = (1.0 + 3.0*e4)/20.0; // Only works for E8, it changes between lattices
            // double eGamma  = (-1.0 + 2.0*e4)/20.0;
            
            epsilon = 2.0*eBeta/eAlpha;
            // ForceModel = "Isotropy Order 4 enforce on Lattice Structure";
            printf("%s  >>>                    cs2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
            printf("%s  >>>                epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
            printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
            printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
            printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
            printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    /* Allocate variables to Memory */
    Tau    = new double [Nl];
    epsiC  = new double [Nl];
    G   = new double [Nl];
    molMass   = new double [Nl];
    Gs  = new double [Nl];
    Rhoref = new double [Nl];
    Gmmix = new double [Nl];
    Psi    = new double [Nl];
    Gmix= 0.0;
    Gint= 0.0;

    F       = new double **** [Nl];
    Ftemp   = new double **** [Nl];
    IsFluid = new bool   **** [Nl];
    Vel     = new Vec3_t ***  [Nl];
    Velprev = new Vec3_t ***  [Nl];
    nvec    = new Vec3_t ***  [Nl];
    Fprev   = new double **** [Nl];
    BForce  = new Vec3_t ***  [Nl];
    Pij     = new Vec3_t **** [Nl];
    Pkin    = new Vec3_t **** [Nl];
    Pxx     = new double *** [Nl];
    Pyy     = new double *** [Nl];
    Pxy     = new double *** [Nl];
    Rho     = new double *** [Nl];
    RhoW     = new double *** [Nl];
    PsiS     = new double *** [Nl];
    PsiC     = new double *** [Nl];
    Nmol    = new double *** [Nl];
    Nmolprev = new double ***  [Nl];
    
    IsSolid = new bool   ***  [Nl];
    IsHBSolid = new bool   ***  [Nl];

    for (size_t i=0;i<Nl;i++)
    {
        molMass [i]    = 1.0;
        Tau     [i]    = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
        epsiC   [i]    = 1.0/epsilon;
        G       [i]    = 0.0;
        Gs      [i]    = 0.0;
        Rhoref  [i]    = 200.0;
        Gmmix   [i]    = 0.0;
        // Rhoref  [i]    = 1.0;
        Psi     [i]    = 4.0;
        // Psi     [i]    = 1.0;
        F       [i]    = new double *** [Ndim(0)];
        Ftemp   [i]    = new double *** [Ndim(0)];
        Fprev   [i]    = new double *** [Ndim(0)];
        IsFluid [i]    = new bool   *** [Ndim(0)];
        Vel     [i]    = new Vec3_t **  [Ndim(0)];
        Velprev [i]    = new Vec3_t **  [Ndim(0)];
        nvec    [i]    = new Vec3_t **  [Ndim(0)];
        BForce  [i]    = new Vec3_t **  [Ndim(0)];
        Pij     [i]    = new Vec3_t *** [Ndim(0)];
        Pkin    [i]    = new Vec3_t *** [Ndim(0)];
        Pxx     [i]    = new double **  [Ndim(0)];
        Pyy     [i]    = new double **  [Ndim(0)];
        Pxy     [i]    = new double **  [Ndim(0)];
        Rho     [i]    = new double **  [Ndim(0)];
        RhoW     [i]    = new double **  [Ndim(0)];
        PsiS    [i]    = new double **  [Ndim(0)];
        PsiC    [i]    = new double **  [Ndim(0)];
        Nmol    [i]    = new double **  [Ndim(0)];
        Nmolprev [i]    = new double **  [Ndim(0)];
        IsSolid [i]    = new bool   **  [Ndim(0)];
        IsHBSolid [i]    = new bool   **  [Ndim(0)];
        for (size_t nx=0;nx<Ndim(0);nx++)
        {
            F       [i][nx]    = new double ** [Ndim(1)];
            Ftemp   [i][nx]    = new double ** [Ndim(1)];
            Fprev   [i][nx]    = new double ** [Ndim(1)];
            IsFluid [i][nx]    = new bool   ** [Ndim(1)];
            Vel     [i][nx]    = new Vec3_t *  [Ndim(1)];
            Velprev [i][nx]    = new Vec3_t *  [Ndim(1)];
            nvec    [i][nx]    = new Vec3_t *  [Ndim(1)];
            BForce  [i][nx]    = new Vec3_t *  [Ndim(1)];
            Pij     [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pkin    [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pxx     [i][nx]    = new double *  [Ndim(1)];
            Pyy     [i][nx]    = new double *  [Ndim(1)];
            Pxy     [i][nx]    = new double *  [Ndim(1)];
            Rho     [i][nx]    = new double *  [Ndim(1)];
            RhoW     [i][nx]    = new double *  [Ndim(1)];
            PsiS    [i][nx]    = new double *  [Ndim(1)];
            PsiC    [i][nx]    = new double *  [Ndim(1)];
            Nmol    [i][nx]    = new double *  [Ndim(1)];
            Nmolprev [i][nx]    = new double *  [Ndim(1)];
            IsSolid [i][nx]    = new bool   *  [Ndim(1)];
            IsHBSolid [i][nx]    = new bool   *  [Ndim(1)];
            for (size_t ny=0;ny<Ndim(1);ny++)
            {
                F       [i][nx][ny]    = new double * [Ndim(2)];
                Ftemp   [i][nx][ny]    = new double * [Ndim(2)];
                Fprev   [i][nx][ny]    = new double * [Ndim(2)];
                IsFluid [i][nx][ny]    = new bool   * [Ndim(2)];
                Vel     [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Velprev [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                nvec    [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                BForce  [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Pij     [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pkin    [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pxx     [i][nx][ny]    = new double   [Ndim(2)];
                Pyy     [i][nx][ny]    = new double   [Ndim(2)];
                Pxy     [i][nx][ny]    = new double   [Ndim(2)];
                Rho     [i][nx][ny]    = new double   [Ndim(2)];
                RhoW    [i][nx][ny]    = new double   [Ndim(2)];
                PsiS    [i][nx][ny]    = new double   [Ndim(2)];
                PsiC    [i][nx][ny]    = new double   [Ndim(2)];
                Nmol    [i][nx][ny]    = new double   [Ndim(2)];
                Nmolprev    [i][nx][ny]    = new double   [Ndim(2)];
                IsSolid [i][nx][ny]    = new bool     [Ndim(2)];
                IsHBSolid [i][nx][ny]    = new bool     [Ndim(2)];
                for (size_t nz=0;nz<Ndim(2);nz++)
                {
                    RhoW [i][nx][ny][nz]    = 0.0;
                    Pij  [i][nx][ny][nz]    = new Vec3_t   [3];
                    Pkin [i][nx][ny][nz]    = new Vec3_t   [3];
                    F    [i][nx][ny][nz]    = new double [Nneigh];
                    Ftemp[i][nx][ny][nz]    = new double [Nneigh];
                    Fprev[i][nx][ny][nz]    = new double [Nneigh];
                    IsFluid[i][nx][ny][nz]  = new bool   [Nneigh];
                    IsSolid[i][nx][ny][nz]  = false;
                    IsHBSolid[i][nx][ny][nz]  = false;
                    for (size_t nn=0;nn<Nneigh;nn++)
                    {
                        F    [i][nx][ny][nz][nn] = 0.0;
                        Ftemp[i][nx][ny][nz][nn] = 0.0;
                        Fprev[i][nx][ny][nz][nn] = 0.0;
                        IsFluid[i][nx][ny][nz][nn] = false;
                    }
                }
            }
        }
    }

    Udist = 0;
    Udiagonal = 0;
    kAxial = 0;
    for (size_t k=1;k<Nneigh;k++)
    {
        if (C[k][0]>0)
        {
            Udist++;
            if ((C[k][1]!=0)) // Diagonal components
            {
                Udiagonal++;
            }
        }
    }
    kAxial = 2*(Udist-Udiagonal)+1; // +1 to account for k=0 

    CBB = new size_t * [Udist]; 
    CBBd = new size_t * [Udiagonal];
    for (size_t k=0;k<Udist;k++)
    {
        CBB[k] = new size_t [4];
    }
    for (size_t k=0;k<Udiagonal;k++)
    {
        CBBd[k] = new size_t [4];
    }
    
    CKA = new size_t * [kAxial];    
    for (size_t k=0;k<kAxial;k++)
    {
        CKA[k] = new size_t [2];
    }
    
    CKA[0][0]=0;
    CKA[0][1]=0;
    size_t ix=1,iy=1;
    for (size_t k=1;k<Nneigh;k++) {
            if (C[k][0]==0) CKA[ix][0]=k, ix++;
            if (C[k][1]==0) CKA[iy][1]=k, iy++;
    }
    
    latticeLength = 0;
    if (Nneigh>=FNneigh) {
        for (size_t k=1;k<Nneigh;k++) {
            size_t clength = fabs(C[k][1]);
            latticeLength = std::max(clength,latticeLength);
            
        } }
    else {
        for (size_t k=1;k<FNneigh;k++) {
            size_t clength = fabs(Cf[k][1]);
            latticeLength = std::max(clength,latticeLength);
        } }
    // latticeLength += 1;
    // std::cout << "TEST " << latticeLength << std::endl;

    OppBB = new size_t [4];
    OppBB[0] = 2;
    OppBB[1] = 3;
    OppBB[2] = 0;
    OppBB[3] = 1;
    DefBoundaryDist();
    // DefNormVec();
    ConstructInteractionArray();

    if (TheMethod==D2Q17)
    {
            Nneigh = 17;
            M.Resize(Nneigh,Nneigh);
            Minv.Resize(Nneigh,Nneigh);
            
            
            S.Resize(Nneigh);
            
            for (size_t n=0;n<Nneigh;n++)
            {
                for (size_t m=0;m<Nneigh;m++)
                {
                    M(n,m) = MD2Q17[n][m];
                }
            }
            Inv(M,Minv);
            
            MAlpha    = new double **  [Nl];
            SVec    = new double *  [Nl];
            // MAlphaTemp1    = new double *  [Nneigh];
            // MAlphaTemp2    = new double *  [Nneigh];
            for (size_t i=0;i<Nl;i++)
            {
                MAlpha[i]  = new double * [Nneigh];
                SVec[i]    = new double   [Nneigh];
                for (size_t n=0;n<Nneigh;n++)
                {
                    MAlpha[i][n]   = new double [Nneigh];
                    SVec[i][n]   = 0.0;
                    // MAlphaTemp1[n]   = new double [Nneigh];
                    // MAlphaTemp2[n]   = new double [Nneigh];
                    for (size_t m=0;m<Nneigh;m++)
                    {
                        MAlpha[i][n][m] = 0.0;
                        // MAlphaTemp1[n][m]   = 0.0;
                        // MAlphaTemp2[n][m]   = 0.0;
                    }
                }
            }

            
            
            for (size_t i=0;i<Nl;i++)
            {
                auto MAlphaTemp1 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto MAlphaTemp2 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto SMat = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                // S = new double [Nneigh];
                double tau = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
                double BulkNu = (2.0/3.0)*(Cs2*(tau-0.5)); // [dimensionless] -- Relaxation of Energy Moments.
                double tauBulk = (0.5 + ((2.0*BulkNu)/Cs2));
                
                S = 1.0/tau, 1.0/tauBulk, 1.0/tauBulk, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau , 1.0/tauBulk, 1.0/tauBulk, 1.0/tau, 1.0/tau;
                //~   0th  ,       1    ,      2     ,   3     ,    4   ,   5    ,    6   ,    7   ,    8   ,    9   ,   10   ,   11   ,   12   ,     13     ,     14     ,   15   ,   16   ~ Moment Order.
                // S = 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau;

                for (size_t k=0;k<Nneigh;k++)
                {
                    (*SMat)(k,k) = S(k);
                    SVec[i][k] = S(k);
                    // std::cout << SVec[i][k] << " ||";
                }
                // std::cout << std::endl;

                double a = 1.0,b = 0.0;

                LinAlg::Gemm(a, *SMat, M, b, *MAlphaTemp1);
                LinAlg::Gemm(a, Minv, *MAlphaTemp1, b, *MAlphaTemp2);
                for (size_t j=0;j<Nneigh;j++)
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        MAlpha[i][j][k]=  (*MAlphaTemp2)(j,k);// fneq2(j,k);//MAlphaTemp2(j,k);
                        // std::cout << MAlpha[i][j][k] << " ||";
                    }
                    // std::cout << std::endl;
                }

                delete(MAlphaTemp1);
                delete(MAlphaTemp2);

            }
            // printf("%s  Still running                    %s\n ",TERM_CLR2,TERM_RST);
            // cout << Matrix3i(Vector3i(2,5,6).asDiagonal()) << endl;
    }



    EEk = new double [Nneigh];
    for (size_t k=0;k<Nneigh;k++)
    {
        EEk[k]    = 0.0;
        for (size_t n=0;n<3;n++)
        for (size_t m=0;m<3;m++)
        {
            EEk[k] += fabs(C[k][n]*C[k][m]);
        }
    }
    printf("%s  Lattice Model                    = %s\n ",TERM_CLR2, LBmodel.c_str());
    printf("%s  ==> lattice sound-speed:   Cs^2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
    printf("%s  ==> Force Model: %s\n",TERM_CLR2, ForceModel.c_str());
    printf("%s    >> epsilon                     = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
    printf("%s  Num of cells                     = %zd%s\n ",TERM_CLR2,Nl*Ncells,TERM_RST);
#ifdef USE_OMP
    omp_init_lock(&lck);
#endif
}


Domain::Domain(LBMethod TheMethod, FOrder ForceOrder, RelaxType TheRelaxType, Array<double> MolarMass, Array<double> nu, iVec3_t TheNdim, double Thedx, double Thedt)
{
    Util::Stopwatch stopwatch;
    printf("\n%s--- Initializing LBM Domain --------------------------------------------%s\n",TERM_CLR1,TERM_RST);
    if (nu.Size()==0) throw new Fatal("LBM::Domain: Declare at leat one fluid please");
    if (TheNdim(2) >1&&(TheMethod==D2Q17 || TheMethod==D2Q9 ||TheMethod==D2Q5 ))  throw new Fatal("LBM::Domain: D2Q9 scheme does not allow for a third dimension, please set Ndim(2)=1 or change to D3Q15");
    if (TheNdim(2)==1&&(TheMethod==D3Q15||TheMethod==D3Q19))  throw new Fatal("LBM::Domain: Ndim(2) is 1. Either change the method to D2Q9 or increase the z-dimension");
    if (MolarMass.Size()==0) throw new Fatal("LBM::Domain: Declare Molecular Mass for at least two fluids");
    
    Time        = 0.0;
    dtR          = Thedt; // Real [seconds]
    dxR          = Thedx; // Real [meters]
    // dt          = dtR;
    // dx          = dxR;
    dt          = 1.0;
    dx          = 1.0;
    Cs          = dx/dt;
    // Cs          = 1.0;
    Step        = 1;
    Sc          = 0.17;
    tmpSum      = OrthoSys::O;
    Nl          = nu.Size();
    Ndim        = TheNdim;
    Ncells      = Ndim(0)*Ndim(1)*Ndim(2);
    IsFirstTime = true;
    // Wzot = new double [3]; // zero-one-three weights
    CalcPressureTensor = false;
    PTconsts    = new Vec3_t  [4]; // c2, A, and B.
    PTconsts[0] = OrthoSys::O;
    PTconsts[1] = OrthoSys::O;
    PTconsts[2] = OrthoSys::O;
    PTconsts[3] = OrthoSys::O;
    // Wzot = new double [3]; // zero-one-three weights


    if (TheMethod==D2Q5)
    {
        Nneigh = 5;
        W      = WEIGHTSD2Q5;
        C      = LVELOCD2Q5;
        Op     = OPPOSITED2Q5;
    }
    if (TheMethod==D2Q9)
    {
        Nneigh = 9;
        W      = WEIGHTSD2Q9;
        C      = LVELOCD2Q9;
        Op     = OPPOSITED2Q9;
        Cs2         = (Cs/(sqrt(3)))*(Cs/(sqrt(3)));
        LBmodel = "D2Q9";
        FeqOrder = 2;
    }
    if (TheMethod==D2Q17)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17;
        C      = LVELOCD2Q17;
        Op     = OPPOSITED2Q17;
        Cs2         = (Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(Cs/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
        LBmodel = "D2Q17";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q17ZOT)
    {
        Nneigh = 17;
        W      = WEIGHTSD2Q17ZOT;
        C      = LVELOCD2Q17ZOT;
        Op     = OPPOSITED2Q17ZOT;
        // Cs2         = Cs*Cs*(1.0/((5.0+sqrt(10.0))/3.0)); // should be the same as below
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q17ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q21)
    {
        Nneigh = 21;
        W      = WEIGHTSD2Q21;
        C      = LVELOCD2Q21;
        Op     = OPPOSITED2Q21;
        Cs2         = (Cs/(sqrt(3.0/2.0)))*(Cs/(sqrt(3.0/2.0)));
        LBmodel = "D2Q21";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25)
    {
        Nneigh = 25;
        W      = WEIGHTSD2Q25;
        C      = LVELOCD2Q25;
        Op     = OPPOSITED2Q25;
        Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.760569),2.0)/2.0)));
        LBmodel = "D2Q25";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q25ZOT)
    {
        Nneigh = 25;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q25ZOT;
        C      = LVELOCD2Q25ZOT;
        Op     = OPPOSITED2Q25ZOT;
        Cs2         = Cs*Cs*(1.0-sqrt(2.0/5.0));
        LBmodel = "D2Q25 ZOT";
        FeqOrder = 3;
    }
    if (TheMethod==D2Q37)
    {
        Nneigh = 37;
        // Wzot[0] = (4.0/45.0)*(4.0 + sqrt(10.0));
        // Wzot[1] = (3.0/80.0)*(8.0 - sqrt(10.0));
        // Wzot[3] = (1.0/720.0)*(16.0 - 5.0*sqrt(10.0));
        W      = WEIGHTSD2Q37;
        C      = LVELOCD2Q37;
        Op     = OPPOSITED2Q37;
        // Cs2         = (Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0)))*(Cs/(1.0/sqrt(pow((1.0/0.846393),2.0)/2.0))); // Ref. Philippi, where a = 0.846393
        Cs2      = pow((1.0/1.19697977039307435897239),2.0); // Ref. Shan, r = that long number
        Cs2     *= Cs*Cs;
        LBmodel = "D2Q37";
        FeqOrder = 4;
    }
    if (TheMethod==D2Q49ZOTT) //* Zero - One - Two - Three (ZOTT) Ref: S. S. Chikatamarla Phys Rev E 79, 046701 (2009)
    {
        Nneigh = 49;
        double tmpcsA = 0, tmpcsB = 0;
        tmpcsA = (7.0/(5.0*(3.0*sqrt(30.0) - 5.0)));
        tmpcsB = 1.0/(7.0/((3.0*sqrt(30.0) - 5.0)));

        Cs2     = (2.0/3.0) + (1.0/3.0)*pow(tmpcsA,(1.0/3.0)) - ((pow(tmpcsB, (1.0/3.0))) / (pow(35.0, (2.0/3.0))));
        Cs2     *= Cs*Cs; // Should equal  == 0.697 953 322 019 683 1, According to PhysRevE.79.046701 (2009)
        // Cs2 = 0.6979533220196831;
        // Cs2 = Cs*Cs*(1.0-sqrt(2.0/5.0)); //* Change ref. temp to match ZOT lattices: 17ZOT and 25ZOT
        Wzot = new double [4];
        Wzot[0] = (1.0/36.0)*(Cs2*(3.0*(14.0 - 5.0*Cs2)*Cs2 - 49.0) + 36.0);
        Wzot[1] = (1.0/16.0)*(Cs2*(Cs2*(5.0*Cs2 - 13.0) + 12.0));
        Wzot[2] = (1.0/40.0)*(Cs2*(5.0*(2.0 - Cs2)*Cs2 - 3.0));
        Wzot[3] = (1.0/720.0)*(Cs2*(15.0*(Cs2 - 1.0)*Cs2 + 4.0));
        C      = LVELOCD2Q49ZOTT;
        Op     = OPPOSITED2Q49ZOTT;
        Wtmp = new double [Nneigh];
        for (size_t n=0;n<Nneigh;n++) {
            size_t xx = (size_t)( (int)fabs(C[n](0)) );
            size_t yy = (size_t)( (int)fabs(C[n](1)) );
            Wtmp[n] = Wzot[xx]*Wzot[yy];
            // std::cout << " W ["<< n <<"] = " << Wtmp[n] << std::endl;
            // std::cout << " x = "<< xx <<", y = " << yy << std::endl;
            // std::cout << " Wx = "<< Wzot[xx] <<", Wy = " << Wzot[yy] << "\n"<< std::endl;
        }
        W = (const double *) Wtmp;
        LBmodel = "D2Q49 ZOTT";
        FeqOrder = 4;
    }

    if (TheMethod==D3Q15)
    {
        Nneigh = 15;
        W      = WEIGHTSD3Q15;
        C      = LVELOCD3Q15;
        Op     = OPPOSITED3Q15;
        M.Resize(Nneigh,Nneigh);
        Minv.Resize(Nneigh,Nneigh);
        std::cout << "miu" << std::endl;
        for (size_t n=0;n<Nneigh;n++)
        for (size_t m=0;m<Nneigh;m++)
        {
            M(n,m) = MD3Q15[n][m];
        }
        Inv(M,Minv);
    }
    if (TheMethod==D3Q19)
    {
        Nneigh = 19;
        W      = WEIGHTSD3Q19;
        C      = LVELOCD3Q19;
        Op     = OPPOSITED3Q19;
    }


    if (ForceOrder==E4)
    {
        //E4
        FNneigh = 8;
        Cf     = LVELOCDFE4;
        Wf     = FWEIGHTSE4;
        ForceModel = "Isotropy Order = 4";
        epsilon = 0.0;
    }
    if (ForceOrder==E8)
    {
        //E8
        FNneigh = 24;
        Cf     = LVELOCDFE8;
        Wf     = FWEIGHTSE8;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] ; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] ; // fourth order Isotropy Tensor for psi
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 8";
        printf("%s  >>> Isotropy Order E8, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s      ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s      ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==E10)
    {
        //E10
        FNneigh = 36;
        Cf     = LVELOCDFE10;
        Wf     = FWEIGHTSE10;
        // e2    = 2.0* Wf[0] + 4.0*Wf[4] + 8.0*Wf[8] + 20.0*Wf[12]  + 16.0*Wf[20] + 18.0*Wf[24] +  40.0*Wf[28]; // Second order Isotropy Tensor for psi
        // e4    = 0.5* Wf[0] + 2.0*Wf[4] + 8.0*Wf[8] + 25.0*Wf[12]  + 32.0*Wf[20] + 40.5*Wf[24] + 100.0*Wf[28]; // fourth order Isotropy Tensor for psi
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 10";
        printf("%s  >>> Isotropy Order E10, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
        // /* Display Output in order to double-check */
        // for (size_t k=0;k<FNneigh;k++)
        // {
        //     std::cout << " Wf[" << k << "] = " << Wf[k] << ",  \n";
        // }
        // std::cout << std::endl;
    }

    if (ForceOrder==E12)
    {
        //E12
        FNneigh = 48;
        Cf     = LVELOCDFE12;
        Wf     = FWEIGHTSE12;
        
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        for (size_t n=0;n<FNneigh;n++)
        {    
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
        }

        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            // eAlpha  = 6.0*e4 - 2.0;
            // eBeta   = 6.0*e4 + 1.0;
            // epsilon = eAlpha/eBeta;
            eAlpha = (e2 + 6.0*e4)/12.0;
            eBeta  = (3.0*e4 - e2)/12.0;
            epsilon = 2.0*eBeta/eAlpha;
            // epsiC = 1.0/epsilon;
        ForceModel = "Isotropy Order = 12";
        printf("%s  >>> Isotropy Order E12, epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
        printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
        printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
        printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }

    if (ForceOrder==ELS)
    {
        //Forcing interaction-range to equal the same as lattice structure.
        FNneigh = Nneigh;
        Wff = new double [FNneigh];  
        symm_groups = new double [FNneigh];
        symm_emax = new double [FNneigh];
        // symm_groups[0] = 0.0, symm_emax[0] = 0.0, Wff[0] = 0.0;
        Cf     = C;
        // for (size_t n=1;n<FNneigh;n++){
        for (size_t n=0;n<FNneigh;n++){
            symm_emax[n] = std::max(fabs(Cf[n](0)),symm_emax[n]);
            symm_emax[n] = std::max(fabs(Cf[n](1)),symm_emax[n]);
            symm_groups[n] = Cf[n](0)*Cf[n](0) + Cf[n](1)*Cf[n](1);
            // std::cout << "\n Symmetry group["<< n << "] = " << symm_groups[n] << ", Cmax = "<< symm_emax[n] << " \n" << std::endl;
            Wff[n]     = W[n]*(1.0/(Cs2));  // sum W*C[k]^2 = 1.0
            // Wff[n]     = W[n];              // sum W*C[k]^2 = cs2
            // std::cout << " W ("<< n <<") = " << W[n] << " || Wf("<< n << ") = " << Wff[n] << "\n" << std::endl;
        }
        Wf = (const double *) Wff; // std::cout << " Wf " << Wf[1] << std::endl;
        
        ForceModel = "equal to lattice structure";
        double tmpe2 = 0.0, tmpe4=0.0;
            for (size_t k=0;k<FNneigh;k++)
            {
                tmpe2 +=  Wf[k]*(Cf[k][0]*Cf[k][0]);
                tmpe4 +=  Wf[k]*(Cf[k][0]*Cf[k][0])*(Cf[k][1]*Cf[k][1]);
            }
            e2 = tmpe2;
            e4 = tmpe4;
            eAlpha = (e2 + 6.0*e4)/12.0; // True for any lattice, and can also consider the case e2!=1
            eBeta  = (3.0*e4 - e2)/12.0;
            // double eEta = (1.0 + 3.0*e4)/20.0; // Only works for E8, it changes between lattices
            // double eGamma  = (-1.0 + 2.0*e4)/20.0;
            
            epsilon = 2.0*eBeta/eAlpha;
            // ForceModel = "Isotropy Order 4 enforce on Lattice Structure";
            printf("%s  >>>                    cs2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
            printf("%s  >>>                epsilon = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
            printf("%s  ==>                  Alpha = %g%s\n ",TERM_CLR2,eAlpha,TERM_RST);
            printf("%s  ==>                   Beta = %g%s\n ",TERM_CLR2,eBeta,TERM_RST);
            printf("%s  ==> fourth order tensor e4 = %g%s\n ",TERM_CLR2,e4,TERM_RST);
            printf("%s  ==> second order tensor e2 = %g%s\n ",TERM_CLR2,e2,TERM_RST);
    }
    

    Tau    = new double [Nl];
    epsiC  = new double [Nl];
    G   = new double [Nl];
    molMass   = new double [Nl];
    Gs  = new double [Nl];
    Rhoref = new double [Nl];
    Gmmix = new double [Nl];
    Psi    = new double [Nl];
    Gmix= 0.0;
    Gint= 0.0;

    F       = new double **** [Nl];
    Ftemp   = new double **** [Nl];
    IsFluid = new bool   **** [Nl];
    Vel     = new Vec3_t ***  [Nl];
    Velprev = new Vec3_t ***  [Nl];
    nvec    = new Vec3_t ***  [Nl];
    Fprev   = new double **** [Nl];
    BForce  = new Vec3_t ***  [Nl];
    Pij     = new Vec3_t **** [Nl];
    Pkin    = new Vec3_t **** [Nl];
    Pxx     = new double *** [Nl];
    Pyy     = new double *** [Nl];
    Pxy     = new double *** [Nl];
    Rho     = new double *** [Nl];
    RhoW     = new double *** [Nl];
    PsiS     = new double *** [Nl];
    PsiC     = new double *** [Nl];
    Nmol    = new double *** [Nl];
    Nmolprev = new double ***  [Nl];
    
    IsSolid = new bool   ***  [Nl];
    IsHBSolid = new bool   ***  [Nl];

    for (size_t i=0;i<Nl;i++)
    {
        molMass [i]    = MolarMass[i];
        Tau     [i]    = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
        epsiC   [i]    = 1.0/epsilon; //! Maybe delete this parameter.
        G       [i]    = 0.0;
        Gs      [i]    = 0.0;
        Rhoref  [i]    = 200.0;
        Gmmix   [i]    = 0.0;
        // Rhoref  [i]    = 1.0;
        Psi     [i]    = 4.0;
        // Psi     [i]    = 1.0;
        F       [i]    = new double *** [Ndim(0)];
        Ftemp   [i]    = new double *** [Ndim(0)];
        Fprev   [i]    = new double *** [Ndim(0)];
        IsFluid [i]    = new bool   *** [Ndim(0)];
        Vel     [i]    = new Vec3_t **  [Ndim(0)];
        Velprev [i]    = new Vec3_t **  [Ndim(0)];
        nvec    [i]    = new Vec3_t **  [Ndim(0)];
        BForce  [i]    = new Vec3_t **  [Ndim(0)];
        Pij     [i]    = new Vec3_t *** [Ndim(0)];
        Pkin    [i]    = new Vec3_t *** [Ndim(0)];
        Pxx     [i]    = new double **  [Ndim(0)];
        Pyy     [i]    = new double **  [Ndim(0)];
        Pxy     [i]    = new double **  [Ndim(0)];
        Rho     [i]    = new double **  [Ndim(0)];
        RhoW     [i]    = new double **  [Ndim(0)];
        PsiS    [i]    = new double **  [Ndim(0)];
        PsiC    [i]    = new double **  [Ndim(0)];
        Nmol    [i]    = new double **  [Ndim(0)];
        Nmolprev [i]    = new double **  [Ndim(0)];
        IsSolid [i]    = new bool   **  [Ndim(0)];
        IsHBSolid [i]    = new bool   **  [Ndim(0)];
        for (size_t nx=0;nx<Ndim(0);nx++)
        {
            F       [i][nx]    = new double ** [Ndim(1)];
            Ftemp   [i][nx]    = new double ** [Ndim(1)];
            Fprev   [i][nx]    = new double ** [Ndim(1)];
            IsFluid [i][nx]    = new bool   ** [Ndim(1)];
            Vel     [i][nx]    = new Vec3_t *  [Ndim(1)];
            Velprev [i][nx]    = new Vec3_t *  [Ndim(1)];
            nvec    [i][nx]    = new Vec3_t *  [Ndim(1)];
            BForce  [i][nx]    = new Vec3_t *  [Ndim(1)];
            Pij     [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pkin    [i][nx]    = new Vec3_t ** [Ndim(1)];
            Pxx     [i][nx]    = new double *  [Ndim(1)];
            Pyy     [i][nx]    = new double *  [Ndim(1)];
            Pxy     [i][nx]    = new double *  [Ndim(1)];
            Rho     [i][nx]    = new double *  [Ndim(1)];
            RhoW     [i][nx]    = new double *  [Ndim(1)];
            PsiS    [i][nx]    = new double *  [Ndim(1)];
            PsiC    [i][nx]    = new double *  [Ndim(1)];
            Nmol    [i][nx]    = new double *  [Ndim(1)];
            Nmolprev [i][nx]    = new double *  [Ndim(1)];
            IsSolid [i][nx]    = new bool   *  [Ndim(1)];
            IsHBSolid [i][nx]    = new bool   *  [Ndim(1)];
            for (size_t ny=0;ny<Ndim(1);ny++)
            {
                F       [i][nx][ny]    = new double * [Ndim(2)];
                Ftemp   [i][nx][ny]    = new double * [Ndim(2)];
                Fprev   [i][nx][ny]    = new double * [Ndim(2)];
                IsFluid [i][nx][ny]    = new bool   * [Ndim(2)];
                Vel     [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Velprev [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                nvec    [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                BForce  [i][nx][ny]    = new Vec3_t   [Ndim(2)];
                Pij     [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pkin    [i][nx][ny]    = new Vec3_t * [Ndim(2)];
                Pxx     [i][nx][ny]    = new double   [Ndim(2)];
                Pyy     [i][nx][ny]    = new double   [Ndim(2)];
                Pxy     [i][nx][ny]    = new double   [Ndim(2)];
                Rho     [i][nx][ny]    = new double   [Ndim(2)];
                RhoW    [i][nx][ny]    = new double   [Ndim(2)];
                PsiS    [i][nx][ny]    = new double   [Ndim(2)];
                PsiC    [i][nx][ny]    = new double   [Ndim(2)];
                Nmol    [i][nx][ny]    = new double   [Ndim(2)];
                Nmolprev    [i][nx][ny]    = new double   [Ndim(2)];
                IsSolid [i][nx][ny]    = new bool     [Ndim(2)];
                IsHBSolid [i][nx][ny]    = new bool     [Ndim(2)];
                for (size_t nz=0;nz<Ndim(2);nz++)
                {
                    RhoW [i][nx][ny][nz]    = 0.0;
                    Pij  [i][nx][ny][nz]    = new Vec3_t   [3];
                    Pkin [i][nx][ny][nz]    = new Vec3_t   [3];
                    F    [i][nx][ny][nz]    = new double [Nneigh];
                    Ftemp[i][nx][ny][nz]    = new double [Nneigh];
                    Fprev[i][nx][ny][nz]    = new double [Nneigh];
                    IsFluid[i][nx][ny][nz]  = new bool   [Nneigh];
                    IsSolid[i][nx][ny][nz]  = false;
                    IsHBSolid[i][nx][ny][nz]  = false;
                    for (size_t nn=0;nn<Nneigh;nn++)
                    {
                        F    [i][nx][ny][nz][nn] = 0.0;
                        Ftemp[i][nx][ny][nz][nn] = 0.0;
                        Fprev[i][nx][ny][nz][nn] = 0.0;
                        IsFluid[i][nx][ny][nz][nn] = false;
                    }
                }
            }
        }
    }

    Udist = 0;
    Udiagonal = 0;
    kAxial = 0;
    for (size_t k=1;k<Nneigh;k++)
    {
        if (C[k][0]>0)
        {
            Udist++;
            if ((C[k][1]!=0)) // Diagonal components
            {
                Udiagonal++;
            }
        }
    }
    kAxial = 2*(Udist-Udiagonal)+1; // +1 to account for k=0 

    CBB = new size_t * [Udist]; 
    CBBd = new size_t * [Udiagonal];
    for (size_t k=0;k<Udist;k++)
    {
        CBB[k] = new size_t [4];
    }
    for (size_t k=0;k<Udiagonal;k++)
    {
        CBBd[k] = new size_t [4];
    }
    
    CKA = new size_t * [kAxial];    
    for (size_t k=0;k<kAxial;k++)
    {
        CKA[k] = new size_t [2];
    }
    
    CKA[0][0]=0;
    CKA[0][1]=0;
    size_t ix=1,iy=1;
    for (size_t k=1;k<Nneigh;k++) {
            if (C[k][0]==0) CKA[ix][0]=k, ix++;
            if (C[k][1]==0) CKA[iy][1]=k, iy++;
    }
    
    latticeLength = 0;
    if (Nneigh>=FNneigh) {
        for (size_t k=1;k<Nneigh;k++) {
            size_t clength = fabs(C[k][1]);
            latticeLength = std::max(clength,latticeLength);
            
        } }
    else {
        for (size_t k=1;k<FNneigh;k++) {
            size_t clength = fabs(Cf[k][1]);
            latticeLength = std::max(clength,latticeLength);
        } }
    // latticeLength += 1;
    // std::cout << "TEST " << latticeLength << std::endl;

    OppBB = new size_t [4];
    OppBB[0] = 2;
    OppBB[1] = 3;
    OppBB[2] = 0;
    OppBB[3] = 1;

    DefBoundaryDist();
    // DefNormVec();
    ConstructInteractionArray();

    if (TheRelaxType==MRT) // Only for D2Q17 currently
    {
        if (TheMethod==D2Q17)
        {
            Nneigh = 17;
            M.Resize(Nneigh,Nneigh);
            Minv.Resize(Nneigh,Nneigh);
            
            
            S.Resize(Nneigh);
            
            for (size_t n=0;n<Nneigh;n++)
            {
                for (size_t m=0;m<Nneigh;m++)
                {
                    M(n,m) = MD2Q17[n][m];
                }
            }
            Inv(M,Minv);
            
            MAlpha    = new double **  [Nl];
            SVec    = new double *  [Nl];
            // MAlphaTemp1    = new double *  [Nneigh];
            // MAlphaTemp2    = new double *  [Nneigh];
            for (size_t i=0;i<Nl;i++)
            {
                MAlpha[i]  = new double * [Nneigh];
                SVec[i]    = new double   [Nneigh];
                for (size_t n=0;n<Nneigh;n++)
                {
                    MAlpha[i][n]   = new double [Nneigh];
                    SVec[i][n]   = 0.0;
                    // MAlphaTemp1[n]   = new double [Nneigh];
                    // MAlphaTemp2[n]   = new double [Nneigh];
                    for (size_t m=0;m<Nneigh;m++)
                    {
                        MAlpha[i][n][m] = 0.0;
                        // MAlphaTemp1[n][m]   = 0.0;
                        // MAlphaTemp2[n][m]   = 0.0;
                    }
                }
            }

            
            
            for (size_t i=0;i<Nl;i++)
            {
                auto MAlphaTemp1 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto MAlphaTemp2 = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                auto SMat = new LinAlg::Matrix<double>(Nneigh, Nneigh);
                // S = new double [Nneigh];
                double tau = nu[i]*dtR/(Cs2*dxR*dxR)+0.5;
                double BulkNu = (2.0/3.0)*(Cs2*(tau-0.5)); // (2.0/3.0)*nu[i];
                double tauBulk = (0.5 + ((2.0*BulkNu)/Cs2));
                
                S = 1.0/tau, 1.0/tauBulk, 1.0/tauBulk, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau , 1.0/tauBulk, 1.0/tauBulk, 1.0/tau, 1.0/tau;
                //~   0th  ,       1    ,      2     ,   3     ,    4   ,   5    ,    6   ,    7   ,    8   ,    9   ,   10   ,   11   ,   12   ,     13      ,     14     ,   15   ,   16   ~ Moment Order.
                // S = 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau, 1.0/tau;
                for (size_t k=0;k<Nneigh;k++)
                {
                    (*SMat)(k,k) = S(k);
                    SVec[i][k] = S(k);
                    // std::cout << SVec[i][k] << " ||";
                }
                // std::cout << std::endl;

                double a = 1.0,b = 0.0;

                LinAlg::Gemm(a, *SMat, M, b, *MAlphaTemp1);
                LinAlg::Gemm(a, Minv, *MAlphaTemp1, b, *MAlphaTemp2);
                for (size_t j=0;j<Nneigh;j++)
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        MAlpha[i][j][k]=  (*MAlphaTemp2)(j,k);// fneq2(j,k);//MAlphaTemp2(j,k);
                        // std::cout << MAlpha[i][j][k] << " ||";
                    }
                    // std::cout << std::endl;
                }

                delete(MAlphaTemp1);
                delete(MAlphaTemp2);

            }
            // printf("%s  Still running                    %s\n ",TERM_CLR2,TERM_RST);
            // cout << Matrix3i(Vector3i(2,5,6).asDiagonal()) << endl;
        }
    }
    
    // dt=dtR;
    // dt=1.0;

    EEk = new double [Nneigh];
    for (size_t k=0;k<Nneigh;k++)
    {
        EEk[k]    = 0.0;
        for (size_t n=0;n<3;n++)
        for (size_t m=0;m<3;m++)
        {
            EEk[k] += fabs(C[k][n]*C[k][m]);
        }
    }
    // Print out 
        printf("%s  Lattice Model                    = %s\n ",TERM_CLR2, LBmodel.c_str());
        printf("%s  ==> lattice sound-speed:   Cs^2 = %g%s\n ",TERM_CLR2,Cs2,TERM_RST);
        printf("%s  ==> Force Model: %s\n",TERM_CLR2, ForceModel.c_str());
        printf("%s    >> epsilon                     = %g%s\n ",TERM_CLR2,epsilon,TERM_RST);
        printf("%s  Num of cells                     = %zd%s\n ",TERM_CLR2,Nl*Ncells,TERM_RST);
#ifdef USE_OMP
    omp_init_lock(&lck);
#endif
}


inline void Domain::WriteXDMF(char const * FileKey)
{
    String fn(FileKey);
    fn.append(".h5");
    hid_t     file_id;
    file_id = H5Fcreate(fn.CStr(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    size_t  Nx = Ndim[0]/Step;
    size_t  Ny = Ndim[1]/Step;
    size_t  Nz = Ndim[2]/Step;
    
    for (size_t j=0;j<Nl;j++)
    {
        // Creating data sets
        double * Density   = new double[  Nx*Ny*Nz];
        double * SolidNode     = new double[  Nx*Ny*Nz];
        double * Vvec      = new double[3*Nx*Ny*Nz];
        double * Fvec      = new double[3*Nx*Ny*Nz];

        size_t i=0;
        #ifdef USE_OMP
        #pragma omp parallel for schedule(static) num_threads(Nproc)
        #endif
        for (size_t m=0;m<Ndim(2);m+=Step)
        for (size_t l=0;l<Ndim(1);l+=Step)
        for (size_t n=0;n<Ndim(0);n+=Step)
        {
            double rho    = 0.0;
            double gamma  = 0.0;
            Vec3_t vel    = OrthoSys::O;
            Vec3_t ForceVec    = OrthoSys::O;

            for (size_t ni=0;ni<Step;ni++)
            for (size_t li=0;li<Step;li++)
            for (size_t mi=0;mi<Step;mi++)
            {
                rho         += Rho    [j][n+ni][l+li][m+mi];
                gamma       += IsSolid[j][n+ni][l+li][m+mi] ? 1.0: 0.0;
                vel         += Vel    [j][n+ni][l+li][m+mi];
                vel         += IsSolid[j][n+ni][l+li][m+mi] ? Vec3_t(0.0,0.0,0.0) : 0.5*dt*BForce [j][n+ni][l+li][m+mi]/Rho[j][n+ni][l+li][m+mi];
                ForceVec    += BForce [j][n+ni][l+li][m+mi];
            }
            
            rho         /= Step*Step*Step;
            gamma       /= Step*Step*Step;
            vel         /= Step*Step*Step;
            ForceVec    /= Step*Step*Step;

            Density [i]  = (double) rho;
            SolidNode   [i]  = (double) gamma;
            Vvec[3*i  ]  = (double) vel(0);
            Vvec[3*i+1]  = (double) vel(1);
            Vvec[3*i+2]  = (double) vel(2);
            Fvec[3*i  ]  = (double) ForceVec(0);
            Fvec[3*i+1]  = (double) ForceVec(1);
            Fvec[3*i+2]  = (double) ForceVec(2);
            i++;
        }
        
        //Writing data to h5 file
        // Scalar Variable
        hsize_t dims[1];
        dims[0] = Nx*Ny*Nz;
        String dsname;
        dsname.Printf("Density_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Density );
        if (j==0)
        {
            dsname.Printf("SolidNode");
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,SolidNode   );
        }
        // Vector Variable
        dims[0] = 3*Nx*Ny*Nz;
        dsname.Printf("Velocity_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Vvec    );
        dims[0] = 3*Nx*Ny*Nz;
        dsname.Printf("Force_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Fvec    );
        // single dimension variables
        dims[0] = 1;
        int N[1];
        N[0] = Nx;
        dsname.Printf("Nx");
        H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);
        dims[0] = 1;
        N[0] = Ny;
        dsname.Printf("Ny");
        H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);
        dims[0] = 1;
        N[0] = Nz;
        dsname.Printf("Nz");
        H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);

        delete [] Density ;
        delete [] SolidNode   ;
        delete [] Vvec    ;
        delete [] Fvec    ;
    }

    //Closing the file
    H5Fflush(file_id,H5F_SCOPE_GLOBAL);
    H5Fclose(file_id);

	// Writing xmf fil
    std::ostringstream oss;

    //std::cout << "2" << std::endl;

    if (Ndim(2)==1)
    {
        oss << "<?xml version=\"1.0\" ?>\n";
        oss << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n";
        oss << "<Xdmf Version=\"2.0\">\n";
        oss << " <Domain>\n";
        oss << "   <Grid Name=\"mesh1\" GridType=\"Uniform\">\n";
        oss << "     <Topology TopologyType=\"2DCoRectMesh\" Dimensions=\"" << Ndim(1) << " " << Ndim(0) << "\"/>\n";
        oss << "     <Geometry GeometryType=\"ORIGIN_DXDY\">\n";
        oss << "       <DataItem Format=\"XML\" NumberType=\"Float\" Dimensions=\"2\"> 0.0 0.0\n";
        oss << "       </DataItem>\n";
        oss << "       <DataItem Format=\"XML\" NumberType=\"Float\" Dimensions=\"2\"> 1.0 1.0\n";
        oss << "       </DataItem>\n";
        oss << "     </Geometry>\n";

        for (size_t j=0;j<Nl;j++)
        {
            oss << "     <Attribute Name=\"Density_" << j << "\" AttributeType=\"Scalar\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Ndim(0) << " " << Ndim(1) << " " << Ndim(2) << "\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Density_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
            oss << "     <Attribute Name=\"Velocity_" << j << "\" AttributeType=\"Vector\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Ndim(0) << " " << Ndim(1) << " " << Ndim(2) << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Velocity_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
            oss << "     <Attribute Name=\"Force_" << j << "\" AttributeType=\"Vector\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Ndim(0) << " " << Ndim(1) << " " << Ndim(2) << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Force_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
        }

        oss << "     <Attribute Name=\"SolidNode\" AttributeType=\"Scalar\" Center=\"Node\">\n";
        oss << "       <DataItem Dimensions=\"" << Ndim(0) << " " << Ndim(1) << " " << Ndim(2) << "\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
        oss << "        " << fn.CStr() <<":/SolidNode\n";
        oss << "       </DataItem>\n";
        oss << "     </Attribute>\n";
        oss << "   </Grid>\n";
        oss << " </Domain>\n";
        oss << "</Xdmf>\n";
    }
    
    else
    {
        oss << "<?xml version=\"1.0\" ?>\n";
        oss << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n";
        oss << "<Xdmf Version=\"2.0\">\n";
        oss << " <Domain>\n";
        oss << "   <Grid Name=\"LBM_Mesh\" GridType=\"Uniform\">\n";
        oss << "     <Topology TopologyType=\"3DCoRectMesh\" Dimensions=\"" << Nz << " " << Ny << " " << Nx << "\"/>\n";
        oss << "     <Geometry GeometryType=\"ORIGIN_DXDYDZ\">\n";
        oss << "       <DataItem Format=\"XML\" NumberType=\"Float\" Dimensions=\"3\"> 0.0 0.0 0.0\n";
        oss << "       </DataItem>\n";
        oss << "       <DataItem Format=\"XML\" NumberType=\"Float\" Dimensions=\"3\"> " << Step*dx << " " << Step*dx  << " " << Step*dx  << "\n";
        oss << "       </DataItem>\n";
        oss << "     </Geometry>\n";
        for (size_t j=0;j<Nl;j++)
        {
            oss << "     <Attribute Name=\"Density_" << j << "\" AttributeType=\"Scalar\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Nz << " " << Ny << " " << Nx << "\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Density_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
            oss << "     <Attribute Name=\"Velocity_" << j << "\" AttributeType=\"Vector\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Nz << " " << Ny << " " << Nx << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Velocity_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
            oss << "     <Attribute Name=\"Force_" << j << "\" AttributeType=\"Vector\" Center=\"Node\">\n";
            oss << "       <DataItem Dimensions=\"" << Nz << " " << Ny << " " << Nx << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
            oss << "        " << fn.CStr() <<":/Force_" << j << "\n";
            oss << "       </DataItem>\n";
            oss << "     </Attribute>\n";
            //
        }
        oss << "     <Attribute Name=\"SolidNode\" AttributeType=\"Scalar\" Center=\"Node\">\n";
        oss << "       <DataItem Dimensions=\"" << Nz << " " << Ny << " " << Nx << "\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">\n";
        oss << "        " << fn.CStr() <<":/SolidNode\n";
        oss << "       </DataItem>\n";
        oss << "     </Attribute>\n";
        oss << "   </Grid>\n";
        oss << " </Domain>\n";
        oss << "</Xdmf>\n";
    }
    fn = FileKey;
    fn.append(".xmf");
    std::ofstream of(fn.CStr(), std::ios::out);
    of << oss.str();
    of.close();

}


inline void Domain::WritePARAMETERS(char const * FileKey)
{
    if(CalcPressureTensor){
        if (Time == 0) {
            printf("\n%s Calculating Pseudo-Potentials ... %s\n",TERM_CLR2,TERM_RST);
            if (Nl>1) {
                CalculatePotentials();
            }
            else {
                CalculatePotentials_SC();
            }
            printf("\n%s -- Calculating: Pressure Tensor:  Pij  ... %s\n",TERM_CLR2,TERM_RST);
            printf("%s            >>>    Storing:  Pxx, Pyy and Pxy ... %s\n",TERM_CLR2,TERM_RST);            
        }
        //* Calculate Interaction Pressure Tensor
            if (Gmix != 0.0) {
                PressureTensor();
            }
            else if (Nl==1) {
                PressureTensor_SingleComp();
            }
            // printf("\n%s !! Calculating Momentum Flux Tensor:  Pij_kin  ... %s\n",TERM_CLR2,TERM_RST);
        //* Calculate the Kinetic Part of the Total Pressure Tensor
            MomentumFluxTensor();
        
        // if (Time == 0) {
        // std::cout << "\n Checking Pressure Tensor Constants " << std::endl;
        // std::cout << " c2/2 = (" << PTconsts[0](0) << ", " << PTconsts[0](1) << ")"<< std::endl;
        // std::cout << " A    = (" << PTconsts[1](0) << ", " << PTconsts[1](1) << ")"<< std::endl;
        // std::cout << " B    = (" << PTconsts[2](0) << ", " << PTconsts[2](1) << ")"<< std::endl;
        // std::cout << " C6   = (" << PTconsts[3](0) << ", " << PTconsts[3](1) << ") \n"<< std::endl;
        // }
    }

    String fn(FileKey);
    fn.append(".h5");
    hid_t     file_id;
    file_id = H5Fcreate(fn.CStr(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    size_t  Nx = Ndim[0]/Step;
    size_t  Ny = Ndim[1]/Step;
    size_t  Nz = Ndim[2]/Step;
    
    for (size_t j=0;j<Nl;j++)
    {
        // Creating data sets
        double * Density   = new double[  Nx*Ny*Nz];
        double * SolidDensity   = new double[  Nx*Ny*Nz];
        // if(CalcPressureTensor){ % Maybe add this throughout to reduce file size...
        double * PTxx       = new double[  Nx*Ny*Nz];
        double * PTyy       = new double[  Nx*Ny*Nz];
        double * PTxy       = new double[  Nx*Ny*Nz];
        double * PTyx       = new double[  Nx*Ny*Nz];
        double * PKxx       = new double[  Nx*Ny*Nz];
        double * PKyy       = new double[  Nx*Ny*Nz];
        double * PKxy       = new double[  Nx*Ny*Nz];
        double * PKyx       = new double[  Nx*Ny*Nz];
        // }
        double * SolidNode     = new double[  Nx*Ny*Nz];
        double * Weights   = new double[  Nneigh];
        double * exyz      = new double[3*Nneigh];
        double * WeightsF   = new double[  FNneigh];
        double * eFxyz      = new double[3*FNneigh];
        double * PTc      = new double[4*3];
        double * Nvec      = new double[3*Nx*Ny*Nz];
        double * Fvec      = new double[3*Nx*Ny*Nz];

        size_t i=0;
        // #ifdef USE_OMP
        // #pragma omp parallel for schedule(static) num_threads(Nproc)
        // #endif
        for (size_t m=0;m<Ndim(2);m+=Step)
        for (size_t l=0;l<Ndim(1);l+=Step)
        for (size_t n=0;n<Ndim(0);n+=Step)
        {
            double rho    = 0.0;
            double rhoW    = 0.0;
            double pxx    = 0.0;
            double pyy    = 0.0;
            double pxy    = 0.0;
            double pyx    = 0.0;
            double mfxx   = 0.0;
            double mfyy   = 0.0;
            double mfxy   = 0.0;
            double mfyx   = 0.0;
            double gamma  = 0.0;
            Vec3_t NormVec    = OrthoSys::O;
            Vec3_t ForceVec    = OrthoSys::O;

            for (size_t ni=0;ni<Step;ni++)
            for (size_t li=0;li<Step;li++)
            for (size_t mi=0;mi<Step;mi++)
            {
                rho         += Rho      [j][n+ni][l+li][m+mi];
                if ((fabs(Gs[j])>1.0e-12)) rhoW         += RhoW      [j][n+ni][l+li][m+mi];
                // pxx         += Pxx      [j][n+ni][l+li][m+mi];
                // pyy         += Pyy      [j][n+ni][l+li][m+mi];
                // pxy         += Pxy      [j][n+ni][l+li][m+mi];
                pxx         += Pij      [j][n+ni][l+li][m+mi][0][0];
                pyy         += Pij      [j][n+ni][l+li][m+mi][1][1];
                pxy         += Pij      [j][n+ni][l+li][m+mi][0][1];
                pyx         += Pij      [j][n+ni][l+li][m+mi][1][0];
                mfxx        += Pkin     [j][n+ni][l+li][m+mi][0][0];
                mfyy        += Pkin     [j][n+ni][l+li][m+mi][1][1];
                mfxy        += Pkin     [j][n+ni][l+li][m+mi][0][1];
                mfyx        += Pkin     [j][n+ni][l+li][m+mi][1][0];
                //                                           [i][j]
                gamma       += IsSolid  [j][n+ni][l+li][m+mi] ? 1.0: 0.0;
                NormVec     += nvec     [j][n+ni][l+li][m+mi];
                ForceVec    += BForce   [j][n+ni][l+li][m+mi];
            }
            
            rho         /= Step*Step*Step;
            if ((fabs(Gs[j])>1.0e-12)) rhoW /= Step*Step*Step;
            pxx         /= Step*Step*Step;
            pyy         /= Step*Step*Step;
            pxy         /= Step*Step*Step;
            pyx         /= Step*Step*Step;
            mfxx        /= Step*Step*Step;
            mfyy        /= Step*Step*Step;
            mfxy        /= Step*Step*Step;
            mfyx        /= Step*Step*Step;
            gamma       /= Step*Step*Step;
            NormVec     /= Step*Step*Step;
            ForceVec    /= Step*Step*Step;

            Density [i]  = (double) rho;
            if ((fabs(Gs[j])>1.0e-12)) SolidDensity [i]  = (double) rhoW;
            PTxx    [i]  = (double) pxx;
            PTyy    [i]  = (double) pyy;
            PTxy    [i]  = (double) pxy;
            PTyx    [i]  = (double) pyx;
            PKxx    [i]  = (double) mfxx;
            PKyy    [i]  = (double) mfyy;
            PKxy    [i]  = (double) mfxy;
            PKyx    [i]  = (double) mfyx;
            SolidNode   [i]  = (double) gamma;
            Nvec[3*i  ]  = (double) NormVec(0);
            Nvec[3*i+1]  = (double) NormVec(1);
            Nvec[3*i+2]  = (double) NormVec(2);
            Fvec[3*i  ]  = (double) ForceVec(0);
            Fvec[3*i+1]  = (double) ForceVec(1);
            Fvec[3*i+2]  = (double) ForceVec(2);
            i++;
        }
        
        //Writing data to h5 file
        // Scalar Variable
        hsize_t dims[1];
        dims[0] = Nx*Ny*Nz;
        String dsname;
        dsname.Printf("Density_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Density );
        if ((fabs(Gs[j])>1.0e-12)) { dsname.Printf("RhoW_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,SolidDensity ); }
        dsname.Printf("Pxx_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PTxx );
        dsname.Printf("Pyy_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PTyy );
        dsname.Printf("Pxy_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PTxy );
        dsname.Printf("Pyx_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PTyx );
        dsname.Printf("Pkinxx_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PKxx );
        dsname.Printf("Pkinyy_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PKyy );
        dsname.Printf("Pkinxy_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PKxy );
        dsname.Printf("Pkinyx_%d",j);
        H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PKyx );
        // Vector Variable
            dims[0] = 3*Nx*Ny*Nz;
            dsname.Printf("Force_%d",j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Fvec    );

        // single dimension variables (double) - Multi-component
            dims[0] = 1;
            double tau[1];
            tau[0] = Tau[j];
            dsname.Printf("Tau_%d", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,tau    );

            double g[1];
            g[0] = G[j];
            dsname.Printf("G_%d", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,g    );

            if ((fabs(Gs[j])>1.0e-12)) {
                double g[1];
                g[0] = Gs[j];
                dsname.Printf("Gs_%d", j);
                H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,g    );
            }

            double Rho0[1];
            Rho0[0] = Rhoref[j];
            dsname.Printf("Rho0_%d", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Rho0    );

        if (j==0) {
            for (size_t k=0;k<Nneigh;k++) {
                Weights[k]  = (double)    W[k];
                exyz[3*k  ]  = (double) C[k][0];
                exyz[3*k+1]  = (double) C[k][1];
                exyz[3*k+2]  = (double) C[k][2];
            }
            for (size_t k=0;k<FNneigh;k++) {
                WeightsF[k]  = (double)    Wf[k];
                eFxyz[3*k  ]  = (double) Cf[k][0];
                eFxyz[3*k+1]  = (double) Cf[k][1];
                eFxyz[3*k+2]  = (double) Cf[k][2];
            }
            for (size_t k=0;k<4;k++) {
                PTc[3*k  ]  = (double) PTconsts[k][0];
                PTc[3*k+1]  = (double) PTconsts[k][1];
                PTc[3*k+2]  = (double) PTconsts[k][2];
            }
            dims[0] = Nx*Ny*Nz;
            dsname.Printf("SolidNode");
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,SolidNode   );
        
        // Vector Variable
            dims[0] = 3*Nx*Ny*Nz;
            dsname.Printf("NormVec", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Nvec    );
            
            dims[0] = 3*Nneigh;
            dsname.Printf("exyz", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,exyz    );
            
            dims[0] = 3*FNneigh;
            dsname.Printf("eFxyz", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,eFxyz    );

            dims[0] = 4*3;
            dsname.Printf("PTconsts", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,PTc    );

        // Single-Array Variable
            dims[0] = Nneigh;
            dsname.Printf("Weights", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,Weights );

            dims[0] = FNneigh;
            dsname.Printf("WeightsF", j);
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,WeightsF );
        

        // single dimension variables (double) - Single-component
            dims[0] = 1;
            double cssqrd[1];
            cssqrd[0] = Cs2;
            dsname.Printf("cs2");
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,cssqrd    );

            g[0] = Gmix;
            dsname.Printf("Gmix");
            H5LTmake_dataset_double(file_id,dsname.CStr(),1,dims,g    );
        // single dimension variables (integer)
            dims[0] = 1;
            int N[1];
            N[0] = Nx;
            dsname.Printf("Nx");
            H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);
            dims[0] = 1;
            N[0] = Ny;
            dsname.Printf("Ny");
            H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);
            dims[0] = 1;
            N[0] = Nz;
            dsname.Printf("Nz");
            H5LTmake_dataset_int(file_id,dsname.CStr(),1,dims,N);
        }

        delete [] Density ;
        delete [] SolidDensity;
        delete [] SolidNode   ;
        delete [] PTxx    ;
        delete [] PTyy    ;
        delete [] PTxy    ;
        delete [] PTyx    ;
        delete [] PKxx    ;
        delete [] PKyy    ;
        delete [] PKxy    ;
        delete [] PKyx    ;
        delete [] Nvec    ;
        delete [] Fvec    ;
        delete [] PTc     ;
    }
    
    // Closing the file
        H5Fflush(file_id,H5F_SCOPE_GLOBAL);
        H5Fclose(file_id);
}

//! Fast power Functions --------------------
    inline double Domain::Exp10T(double x) { //* Very fast Exponential Function for calculating PsiInt*
        //* due to 1024/2 = 512 -> 256 -> 128 -> 64 ... n=10 times, hence Exp10
            // Ref: codingforspeed.com/using-faster-exponential-approximation/
        x = 1.0 + x/1024.0; 
        x *= x; x *= x; x *= x; x *= x; x *= x; x *= x;
        x *= x; x *= x; x *= x; x *= x;
        return x;
    }
    //! Fast power function supressed due to inaccurate results
    // inline double Domain::PowF(double a, double b) { //! Warning: Large Error encountered due to double b. 
    //! more accuracte alternative is to compile with Flag: -ffmath-math 
    //     //* Twice as fast as Pow: calculate approximation with fraction of the exponent.
            // Ref: martin.ankerl.com/2012/01/25/optimized-approximative-pow-in-c-and-cpp/
    //     int e = (int) b;
    //     union {
    //         double d;
    //         int x[2];
    //     } u = { a };
    //     u.x[1] = (int)((b - e) * (u.x[1] - 1072632447) + 1072632447);
    //     u.x[0] = 0;
    //     double r = 1.0;
    //     while (e) {
    //         if (e & 1) r *= a;
    //         a *= a;
    //         e >>= 1;
    //     }
    //   return r * u.d;
    // }
//! END Fast power Functions --------------------


inline double Domain::Feq(size_t k, double Nmol, Vec3_t & V) {
    double VC = dot(V,C[k])/Cs2;
    double VV = dot(V,V)/Cs2; // Note: norm vel squared: |u|^2 = sqrt(dot(V,V))^2 = dot(V,V)
    double Feq1temp, Feq2temp, Feq3temp = 0.0, Feq4temp = 0.0;
    switch (FeqOrder) {//* Note: see Geq for explicitly version of feq orders -- this one with VC and VV is apprx. 10% faster!
        case 4: //* Recover full N-S equations: incl. thermal effects, see Ref. X. Shan and H. Chen, Int. J. Mod. Phys. C 18, 635 (2007)
                    // [https://www.researchgate.net/profile/Xiaowen_Shan4/publication/234356615_a_General_Multiple-Relaxation_Boltzmann_Collision_Model/links/59823f0f458515a60df7fd9b/a-General-Multiple-Relaxation-Boltzmann-Collision-Model.pdf]
            Feq4temp = (1.0/24.0)*((VC*VC*VC*VC) - (6.0*VV)*(VC*VC) + (3.0*VV*VV));
            // std::cout << "running 4rd Feq \n" << std::endl;
        case 3: //* Recover full N-S equations: incl. pressure tensor  */
            Feq3temp = (VC/6.0)*((VC*VC) - (3.0*VV));
            // std::cout << "running 3rd Feq \n" << std::endl;
        default: //* Standard equilibrium distribution function (incompressible flow) */
            Feq1temp = VC;
            Feq2temp = 0.5*((VC*VC) - (VV));
    }
    return W[k]*Nmol*(1.0 + Feq1temp + Feq2temp + Feq3temp + Feq4temp);
}

inline double Domain::Geq(size_t k, Vec3_t & V) {
    // double VdotC = dot(V,C[k]);
    // double VdotV = dot(V,V);
    double VC = dot(V,C[k])/Cs2;
    double VV = dot(V,V)/Cs2; // Note: norm vel squared: |u|^2 = sqrt(dot(V,V))^2 = dot(V,V)
    double Feq1temp, Feq2temp, Feq3temp = 0.0, Feq4temp = 0.0;
    switch (FeqOrder) {
        case 4: //* Recover full N-S equations: incl. thermal effects */
            Feq4temp = (1.0/24.0)*((VC*VC*VC*VC) - (6.0*VV)*(VC*VC) + (3.0*VV*VV));
            // Feq4temp = (1.0/24.0)*( ((VdotC*VdotC*VdotC*VdotC)/(Cs2*Cs2*Cs2*Cs2)) - (6.0*VdotV/Cs2)*((VdotC*VdotC)/(Cs2*Cs2)) + ((3.0*VdotV*VdotV)/(Cs2*Cs2)));
        case 3: //* Recover full N-S equations: incl. pressure tensor  */
            Feq3temp = (VC/6.0)*((VC*VC) - (3.0*VV));
            // Feq3temp = (VdotC/(6.0*Cs2))*(((VdotC*VdotC)/(Cs2*Cs2)) - (3.0*VdotV/Cs2));
        default: //* Standard equilibrium distribution function (incompressible flow) */
            Feq1temp = VC;
            Feq2temp = 0.5*((VC*VC) - (VV));
            // Feq1temp = VdotC/Cs2;
            // Feq2temp = 0.5*(((VdotC*VdotC)/(Cs2*Cs2)) - (VdotV/Cs2));
    }
    return W[k]*(1.0 + Feq1temp + Feq2temp + Feq3temp + Feq4temp);
}

inline double Domain::ForceFeq(size_t k, double Rho, double * F, Vec3_t & V, Vec3_t & Acc, Vec3_t * & PTij, size_t maxDim) {// Vec3_t * & PTij, size_t maxDim) {//
    double invCs2 = 1.0/Cs2;
    double VC = dot(V,C[k])*invCs2;
    double AC = dot(Acc,C[k])*invCs2; // Acc = Force/Rho
    double AV = dot(Acc,V)*invCs2;
    double Feq1temp = 0.0, Feq2temp=0.0, Feq3temp = 0.0;//, Feq4temp = 0.0;
    double H2ij = 0.0; // second order hermite polynomial (a tensor of rank n-order, in this case Rank 2)
    Vec3_t Iij[3] = { // Kronecker Delta
                        {1.0, 0.0, 0.0},
                        {0.0, 1.0, 0.0}, 
                        {0.0, 0.0, 1.0} // 1.Band (axial)
                    }; 
    Vec3_t Qij[3] = { // traceless stress tensor 
                        {0.0, 0.0, 0.0},
                        {0.0, 0.0, 0.0}, 
                        {0.0, 0.0, 0.0}
                    }; 
    switch (FeqOrder) {
        //     case 4: //* 
            // Feq4temp = 0.0; //(1.0/24.0)*((VC*VC*VC*VC) - (6.0*VV)*(VC*VC) + (3.0*VV*VV));
            // need to be derived still..
        case 3: //* see Ref. X. Shan et al JFM (2006), and K. Suga Fluid Dyn. Res. 45 (2013) 034501
            for (size_t n=0;n<maxDim;n++)// maxDim == 2 for 2D
            for (size_t m=0;m<maxDim;m++) {
                for (size_t i=1;i<Nneigh;i++) { // Automatically Skip the first Q, since Cx(0)=0 and Cy(0)=0
                    Qij[n][m] += F[i]*C[i][n]*C[i][m];//= P + puu = sum(F.ci.cj) EEk[i];//
                }
                H2ij = C[k][n]*C[k][m] - Cs2*Iij[n][m];
                // Feq3temp += (1.0/(2.0*Rho*Cs2))*(PTij[n][m] + Rho*V[n]*V[m])*(AC*H2ij - 2.0*(Acc[n]*C[k][m])*invCs2); // * If using Pij (stress -- sigma) directly // ! Need delete for-loop above
                // Feq3temp += (1.0/(2.0*Rho*Cs2))*(Qij[n][m] - Rho*Cs2*Iij[n][m] + PTij[n][m])*(AC*H2ij*invCs2 - 2.0*(Acc[n]*C[k][m])*invCs2); //* if using (F-Feq) i.e., see CollideEF_MP_Test()
                Feq3temp += (1.0/(2.0*Rho*Cs2))*(PTij[n][m] + Qij[n][m])*(AC*H2ij*invCs2 - 2.0*(Acc[n]*C[k][m])*invCs2);
                // Feq3temp += (1.0/(2.0*Rho*Cs2))*(Rho*V[n]*V[m] + PTij[n][m])*(AC*H2ij*invCs2 - 2.0*(Acc[n]*C[k][m])*invCs2); //* if using (F-Feq) i.e., see CollideEF_MP_Test()
                // Feq3temp += (1.0/(2.0*Rho*Cs2))*(Qij[n][m] - Rho*Cs2*Iij[n][m])*(AC*H2ij - 2.0*(Acc[n]*C[k][m])*invCs2); //* if using (F) i.e., see CollideEF_MP_Test_B()
            }
            // Feq3temp *= 0.5*invCs2;//(1.0/(2.0*Rho*Cs2));
        default: // The first two orders
            Feq1temp = AC;
            Feq2temp = AC*VC - AV;
    }
    return W[k]*Rho*(Feq1temp + Feq2temp + Feq3temp);
}

inline void Domain::Initialize(size_t il, iVec3_t idx, double TheRho, Vec3_t & TheVel)
{
    size_t ix = idx(0);
    size_t iy = idx(1);
    size_t iz = idx(2);
    //std::cout << Cs2 << std::endl;
    BForce[il][ix][iy][iz] = OrthoSys::O;
    Vel[il][ix][iy][iz] = OrthoSys::O;
    Rho[il][ix][iy][iz] = 0.0;
    Nmol[il][ix][iy][iz] = 0.0;
    double mMass = molMass[il];
    // if (!IsSolid[il][ix][iy][iz]) // if (!IsHBSolid[il][ix][iy][iz])
    // {
    for (size_t k=0;k<Nneigh;k++)
    {
        F[il][ix][iy][iz][k] = Feq(k,TheRho,TheVel);
    }
    // }
    // if (!IsSolid[il][ix][iy][iz]) // if (!IsHBSolid[il][ix][iy][iz])
    // {
        for (size_t k=0;k<Nneigh;k++)
        {
            if (std::isnan(F[il][ix][iy][iz][k])) F[il][ix][iy][iz][k] = 1.0e-12;
            Vel[il][ix][iy][iz]     += F[il][ix][iy][iz][k]*C[k];
            Nmol[il][ix][iy][iz]    += F[il][ix][iy][iz][k];
        }
        Rho[il][ix][iy][iz] = Nmol[il][ix][iy][iz]*mMass;
        Vel[il][ix][iy][iz] *=Cs/Nmol[il][ix][iy][iz];
    // }
    // else
    // {
    //     Vel[il][ix][iy][iz] = OrthoSys::O;
    //     Rho[il][ix][iy][iz] = TheRho*mMass;
    //     Nmol[il][ix][iy][iz] = TheRho;
    // }

    for (size_t k=0;k<Nneigh;k++)
    {
        Fprev[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];
    }
    Nmolprev[il][ix][iy][iz] = Nmol[il][ix][iy][iz];
    
    // Nmol[il][ix][iy][iz] = TheRho;
    // Rho[il][ix][iy][iz] = TheRho*mMass;
    
    // if (!IsSolid[il][ix][iy][iz]) // if (!IsHBSolid[il][ix][iy][iz])
    // {
    //     Vel[il][ix][iy][iz] = TheVel;
    //     // Nmol[il][ix][iy][iz] = TheRho;
    //     // Rho[il][ix][iy][iz] = TheRho*mMass;
    // }
    // else
    // {
    //     Vel[il][ix][iy][iz] = OrthoSys::O;
    //     // Rho[il][ix][iy][iz] = 0.0;
    //     // Nmol[il][ix][iy][iz] = 0.0;
    // }

}

inline void Domain::AssignFluidParameters(size_t il, iVec3_t idx, double TheRho, Vec3_t & TheVel)
{
    size_t ix = idx(0);
    size_t iy = idx(1);
    size_t iz = idx(2);
    // if (IsSolid[il][ix][iy][iz]) continue;
    BForce[il][ix][iy][iz] = OrthoSys::O;
    Vel[il][ix][iy][iz] = TheVel;
    Rho[il][ix][iy][iz] = TheRho*molMass[il];
    Nmol[il][ix][iy][iz] = TheRho;
    if (IsSolid[il][ix][iy][iz]) RhoW[il][ix][iy][iz] = 1.0;
    if ((il == (Nl)) && (ix == (Ndim(0)-1)) && (iy == (Ndim(1)-1))) printf("\n%s Note: Remember to call InitializeMixture in Script! %s\n",TERM_CLR1,TERM_RST);
}

inline void Domain::InitializeEF(size_t il, iVec3_t idx, double TheNmol, Vec3_t & TheVel) ///< Initialize each cell with a given density and MIXture velocity
{
    size_t ix = idx(0);
    size_t iy = idx(1);
    size_t iz = idx(2);
    //std::cout << Cs2 << std::endl;
    // BForce[il][ix][iy][iz] = OrthoSys::O;
    double mMass = molMass[il];
    // if (!IsSolid[il][ix][iy][iz]) // if (!IsHBSolid[il][ix][iy][iz])
    // {
    for (size_t k=0;k<Nneigh;k++)
    {
        F[il][ix][iy][iz][k] = Feq(k,TheNmol,TheVel);
    }
    // }
    if (!IsSolid[il][ix][iy][iz]) // if (!IsHBSolid[il][ix][iy][iz])
    {
        for (size_t k=0;k<Nneigh;k++)
        {
            if (std::isnan(F[il][ix][iy][iz][k])) F[il][ix][iy][iz][k] = 1.0e-12;
            Vel[il][ix][iy][iz]     += F[il][ix][iy][iz][k]*C[k];
            Nmol[il][ix][iy][iz]    += F[il][ix][iy][iz][k];
        }
        Rho[il][ix][iy][iz] = Nmol[il][ix][iy][iz]*mMass;
        Vel[il][ix][iy][iz] *= Cs/Rho[il][ix][iy][iz];
    }
    else
    {
        Vel[il][ix][iy][iz] = OrthoSys::O;
        Rho[il][ix][iy][iz] = 0.0;
        Nmol[il][ix][iy][iz] = 0.0;
    }

    for (size_t k=0;k<Nneigh;k++)
    {
        Fprev[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];
    }
    Nmolprev[il][ix][iy][iz] = Nmol[il][ix][iy][iz];
}


inline void Domain::InitializeMixture() ///< Initialize MIXture equilibrium Distribution
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++) {
        Vec3_t Vmix     = OrthoSys::O;
        double den      = 0.0;
        
        for (size_t il=0;il<Nl;il++) {
            Vmix += (Vel[il][ix][iy][iz] * Rho[il][ix][iy][iz] + 0.5 * dt * BForce[il][ix][iy][iz]) / Tau[il];
            den  += Rho[il][ix][iy][iz]/Tau[il];
        }
        Vmix /= den;

        for (size_t il=0;il<Nl;il++) {
            // if (!IsSolid[il][ix][iy][iz])
            // {
                double mMass = molMass[il];
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t vel = 0.0;

                for (size_t k=0;k<Nneigh;k++) {
                    F[il][ix][iy][iz][k] = Feq(k,nmol,Vmix);
                }

                Nmol[il][ix][iy][iz]    = 0.0;
                for (size_t k=0;k<Nneigh;k++) {
                    if (std::isnan(F[il][ix][iy][iz][k])) F[il][ix][iy][iz][k] = 1.0e-12;
                        vel                     += F[il][ix][iy][iz][k]*C[k];
                        Nmol[il][ix][iy][iz]    += F[il][ix][iy][iz][k];
                }
                Rho[il][ix][iy][iz] = Nmol[il][ix][iy][iz]*mMass;
                vel *=Cs/Nmol[il][ix][iy][iz];
                if (!IsSolid[il][ix][iy][iz]) Vel[il][ix][iy][iz] = vel;

                for (size_t k=0;k<Nneigh;k++) {
                    Fprev[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];
                }
                Nmolprev[il][ix][iy][iz] = Nmol[il][ix][iy][iz];
                BForce[il][ix][iy][iz] = OrthoSys::O;
            // }
        }
    }

}

inline void Domain::StorePrevious()
{
    for (size_t ix=0; ix<Ndim(0); ix++)
    for (size_t iy=0; iy<Ndim(1); iy++)
    for (size_t iz=0; iz<Ndim(2); iz++)  
    {
        for (size_t il=0;il<Nl;il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Fprev[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];
                }
                Nmolprev[il][ix][iy][iz] = Nmol[il][ix][iy][iz];
            }
        }
    }
}

inline void Domain::FluidSolidInteraction() // Force diffused boundary conditions only.
{
    tmpSum = OrthoSys::O;
    // size_t num = 0;
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t il=0;il<Nl;il++)
    for (size_t ix=0; ix<Ndim(0); ix++)
    for (size_t iy=0; iy<Ndim(1); iy++)
    for (size_t iz=0; iz<Ndim(2); iz++)  
    {
        // if (!IsSolid[il][ix][iy][iz]) continue;
        // else
        // {
        if (!IsHBSolid[il][ix][iy][iz]) continue;
            // BForce[il][ix][iy][iz] = OrthoSys::O;
            Vec3_t velB = Vel[il][ix][iy][iz];// + 0.5*dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
            // Vec3_t tmpvec(1.0,1.0,1.0);
            Vec3_t sumvec = OrthoSys::O;
            for (size_t k=0;k<Nneigh;k++)
            {
                // size_t ixc = ix + (int)C[k](0);
                // size_t iyc = iy + (int)C[k](1);
                // size_t izc = iz + (int)C[k](2);
                // if (!IsSolid[il][ixc][iyc][izc])
                // {
                if(!IsFluid[il][ix][iy][iz][k]) continue;
                    // Vec3_t velB = Vel[il][ixc][iyc][izc];
                    // tmpSum(1) += Vel[il][ixc][iyc][izc][0];
                    sumvec -= (F[il][ix][iy][iz][k]*((C[k]-velB)/dt) - Fprev[il][ix][iy][iz][Op[k]]*((C[Op[k]]-velB)/dt));
                    // sumvec += (F[il][ix][iy][iz][Op[k]] + Fprev[il][ix][iy][iz][k])*(C[Op[k]]/dt);
                    // num++;
                // }
            }
            BForce[il][ix][iy][iz] += 2.0*sumvec; // std::cout << " Fluid Solid Force(x,y)  = (" << BForce[il][ix][iy][iz][0] << ", " << BForce[il][ix][iy][iz][1] << ")" << std::endl;
            tmpSum += sumvec;
        // }
    }
    // tmpSum(2) += num;
    // std::cout << "sum of Fluid Solid Force(x)  = " << tmpSum(0) << std::endl;
}

inline void Domain::DefNormVec()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);
    
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    for (size_t il=0;il<Nl;il++)
    {
        if (IsSolid[il][ix][iy][iz]) // ! For HB modification, add IsHBSolid here and in Collision.
        {
            size_t  numA,       numD;// numDD;   // number (counters) of axial and diagonal dist
                    numA = 0,   numD = 0;// numDD = 0;
            Vec3_t tmpC(0.0,0.0,0.0);// = OrthoSys::O; // axial unit vec
            Vec3_t tmpCA(0.0,0.0,0.0);// = OrthoSys::O; // axial unit vec
            // Vec3_t tmpCD[3];
            // tmpCD[0] = 0.0,0.0,0.0;
            // tmpCD[1] = 0.0,0.0,0.0;
            // tmpCD[2] = 0.0,0.0,0.0;
            Vec3_t tmpCD[3] = { {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0} };// = OrthoSys::O; // diagonal unit vec
            bool tmptest = false;
            for (size_t k=1;k<Nneigh;k++)
            {
                size_t ixc = ix;
                size_t iyc = iy;
                size_t izc = iz;
                // std::cout << "\n numD = " << numD << "\n" << std::endl;
                // IsFluid[il][ix][iy][iz][k]=0;
                if ( (((int)ix + (int)C[k](0))>0) && (((int)ix + (int)C[k](0))<nx)) ixc = (size_t)((int)ix + (int)C[k](0));
                if ( (((int)iy + (int)C[k](1))>0) && (((int)iy + (int)C[k](1))<ny)) iyc = (size_t)((int)iy + (int)C[k](1));
                if ( (((int)iz + (int)C[k](2))>0) && (((int)iz + (int)C[k](2))<nz)) izc = (size_t)((int)iz + (int)C[k](2));

                if (!IsSolid[il][ixc][iyc][izc])
                {
                    IsFluid[il][ix][iy][iz][k] = true;//k;
                    tmptest =true;
                    // double ddx = ix-ixc;///fabs(C[k](0));
                    // double ddy = iy-iyc;///fabs(C[k](1));
                    // tmpC = C[k];
                    // if (ddx<0.0 && ddy<0.0)         tmpC(0) += C[k](0),tmpC(1) += C[k](1);
                    // else if (ddx>0.0 && ddy>0.0)    tmpC(0) += C[k](0), tmpC(1) += C[k](1);
                    // else if (ddx<0.0 && ddy>0.0)    tmpC(0) -= C[k](0), tmpC(1) -= C[k](1);
                    // else if (ddx>0.0 && ddy<0.0)    tmpC(0) -= C[k](0), tmpC(1) -= C[k](1);
                    // else tmpC += C[k];
                    tmpC += C[k];
                    // tmpC(0) += ddx;
                    // tmpC(1) += ddy;
                }
            }

            if (tmptest) 
            {

                double nnvec = sqrt(tmpC(0)*tmpC(0) + tmpC(1)*tmpC(1) + tmpC(2)*tmpC(2));
                    if (nnvec==0) 
                    {
                        std::cout << "\n ** nnvec=0, where numA = " << numA << ", numD = " << numD << std::endl;
                        std::cout << "-- tmpCA = (" << tmpCA[0] << ", " << tmpCA[1] << ") \n";
                        std::cout << "&- tmpCD[1,2] = (" << tmpCD[0][0] << ", " << tmpCD[0][1] << "), && (" << tmpCD[1][0] << ", " << tmpCD[1][1] << ") \n";
                        std::cout << ">> tmpC = (" << tmpC[0] << ", " << tmpC[1] << ") \n";
                        std::cout << "-- domain(x,y) = (" << ix << ", " << iy << ")" << std::endl;
                    }
                    // else std::cout << "\n |C(x,y)| = " << nnvec << ", C(x,y) = ("<< tmpC(0) << ", "<< tmpC(1) << ") \n" << std::endl;
                
                nvec[il][ix][iy][iz] = (1.0/nnvec)*tmpC; // Unit vector normal to wall, in the directing towards the fluid nodes
                // std::cout << "nvec(x,y) = (" << nvec[il][ix][iy][iz][0] << ", " << nvec[il][ix][iy][iz][1] << ")" << std::endl;
            }
            // else if (tmptest)
            // {
            //         std::cout << "\n ** nnvec=0, where numA = " << numA << ", numD = " << numD << std::endl;
            //         std::cout << "-- tmpCA = (" << tmpCA[0] << ", " << tmpCA[1] << ") \n";
            //         std::cout << "&- tmpCD[1,2] = (" << tmpCD[0][0] << ", " << tmpCD[0][1] << "), && (" << tmpCD[1][0] << ", " << tmpCD[1][1] << ") \n";
            //         std::cout << ">> tmpC = (" << tmpC[0] << ", " << tmpC[1] << ") \n";
            //         std::cout << "-- domain(x,y) = (" << ix << ", " << iy << ")" << std::endl;
            // }
            // std::cout << "nvec(x,y) = (" << nvec[il][ix][iy][iz][0] << ", " << nvec[il][ix][iy][iz][1] << ")" << std::endl;
        }
    }
}

inline void Domain::DefBoundaryDist()
{
    size_t iR, iL, iT, iB;
    size_t iRd, iLd, iTd, iBd;
    iR=0, iL=0, iT=0, iB=0;
    iRd=0, iLd=0, iTd=0, iBd=0;
    for (size_t k=1;k<Nneigh;k++)
    {
        // if (C[k][0]<0) // Right
        // {
        //     // if (CBB[iR][0]==0)
        //     // {
                
        //         CBB[iR][0] = k;
        //         CBB[iR][2] = Op[k];
                
        //         if (C[k][1]!=0) // Diagonal components
        //         {
        //             CBBd[iRd][0] = Op[k];
        //             CBBd[iRd][2] = Op[k];
        //             // if (CBBd[iRd][0] == CBBd[iRd][2] + 1)
        //             // {
        //                 // if (fabs(C[k][0])==fabs(C[k][1]))
        //                 // {
        //                     // CBB[iR][0] = k+1;
        //                     // CBBd[iRd][0] = k+1;
        //                     // CBB[iR+1][0] = k;
        //                     // CBBd[iRd+1][0] = k;
        //                     // std::cout << "True" << std::endl;
        //                 // }
        //                 // else
        //                 // {
        //                 //     size_t ipush = (size_t)(fabs(fabs(C[k][0])-fabs(C[k][1])));
        //                 //     CBB[iR][0] = k+ipush;
        //                 //     CBBd[iRd][0] = k+ipush;
        //                 //     CBB[iR+ipush][0] = k;
        //                 //     CBBd[iRd+ipush][0] = k;
        //                 // }
        //             // }
        //             iRd++;
        //         }
        //         // std::cout << "TEST k=" << CBB[iR][0] << ", @"<< iR << std::endl;
        //         iR++;
        //     // }
        //     // else
        //     // {
        //     //     iR++;
        //     //     if (C[k][1]!=0)iRd++;

        //     // }
        // }
        if (C[k][0]>0) // LEFT
        {
                CBB[iL][2] = k;
                CBB[iL][0] = Op[k];
                
                if ((C[k][1]!=0)) // Diagonal components
                {
                    CBBd[iLd][2] = k;
                    CBBd[iLd][0] = Op[k];
                        iLd++;
                }
                iL++;
        }
        // if (C[k][1]<0) // TOP
        // {
        //     CBB[iT][1] = k;
        //     iT++;
        //     if ((C[k][0]!=0)) // Diagonal components
        //     {
        //         CBBd[iTd][1] = k;
        //         iTd++;
        //     }
        // }
        if (C[k][1]>0)  // BOTTOM
        {
            CBB[iB][3] = k;
            CBB[iB][1] = Op[k];
            if ((C[k][0]!=0)) // Diagonal components
            {
                CBBd[iBd][3] = k;
                CBBd[iBd][1] = Op[k];
                iBd++;
            }
            iB++;
        }
        
    }

    // * Display Output in order to double-check * //
        // for (size_t k=0;k<kAxial;k++)
        // {
        //     std::cout << "known x|{ " << CKA[k][0] << " }| ";
        //     std::cout << "known y|{ " << CKA[k][1] << " }| ";
        // }

        // std::cout << std::endl;

        // std::cout << " Number of unknowns (Right,Top,Ld,Bd) = {" << iR << ", " << iT<< ", " << iLd << ", " << iBd << "}"<< std::endl;
        // std::cout << "Number of Axial Distributions  :     |{ " << kAxial-1 << " }|\n" << "^^ *(-1 since we do not consider C[k=0](0,0) as an axial component)* \n";
        // std::cout << "Number of Unknown Distributions:     |{ " << Udist << " }|\n";
        // std::cout << "               --->   Diagonals:     |{ " << Udiagonal << " }|\n";
        

        // for (size_t k=0;k<Udist;k++)
        // {
        //     std::cout << "Left |{ " << CBB[k][2] << " }|\n";
        //     std::cout << "Right|{ " << CBB[k][0] << " }|\n";
        // }
        // std::cout << std::endl;
}

void Domain::VelBB(size_t il, BoundaryLocation Location, iVec3_t idx, iVec3_t ldx, Vec3_t & VelO)
{
    size_t nx = idx(0);
    size_t ny = idx(1);
    size_t nz = idx(2);
    size_t mx = ldx(0);
    size_t my = ldx(1);
    size_t mz = ldx(2);
    size_t x=0,y=0, Loc=0;
    double dirX = 1.0;
    double dirY = 1.0;
    double vxy = 0.0;
    double vyx = 1.0;
        // vxy = false;
        if (Location == Right)      Loc = 0,dirX=-1.0,nx=mx-1,mx=latticeLength;//mx=latticeLength;//
        if (Location == Top)        Loc = 1,dirY=-1.0,ny=my-1,my=latticeLength;//my=latticeLength;//
        if (Location == Left)       Loc = 2,mx=latticeLength;
        if (Location == Bottom)     Loc = 3,my=latticeLength;
        if (Loc==0 || Loc==2) 
        {
            x=0;
            y=1;
        }
        else
        {
            x=1;
            y=0;
        }
        if (VelO(y)!=0.0)
        {
            vxy = 1.0;
            if (VelO(x)=0.0) vxy = 0.0;
        }
        
        double g[Nneigh];
        double sumUdist =0.0;
        double sumUdiag =0.0;
        double sumWdiag =0.0;
        // double sumKACY = 0.0;
            // std::cout << "TEST " << x << ", "<< y << " @"<< Loc << std::endl;
            // std::cout << "TEST @ "<< Loc << std::endl;
            // std::cout << "      nx = " << nx << ", mx = "<< mx << std::endl;
            // std::cout << "      ny = " << ny << ", my = "<< my << std::endl;
        for (size_t k=0;k<Nneigh;k++)
        {
            g[k] = Geq(k,VelO);
        }
        // for (size_t k=0;k<Nneigh;k++)
        // {
            // sumKACX      += C[k][x]*F[il][ix][iy][0][k];
            // sumKACY      += C[k][y]*g[k];
        // }
        
        for (size_t k=0;k<Udist;k++)
        {
            size_t i = CBB[k][Loc];
            size_t iopp = CBB[k][OppBB[Loc]];
            sumUdist += g[i] - g[iopp];
            // sumKACY += C[i][y]*g[i] - C[iopp][y]*g[iopp];
            // std::cout << "C(i) = {" << i << " }| ";
            // std::cout << "C(iopp) = {" << iopp << " }| ";
        }
        // std::cout << " @"<< Loc << std::endl;
        // }
        for (size_t k=0;k<Udiagonal;k++)
        {
            size_t i = CBBd[k][Loc];
            size_t iopp = CBBd[k][OppBB[Loc]];
            sumUdiag += C[i][y]*g[i] + C[iopp][y]*g[iopp];
            sumWdiag += C[i][x]*C[i][x]*W[i];
        }
            // std::cout << "Cd(i,y) = {" << i << "," << -C[i][y] << " }| \n";
            // std::cout << "Cd(iopp,y) = {" << iopp << "," << -C[iopp][y] << " }| \n";
        // }
        // std::cout << " sumWdiag = " << sumWdiag << ",  @"<< Loc << std::endl;
        // }
    
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t i=0; i<mx; i++)
    {
        for (size_t j=0; j<my; j++)
        {   
            // for (size_t il=0;il<Nl;il++)
            // {
                size_t ix = nx + i*(int)dirX;
                size_t iy = ny + j*(int)dirY;
                // if (dirX<0)std::cout << "TEST ix = " << ix << ", iy = "<< iy << " @"<< Loc << std::endl;
                if (!IsSolid[il][ix][iy][0])
                {
                    double sumKA =0.0;
                    double sumKAC =0.0;
                    double sumKdist =0.0;
                    for (size_t k=0;k<kAxial;k++)
                    {
                        size_t ii     = CKA[k][y];
                        sumKA       += F[il][ix][iy][0][ii];
                        sumKAC      += C[ii][y]*F[il][ix][iy][0][ii];
                    }

                    for (size_t k=0;k<Udist;k++)
                    {
                        size_t iopp  = CBB[k][OppBB[Loc]];
                        sumKdist    += F[il][ix][iy][0][iopp];
                    }

                    double rho      = (sumKA + 2.0*sumKdist)/(1.0 - sumUdist);
                    double EqConst  = (sumUdiag*rho - sumKAC)/(sumWdiag);

                    for (size_t k=0;k<Udist;k++)
                    {
                        size_t ii = CBB[k][Loc];
                        size_t iopp = CBB[k][OppBB[Loc]];
                        double fopp = F[il][ix][iy][0][iopp];
                        F[il][ix][iy][0][ii] = fopp + (g[ii] - g[iopp])*rho + C[ii][y]*W[ii]*EqConst;
                    }
                    
                    Vel   [il][ix][iy][0] = OrthoSys::O;
                    Rho   [il][ix][iy][0] = 0.0;
                    Nmol  [il][ix][iy][0] = 0.0;
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        Nmol[il][ix][iy][0]  +=  F[il][ix][iy][0][k];
                        Vel[il][ix][iy][0]   +=  F[il][ix][iy][0][k]*C[k];
                    }
                    // Nmol[il][ix][iy][0]     = rho;//
                    Rho[il][ix][iy][0]     = Nmol[il][ix][iy][0]*molMass[il];
                    // Vel[il][ix][iy][0]     = VelO;// 
                    Vel[il][ix][iy][0]     *= Cs/Rho[il][ix][iy][0]; //
                    // if(!vxy) Vel[il][ix][iy][0][y] = 0.0;//, std::cout << "TEST" << std::endl;
                    // Vel[il][ix][iy][0][y] *= vxy; // Account for transverse component, if flow is not -> normal to boundary.
                    // Vel[il][ix][iy][0][x] *= vyx; // Account for transverse component, if flow is not -> normal to boundary.
                // }
            }
        }
    }
}

void Domain::PressBB(size_t il, BoundaryLocation Location, iVec3_t idx, iVec3_t ldx, Array<double> TheRho)
{
    size_t nx = idx(0);
    size_t ny = idx(1);
    size_t nz = idx(2);
    size_t mx = ldx(0);
    size_t my = ldx(1);
    size_t mz = ldx(2);
    size_t x=0,y=0, Loc=0;
    double dirX = 1.0;
    double dirY = 1.0;
        if (Location == Right)      Loc = 0,dirX=-1.0,nx=mx-1,mx=latticeLength;//mx=latticeLength;
        if (Location == Top)        Loc = 1,dirY=-1.0,ny=my-1,my=latticeLength;//my=latticeLength;
        if (Location == Left)       Loc = 2,mx=latticeLength;
        if (Location == Bottom)     Loc = 3,my=latticeLength;
        if (Loc==0 || Loc==2) 
        {
            x=0;
            y=1;
        }
        else
        {
            x=1;
            y=0;
        }
        Vec3_t VelO(y,x,0.0);
        /*  --- Note ---
            VelO should be defined as (ux,uy,uz); 
            for example, ourely axial flow --> VelO(ux,0.0,0.0) 
            ------------    */
        
            // std::cout << "TEST " << x << ", "<< y << " @"<< Loc << std::endl;
            // std::cout << "TEST @ "<< Loc << std::endl;
            // std::cout << "      nx = " << nx << ", mx = "<< mx << std::endl;
            // std::cout << "      ny = " << ny << ", my = "<< my << std::endl;

        double rho = TheRho[il];
        double sumWdiag =0.0;
        for (size_t k=0;k<Udiagonal;k++)
        {
            size_t i = CBBd[k][Loc];
            size_t iopp = CBBd[k][OppBB[Loc]];
            sumWdiag += C[i][x]*C[i][x]*W[i];
        }
        // std::cout << "TEST rho = " << TheRho << std::endl;
    
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t i=0; i<mx; i++)
    {
        for (size_t j=0; j<my; j++)
        {
            // for (size_t il=0;il<Nl;il++)
            // {
                size_t ix = nx + i*(int)dirX;
                size_t iy = ny + j*(int)dirY;
                // std::cout << "TEST ix = " << ix << std::endl;
            if (!IsSolid[il][ix][iy][0])
            {
                double sumUdiag =0.0;
                double sumKA =0.0;
                double sumKAC =0.0;
                double sumKACX =0.0;
                double sumKACY =0.0;
                double sumKdist =0.0;
                for (size_t k=0;k<kAxial;k++)
                {
                    size_t ii     = CKA[k][y];
                    sumKA       += F[il][ix][iy][0][ii];
                    sumKAC      += C[ii][y]*F[il][ix][iy][0][ii];
                }
                for (size_t k=0;k<Udist;k++)
                {
                    size_t iopp  = CBB[k][OppBB[Loc]];
                    sumKdist    += F[il][ix][iy][0][iopp];
                }
                // for (size_t k=0;k<Nneigh;k++)
                // {
                //     sumKACX      += C[k][x]*F[il][ix][iy][0][k];
                //     sumKACY      += C[k][y]*F[il][ix][iy][0][k];
                // }
                // std::cout << "TEST sumX = " << sumKACX << ",  @ rho = "<< rho << std::endl;
                // std::cout << "TEST sumY = " << sumKACX << ",  @ rho = "<< rho << std::endl;

                double vel      = -1.0 + (sumKA + 2.0*sumKdist)/rho;
                // double velx = sumKACX/rho;
                // double vely = sumKACY/rho;
                // std::cout << "TEST vx = " << velx << ",  @ ix = "<< ix << std::endl;
                // std::cout << "TEST vy = " << vely << ",  @ iy = "<< iy << std::endl;
                // Vec3_t velvec(velx,vely,0.0); //
                Vec3_t velvec(vel,0.0,0.0); 
                double g[Nneigh];
                for (size_t k=0;k<Nneigh;k++)
                {
                    g[k] = Feq(k,rho,velvec);//
                    // g[k] = Geq(k,velvec);//
                }

                for (size_t k=0;k<Udiagonal;k++)
                {
                    size_t ii = CBBd[k][Loc];
                    size_t iopp = CBBd[k][OppBB[Loc]];
                    sumUdiag += C[ii][y]*g[ii] + C[iopp][y]*g[iopp];
                }

                double EqConst  = (sumUdiag - sumKAC)/(sumWdiag);

                for (size_t k=0;k<Udist;k++)
                {
                    size_t ii = CBB[k][Loc];
                    size_t iopp = CBB[k][OppBB[Loc]];
                    double fopp = F[il][ix][iy][0][iopp];
                    F[il][ix][iy][0][ii] = fopp + (g[ii] - g[iopp]) + C[ii][y]*W[ii]*EqConst;
                }
                
                Vel   [il][ix][iy][0] = OrthoSys::O;
                Rho   [il][ix][iy][0] = 0.0;
                Nmol  [il][ix][iy][0] = 0.0;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Nmol[il][ix][iy][0]  +=  F[il][ix][iy][0][k];
                    Vel[il][ix][iy][0]   +=  F[il][ix][iy][0][k]*C[k];
                }
                // Nmol[il][ix][iy][0]     = rho;//
                Rho[il][ix][iy][0]     = Nmol[il][ix][iy][0]*molMass[il];
                Vel[il][ix][iy][0]    *= Cs/Rho[il][ix][iy][0];
                // Vel[il][ix][iy][0][y] = 0.0;
            }
        }
    }
}

inline void Domain::SolidPhase(Vec3_t const & XC, double RC)
{
    for (size_t m=0; m<Ndim(0); m++)
    for (size_t n=0; n<Ndim(1); n++)
    for (size_t l=0; l<Ndim(2); l++)  
    {
        Vec3_t XX(m,n,l);
        if (norm(XX-XC) < RC)    IsSolid [0][m][n][l] = true ;                     
    }
}

inline double Domain::PsiEq(size_t il, double rho, double Rho_reference, double Psi_reference)
{
    // double Rho_ref = Rho_reference;
    // double Psi_ref = Psi_reference;
    // if (Rho_reference == 0.0) 
    // {
    //     Rho_ref = Rhoref[il]; 
    //     Psi_ref = Psi[il];
    // }
    double Rho_ref = Rhoref[il]; 
    double Psi_ref = Psi[il];
    return Psi_ref*exp(-Rho_ref/rho);
    // return sqrt(Rho_ref)*(1.0 - Psi_ref*exp(-rho/Rho_ref));

    // double a = 0.0015;
    // double b = 1.0/3000;
    //double RT= 1.0/3.0; // hence., == Cs2
    // double RT = Cs2;
    // double p=RT*(rho/(1.0-b*rho))-a*rho*rho/(1.0+rho*b);
    // if (RT*rho-p>0.0) return  sqrt( 2.0*(p - RT*rho)/(RT*G[il]));
    // else              return -sqrt(-2.0*(p - RT*rho)/(RT*G[il]));
}

inline double Domain::PsiType(size_t il, double rho, int Type) { //* Switch Case Algorithm for PseudoPotentials
    // double Psitmp = 0.0;
    double Psi_out = 0.0;
    switch (Type) {
        case 1  :
            Psi_out = rho;
            // std::cout << " Test 1 " << std::endl;
            break;
        case 2 :
            Psi_out = exp(-1.0/rho);
            // std::cout << " Test 2 " << std::endl;
            break;
        case 3 :
            Psi_out = Rhoref[il]*(1.0 - exp(-(rho/Rhoref[il])));
            // std::cout << " Test 3 " << std::endl;
            // Psi_out = Rho_ref*atan(rho/Rho_ref);
            break;
        case 4 :
            Psi_out = pow(rho/(epsilon + rho), (1.0/epsilon));
            // std::cout << " Test 4 " << std::endl;
            break;
        case 5 :
            Psi_out = pow(rho/(epsilon + pow(Rhoref[il],-epsilon)*rho), (1.0/epsilon));
            // std::cout << " Test 4 " << std::endl;
            break;
        default :
                std::cout << " No Potential Type Specified ... "<< "intra Type = " << PsiIntraType <<" inter Type = " << PsiInterType << std::endl;
    }
    double Psiout = Psi_out;
    return Psiout;
}

inline double Domain::dPsiType(size_t il, double rho, int Type) { //* Switch Case Algorithm for dPsi/dRho
    double Psi_out = 0.0;
    switch (Type) {
        case 1  :
            Psi_out = 1.0;
            // std::cout << " Test 1 " << std::endl;
            break;
        case 2 :
            Psi_out = exp(-1.0/rho)/(pow(rho, 2.0));
            // std::cout << " Test 2 " << std::endl;
            break;
        case 3 :
            Psi_out = exp(-(rho/Rhoref[il]));
            // std::cout << " Test 3 " << std::endl;
            break;
        case 4 :
            Psi_out = pow(rho/(epsilon + rho), ((1.0/epsilon) - 1.0))*(1.0/(pow((epsilon + rho),2.0)));
            // std::cout << " Test 4 " << std::endl;
            break;
        case 5 :
            Psi_out = pow(rho/(epsilon + pow(Rhoref[il],-epsilon)*rho), ((1.0/epsilon) - 1.0))*(1.0/(pow((epsilon + pow(Rhoref[il],-epsilon)*rho),2.0)));
            // std::cout << " Test 4 " << std::endl;
            break;
        default :
                std::cout << " No Potential Type Specified ... "<< "intra Type = " << PsiIntraType <<" inter Type = " << PsiInterType << std::endl;
    }
    double Psiout = Psi_out;
    return Psiout;
}

inline double Domain::PsiIntraInt(size_t il, double rho) // * Self Interaction Pseudo-Potential
{
    // double epsipow = (1.0/epsilon);
    // return pow(rho/(epsilon + rho), epsipow);
    // return pow(rho/(epsilon*(1.0 + epsiC[il]*rho)), epsipow);
    
    // return exp(-(1.0/rho));

    double Rho_ref = Rhoref[il];
    double PsiOut = Rho_ref*(1.0 - exp(-(rho/Rho_ref)));
    //     // double PsiOut = Rho_ref*(1.0 - Exp10T(-(rho/Rho_ref))); // Much faster Exp function
    //     // return sqrt(Rho_ref)*(1.0 - exp(-(rho/Rho_ref)));
    // return PsiOut;
    
    // double Psi_ref = Psi[il];
    // return Psi_ref*exp(-Rho_ref/rho);

    // return Rho_ref*atan(rho/Rho_ref);
    // return rho;

    // double epsipow = (1.0/epsilon);
    // return pow(rho/(epsilon*(1.0 + epsiC[il]*rho)), epsipow);
}

inline double Domain::PsiInterInt(size_t il, double rho) // * Cross Interaction Pseudo-Potential
{
    // double Rho_ref = Rhoref[il];
    // return sqrt(Rho_ref)*(1.0 - exp(-(rho/Rho_ref)));
    return rho;
    // double epsipow = (1.0/epsilon);
    // return pow(rho/(epsilon*(1.0 + epsiC[il]*rho)), epsipow);
}

inline void Domain::CalculatePotentials_SC() // for calculating Pressure Tensors on the fly.
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);
    if (PsiIntraType == 0){
        #ifdef USE_OMP
        #pragma omp parallel for num_threads(Nproc) schedule(static)
        #endif
        for (size_t ix=0;ix<nx;ix++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t iz=0;iz<nz;iz++)
        {
                // Reset by equal
                PsiS[0][ix][iy][iz] = 0.0;
                PsiS[0][ix][iy][iz] = PsiIntraInt(0,Rho[0][ix][iy][iz]); // * Self Interaction
        }
    }
    else{
        #ifdef USE_OMP
        #pragma omp parallel for num_threads(Nproc) schedule(static)
        #endif
        for (size_t ix=0;ix<nx;ix++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t iz=0;iz<nz;iz++)
        {
            for (size_t il=0;il<Nl;il++)
            {
                // Reset by equal
                PsiS[il][ix][iy][iz] = 0.0;
                PsiS[il][ix][iy][iz] = PsiType(il,Rho[il][ix][iy][iz],PsiIntraType); // * Self Interaction
            }
        }
    }
}

inline void Domain::CalculatePotentials() // for calculating Pressure Tensors on the fly.
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);
    if (PsiIntraType == 0 && PsiInterType == 0){
        #ifdef USE_OMP
        #pragma omp parallel for num_threads(Nproc) schedule(static)
        #endif
        for (size_t ix=0;ix<nx;ix++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t iz=0;iz<nz;iz++)
        {
            for (size_t il=0;il<Nl;il++)
            {
                // Reset by equal 
                PsiS[il][ix][iy][iz] = 0.0;
                PsiC[il][ix][iy][iz] = 0.0;

                PsiS[il][ix][iy][iz] = PsiIntraInt(il,Rho[il][ix][iy][iz]); // * Self Interaction
                PsiC[il][ix][iy][iz] = PsiInterInt(il,Rho[il][ix][iy][iz]); // * Cross Interaction
            }
        }
    }
    else{
        #ifdef USE_OMP
        #pragma omp parallel for num_threads(Nproc) schedule(static)
        #endif
        for (size_t ix=0;ix<nx;ix++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t iz=0;iz<nz;iz++)
        {
            for (size_t il=0;il<Nl;il++)
            {
                // Reset by equal 
                PsiS[il][ix][iy][iz] = 0.0;
                PsiC[il][ix][iy][iz] = 0.0;

                PsiS[il][ix][iy][iz] = PsiType(il,Rho[il][ix][iy][iz],PsiIntraType); // * Self Interaction
                PsiC[il][ix][iy][iz] = PsiType(il,Rho[il][ix][iy][iz],PsiInterType); // * Cross Interaction
            }
        }
    }
}

inline void Domain::PressureTensor_SingleComp() // * Exact Lattice Theory, Single Comp
{
    size_t nx = Ndim(0);
    size_t ny = Ndim(1);
    size_t nz = 0; //* <-- Programmed for 2D only currently! i.e., nnp and nnx are only shifted in 2D
    
    size_t kO = 0;
    size_t  maxDim = 2; //for 2D simulations

    // #ifdef USE_OMP
    // #pragma omp parallel for num_threads(Nproc) schedule(static)
    // #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++) // for (size_t iz=0;iz<nz;iz++)
    {
        // for (size_t il=0;il<Nl;il++) //! Todo in future... make it automated for any number of components.
        for (size_t i=0;i<maxDim;i++) // maxDim == 2, for 2D simulations.
        for (size_t j=0;j<maxDim;j++)
        {
            double SumPT[Nl] = {0.0}; // Reset Summation
            if (i==j && ix==0 && iy==0){
                PTconsts[0][i] = 0.0;
                PTconsts[1][i] = 0.0;
                PTconsts[2][i] = 0.0;
                PTconsts[3][i] = 0.0;
            }

            for (size_t k=kO;k<FNneigh;k++)
            {
                // symm_emax[k] // max length in symmetry group z
                double c_frac   = Wf[k]/(2.0*symm_emax[k]);
                if (symm_emax[k] == 0) c_frac = 0;
                double psiNC[Nl] = {0.0};
                double psiNnC[Nl] = {0.0};
                double psiNS[Nl] = {0.0};
                double psiNnS[Nl] = {0.0};

                size_t jx = (size_t)((int)ix + (int)Cf[k](0) + (int)Ndim(0))%Ndim(0);
                size_t jy = (size_t)((int)iy + (int)Cf[k](1) + (int)Ndim(1))%Ndim(1);

                // if (!IsSolid[0][ix][iy][nz])
                // {
                    if (i==j && ix==0 && iy==0){
                        PTconsts[0][i] += c_frac*Cf[k](i)*Cf[k](j);
                        PTconsts[1][i] += 0.5*c_frac*Cf[k](i)*Cf[k](j)*(Cf[k](0)*Cf[k](0));
                        PTconsts[3][i] += (1.0/24.0)*c_frac*Cf[k](i)*Cf[k](j)*(Cf[k](0)*Cf[k](0)*Cf[k](0)*Cf[k](0));
                    }

                    if (symm_groups[k]<=2.0)
                    {
                        //* Self Interaction
                            psiNS[0]  = PsiS[0][ix][iy][nz];
                            psiNnS[0] = PsiS[0][jx][jy][nz];

                        SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0]);

                    }
                    else if (symm_groups[k]>2.0)
                    {
                        if ( (Cf[k](0)*Cf[k](0) == Cf[k](1)*Cf[k](1)) || Cf[k](0) == 0.0 || Cf[k](1) == 0.0 ) 
                        { 
                            //* Self Interaction
                                psiNS[0]  = PsiS[0][ix][iy][nz];
                                psiNnS[0] = PsiS[0][jx][jy][nz];

                            SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0]);

                            Vec3_t nij = OrthoSys::O;
                            double pushX = 0.0;
                            double pushY = 0.0;
                            if (Cf[k][0] != 0.0) nij(0) = Cf[k](0)/symm_emax[k], pushX = Cf[k](0)/symm_emax[k];
                            if (Cf[k][1] != 0.0) nij(1) = Cf[k](1)/symm_emax[k], pushY = Cf[k](1)/symm_emax[k];
                            
                            size_t symmMax = (size_t)((int)symm_emax[k]);
                            for (size_t nn =1;nn < symmMax; nn++) 
                            { // * For all possible Force Vectors over a Unit-Area Element
                                // * positive push
                                size_t npx = (size_t)((int)ix + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t npy = (size_t)((int)iy + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                // * negative push
                                size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                
                                // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue; //! Unless Gs!=0 ... TODO implement in the Future!
                                //* Self Interaction
                                    psiNS[0]  = PsiS[0][npx][npy][nz];
                                    psiNnS[0] = PsiS[0][nnx][nny][nz];

                                SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0]);

                                if (i==j && ix==0 && iy==0){
                                    PTconsts[0][i] += c_frac*Cf[k](i)*Cf[k](j);
                                    PTconsts[1][i] += 0.5*c_frac*Cf[k](i)*Cf[k](j)*(nij(0)*nij(0) + pow((- Cf[k](0) + nij(0)),2.0));
                                    PTconsts[2][i] -= c_frac*Cf[k](i)*Cf[k](j)*(nij(0)*(- Cf[k](0) + nij(0)));
                                    PTconsts[3][i] += 0.25*c_frac*Cf[k](i)*Cf[k](j)*pow(nij(0)*(- Cf[k](0) + nij(0)),2.0);
                                }

                                if (nij(0) != 0.0) nij(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                if (nij(1) != 0.0) nij(1) += pushY; // so that +- is accounted for (cannot use nij++)
                            }
                        }
                        else if (Cf[k](0) != 0.0 && Cf[k](1) != 0.0) 
                        {//* Symmetry group: z-Hat -- Mixed Components (rare for lattices with Nneigh<25)
                            //* Self Interaction
                                psiNS[0]  = PsiS[0][ix][iy][nz];
                                psiNnS[0] = PsiS[0][jx][jy][nz];

                            //* Force Vectors 1 and 2
                            SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0]);

                            Vec3_t eUnit(1.0, 1.0, 0.0); //= signbit(Cf[k]);
                            double pushX = 1.0;
                            double pushY = 1.0;
                            if (Cf[k][0]<0.0) eUnit[0] = -1.0, pushX = -1.0;
                            if (Cf[k][1]<0.0) eUnit[1] = -1.0, pushY = -1.0;
                            
                                size_t symmMax = (size_t)((int)symm_emax[k]);
                                for (size_t nn =1;nn < symmMax; nn++) 
                                { // * For all possible Force Vectors over a Unit-Area Element
                                    double tmp_frac = 1.0;
                                    // if ((symm_groups[k]==13) && ((symm_emax[k]*2)<=(fabs(Cf[k](0))*fabs(Cf[k](1))))) tmp_frac = 2.0*(2.50 - (double)nn)/2.0;
                                    if ((symm_groups[k]==13)) tmp_frac = 2.0*(2.50 - (double)nn)/2.0;
                                    // if (symm_groups[k]==10) tmp_frac = 2.0*((double)nn/(double)symmMax);
                                    // if (symm_groups[k]==10) tmp_frac = (double)nn + ((double)nn -1.0 - 2.0*(double)nn/symmMax);
                                    // * positive push
                                    size_t npx = (size_t)((int)ix + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                    size_t npy = (size_t)((int)iy + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                    // * negative push
                                    size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                    size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                    
                                    // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue;

                                    //* Self Interaction
                                        psiNS[0]  = PsiS[0][npx][npy][nz];
                                        psiNnS[0] = PsiS[0][nnx][nny][nz];

                                    //* Force Vectors 3 and 4
                                    SumPT[0] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0]);

                                    if (i==j && ix==0 && iy==0){
                                        PTconsts[0][i] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j);
                                        PTconsts[1][i] += tmp_frac*0.5*c_frac*Cf[k](i)*Cf[k](j)*(eUnit(0)*eUnit(0) + pow((- Cf[k](0) + eUnit(0)),2.0));
                                        PTconsts[2][i] -= tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(eUnit(0)*(- Cf[k](0) + eUnit(0)));
                                        PTconsts[3][i] += tmp_frac*0.25*c_frac*Cf[k](i)*Cf[k](j)*pow(eUnit(0)*(- Cf[k](0) + eUnit(0)),2.0);
                                    }
                                    if ((eUnit(0) - Cf[k](0)) != 0.0) eUnit(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                    if ((eUnit(1) - Cf[k](1)) != 0.0) eUnit(1) += pushY; // so that +- is accounted for (cannot use nij++)
                                }
                        }
                    }
                // }
                
            }
            // if (i==1 && i==j && ix==0 && iy==0){ //* Display only once after all components have been calculated.
            //     std::cout << "\n Checking Pressure Tensor Constants " << std::endl;
            //     std::cout << " c2 = (" << PTconsts[0](0) << ", " << PTconsts[0](1) << ")"<< std::endl;
            //     std::cout << " A  = (" << PTconsts[1](0) << ", " << PTconsts[1](1) << ")"<< std::endl;
            //     std::cout << " B  = (" << PTconsts[2](0) << ", " << PTconsts[2](1) << ")"<< std::endl;
            //     std::cout << " C6  = (" << PTconsts[3](0) << ", " << PTconsts[3](1) << ") \n"<< std::endl;
            // }

            if (!IsSolid[0][ix][iy][nz])
            {
                Pij[0][ix][iy][nz][i][j] = SumPT[0];
            }
        }
    }
}

inline void Domain::PressureTensor() // * Exact Lattice Theory
{
    size_t nx = Ndim(0);
    size_t ny = Ndim(1);
    size_t nz = 0; //* <-- Programmed for 2D only currently! i.e., nnp and nnx are only shifted in 2D
    
    size_t kO = 0;
        // if (FNneigh == Nneigh) kO = 1; // Checking if using directly on-lattice (kO=1 to skip Q = 0) or a specific forcing model (e.g., E8 or E10)
    size_t  maxDim = 2; //for 2D simulations

    // #ifdef USE_OMP
    // #pragma omp parallel for num_threads(Nproc) schedule(static)
    // #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++) // for (size_t iz=0;iz<nz;iz++)
    {
        // for (size_t il=0;il<Nl;il++) //! Todo in future... make it automated for any number of components.
        for (size_t i=0;i<maxDim;i++) // maxDim == 2, for 2D simulations.
        for (size_t j=0;j<maxDim;j++)
        {
            double SumPT[Nl] = {0.0}; // Reset Summation
            if (i==j && ix==0 && iy==0){
                PTconsts[0][i] = 0.0;
                PTconsts[1][i] = 0.0;
                PTconsts[2][i] = 0.0;
                PTconsts[3][i] = 0.0;
            }

            for (size_t k=kO;k<FNneigh;k++)
            {
                // symm_emax[k] // max length in symmetry group z
                double c_frac   = Wf[k]/(2.0*symm_emax[k]);
                if (symm_emax[k] == 0) c_frac = 0;
                double psiNC[Nl] = {0.0};
                double psiNnC[Nl] = {0.0};
                double psiNS[Nl] = {0.0};
                double psiNnS[Nl] = {0.0};
                double Gc0 = 0.0, Gc1 = 0.0; // * cross interactions
                size_t jx = (size_t)((int)ix + (int)Cf[k](0) + (int)Ndim(0))%Ndim(0);
                size_t jy = (size_t)((int)iy + (int)Cf[k](1) + (int)Ndim(1))%Ndim(1);

                // if (IsSolid[0][ix][iy][nz] || IsSolid[0][jx][jy][nz] || IsSolid[1][ix][iy][nz] || IsSolid[1][jx][jy][nz]) continue;
                // if (!IsSolid[0][ix][iy][nz] || !IsSolid[1][jx][jy][nz])
                // {
                    // || !IsSolid[1][ix][iy][nz] || !IsSolid[1][jx][jy][nz]){ (if solid for one comp, then it must be solid for the other!)
                    // if (i==0 && j==0 && ix==0 && iy==0 ){
                    //     std::cout << "\n Checking Interaction at x=y=0, dim("<< nx <<", "<< ny <<") for E[" << k << "]" << std::endl;
                    // }

                    if (i==j && ix==0 && iy==0){
                        PTconsts[0][i] += c_frac*Cf[k](i)*Cf[k](j);
                        PTconsts[1][i] += 0.5*c_frac*Cf[k](i)*Cf[k](j)*(Cf[k](0)*Cf[k](0));
                        PTconsts[3][i] += (1.0/24.0)*c_frac*Cf[k](i)*Cf[k](j)*(Cf[k](0)*Cf[k](0)*Cf[k](0)*Cf[k](0));
                    }

                    if (symm_groups[k]<=2.0)
                    {
                        //* Cross Interaction
                            psiNC[0]  = PsiC[0][ix][iy][nz];
                            psiNC[1]  = PsiC[1][ix][iy][nz];
                            psiNnC[0] = PsiC[0][jx][jy][nz];
                            psiNnC[1] = PsiC[1][jx][jy][nz];
                        //* Self Interaction
                            psiNS[0]  = PsiS[0][ix][iy][nz];
                            psiNS[1]  = PsiS[1][ix][iy][nz];
                            psiNnS[0] = PsiS[0][jx][jy][nz];
                            psiNnS[1] = PsiS[1][jx][jy][nz];
                        Gc0 = Gmix;
                        Gc1 = Gmix;
                            
                        //* Solid Boundaries
                            // if (IsSolid[0][ix][iy][nz]) psiNS[0] = 0.0, psiNC[0] = RhoW[0][ix][iy][nz], Gc0 = Gs[0];//
                            // if (IsSolid[0][jx][jy][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][jx][jy][nz], Gc0 = Gs[0];//
                            // if (IsSolid[1][ix][iy][nz]) psiNS[1] = 0.0, psiNC[1] = RhoW[1][ix][iy][nz], Gc1 = Gs[1];//
                            // if (IsSolid[1][jx][jy][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][jx][jy][nz], Gc1 = Gs[1];//

                            if (IsSolid[0][jx][jy][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][jx][jy][nz], psiNC[0] = Rho[0][ix][iy][nz], Gc0 = Gs[0];//
                            if (IsSolid[0][ix][iy][nz]) psiNS[0] = 0.0, psiNC[0] = 0.0, Gc0 = Gs[0];//
                            if (IsSolid[1][jx][jy][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][jx][jy][nz], psiNC[1] = Rho[1][ix][iy][nz], Gc1 = Gs[1];//
                            if (IsSolid[1][ix][iy][nz]) psiNS[1] = 0.0, psiNC[1] = 0.0, Gc1 = Gs[1];//

                        SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);

                        SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);

                    }
                    else if (symm_groups[k]>2.0)
                    {
                        if ( (Cf[k](0)*Cf[k](0) == Cf[k](1)*Cf[k](1)) || Cf[k](0) == 0.0 || Cf[k](1) == 0.0 ) 
                        { //* Symmetry group: z-Bar -- purely Axial or Diagonal
                            // c_frac = Wf[k]/4.0;
                            //* Cross Interaction
                                psiNC[0]  = PsiC[0][ix][iy][nz];
                                psiNC[1]  = PsiC[1][ix][iy][nz];
                                psiNnC[0] = PsiC[0][jx][jy][nz];
                                psiNnC[1] = PsiC[1][jx][jy][nz];
                            //* Self Interaction
                                psiNS[0]  = PsiS[0][ix][iy][nz];
                                psiNS[1]  = PsiS[1][ix][iy][nz];
                                psiNnS[0] = PsiS[0][jx][jy][nz];
                                psiNnS[1] = PsiS[1][jx][jy][nz];
                            Gc0 = Gmix;
                            Gc1 = Gmix;

                            //* Solid Boundaries
                                if (IsSolid[0][jx][jy][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][jx][jy][nz], psiNC[0] = Rho[0][ix][iy][nz], Gc0 = Gs[0];//
                                if (IsSolid[0][ix][iy][nz]) psiNS[0] = 0.0, psiNC[0] = 0.0, Gc0 = Gs[0];//
                                if (IsSolid[1][jx][jy][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][jx][jy][nz], psiNC[1] = Rho[1][ix][iy][nz], Gc1 = Gs[1];//
                                if (IsSolid[1][ix][iy][nz]) psiNS[1] = 0.0, psiNC[1] = 0.0, Gc1 = Gs[1];//

                            SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);

                            SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);

                            Vec3_t nij = OrthoSys::O;
                            double pushX = 0.0;
                            double pushY = 0.0;
                            if (Cf[k][0] != 0.0) nij(0) = Cf[k](0)/symm_emax[k], pushX = Cf[k](0)/symm_emax[k];
                            if (Cf[k][1] != 0.0) nij(1) = Cf[k](1)/symm_emax[k], pushY = Cf[k](1)/symm_emax[k];
                            
                            // double frac_tmp = 1.0;
                            // if (symm_emax[k]>2.0) frac_tmp = 1.0/symm_emax[k];
                            size_t symmMax = (size_t)((int)symm_emax[k]);
                            for (size_t nn =1;nn < symmMax; nn++) 
                            { // * For all possible Force Vectors over a Unit-Area Element
                                    // double tmp_frac = 1.0;
                                    // if (symm_groups[k]>=9) tmp_frac = 2.0*((double)nn/(double)symmMax);
                                // * positive push
                                size_t npx = (size_t)((int)ix + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t npy = (size_t)((int)iy + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                // * negative push
                                size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                
                                // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue; //! Unless Gs!=0 ... TODO implement in the Future!

                                //* Cross Interaction
                                    psiNC[0]  = PsiC[0][npx][npy][nz];
                                    psiNC[1]  = PsiC[1][npx][npy][nz];
                                    psiNnC[0] = PsiC[0][nnx][nny][nz];
                                    psiNnC[1] = PsiC[1][nnx][nny][nz];
                                //* Self Interaction
                                    psiNS[0]  = PsiS[0][npx][npy][nz];
                                    psiNS[1]  = PsiS[1][npx][npy][nz];
                                    psiNnS[0] = PsiS[0][nnx][nny][nz];
                                    psiNnS[1] = PsiS[1][nnx][nny][nz];
                                Gc0 = Gmix;
                                Gc1 = Gmix;
                                
                                //* Solid Boundaries
                                    // if (IsSolid[0][npx][npy][nz]) psiNS[0] = 0.0, psiNC[0] = RhoW[0][npx][npy][nz], Gc0 = Gs[0];//
                                    // if (IsSolid[0][nnx][nny][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][nnx][nny][nz], Gc0 = Gs[0];//
                                    // if (IsSolid[1][npx][npy][nz]) psiNS[1] = 0.0, psiNC[1] = RhoW[1][npx][npy][nz], Gc1 = Gs[1];//
                                    // if (IsSolid[1][nnx][nny][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][nnx][nny][nz], Gc1 = Gs[1];//

                                    if (IsSolid[0][nnx][nny][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][nnx][nny][nz], psiNC[0] = Rho[0][npx][npy][nz], Gc0 = Gs[0];//
                                    if (IsSolid[0][npx][npy][nz]) psiNS[0] = 0.0, psiNC[0] = 0.0, Gc0 = Gs[0];//
                                    if (IsSolid[1][nnx][nny][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][nnx][nny][nz], psiNC[1] = Rho[1][npx][npy][nz], Gc1 = Gs[1];//
                                    if (IsSolid[1][npx][npy][nz]) psiNS[1] = 0.0, psiNC[1] = 0.0, Gc1 = Gs[1];//

                                SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);

                                SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);

                                if (i==j && ix==0 && iy==0){
                                    PTconsts[0][i] += c_frac*Cf[k](i)*Cf[k](j);
                                    PTconsts[1][i] += 0.5*c_frac*Cf[k](i)*Cf[k](j)*(nij(0)*nij(0) + pow((- Cf[k](0) + nij(0)),2.0));
                                    PTconsts[2][i] -= c_frac*Cf[k](i)*Cf[k](j)*(nij(0)*(- Cf[k](0) + nij(0)));
                                    PTconsts[3][i] += 0.25*c_frac*Cf[k](i)*Cf[k](j)*pow(nij(0)*(- Cf[k](0) + nij(0)),2.0);
                                }
                                
                                // if (i==0 && j==0 && ix==0 && iy==0 ){
                                //     std::cout << "\n Checking Interaction at x=y=0, dim("<< nx <<", "<< ny <<") for E[" << k << "]" << std::endl;
                                //     std::cout << " E = (" << Cf[k](0) << ", " << Cf[k](1) << ")"<< std::endl;
                                //     std::cout << " > for the" << nn <<"th vector: "<< std::endl;
                                //     std::cout << " >>  n_push (" << nij(0) << ", " << nij(1) << ")" << std::endl;
                                //     std::cout << " >>  n_neg  (" << - (int)Cf[k](0) + (int)nij(0) << ", " << - (int)Cf[k](1) + (int)nij(1) << ") \n" << std::endl;
                                // }
                                
                                if (nij(0) != 0.0) nij(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                if (nij(1) != 0.0) nij(1) += pushY; // so that +- is accounted for (cannot use nij++)

                            }
                        }
                        else if (Cf[k](0) != 0.0 && Cf[k](1) != 0.0) 
                        {//* Symmetry group: z-Hat -- Mixed Components (rare for lattices with Nneigh<25)
                            // size_t symmMax = 2;//(size_t)((int)symm_emax[k]*2);
                            // if ((symm_emax[k]*2)<=(fabs(Cf[k](0))*fabs(Cf[k](1)))) symmMax ++;
                            // double tmp_frac = Wf[k]/(2*symmMax);//4.0;
                            // double tmp_frac = Wf[k]/(2.0*4.0);
                            //* Cross Interaction
                                psiNC[0]  = PsiC[0][ix][iy][nz];
                                psiNC[1]  = PsiC[1][ix][iy][nz];
                                psiNnC[0] = PsiC[0][jx][jy][nz];
                                psiNnC[1] = PsiC[1][jx][jy][nz];
                            //* Self Interaction
                                psiNS[0]  = PsiS[0][ix][iy][nz];
                                psiNS[1]  = PsiS[1][ix][iy][nz];
                                psiNnS[0] = PsiS[0][jx][jy][nz];
                                psiNnS[1] = PsiS[1][jx][jy][nz];

                            Gc0 = Gmix;
                            Gc1 = Gmix;
                            
                            //* Solid Boundaries
                                if (IsSolid[0][jx][jy][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][jx][jy][nz], psiNC[0] = Rho[0][ix][iy][nz], Gc0 = Gs[0];//
                                if (IsSolid[0][ix][iy][nz]) psiNS[0] = 0.0, psiNC[0] = 0.0, Gc0 = Gs[0];//
                                if (IsSolid[1][jx][jy][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][jx][jy][nz], psiNC[1] = Rho[1][ix][iy][nz], Gc1 = Gs[1];//
                                if (IsSolid[1][ix][iy][nz]) psiNS[1] = 0.0, psiNC[1] = 0.0, Gc1 = Gs[1];//

                            //* Force Vectors 1 and 2
                            SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);

                            SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);
                            Vec3_t eUnit(1.0, 1.0, 0.0); //= signbit(Cf[k]);
                            double pushX = 1.0;
                            double pushY = 1.0;
                            if (Cf[k][0]<0.0) eUnit[0] = -1.0, pushX = -1.0;
                            if (Cf[k][1]<0.0) eUnit[1] = -1.0, pushY = -1.0;
                            
                            // size_t px = (size_t)(fabs(Cf[k](0)));
                            // size_t py = (size_t)(fabs(Cf[k](1)));
                            
                            // for (size_t ixx =1;ixx < px; ixx++) 
                            // {
                            //     pushX += ((int)ixx - 1)*tmp_pushX;
                                size_t symmMax = (size_t)((int)symm_emax[k]);
                                for (size_t nn =1;nn < symmMax; nn++) 
                                { // * For all possible Force Vectors over a Unit-Area Element
                                    double tmp_frac = 1.0;
                                    // if ((symm_groups[k]==13) && ((symm_emax[k]*2)<=(fabs(Cf[k](0))*fabs(Cf[k](1))))) tmp_frac = 2.0*(2.50 - (double)nn)/2.0;
                                    if ((symm_groups[k]==13)) tmp_frac = 2.0*(2.50 - (double)nn)/2.0;
                                    // if (symm_groups[k]==10) tmp_frac = 2.0*((double)nn/(double)symmMax);
                                    // if (symm_groups[k]==10) tmp_frac = (double)nn + ((double)nn -1.0 - 2.0*(double)nn/symmMax);
                                    // pushY += ((int)iyy -1)*tmp_pushY;
                                    // * positive push
                                    size_t npx = (size_t)((int)ix + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                    size_t npy = (size_t)((int)iy + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                    // * negative push
                                    size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                    size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                    
                                    // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue;

                                    //* Cross Interaction
                                        psiNC[0]  = PsiC[0][npx][npy][nz];
                                        psiNC[1]  = PsiC[1][npx][npy][nz];
                                        psiNnC[0] = PsiC[0][nnx][nny][nz];
                                        psiNnC[1] = PsiC[1][nnx][nny][nz];
                                    //* Self Interaction
                                        psiNS[0]  = PsiS[0][npx][npy][nz];
                                        psiNS[1]  = PsiS[1][npx][npy][nz];
                                        psiNnS[0] = PsiS[0][nnx][nny][nz];
                                        psiNnS[1] = PsiS[1][nnx][nny][nz];
                                    
                                    Gc0 = Gmix;
                                    Gc1 = Gmix;
                                    
                                    //* Solid Boundaries

                                        if (IsSolid[0][nnx][nny][nz]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][nnx][nny][nz], psiNC[0] = Rho[0][npx][npy][nz], Gc0 = Gs[0];//
                                        if (IsSolid[0][npx][npy][nz]) psiNS[0] = 0.0, psiNC[0] = 0.0, Gc0 = Gs[0];//
                                        if (IsSolid[1][nnx][nny][nz]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][nnx][nny][nz], psiNC[1] = Rho[1][npx][npy][nz], Gc1 = Gs[1];//
                                        if (IsSolid[1][npx][npy][nz]) psiNS[1] = 0.0, psiNC[1] = 0.0, Gc1 = Gs[1];//

                                    //* Force Vectors 3 and 4
                                    SumPT[0] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);

                                    SumPT[1] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);

                                    if (i==j && ix==0 && iy==0){
                                        PTconsts[0][i] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j);
                                        PTconsts[1][i] += tmp_frac*0.5*c_frac*Cf[k](i)*Cf[k](j)*(eUnit(0)*eUnit(0) + pow((- Cf[k](0) + eUnit(0)),2.0));
                                        PTconsts[2][i] -= tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(eUnit(0)*(- Cf[k](0) + eUnit(0)));
                                        PTconsts[3][i] += tmp_frac*0.25*c_frac*Cf[k](i)*Cf[k](j)*pow(eUnit(0)*(- Cf[k](0) + eUnit(0)),2.0);
                                    }
                                    // if (i==0 && j==0 && ix==0 && iy==0){// && symmMax>=3 ){
                                    //     std::cout << "\n Checking Interaction at x=y=0, dim("<< nx <<", "<< ny <<") for E[" << k << "]" << std::endl;
                                    //     std::cout << " E = (" << Cf[k](0) << ", " << Cf[k](1) << ")"<< std::endl;
                                    //     std::cout << " > tmpFrac " << tmp_frac << ", Symm Group" << symm_groups[k] << std::endl;
                                    //     std::cout << " > for the" << nn <<"th vector: "<< std::endl;
                                    //     std::cout << " >>  n_push (" << eUnit(0) << ", " << eUnit(1) << ")" << std::endl;
                                    //     std::cout << " >>  n_neg  (" << - (int)Cf[k](0) + (int)eUnit(0) << ", " << - (int)Cf[k](1) + (int)eUnit(1) << ") \n" << std::endl;
                                    // }
                                    // frac_count += 1.0/4.0;
                                    if ((eUnit(0) - Cf[k](0)) != 0.0) eUnit(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                    if ((eUnit(1) - Cf[k](1)) != 0.0) eUnit(1) += pushY; // so that +- is accounted for (cannot use nij++)
                                }
                            // }
                            // if (symm_groups[k]==10 && i==0 && j==0  && ix==0 && iy==0) std::cout << "checking ... frack = "<< frac_count << std::endl;
                        }
                    }
                // }
                
            }

            if (!IsSolid[0][ix][iy][nz] || !IsSolid[1][ix][iy][nz])
            {
                Pij[0][ix][iy][nz][i][j] = SumPT[0];
                Pij[1][ix][iy][nz][i][j] = SumPT[1];
            }
        }
    }
}

inline void Domain::PressureTensorCalc() //(size_t ix, size_t iy,  size_t nz, size_t i, size_t j) //* for calculating Pressure Tensors on the fly.
{
    size_t nx = Ndim(0);
    size_t ny = Ndim(1);
    size_t nz = 0; //* <-- Programmed for 2D only currently!
    
    size_t kO = 0;
        // if (FNneigh == Nneigh) kO = 1; // Checking if using directly on-lattice (kO=1 to skip Q = 0) or a specific forcing model (e.g., E8 or E10)
    size_t  maxDim = 2; //for 2D simulations

    #ifdef USE_OMP
    #pragma omp parallel for num_threads(Nproc) schedule(static)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++) // for (size_t iz=0;iz<nz;iz++)
    {
        // for (size_t il=0;il<Nl;il++) //! Todo in future... make it automated for any number of components.
        for (size_t i=0;i<maxDim;i++) // maxDim == 2, for 2D simulations.
        for (size_t j=0;j<maxDim;j++)
        {
            double SumPT[Nl] = {0.0}; // Reset Summation
            for (size_t k=kO;k<FNneigh;k++)
            {
                // symm_emax[k] // max length in symmetry group z
                double c_frac   = Wf[k]/(2.0*symm_emax[k]);
                if (symm_emax[k] == 0) c_frac = 0;
                double psiNC[Nl] = {0.0};
                double psiNnC[Nl] = {0.0};
                double psiNS[Nl] = {0.0};
                double psiNnS[Nl] = {0.0};

                size_t jx = (size_t)((int)ix + (int)Cf[k](0) + (int)Ndim(0))%Ndim(0);
                size_t jy = (size_t)((int)iy + (int)Cf[k](1) + (int)Ndim(1))%Ndim(1);

                // if (IsSolid[0][ix][iy][nz] || IsSolid[0][jx][jy][nz] || IsSolid[1][ix][iy][nz] || IsSolid[1][jx][jy][nz]) continue;
                if (!IsSolid[0][ix][iy][nz] || !IsSolid[0][jx][jy][nz])
                {// || !IsSolid[1][ix][iy][nz] || !IsSolid[1][jx][jy][nz]){ (if solid for one comp, then it must be solid for the other!)

                        //* Cross Interaction
                            psiNC[0]  = PsiC[0][ix][iy][nz];
                            psiNC[1]  = PsiC[1][ix][iy][nz];
                            psiNnC[0] = PsiC[0][jx][jy][nz];
                            psiNnC[1] = PsiC[1][jx][jy][nz];
                        //* Self Interaction
                            psiNS[0]  = PsiS[0][ix][iy][nz];
                            psiNS[1]  = PsiS[1][ix][iy][nz];
                            psiNnS[0] = PsiS[0][jx][jy][nz];
                            psiNnS[1] = PsiS[1][jx][jy][nz];

                        SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gmix*psiNC[0]*psiNnC[1]);
                        SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gmix*psiNC[1]*psiNnC[0]);

                    if (symm_groups[k]>2.0) {
                        if ( (Cf[k](0)*Cf[k](0) == Cf[k](1)*Cf[k](1)) || Cf[k](0) == 0.0 || Cf[k](1) == 0.0 ) 
                        { //* Symmetry group: z-Bar -- purely Axial or Diagonal
                            
                            Vec3_t nij = OrthoSys::O;
                            double pushX = 0.0;
                            double pushY = 0.0;
                            if (Cf[k][0] != 0.0) nij(0) = Cf[k](0)/symm_emax[k], pushX = Cf[k](0)/symm_emax[k];
                            if (Cf[k][1] != 0.0) nij(1) = Cf[k](1)/symm_emax[k], pushY = Cf[k](1)/symm_emax[k];
                            
                            double frac_tmp = 1.0;
                            // if (symm_emax[k]>2.0) frac_tmp = 1.0/symm_emax[k];
                            size_t symmMax = (size_t)((int)symm_emax[k]);
                            for (size_t nn =1;nn < symmMax; nn++) { // * For all possible Force Vectors over a Unit-Area Element
                                
                                // * positive push
                                size_t npx = (size_t)((int)ix + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t npy = (size_t)((int)iy + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                // * negative push
                                size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)nij(0) + (int)Ndim(0))%Ndim(0);
                                size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)nij(1) + (int)Ndim(1))%Ndim(1);
                                
                                // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue; //! Unless Gs!=0 ... TODO implement in the Future!

                                //* Cross Interaction
                                    psiNC[0]  = PsiC[0][npx][npy][nz];
                                    psiNC[1]  = PsiC[1][npx][npy][nz];
                                    psiNnC[0] = PsiC[0][nnx][nny][nz];
                                    psiNnC[1] = PsiC[1][nnx][nny][nz];
                                //* Self Interaction
                                    psiNS[0]  = PsiS[0][npx][npy][nz];
                                    psiNS[1]  = PsiS[1][npx][npy][nz];
                                    psiNnS[0] = PsiS[0][nnx][nny][nz];
                                    psiNnS[1] = PsiS[1][nnx][nny][nz];

                                SumPT[0] += c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gmix*psiNC[0]*psiNnC[1]);

                                SumPT[1] += c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gmix*psiNC[1]*psiNnC[0]);
                                
                                if (nij(0) != 0.0) nij(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                if (nij(1) != 0.0) nij(1) += pushY; // so that +- is accounted for (cannot use nij++)
                                // frac_tmp += 1.0/symm_emax[k];
                            }
                        }
                        else if (Cf[k](0) != 0.0 && Cf[k](1) != 0.0) 
                        {//* Symmetry group: z-Hat -- Mixed Components (rare for lattices with Nneigh<25)

                            Vec3_t eUnit(1.0, 1.0, 0.0); //= signbit(Cf[k]);
                            double pushX = 1.0;
                            double pushY = 1.0;
                            if (Cf[k][0]<0.0) eUnit[0] = -1.0, pushX = -1.0;
                            if (Cf[k][1]<0.0) eUnit[1] = -1.0, pushY = -1.0;
                            
                            size_t symmMax = (size_t)((int)symm_emax[k]);
                            for (size_t nn =1;nn < symmMax; nn++) { // * For all possible Force Vectors over a Unit-Area Element
                                double tmp_frac = 1.0;
                                if ((symm_groups[k]==13) && ((symm_emax[k]*2)<=(fabs(Cf[k](0))*fabs(Cf[k](1))))) tmp_frac = 2.0*(2.50 - (double)nn)/2.0;
                                
                                // * positive push
                                size_t npx = (size_t)((int)ix + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                size_t npy = (size_t)((int)iy + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                // * negative push
                                size_t nnx = (size_t)((int)ix - (int)Cf[k](0) + (int)eUnit(0) + (int)Ndim(0))%Ndim(0);
                                size_t nny = (size_t)((int)iy - (int)Cf[k](1) + (int)eUnit(1) + (int)Ndim(1))%Ndim(1);
                                
                                // if (IsSolid[0][npx][npy][nz] || IsSolid[0][nnx][nny][nz]) continue;

                                //* Cross Interaction
                                    psiNC[0]  = PsiC[0][npx][npy][nz];
                                    psiNC[1]  = PsiC[1][npx][npy][nz];
                                    psiNnC[0] = PsiC[0][nnx][nny][nz];
                                    psiNnC[1] = PsiC[1][nnx][nny][nz];
                                //* Self Interaction
                                    psiNS[0]  = PsiS[0][npx][npy][nz];
                                    psiNS[1]  = PsiS[1][npx][npy][nz];
                                    psiNnS[0] = PsiS[0][nnx][nny][nz];
                                    psiNnS[1] = PsiS[1][nnx][nny][nz];

                                //* Force Vectors 3 and 4
                                SumPT[0] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(G[0]*psiNS[0]*psiNnS[0] + Gmix*psiNC[0]*psiNnC[1]);

                                SumPT[1] += tmp_frac*c_frac*Cf[k](i)*Cf[k](j)*(G[1]*psiNS[1]*psiNnS[1] + Gmix*psiNC[1]*psiNnC[0]);

                                // frac_count += 1.0/(2.0*symm_emax[k]);
                                if ((eUnit(0) - Cf[k](0)) != 0.0) eUnit(0) += pushX; // so that +- is accounted for (cannot use nij++)
                                if ((eUnit(1) - Cf[k](1)) != 0.0) eUnit(1) += pushY; // so that +- is accounted for (cannot use nij++)
                            }
                        }
                    }
                }
            }
            Pij[0][ix][iy][nz][i][j] = SumPT[0];
            Pij[1][ix][iy][nz][i][j] = SumPT[1];
        }
    }
}

inline void Domain::MomentumFluxTensor() {
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);
    size_t maxDim = 2;

    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++) {
        Vec3_t Vmix = OrthoSys::O;
        double den = 0.0;
        for (size_t il = 0; il < Nl; il++) {
            Vmix    += (Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz]) / Tau[il];
            den     += Rho[il][ix][iy][iz] / Tau[il];
        }
        Vmix /= den;

        for (size_t il=0;il<Nl;il++) {
            // Full second order moment (Pij + puu) 
            Vec3_t Qij[3] = { {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0} };
            Vec3_t Iij[3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} }; // Kronecker Delta
            // Vec3_t velS = Vel[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
            Vec3_t vel = Vmix;// + 0.5*dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
            double rho = Rho[il][ix][iy][iz];
            // Vec3_t vel = Vmix + dt*Tau[il]*BForce[il][ix][iy][iz]/rho;
            // double rhotmp = 0.0;
            Vec3_t acc = BForce[il][ix][iy][iz]/rho; //
            // Vec3_t velS = OrthoSys::O;
            // velS = vel + dt*acc;//* For EDM
            double invtau = dt/Tau[il];
            double * Fdist = F[il][ix][iy][iz];

            double NonEq[Nneigh];
            double Equil[Nneigh];
            double NonS[Nneigh];

            // Vec3_t veltmp = OrthoSys::O;
            for (size_t k=0;k<Nneigh;k++) {
                Equil[k]    = Feq(k, rho, vel);//Fdist[k];//
                // NonS[k]     = Feq(k, rho, velS) - Feq(k, rho, vel); //* For EDM
                NonEq[k]    = (Fdist[k] - Feq(k, rho, vel));
                NonS[k]    = (dt*Feq(k, rho, vel)*(dot(BForce[il][ix][iy][iz],C[k] - vel)/(rho*Cs2)));
                // Equil[k]    = (1.0 - 0.5*invtau)*Fdist[k] + (0.5*invtau)*(Feq(k, rho, vel));
                // NonS[k]    = (1.0 - 0.5*invtau)*(dt*Feq(k, rho, vel)*(dot(BForce[il][ix][iy][iz],C[k] - vel)/(rho*Cs2)));
                // NonS[k] = (dot(C[k],C[k]) - dot(vel,vel));// centered stress tensor, Ref. PhysRevE90 043306 (2014), Eq. (12)
            }

            // for (size_t k=0;k<Nneigh;k++) {
            //     NonS[k]    = dt*ForceFeq(k, rho, NonEq, vel, acc, Pij[il][ix][iy][iz], 2);
            // }

            for (size_t i=0;i<maxDim;i++) // maxDim == 2, for 2D simulations.
            for (size_t j=0;j<maxDim;j++) {

                Pkin[il][ix][iy][iz][i][j] = 0.0;
                // Vec3_t veltmp = OrthoSys::O;

                for (size_t k=0;k<Nneigh;k++) {
                    // Qij[i][j]   += Fdist[k]*C[k][i]*C[k][j];
                    // Equil[k]*C[k][i]*C[k][j] + NonS[k]*C[k][i]*C[k][j]; //* For EDM
                    // Qij[i][j]   -= (1.0 - 0.5*invtau)*NonEq[k]*C[k][i]*C[k][j] - 0.5*NonS[k]*C[k][i]*C[k][j]; // Fdist[k]*C[k][i]*C[k][j]; //
                    // Qij[i][j]   += Equil[k]*C[k][i]*C[k][j] + 1.0*(1.0 - 0.5*invtau)*NonS[k]*C[k][i]*C[k][j]; // Fdist[k]*C[k][i]*C[k][j]; //
                    Qij[i][j]   += Equil[k]*C[k][i]*C[k][j] + NonS[k]*C[k][i]*C[k][j]; //+ 1.0*(Tau[il] - 0.5)*NonS[k]*C[k][i]*C[k][j]; //
                    // Qij[i][j]   += Equil[k]*C[k][i]*C[k][j] + (1.0 - 0.5*invtau)*(C[k][i]*C[k][j] - 0.5*Iij[j][i]*dot(C[k],C[k]))*NonEq[k] + (1.0 - 0.5*invtau)*0.5*NonS[k]*C[k][i]*C[k][j];
                    // Qij[i][j]   += Equil[k]*C[k][i]*C[k][j] -(1.0 - 0.5*invtau)*NonEq[k]*C[k][i]*C[k][j] + 1.0*(1.0 - 0.5*invtau)*NonS[k]*C[k][i]*C[k][j];
                    // Qij[i][j]   += Fdist[k]*( (C[k][i] -vel[i])*(C[k][j] -vel[j]) - Iij[j][i]*NonS[k]);
                }

                // veltmp      +=  0.5*(1.0 - 0.5*invtau)*dt*BForce[il][ix][iy][iz];
                // veltmp      *=  Cs/rho;
                // Pkin[il][ix][iy][iz][i][j] = Qij[i][j];
                // Pkin[il][ix][iy][iz][i][j] = Qij[i][j] + rho*vel[i]*vel[j];
                Pkin[il][ix][iy][iz][i][j] = (Qij[i][j] - rho*Cs2*Iij[i][j]);// + (1.0/rho)*pow((Tau[il]- 0.5),2.0)*acc[i]*acc[j];
                // Pkin[il][ix][iy][iz][i][j] = rho*vel[i]*vel[j]; // rho*veltmp[i]*veltmp[j]; //
            }
        }
    }
}

inline void Domain::ApplyForcesSC()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic = 0.0;
        double psin = 0.0;

        IsSolid[0][ixc][iyc][izc] ? psic = 0.0 : psic = Psi[0]*exp(-Rhoref[0]/Rho[0][ixc][iyc][izc]);
        IsSolid[0][ixn][iyn][izn] ? psin = 0.0 : psin = Psi[0]*exp(-Rhoref[0]/Rho[0][ixn][iyn][izn]);

        Vec3_t bforce = -G[0]*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;
    }
}

inline void Domain::ApplyForcesSCSS()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic  = 0.0;
        double psin  = 0.0;
        bool   solid = false;

        IsSolid[0][ixc][iyc][izc] ? psic = 1.0, solid = true : psic = Rho[0][ixc][iyc][izc];
        IsSolid[0][ixn][iyn][izn] ? psin = 1.0, solid = true : psin = Rho[0][ixn][iyn][izn];

        Vec3_t bforce = OrthoSys::O;
        if (solid) bforce = -Gs[0]*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;
    }
}

inline void Domain::ApplyForcesMP()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic = 0.0;
        double psin = 0.0;

        double Gt   = Gmix;

        IsSolid[0][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[1] : psic = Rho[0][ixc][iyc][izc];
        IsSolid[1][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[0] : psin = Rho[1][ixn][iyn][izn];

        Vec3_t bforce = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[1][ixn][iyn][izn] -= bforce;

        Gt          = Gmix;

        IsSolid[1][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[0] : psic = Rho[1][ixc][iyc][izc];
        IsSolid[0][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[1] : psin = Rho[0][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;
    }
}

inline void Domain::ApplyForcesSCMP()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic = 0.0;
        double psin = 0.0;

        IsHBSolid[0][ixc][iyc][izc] ? psic = 0.0 : psic = Psi[0]*exp(-Rhoref[0]/Rho[0][ixc][iyc][izc]);
        IsHBSolid[0][ixn][iyn][izn] ? psin = 0.0 : psin = Psi[0]*exp(-Rhoref[0]/Rho[0][ixn][iyn][izn]);

        Vec3_t bforce = -G[0]*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;

        IsHBSolid[1][ixc][iyc][izc] ? psic = 0.0 : psic = Psi[1]*exp(-Rhoref[1]/Rho[1][ixc][iyc][izc]);
        IsHBSolid[1][ixn][iyn][izn] ? psin = 0.0 : psin = Psi[1]*exp(-Rhoref[1]/Rho[1][ixn][iyn][izn]);

        bforce        = -G[1]*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        BForce[1][ixn][iyn][izn] -= bforce;

        double Gt   = Gmix;

        IsHBSolid[0][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[1] : psic = Rho[0][ixc][iyc][izc];
        IsHBSolid[1][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[0] : psin = Rho[1][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[1][ixn][iyn][izn] -= bforce;

        Gt          = Gmix;

        IsHBSolid[1][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[0] : psic = Rho[1][ixc][iyc][izc];
        IsHBSolid[0][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[1] : psin = Rho[0][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;
    }
}


inline void Domain::FApplyForcesSCMP() // Quick version ApplyForcesSCMP but with no solids.
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic = 0.0;
        double psin = 0.0;

        psic = PsiEq(0, Rho[0][ixc][iyc][izc], Rhoref[0], Psi[0]);//Psi[0]*exp(-Rhoref[0]/Rho[0][ixc][iyc][izc]);
        psin = PsiEq(0, Rho[0][ixn][iyn][izn], Rhoref[0], Psi[0]);//Psi[0]*exp(-Rhoref[0]/Rho[0][ixn][iyn][izn]);

        Vec3_t bforce = -G[0]*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        // BForce[0][ixn][iyn][izn] -= bforce;

        psic = PsiEq(1, Rho[1][ixc][iyc][izc], Rhoref[1], Psi[1]);//Psi[0]*exp(-Rhoref[0]/Rho[0][ixc][iyc][izc]);
        psin = PsiEq(1, Rho[1][ixn][iyn][izn], Rhoref[1], Psi[1]);//Psi[0]*exp(-Rhoref[0]/Rho[0][ixn][iyn][izn]);

        bforce        = -G[1]*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        // BForce[1][ixn][iyn][izn] -= bforce;

        double Gt   = Gmix;

        psic = Rho[0][ixc][iyc][izc];
        psin = Rho[1][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        // BForce[1][ixn][iyn][izn] -= bforce;

        Gt          = Gmix;

        psic = Rho[1][ixc][iyc][izc];
        psin = Rho[0][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        // BForce[0][ixn][iyn][izn] -= bforce;
    }
}


inline void Domain::ApplyForcesTEST()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        Vec3_t bforce   = OrthoSys::O;
        double Gt       = 0.0;
        double psic     = 0.0;
        double psin     = 0.0;
        // eAlpha = 1.0 - 3.0*e4;
        // eBeta = 6.0*e4 + 1.0;
        // epsilon = (-2.0*(eAlpha - 24*Gmix*0.125))/eBeta;
        // double epsipow = (1.0/epsilon);
        

        IsSolid[0][ixc][iyc][izc] ? psic = 0.0 : psic = PsiEq(0, Nmol[0][ixc][iyc][izc], Rhoref[0], Psi[0]);
        IsSolid[0][ixn][iyn][izn] ? psin = 0.0 : psin = PsiEq(0, Nmol[0][ixn][iyn][izn], Rhoref[0], Psi[0]);

        bforce = -G[0]*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;

        IsSolid[1][ixc][iyc][izc] ? psic = 0.0 : psic = PsiEq(1, Nmol[1][ixc][iyc][izc], Rhoref[1], Psi[1]);
        IsSolid[1][ixn][iyn][izn] ? psin = 0.0 : psin = PsiEq(1, Nmol[1][ixn][iyn][izn], Rhoref[1], Psi[1]);

        bforce        = -G[1]*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        BForce[1][ixn][iyn][izn] -= bforce;

        Gt   = Gmix;///(1.0-epsilon);

        IsSolid[0][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[1] : psic = Nmol[0][ixc][iyc][izc];
        IsSolid[1][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[0] : psin = Nmol[1][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[0][ixc][iyc][izc] += bforce;
        BForce[1][ixn][iyn][izn] -= bforce;

        Gt          = Gmix;

        IsSolid[1][ixc][iyc][izc] ? psic = 1.0, Gt = Gs[0] : psic = Nmol[1][ixc][iyc][izc];
        IsSolid[0][ixn][iyn][izn] ? psin = 1.0, Gt = Gs[1] : psin = Nmol[0][ixn][iyn][izn];

        bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;

        BForce[1][ixc][iyc][izc] += bforce;
        BForce[0][ixn][iyn][izn] -= bforce;
    }
}

inline void Domain::ApplyForces3E()
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);

        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);

        double psic = 0.0;
        double psin = 0.0;
        double psinx = 0.0;
        double epsipow = (1.0/epsilon);

        //Component 1
            psic = pow(Nmol[0][ixc][iyc][izc]/(epsilon + Nmol[0][ixc][iyc][izc]), epsipow);
            psin = pow(Nmol[0][ixn][iyn][izn]/(epsilon + Nmol[0][ixn][iyn][izn]), epsipow);

            Vec3_t bforce = -G[0]*Wf[k]*Cf[k]*psic*psin;

            BForce[0][ixc][iyc][izc] += bforce;
            BForce[0][ixn][iyn][izn] -= bforce;
        //Component 2
            psic = pow(Nmol[1][ixc][iyc][izc]/(epsilon + Nmol[1][ixc][iyc][izc]), epsipow);
            psin = pow(Nmol[1][ixn][iyn][izn]/(epsilon + Nmol[1][ixn][iyn][izn]), epsipow);

            bforce        = -G[1]*Wf[k]*Cf[k]*psic*psin;

            BForce[1][ixc][iyc][izc] += bforce;
            BForce[1][ixn][iyn][izn] -= bforce;
        //Component 3
            psic = pow(Nmol[2][ixc][iyc][izc]/(epsilon + Nmol[2][ixc][iyc][izc]), epsipow);
            psin = pow(Nmol[2][ixn][iyn][izn]/(epsilon + Nmol[2][ixn][iyn][izn]), epsipow);

            bforce        = -G[2]*Wf[k]*Cf[k]*psic*psin;

            BForce[2][ixc][iyc][izc] += bforce;
            BForce[2][ixn][iyn][izn] -= bforce;

            double Gt   = Gmix;

            psic    = pow(Nmol[0][ixc][iyc][izc]/(epsilon + Nmol[0][ixc][iyc][izc]), epsipow);
            psin    = pow(Nmol[1][ixn][iyn][izn]/(epsilon + Nmol[1][ixn][iyn][izn]), epsipow);
            psinx   = pow(Nmol[2][ixn][iyn][izn]/(epsilon + Nmol[2][ixn][iyn][izn]), epsipow);

            bforce      = -Gt*Wf[k]*Cf[k]*psic*psin; 
            Vec3_t bforcex = -Gt*Wf[k]*Cf[k]*psic*psinx;

            BForce[0][ixc][iyc][izc] += bforce + bforcex;
            BForce[1][ixn][iyn][izn] -= bforce;
            BForce[2][ixn][iyn][izn] -= bforcex;

            double Gtx   = Gint; // specific paired interacting

            psic    = pow(Nmol[1][ixc][iyc][izc]/(epsilon + Nmol[1][ixc][iyc][izc]), epsipow);
            psin    = pow(Nmol[0][ixn][iyn][izn]/(epsilon + Nmol[0][ixn][iyn][izn]), epsipow);
            psinx   = pow(Nmol[2][ixn][iyn][izn]/(epsilon + Nmol[2][ixn][iyn][izn]), epsipow);

            bforce      = -Gt*Wf[k]*Cf[k]*psic*psin;
            bforcex     = -Gtx*Wf[k]*Cf[k]*psic*psinx;

            BForce[1][ixc][iyc][izc] += bforce + bforcex;
            BForce[0][ixn][iyn][izn] -= bforce;
            BForce[2][ixn][iyn][izn] += bforcex;

            psic    = pow(Nmol[2][ixc][iyc][izc]/(epsilon + Nmol[2][ixc][iyc][izc]), epsipow);
            psin    = pow(Nmol[0][ixn][iyn][izn]/(epsilon + Nmol[0][ixn][iyn][izn]), epsipow);
            psinx   = pow(Nmol[1][ixn][iyn][izn]/(epsilon + Nmol[1][ixn][iyn][izn]), epsipow);

            bforce      = -Gt*Wf[k]*Cf[k]*psic*psin; 
            bforcex     = -Gtx*Wf[k]*Cf[k]*psic*psinx;

            BForce[2][ixc][iyc][izc] += bforce + bforcex;
            BForce[0][ixn][iyn][izn] -= bforce;
            BForce[1][ixn][iyn][izn] += bforcex; // Self interaction, hence  neg-neg --> +
    }
}

inline void Domain::ApplyForcesE() 
{
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        size_t ixc = idxc(0), iyc = idxc(1), izc = idxc(2);
        size_t ixn = idxn(0), iyn = idxn(1), izn = idxn(2);

        double psiNC[Nl] = {0.0};
        double psiNnC[Nl] = {0.0};
        double psiNS[Nl] = {0.0};
        double psiNnS[Nl] = {0.0};
        double Gc0   = Gmix;
        double Gc1   = Gmix;

        psiNC[0] = PsiInterInt(0,Nmol[0][ixc][iyc][izc]);
        psiNC[1] = PsiInterInt(1,Nmol[1][ixc][iyc][izc]);
        psiNnC[0] = PsiInterInt(0,Nmol[0][ixn][iyn][izn]);
        psiNnC[1] = PsiInterInt(1,Nmol[1][ixn][iyn][izn]);
        
        psiNS[0] = PsiIntraInt(0,Nmol[0][ixc][iyc][izc]);
        psiNS[1] = PsiIntraInt(1,Nmol[1][ixc][iyc][izc]);
        psiNnS[0] = PsiIntraInt(0,Nmol[0][ixn][iyn][izn]);
        psiNnS[1] = PsiIntraInt(1,Nmol[1][ixn][iyn][izn]);

        if (IsSolid[0][ixc][iyc][izc]) psiNS[0] = 0.0, psiNC[0] = 1.0, Gc0 = Gs[0];//
        if (IsSolid[0][ixn][iyn][izn]) psiNnS[0] = 0.0, psiNnC[0] = 1.0, Gc0 = Gs[0];//
        if (IsSolid[1][ixc][iyc][izc]) psiNS[1] = 0.0, psiNC[1] = 1.0, Gc1 = Gs[1];//
        if (IsSolid[1][ixn][iyn][izn]) psiNnS[1] = 0.0, psiNnC[1] = 1.0, Gc1 = Gs[1];//

        BForce[0][ixc][iyc][izc] -= Cs2*(G[0]*Wf[k]*Cf[k]*psiNS[0]*psiNnS[0] + Gc0*Wf[k]*Cf[k]*psiNC[0]*psiNnC[1]); // Psi(x-e_k), where e_k is a tensor; i.e., e(x,y,z)_k
        // BForce[0][ixn][iyn][izn] -= bforce; // Psi(x+e_k)
        // hence the above ensures that ~= Psi(x-e_k) - Psi(x+e_k); directions defined by vector Cf.
        BForce[1][ixc][iyc][izc] -= Cs2*(G[1]*Wf[k]*Cf[k]*psiNS[1]*psiNnS[1] + Gc1*Wf[k]*Cf[k]*psiNC[1]*psiNnC[0]);
    }
}

inline void Domain::FFApplyForcesE_SC() // * Single-component Simulations
{
    // double Rho_ref = Rhoref[0]*Rhoref[1];
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        size_t ixc = idxc(0), iyc = idxc(1), izc = idxc(2);
        size_t ixn = idxn(0), iyn = idxn(1), izn = idxn(2);
        double psiNS = 0.0;
        double psiNnS = 0.0;
        double G0 = G[0];
            psiNS  = PsiS[0][ixc][iyc][izc];
            psiNnS = PsiS[0][ixn][iyn][izn];
        if (IsSolid[0][ixc][iyc][izc]) psiNS = 0.0, G0 = Gs[0];//
        if (IsSolid[0][ixn][iyn][izn]) psiNnS = 0.0, G0 = Gs[0];//
        // Calucate total interaction force at x link-alpha
        BForce[0][ixc][iyc][izc] -= Wf[k]*Cf[k]*G0*psiNS*psiNnS;
        
    }
}

inline void Domain::FApplyForcesE() // * Two-component simulations
{
    // double Rho_ref = Rhoref[0]*Rhoref[1];
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        // if (k==0) std::cout << "checking k= " << k << "\n" <<std::endl; // *Will Show Output for force-schemes. No output for ELS
        size_t ixc = idxc(0), iyc = idxc(1), izc = idxc(2);
        size_t ixn = idxn(0), iyn = idxn(1), izn = idxn(2);

        // if (!IsSolid[0][ixc][iyc][izc] || !IsSolid[0][ixn][iyn][izn] || !IsSolid[1][ixc][iyc][izc] || !IsSolid[1][ixn][iyn][izn]) //continue;
        // {
            // if (IsSolid[0][ixc][iyc][izc]) continue;
            // if (IsSolid[0][ixn][iyn][izn]) continue;
        //* Self Interaction
            double psiS0c = PsiIntraInt(0,Nmol[0][ixc][iyc][izc]); // PsiIntraInt(0,psic0);
            double psiS0n = PsiIntraInt(0,Nmol[0][ixn][iyn][izn]); // PsiIntraInt(0,psin0);
            
            double psiS1c = PsiIntraInt(1,Nmol[1][ixc][iyc][izc]); // PsiIntraInt(1,psic1);
            double psiS1n = PsiIntraInt(1,Nmol[1][ixn][iyn][izn]); // PsiIntraInt(1,psin1);
        //* Cross-Interaction
            double psic0 =  PsiInterInt(0,Nmol[0][ixc][iyc][izc]); //.. Nmol[0][ixc][iyc][izc]; //..
            double psin0 =  PsiInterInt(0,Nmol[0][ixn][iyn][izn]); //.. Nmol[0][ixn][iyn][izn]; //..
            
            double psic1 =  PsiInterInt(1,Nmol[1][ixc][iyc][izc]); //.. Nmol[1][ixc][iyc][izc]; //..
            double psin1 =  PsiInterInt(1,Nmol[1][ixn][iyn][izn]); //.. Nmol[1][ixn][iyn][izn]; //..
        
        // likely not worth having any of these conditions ..
            // if (IsSolid[0][ixc][iyc][izc]) psic0 = 1.0, psiS0c = 0.0, psic1 = 1.0, psiS1c = 0.0;
            // if (IsSolid[0][ixn][iyn][izn]) psin0 = 1.0, psiS0n = 0.0, psin1 = 1.0, psiS1n = 0.0;
            // if (psic0<1.0e-4) psic0 = 0.0, psiS0c = 0.0;
            // if (psin0<1.0e-4) psin0 = 0.0, psiS0n = 0.0;
            // if (psic1<1.0e-4) psic1 = 0.0, psiS1c = 0.0;
            // if (psin1<1.0e-4) psin1 = 0.0, psiS1n = 0.0;
            // if (std::isnan(psic0)) psic0 = 0.0, psiS0c = 0.0;
            // if (std::isnan(psin0)) psin0 = 0.0, psiS0n = 0.0;
            // if (std::isnan(psic1)) psic1 = 0.0, psiS1c = 0.0;
            // if (std::isnan(psin1)) psin1 = 0.0, psiS1n = 0.0;
        // Calucate total interaction force at x link-alpha
        BForce[0][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[0]*psiS0c*psiS0n + Gmix*psic0*psin1);
        
        BForce[1][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[1]*psiS1c*psiS1n + Gmix*psic1*psin0);
        // }
    }
}

inline void Domain::FFApplyForcesE() // * Two-component simulations
{
    // double Rho_ref = Rhoref[0]*Rhoref[1];
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        size_t ixc = idxc(0), iyc = idxc(1), izc = idxc(2);
        size_t ixn = idxn(0), iyn = idxn(1), izn = idxn(2);
        double psiNC[Nl] = {0.0};
        double psiNnC[Nl] = {0.0};
        double psiNS[Nl] = {0.0};
        double psiNnS[Nl] = {0.0};
        double Gc0   = Gmix;
        double Gc1   = Gmix;

        //* Cross Interaction
            psiNC[0]  = PsiC[0][ixc][iyc][izc];
            psiNC[1]  = PsiC[1][ixc][iyc][izc];
            psiNnC[0] = PsiC[0][ixn][iyn][izn];
            psiNnC[1] = PsiC[1][ixn][iyn][izn];
        //* Self Interaction
            psiNS[0]  = PsiS[0][ixc][iyc][izc];
            psiNS[1]  = PsiS[1][ixc][iyc][izc];
            psiNnS[0] = PsiS[0][ixn][iyn][izn];
            psiNnS[1] = PsiS[1][ixn][iyn][izn];
        //* Solid Boundaries
            // if (IsSolid[0][ixc][iyc][izc]) psiNS[0] = 0.0, psiNC[0] = RhoW[0][ixc][iyc][izc], Gc0 = Gs[0];//
            // if (IsSolid[0][ixn][iyn][izn]) psiNnS[0] = 0.0, psiNnC[0] = RhoW[0][ixn][iyn][izn], Gc0 = Gs[0];//
            // if (IsSolid[1][ixc][iyc][izc]) psiNS[1] = 0.0, psiNC[1] = RhoW[1][ixc][iyc][izc], Gc1 = Gs[1];//
            // if (IsSolid[1][ixn][iyn][izn]) psiNnS[1] = 0.0, psiNnC[1] = RhoW[1][ixn][iyn][izn], Gc1 = Gs[1];//
            if (IsSolid[0][ixn][iyn][izn]) psiNnS[0] = 0.0, psiNnC[1] = RhoW[0][ixn][iyn][izn], psiNC[0] = Rho[0][ixc][iyc][izc], Gc0 = Gs[0];//
            if (IsSolid[0][ixc][iyc][izc]) psiNS[0]  = 0.0, psiNC[0]  = 0.0;// Gc0 = Gs[0];//
            if (IsSolid[1][ixn][iyn][izn]) psiNnS[1] = 0.0, psiNnC[0] = RhoW[1][ixn][iyn][izn], psiNC[1] = Rho[1][ixc][iyc][izc], Gc1 = Gs[1];//
            if (IsSolid[1][ixc][iyc][izc]) psiNS[1]  = 0.0, psiNC[1]  = 0.0;// Gc1 = Gs[1];//
        
        
        // Calucate total interaction force at x link-alpha
        BForce[0][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[0]*psiNS[0]*psiNnS[0] + Gc0*psiNC[0]*psiNnC[1]);
        
        BForce[1][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[1]*psiNS[1]*psiNnS[1] + Gc1*psiNC[1]*psiNnC[0]);
    }
}

inline void Domain::FApplyForces3E() // * Multiple components (>2)
{
    // double Rho_ref = Rhoref[0]*Rhoref[1];
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        // if (k==0) std::cout << "checking k= " << k << "\n" <<std::endl; // *Will Show Output for force-schemes. No output for ELS
        size_t ixc = idxc(0), iyc = idxc(1), izc = idxc(2);
        size_t ixn = idxn(0), iyn = idxn(1), izn = idxn(2);
        double psiNC[Nl];
        double psiNnC[Nl];
        double psiNS[Nl];
        double psiNnS[Nl];

        //* Cross Interaction
            psiNC[0]  = PsiC[0][ixc][iyc][izc];
            psiNC[1]  = PsiC[1][ixc][iyc][izc];
            psiNC[2]  = PsiC[2][ixc][iyc][izc];
            psiNnC[0] = PsiC[0][ixn][iyn][izn];
            psiNnC[1] = PsiC[1][ixn][iyn][izn];
            psiNnC[2] = PsiC[2][ixn][iyn][izn];
        //* Self Interaction
            psiNS[0]  = PsiS[0][ixc][iyc][izc];
            psiNS[1]  = PsiS[1][ixc][iyc][izc];
            psiNS[2]  = PsiS[2][ixc][iyc][izc];
            psiNnS[0] = PsiS[0][ixn][iyn][izn];
            psiNnS[1] = PsiS[1][ixn][iyn][izn];
            psiNnS[2] = PsiS[2][ixn][iyn][izn];
            
            double psiS0c = PsiIntraInt(0,Nmol[0][ixc][iyc][izc]); // PsiIntraInt(0,psic0);
            double psiS0n = PsiIntraInt(0,Nmol[0][ixn][iyn][izn]); // PsiIntraInt(0,psin0);
            double psiS1c = PsiIntraInt(1,Nmol[1][ixc][iyc][izc]); // PsiIntraInt(1,psic1);
            double psiS1n = PsiIntraInt(1,Nmol[1][ixn][iyn][izn]); // PsiIntraInt(1,psin1);
            double psiS2c = PsiIntraInt(2,Nmol[2][ixc][iyc][izc]); // PsiIntraInt(2,psic2);
            double psiS2n = PsiIntraInt(2,Nmol[2][ixn][iyn][izn]); // PsiIntraInt(2,psin2);
        
        BForce[0][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[0]*psiNS[0]*psiNnS[0] + Gmmix[0]*psiNC[0]*psiNnC[1] + Gmmix[1]*psiNC[0]*psiNnC[2]); // + Gmmix[0]*psiS0c*psiS1n + Gmmix[1]*psiS0c*psiS2n);
        
        BForce[1][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[1]*psiNS[1]*psiNnS[1] + Gmmix[0]*psiNC[1]*psiNnC[0] + Gmmix[2]*psiNC[1]*psiNnC[2]); // + Gmmix[0]*psiS1c*psiS0n + Gmmix[2]*psiS1c*psiS2n);

        BForce[2][ixc][iyc][izc] -= Wf[k]*Cf[k]*(G[2]*psiNS[2]*psiNnS[2] + Gmmix[1]*psiNC[2]*psiNnC[0] + Gmmix[2]*psiNC[2]*psiNnC[1]); // + Gmmix[1]*psiS2c*psiS0n + Gmmix[2]*psiS2c*psiS1n);
    }
}

inline void Domain::FApplyForcesMPI() // * Multiple PseudoPotential Interaction (MPI)
{
    double Rho_ref = Rhoref[0]*Rhoref[1];
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t n=0;n<NCellPairs;n++)
    {
        iVec3_t idxc,idxn;
        size_t k = CellPairs[n](2);
        idx2Pt(CellPairs[n](0),idxc,Ndim);
        idx2Pt(CellPairs[n](1),idxn,Ndim);
        // if (k==0) std::cout << "checking k= " << k << "\n" <<std::endl;
        size_t ixc = idxc(0);
        size_t iyc = idxc(1);
        size_t izc = idxc(2);
        size_t ixn = idxn(0);
        size_t iyn = idxn(1);
        size_t izn = idxn(2);
        
        double psic0 = Nmol[0][ixc][iyc][izc]; // PsiIntraInt(0,Nmol[0][ixc][iyc][izc]); //
        double psin0 = Nmol[0][ixn][iyn][izn]; // PsiIntraInt(0,Nmol[0][ixn][iyn][izn]); //
        double psic1 = Nmol[1][ixc][iyc][izc]; // PsiIntraInt(1,Nmol[1][ixc][iyc][izc]); //
        double psin1 = Nmol[1][ixn][iyn][izn]; // PsiIntraInt(1,Nmol[1][ixn][iyn][izn]); //
        
        double psiS0c = PsiIntraInt(0,Nmol[0][ixc][iyc][izc]);
        double psiS0n = PsiIntraInt(0,Nmol[0][ixn][iyn][izn]);
        
        double psiS1c = PsiIntraInt(1,Nmol[1][ixc][iyc][izc]);
        double psiS1n = PsiIntraInt(1,Nmol[1][ixn][iyn][izn]);
        
        BForce[0][ixc][iyc][izc] += Cs2*0.80*G[0]*Wf[k]*Cf[k]*psiS0c*psiS0n;
        BForce[1][ixc][iyc][izc] += Cs2*0.80*G[1]*Wf[k]*Cf[k]*psiS1c*psiS1n;

        // BForce[0][ixc][iyc][izc] += Cs2*(G[0]*Wf[k]*Cf[k]*psiS0c*psiS0n - (Gmix/Rho_ref)*Wf[k]*Cf[k]*psic0*psin1);
        // BForce[1][ixc][iyc][izc] += Cs2*(G[1]*Wf[k]*Cf[k]*psiS1c*psiS1n - (Gmix/Rho_ref)*Wf[k]*Cf[k]*psic1*psin0);
        
        if (k<=4)
        {
            BForce[0][ixc][iyc][izc] -= Cs2*G[0]*(1.0/3.0)*Cf[k]*psiS0c*psiS0n;
            BForce[1][ixc][iyc][izc] -= Cs2*G[1]*(1.0/3.0)*Cf[k]*psiS1c*psiS1n;
            // BForce[0][ixc][iyc][izc] -= Cs2*(Gmix/Rho_ref)*(1.0/3.0)*Cf[k]*psic0*psin1;
            // BForce[1][ixc][iyc][izc] -= Cs2*(Gmix/Rho_ref)*(1.0/3.0)*Cf[k]*psic1*psin0;
            BForce[0][ixc][iyc][izc] -= Cs2*Gmix*(1.0/3.0)*Cf[k]*psic0*psin1;
            BForce[1][ixc][iyc][izc] -= Cs2*Gmix*(1.0/3.0)*Cf[k]*psic1*psin0;
        }
        else if (k<=8)
        {
            BForce[0][ixc][iyc][izc] -= Cs2*G[0]*(1.0/12.0)*Cf[k]*psiS0c*psiS0n;
            BForce[1][ixc][iyc][izc] -= Cs2*G[1]*(1.0/12.0)*Cf[k]*psiS1c*psiS1n;
            // BForce[0][ixc][iyc][izc] -= Cs2*(Gmix/Rho_ref)*(1.0/12.0)*Cf[k]*psic0*psin1;
            // BForce[1][ixc][iyc][izc] -= Cs2*(Gmix/Rho_ref)*(1.0/12.0)*Cf[k]*psic1*psin0;
            BForce[0][ixc][iyc][izc] -= Cs2*Gmix*(1.0/12.0)*Cf[k]*psic0*psin1;
            BForce[1][ixc][iyc][izc] -= Cs2*Gmix*(1.0/12.0)*Cf[k]*psic1*psin0;
        }

    }
}


inline void Domain::CollideSC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            double NonEq[Nneigh];
            double Q = 0.0;
            double tau = Tau[0];
            double rho = Rho[0][ix][iy][iz];
            // double nmol = Nmol[0][ix][iy][iz];
            Vec3_t vel = Vel[0][ix][iy][iz]+ dt*tau*BForce[0][ix][iy][iz]/rho;
            // if (fabs(BForce[0][ix][iy][iz][0])!=0.0)  std::cout << "Force calculation incldued "<< std::endl;
            
            for (size_t k=0;k<Nneigh;k++)
            {
                double Feqtmp = Feq(k,rho,vel); 
                NonEq[k] = F[0][ix][iy][iz][k] - Feqtmp;
                Q +=  NonEq[k]*NonEq[k]*EEk[k];
            }

            Q = sqrt(2.0*Q);
            // tau = sqrt(tau*tau + (0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2))))); //Filtered mean for finite Reynolds
            // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));
            tau = 0.5*(tau+sqrt(tau*tau + (Cs2*18.0*Q*Sc)/rho));
            // Tau[0] = tau;

            bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            size_t num = 0;
            while (valid)
            {
                num++;
                alpha = alphat;
                valid = false;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - alpha*NonEq[k]/tau;
                    if (Ftemp[0][ix][iy][iz][k]<-1.0e-12&&num<2)
                    {
                        //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        double temp =  tau*F[0][ix][iy][iz][k]/NonEq[k];
                        if (temp<alphat) alphat = temp;
                        valid = true;
                    }
                    // if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                    // {
                    //     std::cout << "CollideSC: Nan found, resetting" << std::endl;
                    //     std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                    //     throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                    // }
                }
            }
        }
        else
        {
            // double rho = Rho[0][ix][iy][iz];
            // Vec3_t vel = Vel[0][ix][iy][iz];
            for (size_t k=0;k<Nneigh;k++)
            {
                // size_t nix = (size_t)((int)ix - (fabs(C[k](0))-1)*(int)C[k](0)/fabs(C[k](0)) + (int)Ndim(0))%Ndim(0);
                // size_t niy = (size_t)((int)iy - (fabs(C[k](1))-1)*(int)C[k](1)/fabs(C[k](1)) + (int)Ndim(1))%Ndim(1);
                Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];// + (Feq(k,rho,vel)-Feq(Op[k],rho,vel));
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideSC_DBC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            double NonEq[Nneigh];
            double Q = 0.0;
            double tau = Tau[0];
            double rho = Rho[0][ix][iy][iz];
            // double nmol = Nmol[0][ix][iy][iz];
            Vec3_t vel = Vel[0][ix][iy][iz] + dt*tau*BForce[0][ix][iy][iz]/rho;
            // if (fabs(BForce[0][ix][iy][iz][0])!=0.0)  std::cout << "Force calculation incldued "<< std::endl;
            
            for (size_t k=0;k<Nneigh;k++)
            {
                double Feqtmp = Feq(k,rho,vel); 
                NonEq[k] = F[0][ix][iy][iz][k] - Feqtmp;
                // Q +=  NonEq[k]*NonEq[k]*EEk[k];
            }

            // Q = sqrt(2.0*Q);
            // tau = sqrt(tau*tau + (0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2))))); //Filtered mean for finite Reynolds
            // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));
            // tau = 0.5*(tau+sqrt(tau*tau + (Cs2*18.0*Q*Sc)/rho));
            // Tau[0] = tau;

            bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            size_t num = 0;
            while (valid)
            {
                num++;
                alpha = alphat;
                valid = false;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - alpha*NonEq[k]/tau;
                    if (Ftemp[0][ix][iy][iz][k]<-1.0e-12&&num<2)
                    {
                        //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        double temp =  tau*F[0][ix][iy][iz][k]/NonEq[k];
                        if (temp<alphat) alphat = temp;
                        valid = true;
                    }
                    // if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                    // {
                    //     std::cout << "CollideSC: Nan found, resetting" << std::endl;
                    //     std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                    //     throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                    // }
                }
            }
        }
        else
        {
            double rho = Rho[0][ix][iy][iz];
            Vec3_t velW = Vel[0][ix][iy][iz] + dt*Tau[0]*BForce[0][ix][iy][iz]/rho;// (0.0, 0.0, 0.0);// 
            /* In order to account for boundary at the edge of the domain
            if ((ix<(nx-latticeLength+1) || ix>(latticeLength-2)) && (iy<(ny-latticeLength+1) || iy>(latticeLength-2)))
            */
            for (size_t k=0;k<Nneigh;k++)
            {
            double FtempOp = 0.0; 
            double FeqtmpOp = 0.0; 
                for (size_t ka=1;ka<Nneigh;ka++)
                {
                    if(!IsFluid[0][ix][iy][iz][ka]) continue;
                        // Vec3_t velW = Vel[0][ixc][iyc][izc];
                        // double C_max = std::max(fabs(C[Op[ka]][0]),fabs(C[Op[ka]][1]));
                        double nC = dot((C[Op[ka]]-velW),nvec[0][ix][iy][iz]);///C_max;
                            // if (nC==0.0) std::cout << "n=0 ! nvec = (" <<nvec[0][ix][iy][iz][0]<< ", " << nvec[0][ix][iy][iz][1] << "),"<< std::endl; std::cout << " C=(" << C[Op[ka]][0] << ", " << C[Op[ka]][0] << ")" << std::endl;
                        if (nC<0.0) // to be reflected , nc needs to be < 0 
                        {
                            FtempOp     += F[0][ix][iy][iz][Op[ka]]*fabs(nC);    //  f' at the fluid node, to be reflected.
                                // std::cout << "F' = " << FtempOp << ", " << std::endl;
                                // std::cout << "(e-u)n = " << nC << ", " << std::endl;
                        }

                        nC = dot((C[ka]-velW),nvec[0][ix][iy][iz]);///C_max;// nnvec);//
                        if (nC>0)
                        {
                            FeqtmpOp    += Geq(ka,velW)*fabs(nC); //  feq' at the fluid node, to be reflected.
                        }
                        
                    // }
                }
                
                double nC = dot((C[k]-velW),nvec[0][ix][iy][iz]);// nnvec);//
                if (nC>0)
                {
                    double FeqW        = Geq(k,velW);     // feq at wall, assuming rho_w=1.0 in domain with <rho>=1.0
                    Ftemp[0][ix][iy][iz][k] =  (FtempOp/FeqtmpOp)*FeqW;  //F[0][ix][iy][iz][Op[k]];//
                        // std::cout << "F' = " << FtempOp << ", " << std::endl;
                        // std::cout << ", Feq' = "<< FeqtmpOp << std::endl;
                }
                else
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//
                }
                
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}


inline void Domain::CollideSC_EFDBC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz]) // ! For HB modification, add IsHBSolid here and in DefNormVec
        {
            double Q = 0.0;
            double invtau   = 1.0/Tau[0];
            double rho   = Rho[0][ix][iy][iz];
            double nmol  = Nmol[0][ix][iy][iz];
            Vec3_t vel   = Vel[0][ix][iy][iz] + 0.5*dt*BForce[0][ix][iy][iz]/rho;
            
            double ForceEq[Nneigh];
            double NonEq[Nneigh];
                
            for (size_t k=0;k<Nneigh;k++)
            {
                double feq  = Feq(k,nmol,vel);
                ForceEq[k]  = feq*dot(BForce[0][ix][iy][iz],(C[k]-vel))/(Cs2*rho);
                ForceEq[k]  *= dt*(1.0 - 0.5*invtau);
                NonEq[k] = F[0][ix][iy][iz][k] - feq;
                NonEq[k] *= dt*invtau;
                // Q +=  NonEq[k]*NonEq[k]*EEk[k];
            }
            // Q = sqrt(2.0*Q);
            // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));
            // tau = 0.5*(tau+sqrt(tau*tau + (Cs2*18.0*Q*Sc)/rho));

            for (size_t k=0;k<Nneigh;k++)
            {
                Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - (NonEq[k] - ForceEq[k]);
                
                if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                {
                    std::cout << "CollideEFDBC: Nan found, resetting" << std::endl;
                    std::cout << " in loc = (" << iVec3_t(ix,iy,iz) << ") at k = " << k << " " << std::endl;
                    throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                }
            }
        }
        else
        {
            double rho = Rho[0][ix][iy][iz];
            Vec3_t velW = Vel[0][ix][iy][iz];// + 0.5*dt*BForce[0][ix][iy][iz]/rho;// (0.0, 0.0, 0.0);// 
            Ftemp[0][ix][iy][iz][0] = F[0][ix][iy][iz][0];//
            if(dot(nvec[0][ix][iy][iz],nvec[0][ix][iy][iz])!=0.0)// continue;
            {
                for (size_t k=1;k<Nneigh;k++)
                {
                    // if(!IsFluid[0][ix][iy][iz][k]) continue;
                    double FtempOp = 0.0; 
                    double FeqtmpOp = 0.0; 
                        // Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]]; //
                    double nC = dot((C[k]-velW),nvec[0][ix][iy][iz]);// nnvec);//
                    if (nC>=0.0) {
                        for (size_t ka=1;ka<Nneigh;ka++) {
                            // if(!IsFluid[0][ix][iy][iz][ka]) continue;
                            // Vec3_t velW = Vel[0][ixc][iyc][izc];
                            // size_t ixc = ix + (int)C[ka][0];
                            // size_t iyc = iy + (int)C[ka][1];
                            // size_t izc = iz + (int)C[ka][2];
                            // velW = Vel[0][ixc][iyc][izc] + 0.5*dt*BForce[0][ixc][iyc][izc]/rho;//;
                            // double C_max = std::max(fabs(C[Op[ka]][0]),fabs(C[Op[ka]][1]));
                            // std::cout << "True, nc = " << nC;
                            double nC = dot((C[ka]-velW),nvec[0][ix][iy][iz]);///C_max;
                                // if (nC==0.0) std::cout << "n=0 ! nvec = (" <<nvec[0][ix][iy][iz][0]<< ", " << nvec[0][ix][iy][iz][1] << "),"<< std::endl; std::cout << " C=(" << C[Op[ka]][0] << ", " << C[Op[ka]][0] << ")" << std::endl;
                                // std::cout << "\n True, nc = " << nC << " \n";
                                // std::cout << "         nvec(x,y) = (" <<nvec[0][ix][iy][iz][0]<< ", " << nvec[0][ix][iy][iz][1] << "), \n"<< std::endl;
                            // if (nC<0.0) // to be reflected , nc needs to be < 0 
                            // {
                                // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = \n" << FeqtmpOp << "\n" <<  std::endl;
                            //     FtempOp     += F[0][ix][iy][iz][ka]*fabs(nC);//fabs(nC);    //  f' at the fluid node, to be reflected.
                                    // std::cout << "F' = " << FtempOp << ", " << std::endl;
                                    // std::cout << "(e-u)n = " << nC << ", " << std::endl;
                            // }
                            // else
                            // {
                            // nC = dot((C[Op[ka]]-velW),nvec[0][ix][iy][iz]);///C_max;// nnvec);//
                                // if (nC==0.0) continue;
                            if (nC>0.0) { 
                                // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n" << std::endl;
                                FtempOp     += F[0][ix][iy][iz][Op[ka]]*fabs(nC);
                                FeqtmpOp    += Geq(ka,velW)*fabs(nC);// fabs(nC); //  feq' at the fluid node, to be reflected.
                            }
                        }
                        // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n"<< std::endl;
                        double FeqW        = Geq(k,velW);//Geq(k,velW);     // feq at wall, assuming rho_w=1.0 in domain with <rho>=1.0
                        Ftemp[0][ix][iy][iz][k] =  (FtempOp/FeqtmpOp)*FeqW;  //F[0][ix][iy][iz][Op[k]];//
                            // std::cout << "F' = " << FtempOp << ", " << std::endl;
                            // std::cout << ", Feq' = "<< FeqtmpOp << std::endl;
                        if (std::isnan(Ftemp[0][ix][iy][iz][k])) Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k];//FeqW;  //F[0][ix][iy][iz][k];//
                        // if (std::isnan(Ftemp[0][ix][iy][iz][k])) Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//FeqW;  //F[0][ix][iy][iz][k];//
                    }
                    else
                    {
                        // std::cout << "No, nc = " << nC << ", FtempOp" << FtempOp << ", Feqtmp = " << FeqtmpOp << std::endl;
                        Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k];//
                    }
                }
            }
            else
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}

inline void Domain::CollideDBC() // Does not include Force 
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            double Q = 0.0;
            double tau   = Tau[0];
            double rho   = Rho[0][ix][iy][iz];
            double nmol  = Nmol[0][ix][iy][iz];
            Vec3_t vel   = Vel[0][ix][iy][iz];
            double NonEq[Nneigh];
                
            for (size_t k=0;k<Nneigh;k++)
            {
                NonEq[k] = F[0][ix][iy][iz][k] - Feq(k,nmol,vel);
                // Q +=  NonEq[k]*NonEq[k]*EEk[k];
            }
            // Q = sqrt(2.0*Q);
            // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));
            // tau = 0.5*(tau+sqrt(tau*tau + (Cs2*18.0*Q*Sc)/rho));

            bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            size_t num = 0;
            while (valid)
            {
                num++;
                alpha = alphat;
                valid = false;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - alpha*(NonEq[k]/tau);
                    if (Ftemp[0][ix][iy][iz][k]<-1.0e-12&&num<2)
                    {
                        //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        double temp =  fabs(F[0][ix][iy][iz][k]/(NonEq[k]/tau));  // !! This temp should result in Ftemp to complete cancle out, Ftemp=0
                        if (temp<alphat) alphat = temp;
                        valid = true;
                    }
                    if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                    {
                        std::cout << "CollideSC: Nan found, resetting" << std::endl;
                        std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                        throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                    }
                }
            }
        }
        else
        {
            double rho = Rho[0][ix][iy][iz];
            Vec3_t velW = Vel[0][ix][iy][iz];
            // Ftemp[0][ix][iy][iz][0] = F[0][ix][iy][iz][0];//
            if(dot(nvec[0][ix][iy][iz],nvec[0][ix][iy][iz])==0.0) continue;
            for (size_t k=0;k<Nneigh;k++)
            {
                // if(!IsFluid[0][ix][iy][iz][k]) continue;
                double FtempOp = 0.0; 
                double FeqtmpOp = 0.0; 
                    // Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]]; //
                    double nC = dot((C[k]-velW),nvec[0][ix][iy][iz]);// nnvec);//
                if (nC>=0.0)
                {
                    for (size_t ka=1;ka<Nneigh;ka++)
                    {

                        double nC = dot((C[ka]-velW),nvec[0][ix][iy][iz]);///C_max;
                        if (nC<0.0) // to be reflected , nc needs to be < 0 
                        {

                            FtempOp     += F[0][ix][iy][iz][ka]*fabs(nC);//*fabs(nC);    //  f' at the fluid node, to be reflected.
                        }
                        if (nC>0.0)
                        {
                            // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n" << std::endl;
                            FeqtmpOp    += Geq(ka,velW)*fabs(nC);//*fabs(nC); //  feq' at the fluid node, to be reflected.
                        }
                    }
                    // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n"<< std::endl;
                    double FeqW        = Geq(Op[k],velW);//Geq(k,velW);     // feq at wall, assuming rho_w=1.0 in domain with <rho>=1.0
                    Ftemp[0][ix][iy][iz][k] =  (FtempOp/FeqtmpOp)*FeqW;  //F[0][ix][iy][iz][Op[k]];//

                    if (std::isnan(Ftemp[0][ix][iy][iz][k])) Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//FeqW;  //F[0][ix][iy][iz][k];//
                }
                else
                {
                    // std::cout << "No, nc = " << nC << ", FtempOp" << FtempOp << ", Feqtmp = " << FeqtmpOp << std::endl;
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}

inline void Domain::CollideMRT()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            //double NonEq[Nneigh];
            //double Q = 0.0;
            //double tau = Tau[0];
            double rho = Rho[0][ix][iy][iz];
            Vec3_t vel = Vel[0][ix][iy][iz]+ Cs2*dt*BForce[0][ix][iy][iz]/rho;
            //double VdotV = dot(vel,vel);
            //for (size_t k=0;k<Nneigh;k++)
            //{
                //double VdotC = dot(vel,C[k]);
                //double Feq   = W[k]*rho*(1.0 + 3.0*VdotC/Cs + 4.5*VdotC*VdotC/(Cs*Cs) - 1.5*VdotV/(Cs*Cs));
                //NonEq[k] = F[0][ix][iy][iz][k] - Feq;
                //Q +=  NonEq[k]*NonEq[k]*EEk[k];
            //}
            //Q = sqrt(2.0*Q);
            //tau = 0.5*(tau+sqrt(tau*tau + 6.0*Q*Sc/rho));
            double *f = F[0][ix][iy][iz];
            double *ft= Ftemp[0][ix][iy][iz];
            double fneq[Nneigh];
            int n=Nneigh,m=1;
            double a = 1.0,b = 0.0;
            
            dgemv_("N",&n,&n,&a,M.data,&n,f,&m,&b,ft,&m);

            ft[ 0] = 0.0; 
            ft[ 1] = S( 1)*(ft[ 1] + rho - rho*dot(vel,vel)/(Cs*Cs));
            ft[ 2] = S( 2)*(ft[ 2] - rho);
            ft[ 3] = 0.0;
            ft[ 4] = S( 4)*(ft[ 4] + 7.0/3.0*rho*vel(0)/Cs); 
            ft[ 5] = 0.0;
            ft[ 6] = S( 6)*(ft[ 6] + 7.0/3.0*rho*vel(1)/Cs); 
            ft[ 7] = 0.0;
            ft[ 8] = S( 8)*(ft[ 8] + 7.0/3.0*rho*vel(2)/Cs); 
            ft[ 9] = S( 9)*(ft[ 9] - rho*(2.0*vel(0)*vel(0)-vel(1)*vel(1)-vel(2)*vel(2))/(Cs*Cs));
            ft[10] = S(10)*(ft[10] - rho*(vel(1)*vel(1)-vel(2)*vel(2))/(Cs*Cs));
            ft[11] = S(11)*(ft[11] - rho*(vel(0)*vel(1))/(Cs*Cs));
            ft[12] = S(12)*(ft[12] - rho*(vel(1)*vel(2))/(Cs*Cs));
            ft[13] = S(13)*(ft[13] - rho*(vel(0)*vel(2))/(Cs*Cs));
            ft[14] = S(14)* ft[14];
            
            dgemv_("N",&n,&n,&a,Minv.data,&n,ft,&m,&b,fneq,&m);

            bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            while (valid)
            {
                alpha = alphat;
                valid = false;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - alpha*fneq[k];
                    if (Ftemp[0][ix][iy][iz][k]<-1.0e-12)
                    {
                        //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        valid = true;
                        double temp =  F[0][ix][iy][iz][k]/fneq[k];
                        if (temp<alphat) alphat = temp;
                    }
                    //if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                    //{
                        //std::cout << "CollideSC: Nan found, resetting" << std::endl;
                        //std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                        //throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                    //}
                }
            }
        }
        else
        {
            for (size_t k=0;k<Nneigh;k++)
            {
                Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideMP()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        double den  = 0.0;
        for (size_t il=0;il<Nl;il++)
        {
            Vmix += Rho[il][ix][iy][iz]*Vel[il][ix][iy][iz]/Tau[il];
            den  += Rho[il][ix][iy][iz]/Tau[il];
        }
        Vmix /= den;

        for (size_t il=0;il<Nl;il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                double rho = Rho[il][ix][iy][iz];
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t vel = Vmix + dt*Tau[il]*BForce[il][ix][iy][iz]/rho; //+ Cs2*dt*Tau[il]*BForce[il][ix][iy][iz]/rho;
                bool valid = true;
                double alpha = 1.0;
                // while (valid)
                // {
                    valid = false;
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        double Feqtmp = Feq(k,nmol,vel);

                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(F[il][ix][iy][iz][k] - Feqtmp)/Tau[il];
                        
                        // if (Ftemp[il][ix][iy][iz][k]<-1.0e-12)
                        // {
                        //     //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        //     double temp =  Tau[il]*F[il][ix][iy][iz][k]/(F[il][ix][iy][iz][k] - Feqtmp);
                        //     if (temp<alpha) alpha = temp;
                        //         valid = true;
                        //     //std::cout << "Checking values .. temp = " << temp << "  ...and " << " Alpha=" << alpha << std::endl;
                        // }
                        // if (std::isnan(Ftemp[il][ix][iy][iz][k]))
                        // {
                        //     std::cout << "CollideMP: Nan found, resetting" << std::endl;
                        //     std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                        //     throw new Fatal("Domain::CollideMP: Distribution funcitons gave nan value, check parameters");
                        // }
                    }
                // }
            }
            else
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];
                }
            }
        }
    }
    
    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideEF() // Single Relaxation.
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        Vec3_t Vmixtmp = OrthoSys::O;
        double den  = 0.0;
        
        for (size_t il=0;il<Nl;il++)
        {
            // Vmix    += Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz]/Tau[il]; //! TEST
            Vmix    += (Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz]) / Tau[il];
            den     += Rho[il][ix][iy][iz]/Tau[il];
        }
        Vmix /= den;  

        for (size_t il=0;il<Nl;il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                // if(Rho[il][ix][iy][iz]>0.0)
                // {
                // double Q = 0.0;
                // double rho = Rho[il][ix][iy][iz];
                double invtau = 1.0/Tau[il]; // ~ compute fractions to improve computational efficiency
                double invRhoCs = 1.0/(Rho[il][ix][iy][iz] * Cs2);
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t vel = Vmix;// + 0.5*dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
                double ForceEq[Nneigh];
                double NonEq[Nneigh];
                // double Q = 0.0; // for LES

                for (size_t k = 0; k < Nneigh; k++)
                {
                    double feq  = Feq(k, nmol, vel);
                    ForceEq[k]  = dt * feq * dot(BForce[il][ix][iy][iz],C[k] - vel)*invRhoCs;
                    ForceEq[k] *= (1.0 - 0.5*invtau);
                    NonEq[k]    = F[il][ix][iy][iz][k] - feq; // Actual NonEq.
                    NonEq[k]   *= invtau; // Relaxed NonEq.
                    // Q +=  NonEq[k]*NonEq[k]*EEk[k];
                }
                // Q = sqrt(2.0*Q);
                // tau = sqrt(tau*tau + (0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2))))); Filtered mean for finite Reynolds
                // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));

                bool valid = true;
                // double alpha = 1.0;
                double alpha = 1.0, alphat = 1.0;
                size_t loopcount = 0.0, looprepeat = 1.0;
                size_t num =0;
                while (valid)
                {
                    num++;
                    alpha = alphat;
                    valid = false;
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(NonEq[k] - ForceEq[k]);
                        
                        // if ((Ftemp[il][ix][iy][iz][k])< -1.0e-12 && num<2)
                        // {
                        //     double temp =  fabs(F[il][ix][iy][iz][k]/(NonEq[k] - ForceEq[k]));  // !! This temp should result in Ftemp to complete cancle out, Ftemp=0
                            
                        //         if (loopcount==Time) 
                        //         {
                        //             // if (looprepeat<2.0) std::cout << "error: Possible Infinite Loop ---- at k= " << k << " ...values: temp= " << temp << "  ...and " << " Alpha=" << alpha << std::endl;
                        //             // else 
                        //             // {
                        //             //     std::cout << " ...# " << looprepeat << " ...loop repeat @ iteration= " << Time << " ...values: temp= " << temp << "... with Alpha=" << alpha << std::endl;//std::endl;
                        //             //     std::cout << "          @ Vector= {" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
                        //             // }
                        //             looprepeat ++;
                                    
                        //             if (looprepeat > Nneigh*2.0)
                        //             {
                        //                 std::cout << "Stopping simulation due to infinite loop (limit=repeat#>"<< Nneigh*2.0 << ")" << std::endl;
                        //                 std::cout <<  " Summary::  lattice(k)= " << k << " | component " << (il+1.0) << "" << std::endl;
                        //                 std::cout <<  "            iteration= " << Time << std::endl; //
                        //                 std::cout <<  "            Vector= {" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
                        //                 std::cout <<  "            ...values: temp= " << temp << "  ...and " << " Alpha= " << alpha << std::endl;
                        //                 // Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(F[il][ix][iy][iz][k] - FFeqtmp)/Tau[il] + dt*FForcetemp*Feqtmp;
                        //                 // It appears to run in inifinite loop due to the requirement for two alpha terms across a D2Q9 grid.
                        //                 // Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - temp*(F[il][ix][iy][iz][k] - FFeqtmp)/Tau[il] + dt*FForcetemp*Feqtmp;
                        //                 std::cout <<  " distribution Ftemp = " << Ftemp[il][ix][iy][iz][k] << std::endl;
                        //                 std::cout <<  "              F     = " << F[il][ix][iy][iz][k] << std::endl;
                        //                 std::cout <<  "              FeqF  = " << ForceEq[k] << std::endl;
                        //                 throw new Fatal("Stopping simulation due to infinite loop");
                        //             }
                        //         }
                        //         else
                        //         {
                        //             loopcount = Time;
                        //         }
                        //     if (temp<alphat) alphat = temp;
                        //         valid = true;
                        // }
                        if (std::isnan(Ftemp[il][ix][iy][iz][k]))
                        {
                            std::cout << "CollideEF: Nan found within fluid domain, resetting" << std::endl;
                            std::cout << " Alpha = " << alpha << " k= " << k << std::endl; //" component " << il << "" << std::endl;
                            std::cout <<  " Vector={" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
                            throw new Fatal("Domain::CollideEF: Distribution funcitons gave nan value, check parameters");
                        }
                    }
                }
                // }
            }
            else
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideEF_MRT()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        double den  = 0.0;
        int n=Nneigh,m=1;
        double a = 1.0,b = 0.0;

        for (size_t il=0;il<Nl;il++)
        {
                Vmix += (Rho[il][ix][iy][iz]*Vel[il][ix][iy][iz]+ 0.5*dt*BForce[il][ix][iy][iz])/Tau[il];
                den  += Rho[il][ix][iy][iz]/Tau[il];
        }
        Vmix /= den;

        for (size_t il=0;il<Nl;il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                if(Rho[il][ix][iy][iz]>0.0)
                {
                    double rho = Rho[il][ix][iy][iz];
                    double nmol = Nmol[il][ix][iy][iz];
                    Vec3_t vel = Vmix;// + 0.5*dt*BForce[il][ix][iy][iz]/rho;

                    double *f = F[il][ix][iy][iz]; // *f {1 by [Nneigh]}
                    double *ft= Ftemp[il][ix][iy][iz];
                    double feqt[Nneigh];
                    double Fft[Nneigh];
                    double feqtemp[Nneigh];
                    double fneq[Nneigh];
                    
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        Fft[k]  = dt*Feq(k,nmol,vel)*(dot(BForce[il][ix][iy][iz],(C[k]-vel))/(rho*Cs2));
                        feqt[k] = Feq(k,nmol,vel)*(1.0 - 0.5*(dot(BForce[il][ix][iy][iz],(C[k]-vel))/(rho*Cs2)));
                    }

                    dgemv_("N",&n,&n,&a,M.data,&n,feqt,&m,&b,feqtemp,&m);
                    dgemv_("N",&n,&n,&a,M.data,&n,f,&m,&b,ft,&m);

                    for (size_t k=0;k<Nneigh;k++)
                    {
                        ft[k] = SVec[il][k]*(ft[k] - feqtemp[k]);
                    }

                    dgemv_("N",&n,&n,&a,Minv.data,&n,ft,&m,&b,fneq,&m);

                    bool valid = true;
                    double alpha = 1.0, alphat = 1.0;
                    size_t num = 0;
                    while (valid)
                    {
                        num++;
                        alpha = alphat;
                        valid = false;
                        for (size_t k=0;k<Nneigh;k++)
                        {
                            Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(fneq[k] - Fft[k]);
                            if (Ftemp[il][ix][iy][iz][k]<-1.0e-12 && num<2)
                            {
                                //std::cout << Ftemp[il][ix][iy][iz][k] << std::endl;
                                double temp =  fabs(F[il][ix][iy][iz][k]/(fneq[k] - Fft[k]));
                                if (temp<alphat) alphat = temp;
                                valid = true;
                            }
                            if (std::isnan(Ftemp[il][ix][iy][iz][k]))
                            {
                                std::cout << "CollideEF - MRT: Nan found within fluid domain, resetting" << std::endl;
                                std::cout << " Alpha = " << alpha << " k= " << k << std::endl; //" component " << il << "" << std::endl;
                                std::cout <<  " Vector={" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
                                throw new Fatal("Domain::CollideEF - MRT: Distribution funcitons gave nan value, check parameters");
                            }
                        }
                    }
                }
            }
            else
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideEF_MRT_Test()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        double den  = 0.0;
        int n=Nneigh,m=1;
        double a = 1.0,b = 0.0;

        for (size_t il=0;il<Nl;il++)
        {
                Vmix += (Rho[il][ix][iy][iz]*Vel[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz])/Tau[il];
                den  += Rho[il][ix][iy][iz]/Tau[il];
        }
        Vmix /= den;

        for (size_t il=0;il<Nl;il++)
        {
            double rho = Rho[il][ix][iy][iz];
            double nmol = Nmol[il][ix][iy][iz];
            Vec3_t vel = Vmix;// + 0.5*dt*BForce[il][ix][iy][iz]/rho;
            double *f = F[il][ix][iy][iz]; // *f {1 by [Nneigh]}
            double *ft= Ftemp[il][ix][iy][iz];
            double feqt[Nneigh];
            double Fft[Nneigh];
            double Fftt[Nneigh];
            double feqtemp[Nneigh];
            double fneq[Nneigh];
            double Ffneq[Nneigh];
            
            for (size_t k=0;k<Nneigh;k++)
            {
                Fft[k]  = dt*Feq(k,nmol,vel)*(dot(BForce[il][ix][iy][iz],(C[k]-vel))/(rho*Cs2));
                feqt[k] = Feq(k,nmol,vel);// - 0.5*Fft[k];
            }

            dgemv_("N",&n,&n,&a,M.data,&n,feqt,&m,&b,feqtemp,&m);
            dgemv_("N",&n,&n,&a,M.data,&n,f,&m,&b,ft,&m);
            dgemv_("N",&n,&n,&a,M.data,&n,Fft,&m,&b,Fftt,&m);

            for (size_t k=0;k<Nneigh;k++)
            {
                ft[k] = SVec[il][k]*(ft[k] - feqtemp[k]);
                Fftt[k] = (1.0 - 0.5*SVec[il][k])*Fftt[k];
            }

            dgemv_("N",&n,&n,&a,Minv.data,&n,ft,&m,&b,fneq,&m);
            dgemv_("N",&n,&n,&a,Minv.data,&n,Fftt,&m,&b,Ffneq,&m);

                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - (fneq[k] - Ffneq[k]);
                }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}

inline void Domain::CollideEF_MP_DBC() // Single Relaxation.
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        // Vec3_t Vmixtmp = OrthoSys::O;
        double den = 0.0;

        for (size_t il = 0; il < Nl; il++)
        {
            Vmix    += (Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz]  + 0.5*dt*BForce[il][ix][iy][iz]) / Tau[il];
            den     += Rho[il][ix][iy][iz] / Tau[il];
        }
        Vmix /= den;

        for (size_t il = 0; il < Nl; il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                // double rho = Rho[il][ix][iy][iz];
                double invtau = 1.0/Tau[il]; // ~ compute fractions to improve computational efficiency
                double invRhoCs = 1.0/(Rho[il][ix][iy][iz] * Cs2);
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t vel = Vmix;// + 0.5*dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
                double ForceEq[Nneigh];
                double NonEq[Nneigh];
                // double Q = 0.0; // for LES

                for (size_t k = 0; k < Nneigh; k++)
                {
                    double feq  = Feq(k, nmol, vel);
                    ForceEq[k]  = dt * feq * dot(BForce[il][ix][iy][iz],C[k] - vel)*invRhoCs;
                    ForceEq[k] *= (1.0 - 0.5*invtau);
                    NonEq[k]    = F[il][ix][iy][iz][k] - feq; // Actual NonEq.
                    NonEq[k]   *= invtau; // Relaxed NonEq.
                    // Q +=  NonEq[k]*NonEq[k]*EEk[k];
                }
                // Q = sqrt(2.0*Q);
                // tau = sqrt(tau*tau + (0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2))))); // Filtered mean for finite Reynolds
                // tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));
                bool valid = true;
                double alpha = 1.0, alphat = 1.0;
                // size_t loopcount = 0.0, looprepeat = 1.0;
                size_t num = 0;
                // while (valid)
                // {
                //     num++;
                //     alpha = alphat;
                //     valid = false;
                    for (size_t k = 0; k < Nneigh; k++)
                    {
                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha * (NonEq[k] - ForceEq[k]);
                        // Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(NonEq[k]/tau - (1.0 - 0.5*(1.0/tau))*ForceEq[k]);

                        // if ((Ftemp[il][ix][iy][iz][k] < -1.0e-12) && (num < 2))
                        // {
                        //     double temp = fabs(F[il][ix][iy][iz][k] / (NonEq[k] - ForceEq[k])); // !! This temp should result in Ftemp to complete cancle out, Ftemp=0

                        //     if (temp < alphat)
                        //         alphat = temp;
                        //     valid = true;
                        // }
                        if (std::isnan(Ftemp[il][ix][iy][iz][k])) {
                            std::cout << "CollideEF_MP_DBC: Nan found within fluid domain, resetting" << std::endl;
                            std::cout << " Alpha = " << alpha << " k= " << k << std::endl;    //" component " << il << "" << std::endl;
                            std::cout << " Vector={" << ix << ", " << iy << "}" << std::endl; // iVec3_t(ix,iy,iz) << std::endl;
                            throw new Fatal("Domain::CollideEF_MP_DBC: Distribution funcitons gave nan value, check parameters");
                        }
                    }
                // }
            }
            else
            {
                // double rho = Rho[il][ix][iy][iz];
                Vec3_t velW = Vmix;//Vel[il][ix][iy][iz];// + 0.5*dt*BForce[il][ix][iy][iz]/rho;// (0.0, 0.0, 0.0);// 
                // Ftemp[il][ix][iy][iz][0] = F[il][ix][iy][iz][0];//
                if(dot(nvec[il][ix][iy][iz],nvec[il][ix][iy][iz])!=0.0)// continue;
                {
                    for (size_t k=0;k<Nneigh;k++) {
                        // if(!IsFluid[0][ix][iy][iz][k]) continue;
                        double FtempOp = 0.0; 
                        double FeqtmpOp = 0.0; 
                        double nC = dot((C[k]-velW),nvec[il][ix][iy][iz]);// nnvec);//
                        if (nC>=0.0){
                            for (size_t ka=0;ka<Nneigh;ka++){
                                double nC = dot((C[ka]-velW),nvec[il][ix][iy][iz]);///C_max;
                                if (nC>0.0) {
                                    FtempOp     += F[il][ix][iy][iz][Op[ka]]*fabs(nC);
                                    FeqtmpOp    += Geq(ka,velW)*fabs(nC);//*fabs(nC); //  feq' at the fluid node, to be reflected.
                                }
                            }
                            // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n"<< std::endl;
                            double FeqW        = Geq(k,velW);//Geq(k,velW);     // feq at wall, assuming rho_w=1.0 in domain with <rho>=1.0
                            Ftemp[il][ix][iy][iz][k] =  (FtempOp/FeqtmpOp)*FeqW;  //F[0][ix][iy][iz][Op[k]];//

                            if (std::isnan(Ftemp[il][ix][iy][iz][k])) Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];//FeqW;  //F[0][ix][iy][iz][k];//
                        }// if (std::isnan(Ftemp[0][ix][iy][iz][k])) Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//FeqW;  //F[0][ix][iy][iz][k];//
                        else {
                            Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];//
                        }
                    }
                }
                else
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];//
                    }
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}

inline void Domain::CollideEDM() // Exact Difference Method. 
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
    for (size_t il = 0; il < Nl; il++)
    {
        // size_t il = 0;
        // double rho = Rho[il][ix][iy][iz];
        double invtau = 1.0/Tau[il]; // ~ compute fractions to improve computational efficiency
        double nmol = Nmol[il][ix][iy][iz];
        Vec3_t vel = Vel[il][ix][iy][iz];
        Vec3_t velS = OrthoSys::O;
        velS = vel + dt*BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
        
        double ForceEq[Nneigh];
        double NonEq[Nneigh];

        for (size_t k = 0; k < Nneigh; k++)
        {
            double feq  = Feq(k, nmol, vel);
            ForceEq[k]  = Feq(k, nmol, velS) - feq;//*invtau;
            NonEq[k]    = (F[il][ix][iy][iz][k] - feq)*invtau; // Actual NonEq.
        }
        bool valid = true;
        double alpha = 1.0, alphat = 1.0;
        // size_t loopcount = 0.0, looprepeat = 1.0;
        size_t num = 0;
        while (valid)
        {
            num++;
            alpha = alphat;
            valid = false;
            for (size_t k = 0; k < Nneigh; k++)
            {
                Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(NonEq[k] - ForceEq[k]);
                if ((Ftemp[il][ix][iy][iz][k] < 0.0) && (num < 2))
                {
                    double temp = fabs(F[il][ix][iy][iz][k] / (NonEq[k] - ForceEq[k])); // !! This temp should result in Ftemp to complete cancle out, Ftemp=0
                    if (temp < alphat)
                        alphat = temp;
                    valid = true;
                }
            }
        }
    }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}

inline void Domain::CollideEF_MP_Test() // Multi-Comp, fully periodic Test Collide Function
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for num_threads(Nproc) schedule(static)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        double den = 0.0;

        for (size_t il = 0; il < Nl; il++)
        {
            Vmix    += (Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz]) / Tau[il];
            den     += Rho[il][ix][iy][iz] / Tau[il];
        }
        Vmix /= den;

        for (size_t il = 0; il < Nl; il++)
        {
                double invtau = 1.0/Tau[il]; // ~ compute fractions to improve computational efficiency
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t Acc = BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
                double ForceEq[Nneigh];
                double NonEq[Nneigh];
                double NNonEq[Nneigh];
                double * Fdist = F[il][ix][iy][iz];

                for (size_t k = 0; k < Nneigh; k++)
                {
                    double feq = Feq(k, nmol, Vmix);
                    NonEq[k] = Fdist[k] - feq; // Actual NonEq.
                    NNonEq[k] = NonEq[k];
                    NonEq[k] *= invtau; // Relaxed NonEq.
                }
                for (size_t k = 0; k < Nneigh; k++)
                {
                    // ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], NNonEq, Vmix, Acc, 2);
                    // ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], F[il][ix][iy][iz], Vmix, Acc, 2); 
                    // ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], F[il][ix][iy][iz], Vmix, Acc, Pij[il][ix][iy][iz], 2); // Should result in the same as the above if ForceEq is correct
                    ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], NNonEq, Vmix, Acc, Pij[il][ix][iy][iz], 2);
                    ForceEq[k] *= (1.0 - 0.5*invtau);
                }
            
            // bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            // size_t num = 0;
            // while (valid)
            // {
            //     num++;
            //     alpha = alphat;
            //     valid = false;
                for (size_t k = 0; k < Nneigh; k++)
                {
                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha * (NonEq[k] - ForceEq[k]);
                    // if ((Ftemp[il][ix][iy][iz][k]) < -1.0e-12 && num < 2)
                    // {
                    //     double temp = fabs(F[il][ix][iy][iz][k] / (NonEq[k] - ForceEq[k]));
                    //     if (temp < alphat)
                    //         alphat = temp;
                    //     valid = true;
                    // }
                }
            // }
        }
    }
    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}


inline void Domain::CollideEF_MP_Test_B() //  Multi-Comp, Test Collide Function inc. Diffuse Scattering
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        Vec3_t Vmix = OrthoSys::O;
        double den = 0.0;

        for (size_t il = 0; il < Nl; il++)
        {
            Vmix    += (Vel[il][ix][iy][iz]*Rho[il][ix][iy][iz] + 0.5*dt*BForce[il][ix][iy][iz]) / Tau[il];
            den     += Rho[il][ix][iy][iz] / Tau[il];
        }
        Vmix /= den;

        for (size_t il = 0; il < Nl; il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
                double invtau = 1.0/Tau[il]; // ~ compute fractions to improve computational efficiency
                double nmol = Nmol[il][ix][iy][iz];
                Vec3_t Acc = BForce[il][ix][iy][iz]/Rho[il][ix][iy][iz];
                double ForceEq[Nneigh];
                double NonEq[Nneigh];

                for (size_t k = 0; k < Nneigh; k++)
                {
                    double feq = Feq(k, nmol, Vmix);
                    NonEq[k] = F[il][ix][iy][iz][k] - feq; // Actual NonEq.
                    NonEq[k] *= invtau; // Relaxed NonEq.
                    // ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], F[il][ix][iy][iz], Vmix, Acc, 2); 
                    ForceEq[k] = dt*ForceFeq(k, Rho[il][ix][iy][iz], F[il][ix][iy][iz], Vmix, Acc, Pij[il][ix][iy][iz], 2); 
                    ForceEq[k] *= (1.0 - 0.5*invtau);
                }

                bool valid = true;
                double alpha = 1.0, alphat = 1.0;
                size_t num = 0;
                while (valid)
                {
                    num++;
                    alpha = alphat;
                    valid = false;
                    for (size_t k = 0; k < Nneigh; k++)
                    {
                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha * (NonEq[k] - ForceEq[k]);
                        // Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] - alpha*(NonEq[k]/tau - (1.0 - 0.5*(1.0/tau))*ForceEq[k]);

                        if ((Ftemp[il][ix][iy][iz][k]) < -1.0e-12 && num < 2)
                        {
                            double temp = fabs(F[il][ix][iy][iz][k] / (NonEq[k] - ForceEq[k])); // !! This temp should result in Ftemp to complete cancle out, Ftemp=0

                            if (temp < alphat)
                                alphat = temp;
                            valid = true;
                        }
                        if (std::isnan(Ftemp[il][ix][iy][iz][k]))
                        {
                            std::cout << "CollideEF: Nan found within fluid domain, resetting" << std::endl;
                            std::cout << " Alpha = " << alpha << " k= " << k << std::endl;    //" component " << il << "" << std::endl;
                            std::cout << " Vector={" << ix << ", " << iy << "}" << std::endl; // iVec3_t(ix,iy,iz) << std::endl;
                            throw new Fatal("Domain::CollideEF: Distribution funcitons gave nan value, check parameters");
                        }
                    }
                }
            }
            else
            {
                // double rho = Rho[il][ix][iy][iz];
                Vec3_t velW = Vmix;
                if(dot(nvec[il][ix][iy][iz],nvec[il][ix][iy][iz])!=0.0)
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        // if(!IsFluid[0][ix][iy][iz][k]) continue;
                        double FtempOp = 0.0; 
                        double FeqtmpOp = 0.0; 
                        double nC = dot((C[k]-velW),nvec[il][ix][iy][iz]);// nnvec);//
                        if (nC>=0.0)
                        {
                            for (size_t ka=0;ka<Nneigh;ka++)
                            {
                                double nC = dot((C[ka]-velW),nvec[il][ix][iy][iz]);///C_max;
                                if (nC>0.0)
                                {
                                    FtempOp     += F[il][ix][iy][iz][Op[ka]]*fabs(nC);
                                    FeqtmpOp    += Geq(ka,velW)*fabs(nC);//*fabs(nC); //  feq' at the fluid node, to be reflected.
                                }
                            }
                            // std::cout << "\n True, nc = " << nC << ", FtempOp = " << FtempOp << ", Feqtmp = " << FeqtmpOp << "\n"<< std::endl;
                            double FeqW        = Geq(k,velW);//Geq(k,velW);     // feq at wall, assuming rho_w=1.0 in domain with <rho>=1.0
                            Ftemp[il][ix][iy][iz][k] =  (FtempOp/FeqtmpOp)*FeqW;  //F[0][ix][iy][iz][Op[k]];//

                            if (std::isnan(Ftemp[il][ix][iy][iz][k])) Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];//FeqW;  //F[0][ix][iy][iz][k];//
                        }// if (std::isnan(Ftemp[0][ix][iy][iz][k])) Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];//FeqW;  //F[0][ix][iy][iz][k];//
                        else
                        {
                            Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k];//
                        }
                    }
                }
                else
                {
                    for (size_t k=0;k<Nneigh;k++)
                    {
                        Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];//
                    }
                }
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
    Fprev = tmp;
}

// Under Construction
    //inline void Domain::CollideEF_MRT()
    //{
    //    size_t nx = Ndim(0);
    //    size_t ny = Ndim(1);
    //    size_t nz = Ndim(2);
    //
    //#ifdef USE_OMP
    //#pragma omp parallel for schedule(static) num_threads(Nproc)
    //#endif
    //    for (size_t ix=0;ix<nx;ix++)
    //        for (size_t iy=0;iy<ny;iy++)
    //            for (size_t iz=0;iz<nz;iz++)
    //            {
    //                Vec3_t Vmix = OrthoSys::O;
    //                double den  = 0.0;
    //
    //                for (size_t il=0;il<Nl;il++)
    //                {
    //                    Vmix += Rho[il][ix][iy][iz]*Vel[il][ix][iy][iz]/Tau[il];
    //                    den  += Rho[il][ix][iy][iz]/Tau[il];
    //                }
    //                Vmix /= den;
    //
    //                for (size_t il=0;il<Nl;il++)
    //                {
    //                    if (!IsSolid[il][ix][iy][iz])
    //                    {
    //                        if(Rho[il][ix][iy][iz]>0.0)
    //                        {
    //                            double rho = Rho[il][ix][iy][iz];
    //                            double nmol = Nmol[il][ix][iy][iz];
    //                            Vec3_t vel = Vmix;
    //
    //                            bool valid = true;
    //                            double alpha = 1.0;
    //                            size_t loopcount = 0.0;
    //                            size_t looprepeat = 1.0;
    //                            while (valid)
    //                            {
    //                                valid = false;
    //
    //                                for (size_t k=0;k<Nneigh;k++)
    //                                {
    //                                    double summ = 0;
    //                                    for (size_t j=0;j<Nneigh;j++)
    //                                    {
    //                                        double XFeqtmp = Feq(j,nmol,vel);
    //                                        double XFtemp  = (dot(Cs2*BForce[il][ix][iy][iz],(C[j]-vel))/(rho*Cs2));
    //                                        double XFFeqtmp = (1.0 - 0.5*dt*XFtemp)*XFeqtmp;
    //                                        summ += MAlpha[il][j][k]*(F[il][ix][iy][iz][j] - XFFeqtmp);
    //                                    }
    //                                    double Feqtmp = Feq(k,nmol,vel);
    //                                    double FForcetemp  = (dot(Cs2*BForce[il][ix][iy][iz],(C[k]-vel))/(rho*Cs2));
    //                                    Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][k] + dt*FForcetemp*Feqtmp - alpha*summ;
    //
    //                                    if ((Ftemp[il][ix][iy][iz][k])<-1.0e-12)
    //                                    {
    //                                        double temp =  (F[il][ix][iy][iz][k] + dt*FForcetemp*Feqtmp)/summ; // !! This temp should result in Ftemp to complete cancle out, Ftemp=0
    //                                        if (loopcount==Time)
    //                                        {
    //                                            looprepeat ++;
    //
    //                                            if (looprepeat > Nneigh)
    //                                            {
    //                                                std::cout << "Stopping simulation due to infinite loop (limit=repeat#>9)" << std::endl;
    //                                                std::cout <<  " Summary::  lattice(k)= " << k << " | component " << (il+1.0) << "" << std::endl;
    //                                                std::cout <<  "            iteration= " << Time << std::endl; //
    //                                                std::cout <<  "            Vector= {" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
    //                                                std::cout <<  "            ...values: temp= " << temp << "  ...and " << " Alpha= " << alpha << std::endl;
    //                                                std::cout <<  " distribution Ftemp = " << Ftemp[il][ix][iy][iz][k] << std::endl;
    //                                                std::cout <<  "              F     = " << F[il][ix][iy][iz][k] << std::endl;
    //                                                std::cout <<  "              FeqF  = " << FForcetemp << std::endl;
    //                                                throw new Fatal("Stopping simulation due to infinite loop");
    //                                            }
    //                                        }
    //                                        else
    //                                        {
    //                                            loopcount = Time;
    //                                        }
    //
    //                                        if ((temp)<(alpha)) alpha = temp;
    //                                        valid = true;
    //                                        // }
    //                                        // alpha = temp;
    //                                        // valid = true;
    //                                        k --;
    //                                    }
    //                                    if (std::isnan(Ftemp[il][ix][iy][iz][k]))
    //                                    {
    //                                        std::cout << "CollideEF: Nan found within fluid domain, resetting" << std::endl;
    //                                        std::cout << " Alpha = " << alpha << " k= " << k << std::endl; //" component " << il << "" << std::endl;
    //                                        std::cout <<  " Vector={" << ix << ", " << iy << "}"<< std::endl;// iVec3_t(ix,iy,iz) << std::endl;
    //                                        throw new Fatal("Domain::CollideEF: Distribution funcitons gave nan value, check parameters");
    //                                    }
    //                                }
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        for (size_t k=0;k<Nneigh;k++)
    //                        {
    //                            Ftemp[il][ix][iy][iz][k] = F[il][ix][iy][iz][Op[k]];
    //                        }
    //                    }
    //                }
    //            }
    //
    //    double ***** tmp = F;
    //    F = Ftemp;
    //    Ftemp = tmp;
    //}
// ----

inline void Domain::CollideEF_SC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            double Q = 0.0;
            double tau   = Tau[0];
            double rho   = Rho[0][ix][iy][iz];
            double nmol = Nmol[0][ix][iy][iz];
            Vec3_t vel   = Vel[0][ix][iy][iz];// + 0.5*dt*BForce[0][ix][iy][iz]/rho;
            double ForceEq[Nneigh];
            double NonEq[Nneigh];
                
            for (size_t k=0;k<Nneigh;k++)
            {
                ForceEq[k]  = dt*Feq(k,nmol,vel)*dot(BForce[0][ix][iy][iz],(C[k]-vel))/(Cs2*rho);
                NonEq[k] = F[0][ix][iy][iz][k] - (Feq(k,nmol,vel) - 0.5*ForceEq[k]);
                    // NonEq[k] /=Tau[il];
                Q +=  NonEq[k]*NonEq[k]*EEk[k];
            }
            Q = sqrt(2.0*Q);
            tau = 0.5*(tau+sqrt(tau*tau + 2.0*Q*Sc/(rho*Cs2)));

            bool valid = true;
            double alpha = 1.0, alphat = 1.0;
            size_t num = 0;
            while (valid)
            {
                num++;
                alpha = alphat;
                valid = false;
                for (size_t k=0;k<Nneigh;k++)
                {
                    Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][k] - alpha*(NonEq[k]/tau - ForceEq[k]);
                    if (Ftemp[0][ix][iy][iz][k]<-1.0e-12&&num<2)
                    {
                        //std::cout << Ftemp[0][ix][iy][iz][k] << std::endl;
                        double temp =  fabs(F[0][ix][iy][iz][k]/(NonEq[k]/tau - ForceEq[k]));  // !! This temp should result in Ftemp to complete cancle out, Ftemp=0
                        if (temp<alphat) alphat = temp;
                        valid = true;
                    }
                    // if (std::isnan(Ftemp[0][ix][iy][iz][k]))
                    // {
                    //     std::cout << "CollideSC: Nan found, resetting" << std::endl;
                    //     std::cout << " " << alpha << " " << iVec3_t(ix,iy,iz) << " " << k << " " << std::endl;
                    //     throw new Fatal("Domain::CollideSC: Distribution funcitons gave nan value, check parameters");
                    // }
                }
            }
        }
        else
        //if    (IsSolid[0][ix][iy][iz])
        {
            for (size_t k=0;k<Nneigh;k++)
            {
                Ftemp[0][ix][iy][iz][k] = F[0][ix][iy][iz][Op[k]];                                        // bounce-back BC scheme --- 20170228
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;
}


inline void Domain::StreamSC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        for (size_t k=0;k<Nneigh;k++)
        {
            size_t nix = (size_t)((int)ix + (int)C[k](0) + (int)Ndim(0))%Ndim(0);
            size_t niy = (size_t)((int)iy + (int)C[k](1) + (int)Ndim(1))%Ndim(1);
            size_t niz = (size_t)((int)iz + (int)C[k](2) + (int)Ndim(2))%Ndim(2);
            Ftemp[0][nix][niy][niz][k] = F[0][ix][iy][iz][k];
            /*Use this to check periodic treatment*/
                // if(ix==0||ix>=nx-1) std::cout << "\n ix = " << ix << ", nix = " << nix << ",  C[k= "<< k << "] = "<< C[k](0) << "\n"<<std::endl;
                // if(iy==0||iy>=ny-1) std::cout << "\n iy = " << iy << ", niy = " << niy << ",  C[k= "<< k << "] = "<< C[k](1) << "\n"<<std::endl;
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        BForce[0][ix][iy][iz] = OrthoSys::O;
        Velprev [0][ix][iy][iz] = Vel   [0][ix][iy][iz];
        Nmolprev [0][ix][iy][iz] = Nmol   [0][ix][iy][iz];
        Vel   [0][ix][iy][iz] = OrthoSys::O;
        Rho   [0][ix][iy][iz] = 0.0;
        Nmol   [0][ix][iy][iz] = 0.0;
        if (!IsSolid[0][ix][iy][iz])
        {
            for (size_t k=0;k<Nneigh;k++)
            {
                Nmol[0][ix][iy][iz] +=  F[0][ix][iy][iz][k];
                Vel [0][ix][iy][iz] +=  F[0][ix][iy][iz][k]*C[k];
            }
            Rho[0][ix][iy][iz] = Nmol[0][ix][iy][iz]*molMass[0];
            Vel[0][ix][iy][iz] *= Cs/Rho[0][ix][iy][iz];
            // BForce[0][ix][iy][iz] = dt*Rho[0][ix][iy][iz]*(Vel[0][ix][iy][iz] - Velprev[0][ix][iy][iz]);
        }
    }
}


inline void Domain::StreamEF_SC()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        for (size_t k=0;k<Nneigh;k++)
        {
            size_t nix = (size_t)((int)ix + (int)C[k](0) + (int)Ndim(0))%Ndim(0);
            size_t niy = (size_t)((int)iy + (int)C[k](1) + (int)Ndim(1))%Ndim(1);
            size_t niz = (size_t)((int)iz + (int)C[k](2) + (int)Ndim(2))%Ndim(2);                          // periodic BC scheme 
            Ftemp[0][nix][niy][niz][k] = F[0][ix][iy][iz][k];                                            //
        }
    }

    double ***** tmp = F;
    F  = Ftemp;
    Ftemp = tmp;

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        if (!IsSolid[0][ix][iy][iz])
        {
            Velprev [0][ix][iy][iz] = Vel[0][ix][iy][iz];
            Nmolprev [0][ix][iy][iz] = Nmol[0][ix][iy][iz];
            BForce[0][ix][iy][iz] = OrthoSys::O; 
            Vel   [0][ix][iy][iz] = OrthoSys::O;                                                               // in fact the solid nodes are specified zero values. Good.   --- 20170302 
            Rho   [0][ix][iy][iz] = 0.0;
            Nmol  [0][ix][iy][iz] = 0.0;
        
            for (size_t k=0;k<Nneigh;k++)
            {
                Nmol[0][ix][iy][iz] +=  F[0][ix][iy][iz][k];                                               // calculating density
                Vel[0][ix][iy][iz] +=  F[0][ix][iy][iz][k]*C[k];
            }
            Rho[0][ix][iy][iz] = Nmol[0][ix][iy][iz]*molMass[0];
            // Vel[0][ix][iy][iz] += 0.5*dt*BForce[0][ix][iy][iz];  // calculating velocity
            Vel[0][ix][iy][iz] *= Cs/Rho[0][ix][iy][iz];
            // BForce[0][ix][iy][iz] = dt*Rho[0][ix][iy][iz]*(Vel[0][ix][iy][iz] - Velprev[0][ix][iy][iz]);
        }
        else{
            BForce[0][ix][iy][iz] = OrthoSys::O;
            Vel   [0][ix][iy][iz] = OrthoSys::O;
        }
    }
}


inline void Domain::StreamMP()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    for (size_t il=0;il<Nl;il++)
    {
        #ifdef USE_OMP
        #pragma omp parallel for schedule(static) num_threads(Nproc)
        #endif
        for (size_t ix=0;ix<nx;ix++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t iz=0;iz<nz;iz++)
        {
            for (size_t k=0;k<Nneigh;k++)
            {
                size_t nix = (size_t)((int)ix + (int)C[k](0) + (int)Ndim(0))%Ndim(0);
                size_t niy = (size_t)((int)iy + (int)C[k](1) + (int)Ndim(1))%Ndim(1);
                size_t niz = (size_t)((int)iz + (int)C[k](2) + (int)Ndim(2))%Ndim(2);
                Ftemp[il][nix][niy][niz][k] = F[il][ix][iy][iz][k];
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        for (size_t il=0;il<Nl;il++)
        {
            double mMass = molMass[il];
            BForce[il][ix][iy][iz] = OrthoSys::O;
            Vel   [il][ix][iy][iz] = OrthoSys::O;
            Rho   [il][ix][iy][iz] = 0.0;
            Nmol  [il][ix][iy][iz] = 0.0;
            if (!IsHBSolid[il][ix][iy][iz])
            {
                for (size_t k=0;k<Nneigh;k++)
                {
                    Nmol[il][ix][iy][iz] +=  F[il][ix][iy][iz][k];
                    Vel[il][ix][iy][iz] +=  F[il][ix][iy][iz][k]*C[k];
                }
                //
                
                Rho[il][ix][iy][iz] = Nmol[il][ix][iy][iz]*mMass; //
                Vel[il][ix][iy][iz] *= Cs/Rho[il][ix][iy][iz]; //
            }
        }
    }
}

inline void Domain::StreamMP_EF()
{
    size_t nx = Ndim(0), ny = Ndim(1), nz = Ndim(2);

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        for (size_t il=0;il<Nl;il++)
        {
            for (size_t k=0;k<Nneigh;k++)
            {
                size_t nix = (size_t)((int)ix + (int)C[k](0) + (int)Ndim(0))%Ndim(0);
                size_t niy = (size_t)((int)iy + (int)C[k](1) + (int)Ndim(1))%Ndim(1);
                size_t niz = (size_t)((int)iz + (int)C[k](2) + (int)Ndim(2))%Ndim(2);
                Ftemp[il][nix][niy][niz][k] = F[il][ix][iy][iz][k];
                /*Use this to check periodic treatment*/
                // if(ix==0||ix>=nx-1) std::cout << "\n ix = " << ix << ", nix = " << nix << ",  C[k= "<< k << "] = "<< C[k](0) << ""<<std::endl;
                // if(iy==0||iy>=ny-1) std::cout << " iy = " << iy << ", niy = " << niy << ",  C[k= "<< k << "] = "<< C[k](1) << "\n"<<std::endl;
            }
        }
    }

    double ***** tmp = F;
    F = Ftemp;
    Ftemp = tmp;

    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(Nproc)
    #endif
    for (size_t ix=0;ix<nx;ix++)
    for (size_t iy=0;iy<ny;iy++)
    for (size_t iz=0;iz<nz;iz++)
    {
        for (size_t il=0;il<Nl;il++)
        {
            if (!IsSolid[il][ix][iy][iz])
            {
            BForce[il][ix][iy][iz] = OrthoSys::O;
            Vel   [il][ix][iy][iz] = OrthoSys::O;
            Rho   [il][ix][iy][iz] = 0.0;
            Nmol  [il][ix][iy][iz] = 0.0;
            
            for (size_t k=0;k<Nneigh;k++){
                Nmol[il][ix][iy][iz] +=  F[il][ix][iy][iz][k];
                Vel[il][ix][iy][iz] +=  F[il][ix][iy][iz][k]*C[k];
            }

            Rho[il][ix][iy][iz] = Nmol[il][ix][iy][iz]*molMass[il];
            if (Nmol[il][ix][iy][iz]<=0.0) continue;
            Vel[il][ix][iy][iz] *=Cs/Rho[il][ix][iy][iz];
            }
            else{
                BForce[il][ix][iy][iz] = OrthoSys::O;
                Vel   [il][ix][iy][iz] = OrthoSys::O;
            }
        }
    }
}

#ifdef USE_OCL
    inline void Domain::UpLoadDevice()
    {

        bF         = new cl::Buffer [Nl];
        bFtemp     = new cl::Buffer [Nl];
        bIsSolid   = new cl::Buffer [Nl];
        bIsHBSolid   = new cl::Buffer [Nl];
        bBForce    = new cl::Buffer [Nl];
        bVel       = new cl::Buffer [Nl];
        bRho       = new cl::Buffer [Nl];
        
        blbmaux    = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(lbm_aux   )              );

        lbm_aux lbmaux[1];

        lbmaux[0].Nx = Ndim(0);
        lbmaux[0].Ny = Ndim(1);
        lbmaux[0].Nz = Ndim(2);
        lbmaux[0].Nneigh    = Nneigh;
        lbmaux[0].FNneigh   = FNneigh;
        lbmaux[0].NCPairs   = NCellPairs;
        lbmaux[0].Nl        = Nl;
        lbmaux[0].Gmix      = Gmix;
        lbmaux[0].Cs        = Cs;
        lbmaux[0].Cs2        = Cs2;
        lbmaux[0].Sc        = Sc;

        for (size_t nn=0;nn<Nneigh;nn++)
        {
        lbmaux[0].C  [nn].s[0] = C  [nn](0);
        lbmaux[0].C  [nn].s[1] = C  [nn](1);
        lbmaux[0].C  [nn].s[2] = C  [nn](2);
        lbmaux[0].EEk[nn]      = EEk[nn]   ;
        lbmaux[0].W  [nn]      = W  [nn]   ;
        lbmaux[0].Op [nn]      = Op [nn]   ;
        }
        for (size_t nn=0;nn<FNneigh;nn++)
        {
        lbmaux[0].Cf  [nn].s[0] = Cf  [nn](0);
        lbmaux[0].Cf  [nn].s[1] = Cf  [nn](1);
        lbmaux[0].Cf  [nn].s[2] = Cf  [nn](2);
        lbmaux[0].Wf  [nn]     = Wf  [nn]  ;
        }
        for (size_t il=0;il<Nl;il++)
        {
            lbmaux[0].Tau     [il]    = Tau     [il];
            lbmaux[0].G       [il]    = G       [il];
            lbmaux[0].Gs      [il]    = Gs      [il];
            lbmaux[0].Rhoref  [il]    = Rhoref  [il];
            lbmaux[0].Psi     [il]    = Psi     [il];

            bF       [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(double    )*Ncells*Nneigh);            
            bFtemp   [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(double    )*Ncells*Nneigh); 
            bIsSolid [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(bool      )*Ncells       );
            bIsHBSolid [il]     = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(bool      )*Ncells       );
            bBForce  [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(cl_double3)*Ncells       );     
            bVel     [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(cl_double3)*Ncells       ); 
            bRho     [il]       = cl::Buffer(CL_Context,CL_MEM_READ_WRITE,sizeof(double    )*Ncells       ); 


            bool            *IsSolidCL,*IsHBSolidCL;
            double          *FCL,*FtempCL,*RhoCL;
            cl_double3      *VelCL,*BForceCL;
            FCL             = new double    [Ncells*Nneigh];
            FtempCL         = new double    [Ncells*Nneigh];
            IsSolidCL       = new bool      [Ncells       ];
            IsHBSolidCL     = new bool      [Ncells       ];
            RhoCL           = new double    [Ncells       ];
            VelCL           = new cl_double3[Ncells       ];
            BForceCL        = new cl_double3[Ncells       ];
        
            size_t Nm = 0;
            for (size_t nz=0;nz<Ndim(2);nz++)
            {
                for (size_t ny=0;ny<Ndim(1);ny++)
                {
                    for (size_t nx=0;nx<Ndim(0);nx++)
                    {
                        IsSolidCL[Nm]      = IsSolid    [il][nx][ny][nz];
                        IsHBSolidCL[Nm]    = IsHBSolid  [il][nx][ny][nz];
                        RhoCL    [Nm]      = Rho        [il][nx][ny][nz];
                        VelCL    [Nm].s[0] = Vel        [il][nx][ny][nz][0];
                        VelCL    [Nm].s[1] = Vel        [il][nx][ny][nz][1];
                        VelCL    [Nm].s[2] = Vel        [il][nx][ny][nz][2];
                        BForceCL [Nm].s[0] = BForce     [il][nx][ny][nz][0];
                        BForceCL [Nm].s[1] = BForce     [il][nx][ny][nz][1];
                        BForceCL [Nm].s[2] = BForce     [il][nx][ny][nz][2];
                        Nm++;
                        for (size_t nn=0;nn<Nneigh;nn++)
                        {
                            FCL    [Nn] = F    [il][nx][ny][nz][nn];
                            FtempCL[Nn] = Ftemp[il][nx][ny][nz][nn];
                            Nn++;
                        }
                    }
                }
            }

            CL_Queue.enqueueWriteBuffer(bF      [il],CL_TRUE,0,sizeof(double    )*Ncells*Nneigh,FCL       );  
            CL_Queue.enqueueWriteBuffer(bFtemp  [il],CL_TRUE,0,sizeof(double    )*Ncells*Nneigh,FtempCL   );  
            CL_Queue.enqueueWriteBuffer(bIsSolid[il],CL_TRUE,0,sizeof(bool      )*Ncells       ,IsSolidCL );  
            CL_Queue.enqueueWriteBuffer(bIsHBSolid[il],CL_TRUE,0,sizeof(bool      )*Ncells     ,IsHBSolidCL);  
            CL_Queue.enqueueWriteBuffer(bBForce [il],CL_TRUE,0,sizeof(cl_double3)*Ncells       ,BForceCL  );  
            CL_Queue.enqueueWriteBuffer(bVel    [il],CL_TRUE,0,sizeof(cl_double3)*Ncells       ,VelCL     );  
            CL_Queue.enqueueWriteBuffer(bRho    [il],CL_TRUE,0,sizeof(double    )*Ncells       ,RhoCL     );  
            

            delete [] FCL       ;
            delete [] FtempCL   ;
            delete [] IsSolidCL ;
            delete [] IsHBSolidCL ;
            delete [] RhoCL     ;
            delete [] VelCL     ;
            delete [] BForceCL  ;
        }
        CL_Queue.enqueueWriteBuffer(blbmaux  ,CL_TRUE,0,sizeof(lbm_aux   )              ,lbmaux    );  

        cl::Kernel kernel = cl::Kernel(CL_Program,"CheckUpLoad");
        kernel.setArg(0,blbmaux );
        CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(1),cl::NullRange);
        CL_Queue.finish();
    }

    inline void Domain::DnLoadDevice()
    {
        for (size_t il=0;il<Nl;il++)
        {
            double     * RhoCL;
            cl_double3 * VelCL;
            RhoCL      = new double    [Ncells];
            VelCL      = new cl_double3[Ncells];

            CL_Queue.enqueueReadBuffer(bRho[il],CL_TRUE,0,sizeof(double    )*Ncells,RhoCL);
            CL_Queue.enqueueReadBuffer(bVel[il],CL_TRUE,0,sizeof(cl_double3)*Ncells,VelCL);

            size_t Nm = 0;
            for (size_t nz=0;nz<Ndim(2);nz++)
            {
                for (size_t ny=0;ny<Ndim(1);ny++)
                {
                    for (size_t nx=0;nx<Ndim(0);nx++)
                    {
                        Rho[il][nx][ny][nz]    =  RhoCL[Nm]     ;
                        Vel[il][nx][ny][nz][0] =  VelCL[Nm].s[0];
                        Vel[il][nx][ny][nz][1] =  VelCL[Nm].s[1];
                        Vel[il][nx][ny][nz][2] =  VelCL[Nm].s[2];
                        Nm++;
                    }
                }
            }
            delete [] RhoCL     ;
            delete [] VelCL     ;
        }
    }

    inline void Domain::ApplyForceCL()
    {
        cl::Kernel kernel;
        if (Nl==1)
        {
            kernel = cl::Kernel(CL_Program,"ApplyForcesSC"  );
            kernel.setArg(0,bIsSolid[0]);
            kernel.setArg(1,bBForce [0]);
            kernel.setArg(2,bRho    [0]);
            kernel.setArg(3,blbmaux    );
            CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
            CL_Queue.finish();
        }
        else if ((fabs(Gs[0])>1.0e-12)||(fabs(Gs[1])>1.0e-12))
        {
            kernel = cl::Kernel(CL_Program,"ApplyForcesSCMP");
            kernel.setArg(0,bIsHBSolid[0]);
            kernel.setArg(1,bIsHBSolid[1]);
            kernel.setArg(2,bBForce [0]);
            kernel.setArg(3,bBForce [1]);
            kernel.setArg(4,bRho    [0]);
            kernel.setArg(5,bRho    [1]);
            kernel.setArg(6,blbmaux    );
            CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
            CL_Queue.finish();
        }
        else
        {
            kernel = cl::Kernel(CL_Program,"ApplyForcesSCMP"  );
            kernel.setArg(0,bIsHBSolid[0]);
            kernel.setArg(1,bIsHBSolid[1]);
            kernel.setArg(2,bBForce [0]);
            kernel.setArg(3,bBForce [1]);
            kernel.setArg(4,bRho    [0]);
            kernel.setArg(5,bRho    [1]);
            kernel.setArg(6,blbmaux    );
            CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
            CL_Queue.finish();
        }
    }

    inline void Domain::CollideCL()
    {
        cl::Kernel kernel;
        if (Nl==1)   
        {
            kernel = cl::Kernel(CL_Program,"CollideSC");
            kernel.setArg(0,bIsSolid[0]);
            kernel.setArg(1,bF      [0]);
            kernel.setArg(2,bFtemp  [0]);
            kernel.setArg(3,bBForce [0]);
            kernel.setArg(4,bVel    [0]);
            kernel.setArg(5,bRho    [0]);
            kernel.setArg(6,blbmaux    );
        }
        else
        {        
            kernel = cl::Kernel(CL_Program,"CollideEF"); // CHANGE FOR MP or EF implementation when needed
            kernel.setArg( 0,bIsSolid[0]);
            kernel.setArg( 1,bIsSolid[1]);
            kernel.setArg( 2,bF      [0]);
            kernel.setArg( 3,bF      [1]);
            kernel.setArg( 4,bFtemp  [0]);
            kernel.setArg( 5,bFtemp  [1]);
            kernel.setArg( 6,bBForce [0]);
            kernel.setArg( 7,bBForce [1]);
            kernel.setArg( 8,bVel    [0]);
            kernel.setArg( 9,bVel    [1]);
            kernel.setArg(10,bRho    [0]);
            kernel.setArg(11,bRho    [1]);
            kernel.setArg(12,blbmaux    );
        }
        CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
        CL_Queue.finish();
    }

    inline void Domain::StreamCL()
    {
        for (size_t il=0;il<Nl;il++)
        {
            cl::Kernel kernel = cl::Kernel(CL_Program,"Stream1");
            kernel.setArg(0,bF      [il]);
            kernel.setArg(1,bFtemp  [il]);
            kernel.setArg(2,blbmaux     );
            CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
            CL_Queue.finish();

            kernel            = cl::Kernel(CL_Program,"StreamEF"); //// originally: Stream2.
            kernel.setArg(0,bIsHBSolid[il]);
            kernel.setArg(1,bF      [il]);
            kernel.setArg(2,bFtemp  [il]);
            kernel.setArg(3,bBForce [il]);
            kernel.setArg(4,bVel    [il]);
            kernel.setArg(5,bRho    [il]);
            kernel.setArg(6,blbmaux     );
            CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(Ncells),cl::NullRange);
            CL_Queue.finish();
        }
    }
#endif


inline void Domain::Solve(double Tf, double dtOut, double whenOut, ptDFun_t ptSetup, ptDFun_t ptReport,
                          char const * TheFileKey, bool RenderVideo, size_t TheNproc) // !! Would be good to add the ability to continue simulations.
{
    #ifdef USE_OCL 
        std::vector<cl::Platform> all_platforms;
        cl::Platform::get(&all_platforms);
        if (all_platforms.size()==0)
        {
            throw new Fatal("FLBM::Domain: There are no GPUs or APUs, please compile with the A_USE_OCL flag turned off");
        }
        cl::Platform default_platform=all_platforms[0];
        std::vector<cl::Device> all_devices; 
        default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
        if (all_devices.size()==0)
        {
            throw new Fatal("FLBM::Domain: There are no GPUs or APUs, please compile with the A_USE_OCL flag turned off");
        }
        CL_Device  = all_devices[0];
        CL_Context = cl::Context(CL_Device);
    
        cl::Program::Sources sources;

        char* pMECHSYS_ROOT;
        pMECHSYS_ROOT = getenv ("MECHSYS_ROOT");
        if (pMECHSYS_ROOT==NULL) pMECHSYS_ROOT = getenv ("HOME");

        String pCL;
        pCL.Printf("%s/mechsys/lib/flbm/lbm.cl",pMECHSYS_ROOT);

        std::ifstream infile(pCL.CStr(),std::ifstream::in);
        std::string kernel_code((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());

        sources.push_back({kernel_code.c_str(),kernel_code.length()}); 

        CL_Program = cl::Program(CL_Context,sources);
        if(CL_Program.build({CL_Device})!=CL_SUCCESS){
            std::cout<<" Error building: "<<CL_Program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(CL_Device)<<"\n";
            exit(1);
        }

        CL_Queue   = cl::CommandQueue(CL_Context,CL_Device);

        N_Groups   = CL_Device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
        #endif


        idx_out     = 0;
        FileKey.Printf("%s",TheFileKey);
        Nproc = TheNproc;

        Util::Stopwatch stopwatch;
        printf("\n%s--- Solving ---------------------------------------------------------------------%s\n",TERM_CLR1    , TERM_RST);
        if (dtR!=1.0)
        {
            printf("%s  Time step [Real time]            =  %g%s\n"       ,TERM_CLR2, dtR                                    , TERM_RST);
        }
        printf("%s  Time step                        =  %g%s\n"       ,TERM_CLR2, dt                                    , TERM_RST);
        for (size_t i=0;i<Nl;i++)
        {
        printf("%s  Tau of Lattice %zd                 =  %g%s\n"       ,TERM_CLR2, i, Tau[i]                             , TERM_RST);
        }
        #ifdef USE_OCL
        //printf("%s  Using GPU:                       =  %c%s\n"     ,TERM_CLR2, Default_Device.getInfo<CL_DEVICE_NAME>(), TERM_RST);
        std::cout 
        << TERM_CLR2 
        << "  Using GPU:                       =  " << CL_Device.getInfo<CL_DEVICE_NAME>() << TERM_RST << std::endl;
    #endif

    //std::cout << "1" << std::endl;
    #ifdef USE_OCL
    UpLoadDevice();
    #endif

    #ifdef USE_HDF5
    String fileParameters;
    fileParameters.Printf    ("%s_Parameters",TheFileKey);
    WritePARAMETERS(fileParameters.CStr());
    #endif
    
    //std::cout << "2" << std::endl;
    double tout = Time; 
    if (whenOut==0)     // Start recording now..
    {
        tout = Time;
    }
    else                // Start recording later.. 
    {
        tout = whenOut;
    }
    
    if (Nl==1) //* Single-component (with/without Multiphase)
    {
        while (Time < Tf)
            {
                if ((Time >= tout) || (Time==(Tf - 1.0)))
                {
                    //std::cout << "3" << std::endl;
                    #ifdef USE_OCL
                    DnLoadDevice();
                    #endif
                    //std::cout << "4" << std::endl;
                    if (TheFileKey!=NULL)
                    {
                        if (ptReport!=NULL) (*ptReport) ((*this), UserData);
                        String fn;
                        fn.Printf    ("%s_%04d", TheFileKey, idx_out);
                        String fileTmpOutput;
                        fileTmpOutput.Printf    ("%s_Out-tmp",TheFileKey);
                        #ifdef USE_HDF5
                        
                        if ( RenderVideo) {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        else {
                            if (Time == whenOut)
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        #endif
                    }
                    tout += dtOut;
                    idx_out++;
                }
                // if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);

                //The LBM dynamics

                #ifdef USE_OCL
                ApplyForceCL();
                CollideCL();
                StreamCL();
                #else
                //* LB Algorithm -----
                    // if      (fabs(G[0]) >1.0e-12) ApplyForcesSC();
                    // else if (fabs(Gs[0])>1.0e-12) ApplyForcesSCSS();
                    // CollideEF_SC();
                    // StreamEF_SC();
                    // CollideEF_MRT();
                    // StreamMP_EF();
                    // CollideSC_DBC();
                    // CollideEF();
                    CollideSC_EFDBC();
                    // CollideEDM();
                    // CollideDBC();
                    // CollideSC();
                    // StreamSC();
                    StreamEF_SC();
                    // FluidSolidInteraction();
                    CalculatePotentials_SC();
                    FFApplyForcesE_SC();
                //* --------------------
                #endif
                // StorePrevious();
                if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);
                Time += dt;
            }
    }
    else if (Nl==2) //* Multicomponent Flows
    {
        if ((fabs(Gs[0])>1.0e-12)||(fabs(Gs[1])>1.0e-12)) //* Including FSI
        {
            while (Time < Tf)
            {
                if ((Time >= tout) || (Time==(Tf - 1.0)))
                {
                    //std::cout << "3" << std::endl;
                    #ifdef USE_OCL
                    DnLoadDevice();
                    #endif
                    //std::cout << "4" << std::endl;
                    if (TheFileKey!=NULL)
                    {
                        if (ptReport!=NULL) (*ptReport) ((*this), UserData);
                        String fn;
                        fn.Printf    ("%s_%04d", TheFileKey, idx_out);
                        String fileTmpOutput;
                        fileTmpOutput.Printf    ("%s_Out-tmp",TheFileKey);
                        #ifdef USE_HDF5
                        
                        if ( RenderVideo) {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        else {
                            if (Time == whenOut)
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        #endif
                    }
                    tout += dtOut;
                    idx_out++;
                }
                // if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);

                //The LBM dynamics

                #ifdef USE_OCL
                ApplyForceCL();
                CollideCL();
                StreamCL();
                #else
                //* LB Algorithm -----
                    // ApplyForcesSCMP();
                    // ApplyForcesTEST();
                    // CollideEF_MRT();
                    // CollideMP();
                    // StreamMP();

                    // ApplyForcesE();
                    CollideEF();
                    
                    // CollideEF_MP_DBC();
                    StreamMP_EF();
                    CalculatePotentials();
                    FFApplyForcesE();
                    // FluidSolidInteraction();
                    // CollideEF_MP_DBC();
                    // StreamMP_EF();
                    // ApplyForcesE();
                    // CalculatePotentials();
                    // FFApplyForcesE();
                //* --------------------
                #endif
                // StorePrevious();
                if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);
                Time += dt;
            }
        }
        else //* Absence of FSI
        {
            while (Time < Tf)
            {
                if ((Time >= tout) || (Time==(Tf - 1.0)))
                {
                    //std::cout << "3" << std::endl;
                    #ifdef USE_OCL
                    DnLoadDevice();
                    #endif
                    //std::cout << "4" << std::endl;
                    if (TheFileKey!=NULL)
                    {
                        if (ptReport!=NULL) (*ptReport) ((*this), UserData);
                        String fn;
                        fn.Printf    ("%s_%04d", TheFileKey, idx_out);
                        String fileTmpOutput;
                        fileTmpOutput.Printf    ("%s_Out-tmp",TheFileKey);
                        #ifdef USE_HDF5
                        
                        if ( RenderVideo) {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        else {
                            if (Time == whenOut)
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                            if (Time >= (Tf-1.0*dtOut))
                            {
                                WriteXDMF(fn.CStr());
                                // #else
                                //WriteVTK (fn.CStr());
                                WritePARAMETERS(fileTmpOutput.CStr());
                            }
                        }
                        #endif
                    }
                    tout += dtOut;
                    idx_out++;
                }
                // if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);

                //The LBM dynamics

                #ifdef USE_OCL
                ApplyForceCL();
                CollideCL();
                StreamCL();
                #else
                //* LB Algorithm -----
                    // StorePrevious();
                    // FApplyForcesSCMP();
                    // ApplyForcesTEST();
                    // CollideMP();
                    // CollideEF_MRT();
                    // CollideEF_MRT_Test();
                    // CollideEDM();
                    CollideEF_MP_DBC();
                    // CollideEF_MP_Test();
                    // CollideEF_MP_Test_B();
                    // CollideEF();
                    // FluidSolidInteraction(); //! Maybe Needed for Diffuse Scattering
                    // StreamMP();
                    StreamMP_EF();
                    CalculatePotentials();
                    FFApplyForcesE();
                    // PressureTensorCalc();
                    // ApplyForcesSCMP();
                    // FApplyForcesMPI();
                //* -----------------------
                #endif
                // StorePrevious();
                if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);
                Time += dt;
            }
        }
    }
    else //* >2 components
    {
        while (Time < Tf)
        {
            if ((Time >= tout) || (Time==(Tf - 1.0)))
            {
                //std::cout << "3" << std::endl;
                #ifdef USE_OCL
                DnLoadDevice();
                #endif
                //std::cout << "4" << std::endl;
                if (TheFileKey!=NULL)
                {
                    if (ptReport!=NULL) (*ptReport) ((*this), UserData);
                    String fn;
                    fn.Printf    ("%s_%04d", TheFileKey, idx_out);
                    String fileTmpOutput;
                    fileTmpOutput.Printf    ("%s_Out-tmp",TheFileKey);
                    #ifdef USE_HDF5
                    
                    if ( RenderVideo) {
                        WriteXDMF(fn.CStr());
                        // #else
                        //WriteVTK (fn.CStr());
                        WritePARAMETERS(fileTmpOutput.CStr());
                        if (Time >= (Tf-1.0*dtOut))
                        {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                        }
                    }
                    else {
                        if (Time == whenOut)
                        {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                        }
                        if (Time >= (Tf-1.0*dtOut))
                        {
                            WriteXDMF(fn.CStr());
                            // #else
                            //WriteVTK (fn.CStr());
                            WritePARAMETERS(fileTmpOutput.CStr());
                        }
                    }
                    #endif
                }
                tout += dtOut;
                idx_out++;
            }
            // if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);

            //The LBM dynamics

            #ifdef USE_OCL
            ApplyForceCL();
            CollideCL();
            StreamCL();
            #else
            //* LB Algorithm -----
                // ApplyForces3E(); // Still under construction.
                // CollideEF_MRT();
                // CollideEF();
                // StreamMP_EF();
                // --------------
                CollideEF_MP_DBC();
                StreamMP_EF();
                FApplyForces3E();
            //* --------------------
            #endif
            // StorePrevious();
            if (ptSetup!=NULL) (*ptSetup) ((*this), UserData);
            Time += dt;
            //std::cout << Time << std::endl;
        }
    }

    printf("\n%s Simulation Complete !! %s\n",TERM_CLR1,TERM_RST);
    // * Write findal simulation output data
    #ifdef USE_HDF5
    printf("\n%s ... Writing Output File ... %s\n",TERM_CLR2,TERM_RST);
    String fn;
    fn.Printf    ("%s_Extra", TheFileKey);
    WriteXDMF(fn.CStr());
    String fileOutput;
    fileOutput.Printf    ("%s_Out",TheFileKey);
    WritePARAMETERS(fileOutput.CStr());
    printf("\n%s ... Done !! %s\n",TERM_CLR2,TERM_RST);
    #endif

}

/* Interaction Forcing models */
    const double Domain::FWEIGHTSE4    [ 8] = { 1.0/3.0, 1.0/3.0, 1.0/3.0, 1.0/3.0, 1.0/12.0, 1.0/12.0, 1.0/12.0, 1.0/12. };
    const double Domain::FWEIGHTSE8    [24] = { 4.0/21.0, 4.0/21.0, 4.0/21.0, 4.0/21.0, // wf(1)
                                                4.0/45.0, 4.0/45.0, 4.0/45.0, 4.0/45.0, // wf(2)
                                                1.0/60.0, 1.0/60.0, 1.0/60.0, 1.0/60.0, // wf(4)
                                                2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, // wf(5)
                                                1.0/5040.0, 1.0/5040.0, 1.0/5040.0, 1.0/5040. // wf(8)
                                            };

    const double Domain::FWEIGHTSE10   [36] = { 262.0/1785.0, 262.0/1785.0, 262.0/1785.0, 262.0/1785.0, // wf(1) 
                                                93.0/1190.0, 93.0/1190.0, 93.0/1190.0, 93.0/1190.0, // wf(2)
                                                7.0/340.0, 7.0/340.0, 7.0/340.0, 7.0/340.0, // wf(4)
                                                6.0/595.0, 6.0/595.0, 6.0/595.0, 6.0/595.0,6.0/595.0, 6.0/595.0, 6.0/595.0, 6.0/595.0, // wf(5)
                                                9.0/9520.0, 9.0/9520.0, 9.0/9520.0, 9.0/9520.0, // wf(8)
                                                2.0/5355.0, 2.0/5355.0, 2.0/5355.0, 2.0/5355.0, // wf(9)
                                                1.0/7140.0, 1.0/7140.0, 1.0/7140.0, 1.0/7140.0,1.0/7140.0, 1.0/7140.0, 1.0/7140.0, 1.0/7140. // wf(10)
                                            }; // Ref. Sbragaglia PhysRev.E75, 026702 (2007)

    const double Domain::FWEIGHTSE12   [48] = { 68.0/585.0, 68.0/585.0, 68.0/585.0, 68.0/585.0, // wf(1) 
                                                68.0/1001.0, 68.0/1001.0, 68.0/1001.0, 68.0/1001.0, // wf(2)
                                                1.0/45.0, 1.0/45.0, 1.0/45.0, 1.0/45.0, // wf(4)
                                                62.0/5005.0, 62.0/5005.0, 62.0/5005.0, 62.0/5005.0,62.0/5005.0, 62.0/5005.0, 62.0/5005.0, 62.0/5005.0, // wf(5)
                                                1.0/520.0, 1.0/520.0, 1.0/520.0, 1.0/520.0, // wf(8)
                                                4.0/4095.0, 4.0/4095.0, 4.0/4095.0, 4.0/4095.0, // wf(9)
                                                2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, // wf(10)
                                                2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, // wf(13)
                                                1.0/480480.0, 1.0/480480.0, 1.0/480480.0, 1.0/480480.0, // wf(16)
                                            }; // Ref. Sbragaglia PhysRev.E75, 026702 (2007) //* Note in Ref.<<--, W(17)==0. Hence there is only 48 vectors.
    // const double Domain::FWEIGHTSE14   [80] = { // As is, Pressure Tensor Calc cannot consider interaction range this large. [unfinished.]
    //                                             68.0/585.0, 68.0/585.0, 68.0/585.0, 68.0/585.0, // wf(1) 
    //                                             68.0/1001.0, 68.0/1001.0, 68.0/1001.0, 68.0/1001.0, // wf(2)
    //                                             1.0/45.0, 1.0/45.0, 1.0/45.0, 1.0/45.0, // wf(4)
    //                                             6.0/5005.0, 6.0/5005.0, 6.0/5005.0, 6.0/5005.0,6.0/5005.0, 6.0/5005.0, 6.0/5005.0, 6.0/5005.0, // wf(5)
    //                                             1.0/520.0, 1.0/520.0, 1.0/520.0, 1.0/520.0, // wf(8)
    //                                             4.0/4095.0, 4.0/4095.0, 4.0/4095.0, 4.0/4095.0, // wf(9)
    //                                             2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, 2.0/4095.0, // wf(10)
    //                                             2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, 2.0/45045.0, // wf(13)
    //                                             1.0/4804800.0, 1.0/4804800.0, 1.0/4804800.0, 1.0/4804800.0, // wf(16)
    //                                             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, // wf(17)
    //                                             4.0/21.0, 4.0/21.0, 4.0/21.0, 4.0/21.0, // wf(18)
    //                                             4.0/45.0, 4.0/45.0, 4.0/45.0, 4.0/45.0, 1.0/60.0, 1.0/60.0, 1.0/60.0, 1.0/60.0, // wf(20)
    //                                             1.0/5040.0, 1.0/5040.0, 1.0/5040.0, 1.0/5040.0, // wf(25) (5-0)
    //                                             2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315.0, 2.0/315. // wf(25) (3-4)
    //                                         }; // Ref. Sbragaglia PhysRev.E75, 026702 (2007)

    const Vec3_t Domain::LVELOCDFE4  [ 8] = { {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}, {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} };

    const Vec3_t Domain::LVELOCDFE8  [24]  = {   {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}
                                                ,{1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}
                                                ,{2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0}
                                                ,{2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0}
                                                ,{2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0}
                                            }; // This considers a interaction range of 2-bands (bands of square lattices)

    const Vec3_t Domain::LVELOCDFE10  [36]  = {  {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}
                                                ,{1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}
                                                ,{2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0}
                                                ,{2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0}
                                                ,{2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0}
                                                ,{3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0}
                                                ,{3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0}
                                            };
    const Vec3_t Domain::LVELOCDFE12  [48]  = {  {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} // wf(1)
                                                ,{1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} // wf(2)
                                                ,{2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0} // wf(4)
                                                ,{2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0} // wf(5)
                                                ,{2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0} // wf(8)
                                                ,{3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0} // wf(9)
                                                ,{3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0} // wf(10)
                                                ,{3,2,0}, {2,3,0}, {-2,3,0}, {-3,2,0}, {-3,-2,0}, {-2,-3,0}, {2,-3,0}, {3,-2,0} // wf(13)
                                                ,{4,0,0}, {0,4,0}, {-4,0,0}, {0,-4,0} // wf(16)
                                            };
    // const Vec3_t Domain::LVELOCDFE14  [80]  = { // As is, Pressure Tensor Calc cannot consider interaction range this large. [unfinished.]
    //                                             {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}, // WF(1)
    //                                             {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}, // WF(2)
    //                                             {2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0}, // WF(4)
    //                                             {2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0}, // WF(5)
    //                                             {2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0}, // WF(8)
    //                                             {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0}, // WF(9)
    //                                             {3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0}, // WF(10)
    //                                             {3,2,0}, {2,3,0}, {-2,3,0}, {-3,2,0}, {-3,-2,0}, {-2,-3,0}, {2,-3,0}, {3,-2,0},// WF(12)
    //                                             {4,0,0}, {0,4,0}, {-4,0,0}, {0,-4,0}, // WF(16)
    //                                             {4,1,0}, {1,4,0}, {-1,4,0}, {-4,1,0}, {-4,-1,0}, {-1,-4,0}, {1,-4,0}, {4,-1,0}, // WF(17)
    //                                             {3,3,0}, {-3,3,0}, {-3,-3,0}, {3,-3,0}, // WF(18)
    //                                             {4,2,0}, {2,4,0}, {-2,4,0}, {-4,2,0}, {-4,-2,0}, {-2,-4,0}, {2,-4,0}, {4,-2,0}, // WF(20)
    //                                             {5,0,0}, {0,5,0}, {-5,0,0}, {0,-5,0}, // WF(25) (5-0)
    //                                             {4,3,0}, {3,4,0}, {-3,4,0}, {-4,3,0}, {-4,-3,0}, {-3,-4,0}, {3,-4,0}, {4,-3,0} // WF(25) (3-4)
    //                                         };

/* Lattice Models */
    const double Domain::WEIGHTSD2Q5   [ 5] = { 2.0/6.0, 1.0/6.0, 1.0/6.0, 1.0/6.0, 1.0/6 };
    const double Domain::WEIGHTSD2Q9   [ 9] = { 4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36. };
    // const double Domain::WEIGHTSD2Q17  [17] = { 441.0/1097.0, 87.0/749.0, 87.0/749.0, 87.0/749.0, 87.0/749.0, 286.0/8665.0, 286.0/8665.0, 286.0/8665.0, 286.0/8665.0, 17.0/214976.0, 17.0/214976.0, 17.0/214976.0, 17.0/214976.0, 4.0/15479.0, 4.0/15479.0, 4.0/15479.0, 4.0/15479.};
    const double Domain::WEIGHTSD2Q17    [17]   = {((575.0 + 193.0*sqrt(193.0))/8100.0),((3355.0 - 91.0*sqrt(193.0))/18000.0), ((3355.0 - 91.0*sqrt(193.0))/18000.0), ((3355.0 - 91.0*sqrt(193.0))/18000.0), ((3355.0 - 91.0*sqrt(193.0))/18000.0), ((655.0 + 17.0*sqrt(193.0))/27000.0), ((655.0 + 17.0*sqrt(193.0))/27000.0), ((655.0 + 17.0*sqrt(193.0))/27000.0), ((655.0 + 17.0*sqrt(193.0))/27000.0), ((685.0 - 49.0*sqrt(193.0))/54000.0), ((685.0 - 49.0*sqrt(193.0))/54000.0), ((685.0 - 49.0*sqrt(193.0))/54000.0), ((685.0 - 49.0*sqrt(193.0))/54000.0), ((1445.0 - 101.0*sqrt(193.0))/162000.0), ((1445.0 - 101.0*sqrt(193.0))/162000.0), ((1445.0 - 101.0*sqrt(193.0))/162000.0), ((1445.0 - 101.0*sqrt(193.0))/162000.0)};
    const double Domain::WEIGHTSD2Q17ZOT [17]   = {2.0*(95.0-4.0*sqrt(10.0))/405.0
                                                , (3.0*(-5.0+4.0*sqrt(10.0))/200.0), (3.0*(-5.0+4.0*sqrt(10.0))/200.0), (3.0*(-5.0+4.0*sqrt(10.0))/200.0), (3.0*(-5.0+4.0*sqrt(10.0))/200.0)
                                                , (3.0*(50.0-13.0*sqrt(10.0))/800.0), (3.0*(50.0-13.0*sqrt(10.0))/800.0), (3.0*(50.0-13.0*sqrt(10.0))/800.0), (3.0*(50.0-13.0*sqrt(10.0))/800.0)
                                                , ((295.0-92.0*sqrt(10.0))/16200.0), ((295.0-92.0*sqrt(10.0))/16200.0), ((295.0-92.0*sqrt(10.0))/16200.0), ((295.0-92.0*sqrt(10.0))/16200.0)
                                                , ((130.0-41.0*sqrt(10.0))/64800.0), ((130.0-41.0*sqrt(10.0))/64800.0), ((130.0-41.0*sqrt(10.0))/64800.0), ((130.0-41.0*sqrt(10.0))/64800.0)}; // PhysRevE.81.036702 (2010)
    const double Domain::WEIGHTSD2Q21  [21] = { 91.0/324.0
                                                , 1.0/12.0, 1.0/12.0, 1.0/12.0, 1.0/12.0, 2.0/27.0, 2.0/27.0, 2.0/27.0, 2.0/27.0 
                                                , 7.0/360.0, 7.0/360.0, 7.0/360.0, 7.0/360.0, 1.0/432.0, 1.0/432.0, 1.0/432.0, 1.0/432.0 
                                                , 1.0/1620.0, 1.0/1620.0, 1.0/1620.0, 1.0/1620.0 };
    const double Domain::WEIGHTSD2Q25  [25] = { 0.239059
                                                , 0.063158 , 0.063158 , 0.063158 , 0.063158 , 8.75957e-2, 8.75957e-2, 8.75957e-2, 8.75957e-2 
                                                , 3.1180e-2 , 3.1180e-2 , 3.1180e-2 , 3.1180e-2 , 6.19896e-3, 6.19896e-3, 6.19896e-3, 6.19896e-3
                                                , 2.02013e-3, 2.02013e-3, 2.02013e-3, 2.02013e-3 
                                                , 8.38224e-5, 8.38224e-5, 8.38224e-5, 8.38224e-5 
                                                };// Ref. Philippi et al PhysRevE.73.056702 (2006)
    const double Domain::WEIGHTSD2Q25ZOT  [25] = { (4.0/45.0)*(4.0 + sqrt(10.0))*(4.0/45.0)*(4.0 + sqrt(10.0))
                                                    , (4.0/45.0)*(4.0 + sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0))
                                                    , (3.0/80.0)*(8.0 - sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(3.0/80.0)*(8.0 - sqrt(10.0))
                                                    , (4.0/45.0)*(4.0 + sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (4.0/45.0)*(4.0 + sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0))
                                                    , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)), (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (3.0/80.0)*(8.0 - sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0))
                                                    , (1.0/720.0)*(16.0 - 5.0*sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (1.0/720.0)*(16.0 - 5.0*sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (1.0/720.0)*(16.0 - 5.0*sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) , (1.0/720.0)*(16.0 - 5.0*sqrt(10.0))*(1.0/720.0)*(16.0 - 5.0*sqrt(10.0)) 
                                                }; // PhysRevE.79.046701 (2009)
    const double Domain::WEIGHTSD2Q37  [37] = { 0.23315066913235250228650
                                                , 0.10730609154221900241246, 0.10730609154221900241246, 0.10730609154221900241246, 0.10730609154221900241246 // wf(1) 
                                                , 0.05766785988879488203006, 0.05766785988879488203006, 0.05766785988879488203006, 0.05766785988879488203006 // wf(2)
                                                , 0.01420821615845075026469, 0.01420821615845075026469, 0.01420821615845075026469, 0.01420821615845075026469 // wf(4)
                                                , 0.00535304900051377523273, 0.00535304900051377523273, 0.00535304900051377523273, 0.00535304900051377523273,0.00535304900051377523273, 0.00535304900051377523273, 0.00535304900051377523273, 0.00535304900051377523273 // wf(5)
                                                , 0.00101193759267357547541, 0.00101193759267357547541, 0.00101193759267357547541, 0.00101193759267357547541 // wf(8)
                                                , 0.00024530102775771734547, 0.00024530102775771734547, 0.00024530102775771734547, 0.00024530102775771734547 // wf(9)
                                                , 0.00028341425299419821740, 0.00028341425299419821740, 0.00028341425299419821740, 0.00028341425299419821740,0.00028341425299419821740, 0.00028341425299419821740, 0.00028341425299419821740, 0.00028341425299419821740 // wf(10)
                                                };  //*  Ref: X. Shan and H. Chen, Int. J. Mod. Phys. C 18, 635 (2007)
                                                    //*  2nd Ref: Philippi et al PhysRevE.73.056702 (2006)
    const double Domain::WEIGHTSD3Q15  [15] = { 2.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0,  1.0/9.0,  1.0/9.0, 1.0/72.0, 1.0/72. , 1.0/72.0, 1.0/72.0, 1.0/72.0, 1.0/72.0, 1.0/72.0, 1.0/72.};
    const double Domain::WEIGHTSD3Q19  [19] = { 1.0/3.0, 1.0/18.0, 1.0/18.0, 1.0/18.0, 1.0/18.0, 1.0/18.0, 1.0/18.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.};
    const size_t Domain::OPPOSITED2Q5  [ 5] = { 0, 3, 4, 1, 2 };                                                       ///< Opposite directions (D2Q5) 
    const size_t Domain::OPPOSITED2Q9  [ 9] = { 0, 3, 4, 1, 2, 7, 8, 5, 6 };                                           ///< Opposite directions (D2Q9) 
    const size_t Domain::OPPOSITED2Q17      [17] = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 15, 16, 13, 14};             ///< Opposite directions (D2Q17)
    const size_t Domain::OPPOSITED2Q17ZOT   [17] = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 15, 16, 13, 14};             ///< Opposite directions (D2Q17ZOT)
    const size_t Domain::OPPOSITED2Q21 [21] = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 15, 16, 13, 14, 19, 20, 17, 18};///< Opposite directions (D2Q21)
    const size_t Domain::OPPOSITED2Q25    [25] = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 15, 16, 13, 14, 19, 20, 17, 18, 23, 24, 21, 22};///< Opposite directions (D2Q25)
    const size_t Domain::OPPOSITED2Q25ZOT [25] = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 17, 18, 19, 20, 13, 14, 15, 16, 23, 24, 21, 22};
    const size_t Domain::OPPOSITED2Q37 [37]    = { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 17, 18, 19, 20, 13, 14, 15, 16, 23, 24, 21, 22,  27, 28, 25, 26,  33, 34, 35, 36, 29, 30, 31, 32};
    const size_t Domain::OPPOSITED2Q49ZOTT [49]= { 0, 3, 4, 1, 2, 7, 8, 5, 6, 11, 12, 9, 10, 17, 18, 19, 20, 13, 14, 15, 16, 23, 24, 21, 22,  27, 28, 25, 26,  33, 34, 35, 36, 29, 30, 31, 32,  41, 42, 43, 44, 37, 38, 39, 40,  45, 46, 47, 48};
    const size_t Domain::OPPOSITED3Q15 [15] = { 0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13};                     ///< Opposite directions (D3Q15)
    const size_t Domain::OPPOSITED3Q19 [19] = { 0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15, 18, 17};     ///< Opposite directions (D3Q19)
    const Vec3_t Domain::LVELOCD2Q5  [ 5] = { {0,0,0}, {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} };
    const Vec3_t Domain::LVELOCD2Q9  [ 9] = { {0,0,0}, {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}, {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} };
    const Vec3_t Domain::LVELOCD2Q17 [17] = { {0,0,0}
                                            , {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} // 1.Band (axial)
                                            , {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} // 1.Band (diag)
                                            , {2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0} // 2.Band (diag)
                                            , {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0} // 3.Band (axial) 
                                            };
    const Vec3_t Domain::LVELOCD2Q17ZOT [17] = { {0,0,0}
                                            , {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} // 1.Band (axial)
                                            , {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} // 1.Band (diag)
                                            , {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0} // 3.Band (axial) 
                                            , {3,3,0}, {-3,3,0}, {-3,-3,0}, {3,-3,0} // 3.Band (diag)
                                            };
    const Vec3_t Domain::LVELOCD2Q21 [21] = { {0,0,0}
                                            , {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} // 1.Band (axial)
                                            , {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} // 1.Band (diag)
                                            , {2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0} // 2.Band (axial)
                                            , {2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0} // 2.Band (diag)
                                            , {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0} // 3.Band (axial) 
                                            };
    const Vec3_t Domain::LVELOCD2Q25 [25] = { {0,0,0} 
                                            , {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0} // 1.Band (axial)
                                            , {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0} // 1.Band (diag)
                                            , {2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0} // 2.Band (axial)
                                            , {2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0} // 2.Band (diag)
                                            , {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0} // 3.Band (axial) 
                                            , {4,0,0}, {0,4,0}, {-4,0,0}, {0,-4,0} // 3.Band (axial) 
                                            };
    const Vec3_t Domain::LVELOCD2Q25ZOT [25] = { {0,0,0}                                // 0
                                            , {1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}      // 1.Band (axial) 1-4
                                            , {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}    // 1.Band (diag) 5-8
                                            , {3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0}      // 3.Band (axial) 9-12
                                            , {3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0} // 3.Band one-three
                                            , {3,3,0}, {-3,3,0}, {-3,-3,0}, {3,-3,0}    // 3.Band (diag) 21-24 (25 if counting zero as 1)
                                            };
    const Vec3_t Domain::LVELOCD2Q37 [37]   = {  {0,0,0}                                // 0
                                                ,{1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}
                                                ,{1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}
                                                ,{2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0}
                                                ,{2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0}
                                                ,{2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0}
                                                ,{3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0}
                                                ,{3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0}
                                            };
    const Vec3_t Domain::LVELOCD2Q49ZOTT [49]   = {  {0,0,0}                                // 0
                                                    ,{1,0,0}, {0,1,0}, {-1,0,0}, {0,-1,0}
                                                    ,{1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}
                                                    ,{2,0,0}, {0,2,0}, {-2,0,0}, {0,-2,0}
                                                    ,{2,1,0}, {1,2,0}, {-1,2,0}, {-2,1,0}, {-2,-1,0}, {-1,-2,0}, {1,-2,0}, {2,-1,0}
                                                    ,{2,2,0}, {-2,2,0}, {-2,-2,0}, {2,-2,0}
                                                    ,{3,0,0}, {0,3,0}, {-3,0,0}, {0,-3,0}
                                                    ,{3,1,0}, {1,3,0}, {-1,3,0}, {-3,1,0}, {-3,-1,0}, {-1,-3,0}, {1,-3,0}, {3,-1,0}
                                                    ,{3,2,0}, {2,3,0}, {-2,3,0}, {-3,2,0}, {-3,-2,0}, {-2,-3,0}, {2,-3,0}, {3,-2,0}
                                                    ,{3,3,0}, {-3,3,0}, {-3,-3,0}, {3,-3,0}
                                                };

    const Vec3_t Domain::LVELOCD3Q15 [15] =
                                            {
                                                { 0, 0, 0}, { 1, 0, 0}, {-1, 0, 0}, { 0, 1, 0}, { 0,-1, 0}, 
                                                { 0, 0, 1}, { 0, 0,-1}, { 1, 1, 1}, {-1,-1,-1}, { 1, 1,-1}, 
                                                {-1,-1, 1}, { 1,-1, 1}, {-1, 1,-1}, { 1,-1,-1}, {-1, 1, 1} 
                                            };
    const Vec3_t Domain::LVELOCD3Q19 [19] =
                                            {
                                                { 0, 0, 0}, 
                                                { 1, 0, 0}, {-1, 0, 0}, { 0, 1, 0}, { 0,-1, 0}, { 0, 0, 1}, { 0, 0,-1}, 
                                                { 1, 1, 0}, {-1,-1, 0}, { 1,-1, 0}, {-1, 1, 0}, { 1, 0, 1}, {-1, 0,-1},
                                                { 1, 0,-1}, {-1, 0, 1}, { 0, 1, 1}, { 0,-1,-1}, { 0, 1,-1}, { 0,-1, 1}
                                            };
    const double Domain::MD3Q15 [15][15]  = 
                                            { 
                                                { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
                                                {-2.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
                                                {16.0,-4.0,-4.0,-4.0,-4.0,-4.0,-4.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
                                                { 0.0, 1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 1.0,-1.0, 1.0,-1.0, 1.0,-1.0, 1.0,-1.0},
                                                { 0.0,-4.0, 4.0, 0.0, 0.0, 0.0, 0.0, 1.0,-1.0, 1.0,-1.0, 1.0,-1.0, 1.0,-1.0},
                                                { 0.0, 0.0, 0.0, 1.0,-1.0, 0.0, 0.0, 1.0,-1.0, 1.0,-1.0,-1.0, 1.0,-1.0, 1.0},
                                                { 0.0, 0.0, 0.0,-4.0, 4.0, 0.0, 0.0, 1.0,-1.0, 1.0,-1.0,-1.0, 1.0,-1.0, 1.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,-1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0,-4.0, 4.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0},
                                                { 0.0, 2.0, 2.0,-1.0,-1.0,-1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                                                { 0.0, 0.0, 0.0, 1.0, 1.0,-1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0,-1.0,-1.0,-1.0,-1.0, 1.0, 1.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0,-1.0},
                                                { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,-1.0,-1.0, 1.0,-1.0, 1.0, 1.0,-1.0} 
                                            };

    const double Domain::MD2Q17 [17][17]  = 
                                        {
                                            {1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0 ,1.0},
                                            {-80.0 ,-63.0 ,-63.0 ,-63.0 ,-63.0 ,-46.0 ,-46.0 ,-46.0 ,-46.0 ,56.0 ,56.0 ,56.0 ,56.0 ,73.0 ,73.0 ,73.0 ,73.0},
                                            {100.0 ,17.0 ,17.0 ,17.0 ,17.0 ,-47.0 ,-47.0 ,-47.0 ,-47.0 ,-32.0 ,-32.0 ,-32.0 ,-32.0 ,37.0 ,37.0 ,37.0 ,37.0},
                                            {0.0 ,1.0 ,0.0 ,-1.0 ,0.0 ,1.0 ,-1.0 ,-1.0 ,1.0 ,2.0 ,-2.0 ,-2.0 ,2.0 ,3.0 ,0.0 ,-3.0 ,0.0},
                                            {0.0 ,-13.0 ,0.0 ,13.0 ,0.0 ,-11.0 ,11.0 ,11.0 ,-11.0 ,2.0 ,-2.0 ,-2.0 ,2.0 ,9.0 ,0.0 ,-9.0 ,0.0},
                                            {0.0 ,0.0 ,1.0 ,0.0 ,-1.0 ,1.0 ,1.0 ,-1.0 ,-1.0 ,2.0 ,2.0 ,-2.0 ,-2.0 ,0.0 ,3.0 ,0.0 ,-3.0},
                                            {0.0 ,0.0 ,-13.0 ,0.0 ,13.0 ,-11.0 ,-11.0 ,11.0 ,11.0 ,2.0 ,2.0 ,-2.0 ,-2.0 ,0.0 ,9.0 ,0.0 ,-9.0},
                                            {0.0 ,1.0 ,-1.0 ,1.0 ,-1.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,9.0 ,-9.0 ,9.0 ,-9.0},
                                            {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,1.0 ,-1.0 ,1.0 ,-1.0 ,4.0 ,-4.0 ,4.0 ,-4.0 ,0.0 ,0.0 ,0.0 ,0.0},
                                            {0.0 ,9.0 ,0.0 ,-9.0 ,0.0 ,-2.0 ,2.0 ,2.0 ,-2.0 ,-11.0 ,11.0 ,11.0 ,-11.0 ,13.0 ,0.0 ,-13.0 ,0.0},
                                            {0.0 ,0.0 ,9.0 ,0.0 ,-9.0 ,-2.0 ,-2.0 ,2.0 ,2.0 ,-11.0 ,-11.0 ,11.0 ,11.0 ,0.0 ,13.0 ,0.0 ,-13.0},
                                            {0.0 ,-3.0 ,0.0 ,3.0 ,0.0 ,2.0 ,-2.0 ,-2.0 ,2.0 ,-1.0 ,1.0 ,1.0 ,-1.0 ,1.0 ,0.0 ,-1.0 ,0.0},
                                            {0.0 ,0.0 ,-3.0 ,0.0 ,3.0 ,2.0 ,2.0 ,-2.0 ,-2.0 ,-1.0 ,-1.0 ,1.0 ,1.0 ,0.0 ,1.0 ,0.0 ,-1.0},
                                            {-137760.0 ,5289.0 ,5289.0 ,5289.0 ,5289.0 ,51988.0 ,51988.0 ,51988.0 ,51988.0 ,-96268.0 ,-96268.0 ,-96268.0 ,-96268.0 ,73431.0 ,73431.0 ,73431.0 ,73431.0},
                                            {28.0 ,-18.0 ,-18.0 ,-18.0 ,-18.0 ,12.0 ,12.0 ,12.0 ,12.0 ,-3.0 ,-3.0 ,-3.0 ,-3.0 ,2.0 ,2.0 ,2.0 ,2.0},
                                            {0.0 ,-9.0 ,9.0 ,-9.0 ,9.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,1.0 ,-1.0 ,1.0 ,-1.0},
                                            {0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,-4.0 ,4.0 ,-4.0 ,4.0 ,1.0 ,-1.0 ,1.0 ,-1.0 ,0.0 ,0.0 ,0.0 ,0.0}
                                        };
        //Off-grid lattices (experimental)
        // --------------------------------------------------------------------------
        // const double Domain::WEIGHTSD2Q16  [16] = { 788.0/3821.0, 788.0/3821.0, 788.0/3821.0, 788.0/3821.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 1.0/48.0, 53.0/25183.0, 53.0/25183.0, 53.0/25183.0, 53.0/25183.}; ///< need to double check exact precision.
        // const size_t Domain::OPPOSITED2Q16 [16] = { 2, 3, 0, 1, 8, 9, 10, 11, 4, 5, 6, 7, 14, 15, 12, 13};                 ///< Opposite directions (D2Q16)
        // const Vec3_t Domain::LVELOCD2Q16 [16] = { {1,1,0}, {-1,1,0}, {-1,-1,0}, {1,-1,0}, 
        //                                           {(3.1462643699),1,0}, {1,(3.1462643699),0}, {-1,(3.1462643699),0}, {-(3.1462643699),1,0}, {-(3.1462643699),-1,0}, {-1,-(3.1462643699),0}, {1,-(3.1462643699),0}, {(3.1462643699),-1,0},
        //                                           {(3.1462643699),(3.1462643699),0}, {-(3.1462643699),(3.1462643699),0}, {-(3.1462643699),-(3.1462643699),0}, {(3.1462643699),-(3.1462643699),0}
        //                                         };
        // --------------------------------------------------------------------------
/* Construct Interaction Array */
    inline void Domain::ConstructInteractionArray() {
        Array<iVec3_t> CPP(0);
        size_t nx = Ndim(0);
        size_t ny = Ndim(1);
        size_t nz = Ndim(2);

        // if (FNneigh==Nneigh)
        // {
        //     size_t k0 = 0;
        //     if (Cf[0](0)==0 && Cf[0](1)==0) k0=1;//, std::cout << "True ! k = " << k0 << std::endl;
        //     for (size_t iz=0;iz<nz;iz++)
        //     for (size_t iy=0;iy<ny;iy++)
        //     for (size_t ix=0;ix<nx;ix++)
        //     {
        //         size_t nc = Pt2idx(iVec3_t(ix,iy,iz),Ndim);
        //         for (size_t k=k0;k<FNneigh;k++)
        //         {
        //             size_t nix = (size_t)((int)ix + (int)Cf[k](0) + (int)Ndim(0))%Ndim(0);
        //             size_t niy = (size_t)((int)iy + (int)Cf[k](1) + (int)Ndim(1))%Ndim(1);
        //             size_t niz = (size_t)((int)iz + (int)Cf[k](2) + (int)Ndim(2))%Ndim(2);
        //             size_t nb  = Pt2idx(iVec3_t(nix,niy,niz),Ndim);
        //             if (nb>nc)
        //             {
        //                 CPP.Push(iVec3_t(nc,nb,k));
        //                 // std::cout << "Pos CPP.Push at k= " << k << "\n" << std::endl;
        //             }
        //             if (nb<nc)
        //             {
        //                 CPP.Push(iVec3_t(nc,nb,k));
        //                 // std::cout << "Neg CPP.Push at k= " << k << "\n" << std::endl;
        //             }
        //         }
        //     }
        // }
        // else
        // {
        for (size_t iz=0;iz<nz;iz++)
        for (size_t iy=0;iy<ny;iy++)
        for (size_t ix=0;ix<nx;ix++)
        {
            size_t nc = Pt2idx(iVec3_t(ix,iy,iz),Ndim);
            for (size_t k=0;k<FNneigh;k++)
            {
                size_t nix = (size_t)((int)ix + (int)Cf[k](0) + (int)Ndim(0))%Ndim(0);
                size_t niy = (size_t)((int)iy + (int)Cf[k](1) + (int)Ndim(1))%Ndim(1);
                size_t niz = (size_t)((int)iz + (int)Cf[k](2) + (int)Ndim(2))%Ndim(2);
                size_t nb  = Pt2idx(iVec3_t(nix,niy,niz),Ndim);
                CPP.Push(iVec3_t(nc,nb,k));
                // if (nb>nc)
                // {
                //     CPP.Push(iVec3_t(nc,nb,k));
                //     // std::cout << "Pos CPP.Push at k= " << k << "\n" << std::endl;
                // }
                // if (nb<nc)
                // {
                //     CPP.Push(iVec3_t(nc,nb,k));
                //     // std::cout << "Neg CPP.Push at k= " << k << "\n" << std::endl;
                // }
            }
        }
        // }

        NCellPairs = CPP.Size();
        CellPairs = new iVec3_t [NCellPairs];
        for (size_t n=0;n<NCellPairs;n++)
        {
            CellPairs[n] = CPP[n];
        }
    }

}
#endif
