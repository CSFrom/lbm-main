//* Simulation of Cellular-like Forcing

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/DomainPoly_Exp.h>

// Other
#include <ctime>
#include <chrono>
#include <thread>
#include <random>

using std::cout;
using std::endl;

struct UserData {
    double      F0;
    double      k;
};
/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */


void Setup(FLBM::Domain & dom, void * UD) {
    UserData & dat = (*static_cast<UserData *>(UD));
        #ifdef USE_OMP
        #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
        #endif
        for (size_t i=0;i<dom.Ndim(0);i++)
        for (size_t j=0;j<dom.Ndim(1);j++) {
        dom.BForce[0][i][j][0][0] -= dat.F0*sin((j+1)*dat.k);
        dom.BForce[0][i][j][0][1] += dat.F0*sin((i+1)*dat.k);
        }
}

int main(int argc, char **argv) try
{
    size_t Nproc = 1;
    if (argc>=2) Nproc=atoi(argv[1]);
    char const * FileKey = "CF_ET-Test";
    if (argc>=3) FileKey=argv[2];
    
    // ! Modelling parameters
        const double PI = 4.0*atan(1.0);     ///< \f$ \pi \f$
        size_t nx = 128*2;
        size_t ny = nx;
        
        double dx = 1.0, dt = 1.0, dt_poly = 1.0;
        
        double Lr   = 2.0*PI; // Real Length of Domain
        double L    = nx; // Lattice dimensionless length
        
        // dx  = Lr/L;
        // dt  = dx;
        // dt_poly = dt;

    // ! Lattice Structures
        double cs = 0.0;
        // * D2Q9 */
            cs    = (1.0/sqrt(3.0))*(dx/dt);
        //* ZOT LATTICES *// (incl. D2Q17ZOT, D2Q25ZOT and D2Q49ZOTT*)
            // cs = sqrt((1.0-sqrt(2.0/5.0)))*(dx/dt);
            
    // ! Dimensionless Numbers */
    double K    = 2.0*(Lr/L); //* inverse length scale
    double kx   = K; 
    double ky   = K;
    double Re   = 1.0;//25.0;
    double Sc   = 1e+3;
    double Ma   = 0.02;
    double De   = 5.0; // equiv. to Wi Numbers, but is technically the Deborah number.
    if (Re<25.0 && De < 10.0) De = 10.0; //* Otherwise De is not large enough at low-Re
    double CouplingStrength = 0.20; // ! Or use PolyCon. below
    double PolyCon = 0.20; //* PolyConcentration
    /* -------------------------------------------- */
    // ! Dimensionless Variables */ 
        double rho0 = 1.0;
        double U0 = Ma*cs;
        double nu = 0.0;
        if (dt<1.0) nu = U0*Lr/(Re), nu *= dt/(dx*dx); //(U0*nx)/Re;//
        else nu = U0/(Re*K); //dx=dt=1
        // double F0R = 2.0;
        // double nuR = sqrt((F0R*Lr*Lr*Lr)/(Re));
        // nu = nuR*(dt/(dx*dx));
        // double nu = U0*L/(Re); // L = nx, only use this when dt == 1.0;
        // double F0 = 2.0*U0*nu*(dx*dx); // if rho0 = 1, and mass (kg) is assumed const. unity.
        double F0 = U0*nu*K*K;
        
        // double ts = 2.0*K*nu/(F0);
        double ts = K*nu/(F0); //* equiv. to ts = L/(U0*Lr)
        // double ts = L/(2.0*U0);
        // if (dt<1.0) ts *= dt;
        // double tau01 = (nu*dt/(cs*cs*dx*dx)) + 0.5; //* Fluid Relaxation.
        double tau01 = (nu/(cs*cs)) + 0.5;
        double PolyDiff = nu/Sc; // ((nu*dt)/(dx*dx))/Sc; // 
        // double pdiff_tau = (PolyDiff*dt/(cs*cs*dx*dx)) + 0.5; //* PolyFluid Diff Relaxation.
        double pdiff_tau = (PolyDiff/(cs*cs)) + 0.5;
        // pdiff_tau = 0.50; //* should result in Diff --> 0; i.e., Sc --> infty
        double ptau = ts*De;//De*nx/U0; //* Poly Relaxation.
        // double ptau = De*(L/(U0*dx)); //* Poly Relaxation.
        
    // ! -------------------------------------------- */
    
    // double TotalTime = ts*60; // 40000.0; //TimeResOut*40.0; //ts*20;
    double TotalTime = ts*1200.0;
    // TotalTime = dt*1000;
    double TimeResOut = TotalTime/120.0;//ts*10; //Result out every TimeResOut
    
    /* Change lattice model if you wish */
    FLBM::Domain Dom(D2Q9, ELS, nu, iVec3_t(nx,ny,1), dx, dt);
    UserData dat;
    Dom.UserData = &dat;
    dat.F0 = F0;
    dat.k = K;
        Dom.dt_poly = dt_poly;
        Dom.Cs_poly = dx/dt_poly;
        Dom.Cs2_poly = cs*cs;
        Dom.polyTau[0]  = ptau;
        // Dom.polyMu[0]   = (PolyCon*nu)/(1.0 - PolyCon);//0.0002*ptau;
        Dom.polyMu[0]   = CouplingStrength*nu;//*((dx*dx)/dt);//
        // Dom.polyMu[0]   = ptau;
        // Dom.polyMu[0]   = (0.5/De)*Dom.polyTau[0];
        // Dom.polyDiff[0]  = PolyDiff;
        Dom.polyLambda[0]  = pdiff_tau;
        Dom.Tau[0] = tau01;

    /* Print out stuff  --------------------------- */
        printf("\n%s--- Re Number           = %g%s\n",TERM_CLR1,Re,TERM_RST);
        printf("\n%s--- Mach Number         = %g%s\n",TERM_CLR1,Ma,TERM_RST);
        printf("\n%s--- Weissenberg Number  = %g%s\n",TERM_CLR1,De,TERM_RST);
        printf("\n%s--- Poly. Relax.        = %g%s\n",TERM_CLR1,ptau,TERM_RST);
        printf("\n%s--- Poly. Diff.Relax.   = %g%s\n",TERM_CLR1,pdiff_tau,TERM_RST);
        printf("\n%s--- Poly. Beta*Wi       = %g%s\n",TERM_CLR1,(Dom.polyMu[0]/Dom.polyTau[0])*De,TERM_RST);
        printf("\n%s--- Force Mag.          = %g%s\n",TERM_CLR1,F0,TERM_RST);
        printf("\n%s--- time scale t* (L/Uo)= %g%s\n",TERM_CLR1,ts,TERM_RST);
        printf("\n%s--- Relax.              = %g%s\n",TERM_CLR1,tau01,TERM_RST);
        printf("%s   # CPU's                         = %zd%s\n ",TERM_CLR2,Nproc,TERM_RST);
    /* -------------------------------------------- */

    Vec3_t V0 = OrthoSys::O;
    // ! Setting up the velocity field */
    for (size_t i=0; i<nx; ++i)
    for (size_t j=0; j<ny; ++j) {
        Dom.AssignFluidParameters(0,iVec3_t(i,j,0),rho0,V0);
    }
    
    
    Vec3_t Force0 = OrthoSys::O;
    for (size_t i=0; i<nx; ++i)
    for (size_t j=0; j<ny; ++j) {
        Force0[0] = -F0*sin((j+1)*K);
        Force0[1] = F0*sin((i+1)*K);
        Dom.BForce[0][i][j][0][0] = Force0[0];
        Dom.BForce[0][i][j][0][1] = Force0[1];
        for (size_t z=0; z<3; ++z) {
            Dom.Cij[0][i][j][0][z][z] = 1.0;
        }
    }
    
    //* Perturbation following Thomases et al. Physica D. (2011)
        size_t NSum = 20; // Number of summations of 'smooth Guassian approximations'.
        double Kconst = Lr/L;
        for (size_t z=0; z<2; ++z)
        for (size_t q=0; q<2; ++q) {
            double Pert [nx][ny];
            for (size_t i=0; i<nx; ++i) {
                srand(time(NULL)); // * Maybe not needed here ?
                for (size_t j=0; j<ny; ++j) {
                    double Pert_tmp = 0.0;
                    size_t k = 0;
                    while (k<NSum) {
                        double iRand = Kconst*(double(rand() % int(nx)));
                        double jRand = Kconst*(double(rand() % int(ny)));
                        
                        double mk = double(rand() % 101);
                        double nk = double(rand() % 101);
                        
                        double tmpC = pow(1.0/(mk + nk), 0.5)*pow(0.5, (mk + nk));// Scaling factor to ensure Pert. is small
                        double tmpFunX = (1.0 + sin((i+1)*Kconst - iRand));
                        double tmpFunY = (1.0 + sin((j+1)*Kconst - jRand));
                        
                        if (iRand==jRand && mk==nk) continue; //* if-statement maybe not req.?
                        Pert_tmp += tmpC*pow(tmpFunX, mk)*pow(tmpFunY, nk);
                        k++;
                    }
                    Pert [i][j] = Pert_tmp;
                    srand(time(NULL)); // Reset new seed number for rand();
                }
            }

            for (size_t i=0; i<nx; ++i)
            for (size_t j=0; j<ny; ++j)
            {        
                Dom.Cij[0][i][j][0][z][q] += Pert[i][j];
            }
        }
    Dom.Nproc = Nproc;
    Dom.UCMtype = 1;
    Dom.InitializeMixture();
    // Dom.PolymerModel();
    

    /* Solve     Total time ,  Time step, Start, Setup, report,    name, render, number of CPU */
    Dom.Solve(   TotalTime,   TimeResOut,  0   , Setup, NULL  , FileKey, true  , Nproc);
    // Dom.Solve(TotalTime*Dom.dt,TimeResOut*Dom.dt,0*Dom.dt,NULL,NULL,FileKey,true,Nproc);
    
    return 0;
}
MECHSYS_CATCH
