
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    Vec3_t             g;
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Setup(FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));

    #ifdef USE_OCL
    if (dom.IsFirstTime)
    {
        dom.IsFirstTime = false;
        dat.bBCg        = cl::Buffer(dom.CL_Context,CL_MEM_READ_WRITE,sizeof(cl_double3));
        cl_double3      gravity[1];
        gravity[0].s[0] = dat.g[0];
        gravity[0].s[1] = dat.g[1];
        gravity[0].s[2] = dat.g[2];
        
        dom.CL_Queue.enqueueWriteBuffer(dat.bBCg,CL_TRUE,0,sizeof(cl_double3),gravity);
        
        char* pMECHSYS_ROOT;
        pMECHSYS_ROOT = getenv ("MECHSYS_ROOT");
        if (pMECHSYS_ROOT==NULL) pMECHSYS_ROOT = getenv ("HOME");

        String pCL;
        pCL.Printf("%s/mechsys/lib/flbm/lbm.cl",pMECHSYS_ROOT);

        std::ifstream infile(pCL.CStr(),std::ifstream::in);
        std::string main_kernel_code((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
        
        std::string BC_kernel_code =
            " void kernel ApplyGravity (global const double3 * g, global double3 * BForce, global double * Rho, global const struct lbm_aux * lbmaux) \n"
            " { \n"
                " size_t ic  = get_global_id(0); \n"
                " BForce[ic] = Rho[ic]*g[0]; \n"
            " } \n"
        ;

        BC_kernel_code = main_kernel_code + BC_kernel_code;

        cl::Program::Sources sources;
        sources.push_back({BC_kernel_code.c_str(),BC_kernel_code.length()});

        dat.UserProgram = cl::Program(dom.CL_Context,sources);
        if(dat.UserProgram.build({dom.CL_Device})!=CL_SUCCESS){
            std::cout<<" Error building: "<<dat.UserProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dom.CL_Device)<<"\n";
            exit(1);
        }

    }
    
    cl::Kernel kernel(dat.UserProgram,"ApplyGravity");
    kernel.setArg(0,dat.bBCg      );
    kernel.setArg(1,dom.bBForce[0]);
    kernel.setArg(2,dom.bRho   [0]);
    kernel.setArg(3,dom.blbmaux   );
    dom.CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(dom.Ncells),cl::NullRange);
    dom.CL_Queue.finish();

    kernel = cl::Kernel(dat.UserProgram,"ApplyGravity");
    kernel.setArg(0,dat.bBCg      );
    kernel.setArg(1,dom.bBForce[1]);
    kernel.setArg(2,dom.bRho   [1]);
    kernel.setArg(3,dom.blbmaux   );
    dom.CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(dom.Ncells),cl::NullRange);
    dom.CL_Queue.finish();

    #else // USE_OCL
    
    for (size_t il=0;il<dom.Nl;il++)
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t ix=0;ix<dom.Ndim(0);ix++)
    for (size_t iy=0;iy<dom.Ndim(1);iy++)
    for (size_t iz=0;iz<dom.Ndim(2);iz++)
    {
        // double cs2    = (1.0/sqrt(3.0))*(1.0/sqrt(3.0));// D2Q9 
        dom.BForce[il][ix][iy][iz] = dom.Rho[il][ix][iy][iz]*dat.g;//*cs2;
    }

    #endif // USE_OCL
}



int main(int argc, char **argv) try
{
    size_t Nproc = 1; 
    if (argc==2) Nproc=atoi(argv[1]);
    size_t ny = 100;
    size_t nx = ny*4.0;
    // size_t ny = nx*4.0;
    double dx = 1.0; // Get real parameters, maybe use diameter of bubble.
    double dt = 1.0;
    double Tau1 = 0.9;
    double cs2    = (1.0/sqrt(3.0))*(1.0/sqrt(3.0));// D2Q9 
    // D2Q17 use this:
    // double cs2    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
    Array<double> nu(2);
    nu[0] = (Tau1-0.5)*cs2*dx*dx;//1.0/6; // 1.0/7.0 works well for D2Q17
    nu[1] = ((Tau1-0.5)*cs2*dx*dx)/1.1;//100.0;//1.0/12.0;

    FLBM::Domain Dom(D2Q9, E8, nu, iVec3_t(nx,ny,1), dx, dt);
    UserData dat;
    Dom.UserData = &dat;
    
    dat.g           = 0.0,-0.001,0.0;

    //dat.g           = 0.0,0.0,0.0;
    for (size_t i=0;i<nx;i++)
    {
        Dom.IsSolid[0][i][0   ][0] = true;
        Dom.IsSolid[0][i][ny-1][0] = true;
        Dom.IsSolid[1][i][0   ][0] = true;
        Dom.IsSolid[1][i][ny-1][0] = true;
        Dom.IsSolid[0][i][1   ][0] = true;
        Dom.IsSolid[0][i][ny-2][0] = true;
        Dom.IsSolid[1][i][1   ][0] = true;
        Dom.IsSolid[1][i][ny-2][0] = true;
        Dom.IsHBSolid[0][i][0   ][0] = true;
        Dom.IsHBSolid[0][i][ny-1][0] = true;
        Dom.IsHBSolid[1][i][0   ][0] = true;
        Dom.IsHBSolid[1][i][ny-1][0] = true;
        // Dom.IsHBSolid[0][i][1   ][0] = true;
        // Dom.IsHBSolid[0][i][ny-2][0] = true;
        // Dom.IsHBSolid[1][i][1   ][0] = true;
        // Dom.IsHBSolid[1][i][ny-2][0] = true;
    }
    for (size_t j=0;j<ny;j++)
    {
        // Dom.IsSolid[0][0   ][j][0] = true;
        // Dom.IsSolid[0][nx-1][j][0] = true;
        // Dom.IsSolid[1][0   ][j][0] = true;
        // Dom.IsSolid[1][nx-1][j][0] = true;
        // Dom.IsSolid[0][1   ][j][0] = true;
        // Dom.IsSolid[0][nx-2][j][0] = true;
        // Dom.IsSolid[1][1   ][j][0] = true;
        // Dom.IsSolid[1][nx-2][j][0] = true;
        // Dom.IsHBSolid[0][0   ][j][0] = true;
        // Dom.IsHBSolid[0][nx-1][j][0] = true;
        // Dom.IsHBSolid[1][0   ][j][0] = true;
        // Dom.IsHBSolid[1][nx-1][j][0] = true;
        // Dom.IsHBSolid[0][1   ][j][0] = true;
        // Dom.IsHBSolid[0][nx-2][j][0] = true;
        // Dom.IsHBSolid[1][1   ][j][0] = true;
        // Dom.IsHBSolid[1][nx-2][j][0] = true;
    }

// Setup bubbles
    int obsY = ny/2;// - ny/4;
    const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
    double rho0tmp = 0.1;
    double rho0 = 66.66;//200.0*0.91;
    double rho1 = 200.0;
	for (size_t i=0; i<nx; ++i)
	for (size_t j=0; j<ny; ++j)
    {

		// if ((int)(j) >= obsY) // circle equation
        if (ceil(obsY+0.01*nx*cos(((2.0*PI)*(2.0*PI)*(1.0/nx)*(int)(i)))) <= j) // circle equation
		{
            Dom.InitializeEF(0,iVec3_t(i,j,0), rho0tmp,OrthoSys::O);
            Dom.InitializeEF(1,iVec3_t(i,j,0), rho1,OrthoSys::O);
		}
        // else if (ceil(obsY-2.0+0.1*nx*cos(((2.0*PI)*(1.0/nx)*(int)(i)))) <= j) // circle equation
		// {
        //     Dom.InitializeEF(0,iVec3_t(i,j,0), rho0/2.0,OrthoSys::O);
        //     Dom.InitializeEF(1,iVec3_t(i,j,0), rho1/2.0,OrthoSys::O);
		// }
        else
        {
            Dom.InitializeEF(0,iVec3_t(i,j,0), rho0,OrthoSys::O);
            Dom.InitializeEF(1,iVec3_t(i,j,0), rho0tmp,OrthoSys::O);
        }
    }

    
    Dom.G[0]     = -1.0;
    Dom.G[1]     = -1.0;
    Dom.Gmix     =  45.0; // Gcrit = (tau-0.5)*[(S*m)/(tau*<n>)] = (1 - 0.5)*[(2*1)/(1*200)] ~~ 0.00675
    Dom.Gs    [0] =  1.0;
    Dom.Gs    [1] =  1.0;
    //Dom.Sc       = 0.0;

	// Solve
    Dom.Solve(1e4,1e3,0,NULL,NULL,"RT_T001",true,Nproc);
}
MECHSYS_CATCH
