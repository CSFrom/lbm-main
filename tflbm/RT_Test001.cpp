
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Simulation of Rayleigh-Taylor immiscible and compressible mixture.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;


struct UserData
{
    Vec3_t          g;
    double          * rho0tmp;
    #ifdef USE_OCL
    cl::Buffer        bBCg;
    cl::Program       UserProgram;
    #endif
};

void Setup(FLBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));

    #ifdef USE_OCL
    if (dom.IsFirstTime)
    {
        dom.IsFirstTime = false;
        dat.bBCg        = cl::Buffer(dom.CL_Context,CL_MEM_READ_WRITE,sizeof(cl_double3));
        cl_double3      gravity[1];
        gravity[0].s[0] = dat.g[0];
        gravity[0].s[1] = dat.g[1];
        gravity[0].s[2] = dat.g[2];
        
        dom.CL_Queue.enqueueWriteBuffer(dat.bBCg,CL_TRUE,0,sizeof(cl_double3),gravity);
        
        char* pMECHSYS_ROOT;
        pMECHSYS_ROOT = getenv ("MECHSYS_ROOT");
        if (pMECHSYS_ROOT==NULL) pMECHSYS_ROOT = getenv ("HOME");

        String pCL;
        pCL.Printf("%s/mechsys/lib/flbm/lbm.cl",pMECHSYS_ROOT);

        std::ifstream infile(pCL.CStr(),std::ifstream::in);
        std::string main_kernel_code((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
        
        std::string BC_kernel_code =
            " void kernel ApplyGravity (global const double3 * g, global double3 * BForce, global double * Rho, global const struct lbm_aux * lbmaux) \n"
            " { \n"
                " size_t ic  = get_global_id(0); \n"
                " BForce[ic] = Rho[ic]*g[0]; \n"
            " } \n"
        ;

        BC_kernel_code = main_kernel_code + BC_kernel_code;

        cl::Program::Sources sources;
        sources.push_back({BC_kernel_code.c_str(),BC_kernel_code.length()});

        dat.UserProgram = cl::Program(dom.CL_Context,sources);
        if(dat.UserProgram.build({dom.CL_Device})!=CL_SUCCESS){
            std::cout<<" Error building: "<<dat.UserProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dom.CL_Device)<<"\n";
            exit(1);
        }

    }
    
    cl::Kernel kernel(dat.UserProgram,"ApplyGravity");
    kernel.setArg(0,dat.bBCg      );
    kernel.setArg(1,dom.bBForce[0]);
    kernel.setArg(2,dom.bRho   [0]);
    kernel.setArg(3,dom.blbmaux   );
    dom.CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(dom.Ncells),cl::NullRange);
    dom.CL_Queue.finish();

    kernel = cl::Kernel(dat.UserProgram,"ApplyGravity");
    kernel.setArg(0,dat.bBCg      );
    kernel.setArg(1,dom.bBForce[1]);
    kernel.setArg(2,dom.bRho   [1]);
    kernel.setArg(3,dom.blbmaux   );
    dom.CL_Queue.enqueueNDRangeKernel(kernel,cl::NullRange,cl::NDRange(dom.Ncells),cl::NullRange);
    dom.CL_Queue.finish();

    #else // USE_OCL

    // free 'open' Outlet boundary - type: CBC (convective boundary condition)
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t il=0;il<dom.Nl;il++)
    {
        double uy_max = 0.0;
        double uy = 0.0;
        int CBCnode = 2.0;
        // for (size_t i=0; i<dom.Ndim(0);i++) 
        // {
        //     size_t j = dom.Ndim(1)-(CBCnode+1)-1;
        //     for (size_t k=0;k<dom.Nneigh;k++)
        //     {
        //         dom.Vel[il][i][j][0]   += dom.F[il][i][j][0][k]*dom.C[k];
        //     }
        //     dom.Nmol[il][i][j][0]   = dat.rho0tmp[il]/dom.molMass[il]; // Predefined density on top, but is implemented as a CBC outlet condition.
        //     dom.Rho[il][i][j][0]    = dat.rho0tmp[il];
        //     dom.Vel[il][i][j][0]    /=dat.rho0tmp[il];
        // }
        size_t j = dom.Ndim(1)-(CBCnode+1)-1;
        for (size_t i=0; i<dom.Ndim(0);i++) // find the maximum.
        {
            uy = dom.Vel[il][i][j][0][1]; // extract y-velocity component.
            uy_max = std::max(uy_max,uy); // is this greater than or not 
        }
        // std::cout << " Max velocity at top = " << u_max << std::endl;

        
        for (size_t i=0; i<dom.Ndim(0);i++) 
        {
            size_t j = dom.Ndim(1)-(CBCnode+1);
            for (size_t k=0;k<dom.Nneigh;k++)
            {
                dom.F[il][i][j][0][k]   = (dom.Fprev[il][i][j][0][k] + uy_max*dom.F[il][i][j-1][0][k])/(1.0+uy_max);
            }
            double summ = 0.0; // use this if density is not predefined for each fluid.
            dom.Vel[il][i][j][0] = OrthoSys::O;
            for (size_t k=0;k<dom.Nneigh;k++)
            {
                summ    += dom.F[il][i][j][0][k];
                dom.Vel[il][i][j][0]   += dom.F[il][i][j][0][k]*dom.C[k];
            }
            dom.Nmol[il][i][j][0]   = summ;
            dom.Rho[il][i][j][0]    = summ*dom.molMass[il];
            dom.Vel[il][i][j][0]    /=summ*dom.molMass[il];
            // dom.Nmol[il][i][j][0]   = dat.rho0tmp[il]/dom.molMass[il]; // Predefined density on top, but is implemented as a CBC outlet condition.
            // dom.Rho[il][i][j][0]    = dat.rho0tmp[il];
            // dom.Vel[il][i][j][0]    /=dat.rho0tmp[il];
        }

        for (size_t j=dom.Ndim(1)-(CBCnode+1); j<dom.Ndim(1)-1;j++) 
        {
            for (size_t i=0; i<dom.Ndim(0);i++) 
            {
                dom.Nmol[il][i][j+1][0]     = (dom.Nmolprev[il][i][j+1][0] + uy_max*dom.Nmol[il][i][j][0])/(1.0+uy_max);
            }
        }
        for (size_t j=dom.Ndim(1)-(CBCnode+1); j<dom.Ndim(1)-1;j++) 
        {
            for (size_t i=0; i<dom.Ndim(0);i++) 
            {
                dom.Vel[il][i][j+1][0] = OrthoSys::O;
                for (size_t k=0;k<dom.Nneigh;k++)
                {
                    dom.Vel[il][i][j+1][0]      += dom.F[il][i][j+1][0][k]*dom.C[k];
                }
                dom.Rho[il][i][j][0]        = dom.Nmol[il][i][j+1][0]*dom.molMass[il];
                dom.Vel[il][i][j+1][0]      /= dom.Nmol[il][i][j+1][0]*dom.molMass[il];
            }
        }

    }

    
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t il=0;il<dom.Nl;il++)
    {
        int CBCnode = 2.0;
        for (size_t i=0; i<dom.Ndim(0); i++)
        for (size_t j=dom.Ndim(1)-(CBCnode+1); j<dom.Ndim(1); j++)
        {
            // if (!dom.IsHBSolid[il][i][j][0])
            // {
            for (size_t k=0;k<dom.Nneigh;k++)
            {
                dom.Fprev[il][i][j][0][k] = dom.F[il][i][j][0][k];
            }
            dom.Nmolprev[il][i][j][0] = dom.Nmol[il][i][j][0];
            // }
        }
    }

    // -----

    for (size_t il=0;il<dom.Nl;il++)
    #ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
    #endif
    for (size_t i=0;i<dom.Ndim(0);i++)
    for (size_t j=0;j<dom.Ndim(1);j++)
    {
        dom.BForce[il][i][j][0] = dom.Rho[il][i][j][0]*dat.g;//*dom.cs2;
    }


    #endif // USE_OCL
}



int main(int argc, char **argv) try
{
    size_t Nproc = 1; 
    if (argc==2) Nproc=atoi(argv[1]);
    size_t nx = 100;
    size_t ny = nx*4.0;
    double dx = 0.1/nx; // 1.0; // Get real parameters, maybe use diameter of bubble.
    // double dt = 1.0;
    double Tau1 = 0.51;
    // double cs2    = (1.0/sqrt(3.0))*(1.0/sqrt(3.0));// D2Q9 
    // D2Q17 use this:
    double cs2    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
    
    Array<double> nu(2);
    // nu[0] = ((Tau1-0.5)*cs2*dx*dx)/1.1;//1.0/6; // 1.0/7.0 works well for D2Q17
    // nu[1] = ((Tau1-0.5)*cs2*dx*dx);//100.0;//1.0/12.0;
    nu[0] = 1.0e-6; // Water [m^2/s]  
    nu[1]  = 1.2e-6; // Oil [m^2/s] 
    double dt = cs2*(Tau1-0.5)*((dx*dx)/nu[1]);
    
    Array<double> molMass(2);
    molMass[0] = 10.0;//18.0; // molecular mass of A [dimensionless ratio]
    molMass[1] = 50.0;//18.0*5.0; // molecular mass of B [dimensionless ratio]

    FLBM::Domain Dom(D2Q17, E10, MRT, molMass, nu, iVec3_t(nx,ny,1), dx, dt);
    UserData dat;
    Dom.UserData = &dat;
    // dat.g           = 0.0,-0.001,0.0;
    dat.g           = 0.0,-9.81/(dx/(dt*dt)),0.0;
    std::cout << " Dimensionless Gravity = " << dat.g[1] << std::endl;
    
    // Set up solid nodes.
    double Nlayers = 4; // For D2Q9 Nlayers = 2, and if using D2Q17 add an additional two layers, e.g. Nlayers = 4
    for (size_t il=0;il<nu.Size();il++)
    {
        for (size_t i=0;i<nx;i++)
        {
            for (size_t j=0;j< Nlayers;j++)
            {
                Dom.IsSolid[il][i][j       ][0]     = true;
                // Dom.IsSolid[il][i][ny-(1+j)][0]     = true;
            }
            for (size_t j=0;j< Nlayers - 1.0;j++) // Half-way Bounce Back nodes
            {    
                Dom.IsHBSolid[il][i][j       ][0]   = true;
                // Dom.IsHBSolid[il][i][ny-(1+j)][0]   = true;
            }
        }
    }
    // std::cout << "number of components" << nu.Size() << std::endl;

    // Setup top and bottom fluids
        Vec3_t v0(0.0,0.0,0.0);
        Vec3_t v1(0.0,0.0,0.0);

        int obsY = ny - ny/2;// - ny/4;
        const double PI     = 4.0*atan(1.0);     ///< \f$ \pi \f$
        double nmol0tmp = 0.0000001;
        double nmol0 = 1.0;//200.0*0.91;
        double nmol1 = 1.0;
        dat.rho0tmp = new double[2];
        dat.rho0tmp[0] = nmol0tmp*molMass[0]; // fluid A abscent at top wall
        dat.rho0tmp[1] = nmol1*molMass[1];

        for (size_t i=0; i<nx; ++i)
        for (size_t j=0; j<ny; ++j)
        {
            // if ((int)(j) >= obsY) // sinusoidal function to initiate profile
            if (ceil(obsY+0.1*nx*cos(((2.0*PI)*(1.0/nx)*(int)(i)))) <= j) // sinusoidal function to initiate profile
            {
                Dom.InitializeEF(0,iVec3_t(i,j,0), nmol0tmp,OrthoSys::O);
                Dom.InitializeEF(1,iVec3_t(i,j,0), nmol1,OrthoSys::O);
            }
            else if (ceil(obsY-1.0+0.1*nx*cos(((2.0*PI)*(1.0/nx)*(int)(i)))) <= j) // sinusoidal function to initiate profile
            {
                Dom.InitializeEF(0,iVec3_t(i,j,0), nmol0tmp,v0); // /molMass[0]
                Dom.InitializeEF(1,iVec3_t(i,j,0), nmol0tmp,v1);
            }
            else
            {
                Dom.InitializeEF(0,iVec3_t(i,j,0), nmol0,OrthoSys::O);
                Dom.InitializeEF(1,iVec3_t(i,j,0), nmol0tmp,OrthoSys::O);
            }
        }

        
    
    Dom.InitializeMixture();
    Dom.StorePrevious();
    

    Dom.G[0]     = -1.0;
    Dom.G[1]     = -1.0;
    Dom.Gmix     =  10.0; // Gmix > Gcrit | example, Gcrit = (tau-0.5)*[(S*m)/(tau*<n>)] = (1 - 0.5)*[(2*1)/(1*200)] ~~ 0.00675
    Dom.Gs    [0] =  0.0;
    Dom.Gs    [1] =  0.0;
    //Dom.Sc       = 0.0;

	// Solve
    Dom.Solve(1000*Dom.dt,100*Dom.dt,1*Dom.dt,Setup,NULL,"RT_T001",true,Nproc);
}
MECHSYS_CATCH
