
/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Coalescence Experimental simulation.

// Std Lib
#include <iostream>
#include <stdlib.h>

// MechSys
#include <mechsys/flbm/Domain.h>

using std::cout;
using std::endl;

int main(int argc, char **argv) try
{
    size_t Nproc = 1; 
    if (argc==2) Nproc=atoi(argv[1]);
    // Setup bubbles
    size_t nx = 200;
    size_t ny = 200;
    int radius =  nx/6.5;
    int radius2 =  radius;
    // double dx = 100e-6/radius; // Real parameters using diameter of bubble to define physical scale.
    double dx = 1.0;
    double dt = 1.0;
    // double Tau0 = 1.0;
    double Tau1 = 0.8;//0.53745;
    double cs2    = (1.0/sqrt(3.0))*(1.0/sqrt(3.0));// D2Q9 
    // D2Q17 use this:
    // double cs2    = (1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))))*(1.0/(sqrt(((125.0 + 5.0*sqrt(193.0))/72.0))));
    Array<double> nu(2);
    // nu[0] = 100.0e-6;//
    // nu[1]  = 1.0e-6; // [m^2/s]
    // double dt = sqrt(cs2*(Tau0-0.5)*(dx*dx*dx/nu[0]));
     nu[0] = ((Tau1-0.5)*cs2*dx*dx);//1.0/12.0;
     nu[1] = ((Tau1-0.5)*cs2*dx*dx)/100.0;//1.0/12.0;
    FLBM::Domain Dom(D2Q17, E10, nu, iVec3_t(nx,ny,1), dx, dt);
    

    int obsX = (nx/2 - radius - 4.0), obsY = ny/2;
    int obsX2 = nx - (nx/2 - radius - 4.0), obsY2 = ny/2;
    double rho0tmp = 0.1;
    
    double rho0 = 1.0;
    double rho1 = 0.9;
    
	for (size_t i=0; i<nx; ++i)
	for (size_t j=0; j<ny; ++j)
    {
        // double rho0 = (200.0 +(1.00*rand())/RAND_MAX)*dx*dx;
		if (pow((int)(i)-obsX,2.0) + pow((int)(j)-obsY,2.0) <= pow(radius,2.0)) // circle equation
		{
            Dom.InitializeEF(0,iVec3_t(i,j,0), rho0,OrthoSys::O);
            Dom.InitializeEF(1,iVec3_t(i,j,0), rho0tmp,OrthoSys::O);
		}
        else if (pow((int)(i)-obsX2,2.0) + pow((int)(j)-obsY2,2.0) <= pow(radius2,2.0)) // circle equation
		{
            Dom.InitializeEF(0,iVec3_t(i,j,0), rho0,OrthoSys::O);
            Dom.InitializeEF(1,iVec3_t(i,j,0), rho0tmp,OrthoSys::O);
		}
        else
        {
            Dom.InitializeEF(0,iVec3_t(i,j,0), rho0tmp,OrthoSys::O);
            Dom.InitializeEF(1,iVec3_t(i,j,0), rho1,OrthoSys::O);
        }
    }
    Dom.InitializeMixture();
    
    Dom.G[0]     = -1.0;
    Dom.G[1]     = -1.0;
    Dom.Gmix     =  0.01; // Gcrit = (tau-0.5)*[(S*m)/(tau*<n>)] = (1 - 0.5)*[(2*1)/(1*200)] ~~ 0.00675
    Dom.Gs    [0] =  0.0;
    Dom.Gs    [1] =  0.0;
    //Dom.Sc       = 0.0;

	// Solve
    Dom.Solve(1000*Dom.dt,100*Dom.dt,0*Dom.dt,NULL,NULL,"CS_T001",true,Nproc);
}
MECHSYS_CATCH
