/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Cylinder drag coefficient

//STD
#include<iostream>

// MechSys
#include <mechsys/lbm/Domain.h>

struct UserData
{
    std::ofstream      oss_ss;       ///< file for particle data
    Array<Cell *> Left;
    Array<Cell *> Right;
    iVec3_t ** RightCBC;
    Array<double> Vel;
    double        vmax;
    double        rho;
    double        R;
    double        nu;
    double        Tf;
    size_t      CBCnode;
};

void Report (LBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    if (dom.idx_out==0)
    {
        String fs;
        fs.Printf("%s_force.res",dom.FileKey.CStr());
        dat.oss_ss.open(fs.CStr());
        dat.oss_ss << Util::_10_6 << "Time" << Util::_8s << "Fx" << Util::_8s << "Fy" << Util::_8s << "Fz" << Util::_8s << "Tx" << Util::_8s << "Ty" << Util::_8s << "Tz" << Util::_8s << "Vx" << Util::_8s << "Fx" << Util::_8s << "F" << Util::_8s << "Rho" << Util::_8s << "Re" << Util::_8s << "CD" << Util::_8s << "CDsphere \n";
    }
    if (!dom.Finished) 
    {
        double M    = 0.0;
        double Vx   = 0.0;
        size_t nc   = 0;
        Vec3_t Flux = OrthoSys::O;
        for (size_t i=0;i<dom.Lat[0].Ncells;i++)
        {
            Cell * c = dom.Lat[0].Cells[i];
            if (c->IsSolid||c->Gamma>1.0e-8) continue;
            Vx   += (1.0 - c->Gamma)*c->Vel(0);
            Flux += c->Rho*c->Vel;
            M    += c->Rho;
            nc++;
        }
        Vx  /=dom.Lat[0].Ncells;
        Flux/=M;
        M   /=nc;
        double CD  = 2.0*dom.Disks[0]->F(0)/(Flux(0)*Flux(0)/M*2.0*dat.R);
        double Re  = 2.0*Flux(0)*dat.R/(M*dat.nu);
        double CDt = 10.7*pow(Re,-0.7239)+0.9402;
        if (Flux(0)<1.0e-12) 
        {
            Flux = OrthoSys::O;
            CD = 0.0;
            Re = 0.0;
        }

        dat.oss_ss << Util::_10_6 << dom.Time << Util::_8s << dom.Disks[0]->F(0) << Util::_8s << dom.Disks[0]->F(1) << Util::_8s << dom.Disks[0]->F(2) << Util::_8s << dom.Disks[0]->T(0) << Util::_8s << dom.Disks[0]->T(1) << Util::_8s << dom.Disks[0]->T(2) << Util::_8s << Vx << Util::_8s << Flux(0) << Util::_8s << norm(Flux) << Util::_8s << M << Util::_8s << Re << Util::_8s << CD << Util::_8s << CDt << std::endl;
    }
    else
    {
        dat.oss_ss.close();
    }
}

void Setup (LBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));

    double vel = std::min(10.0*dat.vmax*dom.Time/dat.Tf,dat.vmax);
	// Cells with prescribed velocity
#ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
#endif
	for (size_t i=0; i<dat.Left.Size(); ++i)
	{
		Cell * c = dat.Left[i];
		// if (c->IsSolid) continue;
		double rho = (c->F[0]+c->F[2]+c->F[4] + 2.0*(c->F[3]+c->F[6]+c->F[7]))/(1.0-vel);
		c->F[1] = c->F[3] + (2.0/3.0)*rho*vel;
		c->F[5] = c->F[7] + (1.0/6.0)*rho*vel - 0.5*(c->F[2]-c->F[4]);
		c->F[8] = c->F[6] + (1.0/6.0)*rho*vel + 0.5*(c->F[2]-c->F[4]);
        c->Rho = c->VelDen(c->Vel);
	}

	/*  free 'open' Outlet boundary - type: CBC (convective boundary condition) */
    double ux_max = 0.0;
#ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
#endif
	for (size_t i=0; i<dat.Right.Size(); ++i)
    {
        Cell * c00 = dom.Lat[0].GetCell(dat.RightCBC[0][i]);
        // ux_max = std::max(ux_max,c00->Vel[0]); // does not work for tlbm02
        ux_max += c00->Vel[0];
        // std::cout << " Max velocity at top = " << u_max << std::endl;
    }
    ux_max /= dat.Right.Size();  // CBC average scheme.
    for (size_t i=0; i<dat.Right.Size(); ++i)
	{
        Cell * c00 = dom.Lat[0].GetCell(dat.RightCBC[0][i]);
        Cell * c01 = dom.Lat[0].GetCell(dat.RightCBC[1][i]);
        
        for (size_t k=0;k<c01->Nneigh;k++)
        {
            c01->F[k]   = (c01->Fprev[k] + ux_max*c00->F[k])/(1.0+ux_max);
        }
        c01->Rho = c01->VelDen(c01->Vel);
    }
    for (size_t i=0; i<dat.Right.Size(); ++i)
	{
        for (size_t j=1;j<(dat.CBCnode);j++)
        {
            Cell * c00 = dom.Lat[0].GetCell(dat.RightCBC[j][i]);
            Cell * c01 = dom.Lat[0].GetCell(dat.RightCBC[j+1][i]);
            c01->Rho     = (c01->Rhoprev + ux_max*c00->Rho)/(1.0+ux_max);
            c01->VelCBC(c01->Vel);
        }
    }
	for (size_t i=0; i<dat.Right.Size(); ++i)
	{
        for (size_t j=0;j<(dat.CBCnode + 1);j++)
        {
            Cell * c = dom.Lat[0].GetCell(dat.RightCBC[j][i]);
            for (size_t k=0;k<c->Nneigh;k++)
            {
                c->Fprev[k] = c->F[k];
            }
            c->Rhoprev = c->Rho;
        }
    }
}

int main(int argc, char **argv) try
{
    size_t nproc = 1; 
    if (argc>=2) nproc=atoi(argv[1]);
    double u_max  = 0.1;                // Poiseuille's maximum velocity
    double Re     = 5000.0;                  // Reynold's number
    if (argc>=3) Re=atof(argv[2]);
    size_t nx = 800;
    size_t ny = 800;
    int radius = 21;           // radius of inner circle (obstacle)
    double nu     = u_max*(2*radius)/Re; // viscocity
    //double nu     = 1.0/6.0; // viscocity
    LBM::Domain Dom(D2Q9, nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    Dom.Fconv = 1.0;
    UserData dat;
    Dom.UserData = &dat;
    dat.R = radius;
    dat.nu = nu;
    dat.Tf = 80000.0;
    //Dom.Sc = 0.01;

    dat.rho  = 1.0;
    dat.vmax = u_max;
    dat.CBCnode = 3;
    //Assigning the left and right cells
    for (size_t i=0;i<ny;i++)
    {
        dat.Left .Push(Dom.Lat[0].GetCell(iVec3_t(0   ,i,0)));
        dat.Right.Push(Dom.Lat[0].GetCell(iVec3_t(nx-1,i,0)));
        /* Pre-allocate cells for CBC ?? Could maybe be faster ?? */
        // dat.RCBC00.Push(Dom.Lat[0].GetCell(iVec3_t(nx-(1+3),i,0)));
        // dat.RCBC01.Push(Dom.Lat[0].GetCell(iVec3_t(nx-(1+2),i,0)));
        // dat.RCBC02.Push(Dom.Lat[0].GetCell(iVec3_t(nx-(1+1),i,0)));
        // dat.RCBC03.Push(Dom.Lat[0].GetCell(iVec3_t(nx-1,i,0)));
    }
    


    dat.RightCBC = new iVec3_t * [dat.CBCnode + 1];
    for (size_t j=0;j<(dat.CBCnode + 1);j++)
    {
        dat.RightCBC[j] = new iVec3_t [ny];
        for (size_t i=0;i<ny;i++)
        {
            dat.RightCBC[j][i] = nx-(1+3-j),i,0;
        }
    }
	// set inner obstacle
	int obsX   = nx/4;   // x position
	int obsY   = ny/2+3; // y position

    Dom.AddDisk(-1,Vec3_t(obsX,obsY,0),Vec3_t(0.0,0.0,0.0),Vec3_t(0.0,0.0,0.0),   3.0,radius,1.0);
    Dom.Disks[0]->FixVeloc();

    double rho0 = dat.rho;
    Vec3_t v0(0.08,0.0,0.0);

    //Initializing values
    for (size_t i=0;i<Dom.Lat[0].Ncells;i++)
    {
        //Dom.Lat[0].Cells[i]->Initialize(rho0, v0);
        Dom.Lat[0].Cells[i]->Initialize(rho0, OrthoSys::O);
    }

    
    printf("\n%s--- Running at Re = %g%s\n",TERM_CLR1,Re,TERM_RST);
    printf("\n%s---    && with Sc = %g%s\n",TERM_CLR1,Dom.Sc,TERM_RST);

    //Solving
    Dom.Time = 0.0;
    Dom.Solve(dat.Tf,dat.Tf/100.0,Setup,Report,"tlbm02",true,nproc);
 
}
MECHSYS_CATCH


