/************************************************************************
 * MechSys - Open Library for Mechanical Systems                        *
 * Copyright (C) 2009 Sergio Galindo                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * any later version.                                                   *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program. If not, see <http://www.gnu.org/licenses/>  *
 ************************************************************************/

// Cylinder drag coefficient

//STD
#include<iostream>

// MechSys
#include <mechsys/lbm/Domain.h>

struct UserData
{
    std::ofstream      oss_ss;       ///< file for particle data
    Array<Cell *> Left;
    Array<Cell *> Right;
    Array<double> Vel;
    double        vmax;
    double        rho;
    double        R;
    double        nu;
    double        Tf;
    Vec3_t        v0;
};

void Report (LBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    if (dom.idx_out==0)
    {
        String fs;
        fs.Printf("%s_force.res",dom.FileKey.CStr());
        dat.oss_ss.open(fs.CStr());
        dat.oss_ss << Util::_10_6 << "Time" << Util::_8s << "Fx" << Util::_8s << "Fy" << Util::_8s << "Fz" << Util::_8s << "Tx" << Util::_8s << "Ty" << Util::_8s << "Tz" << Util::_8s << "Vx" << Util::_8s << "Fx" << Util::_8s << "F" << Util::_8s << "Rho" << Util::_8s << "Re" << Util::_8s << "CD" << Util::_8s << "CDsphere \n";
    }
    if (!dom.Finished) 
    {
        double M    = 0.0;
        double Vx   = 0.0;
        size_t nc   = 0;
        Vec3_t Flux = OrthoSys::O;
        for (size_t i=0;i<dom.Lat[0].Ncells;i++)
        {
            Cell * c = dom.Lat[0].Cells[i];
            if (c->IsSolid||c->Gamma>1.0e-8) continue;
            Vx   += (1.0 - c->Gamma)*c->Vel(0);
            Flux += c->Rho*c->Vel;
            M    += c->Rho;
            nc++;
        }
        Vx  /=dom.Lat[0].Ncells;
        Flux/=M;
        M   /=nc;
        double CD  = 2.0*dom.Disks[0]->F(0)/(Flux(0)*Flux(0)/M*2.0*dat.R);
        double Re  = 2.0*Flux(0)*dat.R/(M*dat.nu);
        double CDt = 10.7*pow(Re,-0.7239)+0.9402;
        if (Flux(0)<1.0e-12) 
        {
            Flux = OrthoSys::O;
            CD = 0.0;
            Re = 0.0;
        }

        dat.oss_ss << Util::_10_6 << dom.Time << Util::_8s << dom.Disks[0]->F(0) << Util::_8s << dom.Disks[0]->F(1) << Util::_8s << dom.Disks[0]->F(2) << Util::_8s << dom.Disks[0]->T(0) << Util::_8s << dom.Disks[0]->T(1) << Util::_8s << dom.Disks[0]->T(2) << Util::_8s << Vx << Util::_8s << Flux(0) << Util::_8s << norm(Flux) << Util::_8s << M << Util::_8s << Re << Util::_8s << CD << Util::_8s << CDt << std::endl;
    }
    else
    {
        dat.oss_ss.close();
    }
}

void Setup (LBM::Domain & dom, void * UD)
{
    UserData & dat = (*static_cast<UserData *>(UD));
    //for (size_t i=0;i<dat.Left.Size();i++)
    //{
        //dat.Left [i]->Initialize(dat.Left [i]->RhoBC,dat.Left [i]->VelBC);
        //dat.Right[i]->Initialize(dat.Right[i]->RhoBC,dat.Right[i]->VelBC);
    //}

    double vel = std::min(10.0*dat.vmax*dom.Time/dat.Tf,dat.vmax);
	// Cells with prescribed velocity
#ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
#endif
	for (size_t i=0; i<dat.Left.Size(); ++i)
	{
		Cell * c = dat.Left[i];
		// if (c->IsSolid) continue;
        double Cs2 = c->Cs2;
        double rho    =    (c->F[0]+c->F[2]+c->F[4]+c->F[14]+c->F[16]  +   2.0*(c->F[3]+c->F[7]+c->F[6])+3.0*(c->F[11]+c->F[10])+4.0*c->F[15])/(1.0-vel);
            /*
                        { from -->>   rho = sum(F[k])            }      {from -->>   u*rho = sum(F[k]*C[k]) + left over from rho=sum(F[k])}             
            */
        double tempconst1    = (c->F[4]) - (c->F[2]); /* for the sake of clarity. */
        double tempconst2    = (c->F[16]*3.0) - (c->F[14]*3.0); /* for the sake of clarity. */
        // double EqConst= (2.0/Cs2)*rho*vel;
        // double EqConst= 2.0*rho*((vel/Cs2) + (vel*vel*vel)*((1.0/(6.0*Cs2*Cs2*Cs2)) - (1.0/(2.0*Cs2*Cs2))));
        double EqConst1= 2.0*rho*((vel/Cs2) + (vel*vel*vel)*((1.0/(6.0*Cs2*Cs2*Cs2)) - (1.0/(2.0*Cs2*Cs2))));
        double EqConst2= 6.0*rho*((vel/Cs2) + (vel*vel*vel)*((1.0/(6.0*Cs2*Cs2*Cs2)) - (1.0/(2.0*Cs2*Cs2))));
        // double EqConst= EqConst1 + EqConst2;
        c->F[1]     = c->F[3]   + c->W[3]*EqConst1; 
        c->F[13]    = c->F[15]  + c->W[15]*EqConst2;
        c->F[5]     = c->F[7]   + c->W[7]*EqConst1      + c->C[5][1]*(1.0/2.0)*tempconst1;
        c->F[8]     = c->F[6]   + c->W[6]*EqConst1      + c->C[8][1]*(1.0/2.0)*tempconst1;
        c->F[9]     = c->F[11]  + c->W[11]*EqConst2      + 0.5*c->C[9][1]*(1.0/4.0)*tempconst2;
        /* 
         2f(9)|2.0~> = 2f(12)/2  - This part is scaled by...     + need to 1/2*C[k] since ||C[k>8]||>>1, but is only used here to define the direction, i.e., +|- 1.0
                                  (W*2*2/Cs2)/2==(W*2/Cs2)                                                                                                      
        */
        c->F[12]    = c->F[10]  + c->W[10]*EqConst2      + 0.5*c->C[12][1]*(1.0/4.0)*tempconst2;
        
        // c->Vel = vel,0.0,0.0;
        c->Rho = c->EFVelDen(c->Vel,dom.dt);
	}

	// Cells with prescribed density
#ifdef USE_OMP
    #pragma omp parallel for schedule(static) num_threads(dom.Nproc)
#endif
	for (size_t i=0; i<dat.Right.Size(); ++i)
	{
		Cell * c = dat.Right[i];
		// if (c->IsSolid) continue;
        double Cs2 = c->Cs2;
        double rho    = (c->F[0]+c->F[2]+c->F[4]+c->F[14]+c->F[16] + 2.0*(c->F[1]+c->F[5]+c->F[8])+3.0*(c->F[9]+c->F[12])+4.0*c->F[13])/(1.0+vel);
        double tempconst1    = (c->F[4]) - (c->F[2]); /* for the sake of clarity. */
        double tempconst2    = (c->F[16]*3.0) - (c->F[14]*3.0); /* for the sake of clarity. */
        // double EqConst= (2.0/Cs2)*rho*vel;
        double EqConst1= 2.0*rho*((vel/Cs2) + (vel*vel*vel)*((1.0/(6.0*Cs2*Cs2*Cs2)) - (1.0/(2.0*Cs2*Cs2))));
        double EqConst2= 6.0*rho*((vel/Cs2) + (vel*vel*vel)*((1.0/(6.0*Cs2*Cs2*Cs2)) - (1.0/(2.0*Cs2*Cs2))));
        // double EqConst= EqConst1 + EqConst2;
        c->F[3]     = c->F[1]   - c->W[1]*EqConst1; 
        c->F[15]    = c->F[13]  - c->W[13]*EqConst2;
        c->F[7]     = c->F[5]   - c->W[5]*EqConst1      + c->C[7][1]*(1.0/2.0)*tempconst1;
        c->F[6]     = c->F[8]   - c->W[8]*EqConst1      + c->C[6][1]*(1.0/2.0)*tempconst1;
        c->F[11]    = c->F[9]   - c->W[9]*EqConst2      + 0.5*c->C[11][1]*(1.0/4.0)*tempconst2;
        c->F[10]    = c->F[12]  - c->W[12]*EqConst2     + 0.5*c->C[10][1]*(1.0/4.0)*tempconst2;
        
        c->Rho = c->EFVelDen(c->Vel,dom.dt);
	}
}

int main(int argc, char **argv) try
{
    size_t nproc = 1; 
    if (argc>=2) nproc=atoi(argv[1]);
    double u_max  = 0.1;                // Poiseuille's maximum velocity
    double Re     = 5000.0;                  // Reynold's number
    if (argc>=3) Re=atof(argv[2]);
    double ScConst = 0.17;              // Smagorinsky Constant
    if (argc>=4) ScConst=atof(argv[3]);
    size_t nx = 800;
    size_t ny = 800;
    int radius = 21;           // radius of inner circle (obstacle)
    double nu     = u_max*(2*radius)/Re; // viscocity
    //double nu     = 1.0/6.0; // viscocity
    LBM::Domain Dom(D2Q17, nu, iVec3_t(nx,ny,1), 1.0, 1.0);
    Dom.Fconv = 1.0;
    UserData dat;
    Dom.UserData = &dat;
    dat.R = radius;
    dat.nu = nu;
    dat.Tf = 80000.0;
    Dom.Sc = ScConst;
    
    dat.vmax = u_max;
    dat.rho  = 1.0;
    //Assigning the left and right cells
    for (size_t i=0;i<ny;i++)
    {
        dat.Left .Push(Dom.Lat[0].GetCell(iVec3_t(0   ,i,0)));
        dat.Right.Push(Dom.Lat[0].GetCell(iVec3_t(nx-1,i,0)));
        
        // set parabolic profile
        //double L  = ny - 2;                       // channel width in cell units
        //double yp = i - 1.5;                      // ordinate of cell
        //double vx = dat.vmax*4/(L*L)*(L*yp - yp*yp); // horizontal velocity
        // double vx = dat.vmax; // horizontal velocity
        // dat.Vel.Push(vx);
        // dat.Left [i]->RhoBC = 1.0;
        //dat.Right[i]->VelBC = 0.08,0.0,0.0;
        // dat.Right[i]->RhoBC = 1.0;

    }
    

    

	// set inner obstacle
	int obsX   = nx/4;   // x position
	int obsY   = ny/2+3; // y position
    //for (size_t i=0;i<nx;i++)
    //for (size_t j=0;j<ny;j++)
    //{
        //if ((i-obsX)*(i-obsX)+(j-obsY)*(j-obsY)<radius*radius)
        //{
            //Dom.Lat[0].GetCell(iVec3_t(i,j,0))->IsSolid = true;
        //}
    //}

    Dom.AddDisk(-1,Vec3_t(obsX,obsY,0),Vec3_t(0.0,0.0,0.0),Vec3_t(0.0,0.0,0.0),   3.0,radius,1.0);
    Dom.Disks[0]->FixVeloc();
    //Dom.AddDisk(0,Vec3_t(nx/2.0,obsY,0),Vec3_t(0.0,0.0,0.0),Vec3_t(0.0,0.0,0.0),30.0,2*radius,1.0);



    //Assigning solid boundaries at top and bottom
    //for (size_t i=0;i<nx;i++)
    //{
        //Dom.Lat[0].GetCell(iVec3_t(i,0   ,0))->IsSolid = true;
        //Dom.Lat[0].GetCell(iVec3_t(i,ny-1,0))->IsSolid = true;
    //}

    double rho0 = 1.0;
    Vec3_t v0(0.08,0.0,0.0);

    //Initializing values
    for (size_t i=0;i<Dom.Lat[0].Ncells;i++)
    {
        //Dom.Lat[0].Cells[i]->Initialize(rho0, v0);
        Dom.Lat[0].Cells[i]->Initialize(rho0, OrthoSys::O);
    }

    // for (size_t i=0; i<dat.Left.Size(); ++i)
	// {
	// 	Cell * cL = dat.Left[i];
    //     // Cell * cR = dat.Right[i];
    //     cL->Vel=0.08,0.0,0.0;
    //     cL->Initialize(rho0, Vec3_t(0.08,0.0,0.0));
    //     // cR->Vel=0.08,0.0,0.0;
    // }
    
    printf("\n%s--- Running at Re = %g%s\n",TERM_CLR1,Re,TERM_RST);
    printf("\n%s---    && with Sc = %g%s\n",TERM_CLR1,Dom.Sc,TERM_RST);
    //Solving
    Dom.Time = 0.0;
    Dom.Solve(dat.Tf,dat.Tf/100.0,Setup,Report,"tlbm02",true,nproc);
}
MECHSYS_CATCH